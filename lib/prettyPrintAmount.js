exports.prettyPrintSymbol = prettyPrintSymbol;

const {Decimal} = require('./decimal');
Decimal.set({ precision: 64, toExpPos: 64, toExpNeg: -64 });

function prettyPrintSymbol (symbol) {
  if (typeof symbol !== 'string') return '';
  if (symbol.includes('.')) {
    const [base, token] = symbol.split('.');
    return `<small>${base.toUpperCase()}</small>.${token.toUpperCase()}`;
  } else return symbol.toUpperCase();
}

// treatAsCurrency -> max two decimals unless < 0.001
exports.prettyPrintAmount = (amount, symbol, treatAsCurrency = false, treatAsBalance = false) => {
  if (isNaN(amount)) return 'n/a';

  const A = /^(0\.0*)(.*?)(0*)$/; // '0.000123000';

  amount = new Decimal(amount);
  let truncated = false;
  if (treatAsBalance) {
    let amountString = amount.toString();
    if (amountString.includes('.')) {
      const decimals = amountString.split('.')[1].length;
      if (decimals > 8) {
        truncated = true;
        amountString = amountString.substr(0, amountString.length - (decimals - 8));
        amount = new Decimal(amountString).toFixed(8);
      }
    }
  } else if (treatAsCurrency) amount = amount.toFixed(2); // 123.4567 -> 123.46

  amount = amount.toString();

  let renderAmount;

  if (/^0\.?0*$/.test(amount)) renderAmount = '0';
  else if (A.test(amount)) renderAmount = '<span style="font-size:75%;">' + A.exec(amount).slice(1, 3).join('</span>'); // '0.000123000' -> '<span>0.000</span>123'
  else if (/^\d*(\.0*)?$/.test(amount)) renderAmount = amount.split('.')[0]; // '123000.0000' -> '123000'
  else renderAmount = /^(.*?)0*$/.exec(amount)[1]; // '123000.0100' -> '123000.01'

  // ensure currencies have two digits
  if (treatAsCurrency && renderAmount.indexOf('.') === -1) renderAmount += '.00'; // '123' -> '123.00'
  else if (treatAsCurrency && renderAmount.split('.')[1].length === 1) renderAmount += '0'; // '123.4' - '123.40'
  symbol = prettyPrintSymbol(symbol);
  return renderAmount +
  (truncated ? '...' : '') +
  (symbol ? '&nbsp;' + symbol : '');
};

exports.renderDate = function (timestamp) {
  const time = new Date(timestamp * 1000);
  const year = time.getFullYear();
  const month = time.getMonth() + 1;
  const defaultOrZeroedMonth = month < 10 ? `0${String(month)}` : month;
  const day = time.getDate();
  const defaultOrZeroedDay = day < 10 ? `0${String(day)}` : day;
  const minutes = time.getMinutes();
  const seconds = time.getSeconds();
  const defaultOrZeroedMinute = minutes < 10 ? `0${String(minutes)}` : minutes;
  const defaultOrZeroedSecond = seconds < 10 ? `0${String(seconds)}` : seconds;

  return `${year}-${defaultOrZeroedMonth}-${defaultOrZeroedDay} ${time.getHours()}:${defaultOrZeroedMinute}:${defaultOrZeroedSecond}`;
};
