exports.constants = {
  WALLET_ASSET_PREFERENCES: {"hy": 1, "cny": 0, "eur": 0, "usd": 0},
  DEFAULT_LANDING_PAGE: 'listAssets',
  LOGOUT_TIMEOUT_MS: 5 * 60 * 1000, // how long before show auto logout warning popup
  LOGOUT_FINAL_TIMEOUT_S: 60, // how many seconds to count down in auto logout warning popup
  storagePrefixes: {
    assetPreferences: 'HY0035',
    contacts: 'HY0036',
    currencyPreference: 'HY0037',
    pendingTransactions: 'HY0038',
    bankingUserAccount: 'HY0039',
    email: 'HY0040'
  }
}
