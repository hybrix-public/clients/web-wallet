const hybrixCss = `url('./png/logo-hybrix-gradient.png')`;

const isHybrix = () => window.location.href.indexOf('internetofcoins') === -1;

exports.isHybrix = isHybrix;

exports.logo = ELEMENT => {
  if (isHybrix()) ELEMENT.style.backgroundImage = hybrixCss;
};

exports.init = () => {
  let title, favIconUrl;
  if (isHybrix()) {
    title = 'hybrix';
    favIconUrl = './png/favicon.hybrix.png';
    const customCss = `
    #login_container .powered-by-hybrix { display:none; }
    #asset-overview-logo { width: 76px; height: 31px; background-size: 76px auto; }
    #asset-list .asset-container,
    .asset-overview-header {
      background-image: linear-gradient(154deg, #50cbaf -40%, #66add6 130%);
    }
    .pure-button-main,
    .pure-button-main:hover {
      background: linear-gradient(44deg, #68ABC6 0%, #E383CE 51%, #E49384 100%);
    }
    .asset-overview-logo { width: 76px; height: 31px; background-size: 76px auto; }
    .asset-container,
    .asset-overview-header {
      background-image: linear-gradient(154deg, #50cbaf -40%, #66add6 130%) !important;
    }
    .add-wallet-container {
      color: #63bbbb;
      background: rgba(103,199,199,.1);
    }
    .asset-list .add-wallet-container .add-wallet-icon svg g {
      fill: #67c7c7;
    }
    .asset .icon svg text {
      color: #787735;
      fill: #787735;
    }
    `;

/* DEPRECATED 2022.02.24

    #login_container div.logo {
      width:110px;
      background-size:110px;
      background-repeat: no-repeat;
    }
    .progress-bar {
      background: linear-gradient(44deg, #68ABC6 0%, #E383CE 51%, #E49384 100%);
      background-size: 400% 400%;
      animation: gradient 7s ease infinite, glow 2s linear infinite, expand 2s linear infinite;
    }
    .nav div.nav-logo {
      width: 70px;

 */

    const style = document.createElement('style');
    if (style.styleSheet) style.styleSheet.cssText = customCss;
    else style.appendChild(document.createTextNode(customCss));
    document.getElementsByTagName('head')[0].appendChild(style);
  } else {
    title = 'Internet of Coins';
    favIconUrl = './png/favicon.ioc.png';
  }

  document.title = `{{LOGIN:WALLET}} | ${title}`;

  const plainFavicon = document.getElementById('plainFavicon');
  if (plainFavicon) {
    document.getElementsByTagName('head')[0].removeChild(plainFavicon);
  }
  const LINK_favicon = document.createElement('LINK');
  LINK_favicon.href = favIconUrl;
  LINK_favicon.rel = 'shortcut icon';
  LINK_favicon.type = 'image/png';
  document.getElementsByTagName('head')[0].appendChild(LINK_favicon);
};
