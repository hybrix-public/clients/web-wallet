function createElement (options) {
  const ELEMENT = document.createElement(options.tagName || 'DIV');
  for (const key in options) {
    if (key === 'style') {
      if (typeof options[key] !== 'object' || options[key] === null) continue;
      for (const property in options[key]) {
        ELEMENT.style[property] = options[key][property];
      }
    } else if (key === 'parentNode') {
      options.parentNode.appendChild(ELEMENT);
    } else if (key === 'checked' && options[key]) ELEMENT.setAttribute(key, options[key]);
    else ELEMENT[key] = options[key];
  }
  return ELEMENT;
}

exports.createElement = createElement;

exports.createCustomCheckBox = function (options) {
  const label = options.label;
  const LABEL = createElement({tagName: 'LABEL', className: 'custom-check', style: options.style});
  const INPUT = createElement({checked: options.checked, tagName: 'INPUT', type: 'checkbox', parentNode: LABEL});
  if (options.id) INPUT.id = options.id;
  const SPAN = createElement({tagName: 'SPAN', parentNode: LABEL});
  let lastClick;

  LABEL.onclick = event => {
    if (lastClick && lastClick >= event.timeStamp - 10) return;
    lastClick = event.timeStamp;
    if (INPUT.hasAttribute('checked')) INPUT.removeAttribute('checked');
    else INPUT.setAttribute('checked', 'checked');

    if (options.onclick) options.onclick(event);
  };

  LABEL.innerHTML += label;
  if (options.parentNode) options.parentNode.appendChild(LABEL);
  return INPUT;
};
