const hybrix = require('./hybrix').hybrix;
const {parseMultiAddress, cleanPrefix} = require('./multi-address');

const contacts = {};
const stringLengthLimit = 256;
const addressLengthLimit = 1024;

/*

  {[contactId] : [{[symbolInUpperCase]: {[label]:[address]}}]

  note that this is a perhaps ineffecient data scheme but already widely used so we're stuck with it
*/

const contactsMergeStrategy = (local, remote) => {
  const localIsObject = typeof local === 'object' && local !== null;
  const remoteIsObject = typeof remote === 'object' && remote !== null;
  if (!localIsObject && !remoteIsObject) return {};
  if (!remoteIsObject) return local;
  if (!localIsObject) return remote;

  for (const contactId in local) {
    if (remote.hasOwnProperty(contactId)) {
      for (const localEntry of local[contactId]) { // {[symbolInUpperCase]: {[label]:[address]}}
        const symbol = Object.keys(localEntry)[0];
        const label = Object.keys(Object.values(localEntry)[0])[0];
        let foundInRemote;
        for (const remoteEntry of remote[contactId]) {
          if (remoteEntry.hasOwnProperty(symbol) && remoteEntry[symbol].hasOwnProperty(label)) foundInRemote = true;
        }
        if (!foundInRemote) remote[contactId].push(localEntry);
      }
    } else remote[contactId] = local[contactId];
  }
  return remote;
};

function loadContacts () {
  hybrix.lib.load({key: hybrix.constants.storagePrefixes.contacts, mergeStrategy: contactsMergeStrategy, sessionKey: true, channel: 'y'},
    loadedContacts => {
      for (const contactId in loadedContacts) contacts[contactId] = loadedContacts[contactId];
    },
    (e) => { if (DEBUG) { console.error(e); } }
  );
}

function saveContacts () {
  hybrix.lib.save({key: hybrix.constants.storagePrefixes.contacts, value: contacts, sessionKey: true, channel: 'y'},
    () => {},
    console.error // TODO
  );
}

function saveContact (data, dataCallback, errorCallback) {
  const symbol = data.symbol.toUpperCase().split('.')[0]; // only base symbol
  const address = addressWithoutPrefix(data).trim().substring(0, addressLengthLimit);
  const label = data.label ? data.label.trim().substring(0, stringLengthLimit) : '-';
  const key = data.contactId.trim().substring(0, stringLengthLimit);
  const entry = {[symbol]: {[label]: address}};
  if (contacts.hasOwnProperty(key)) contacts[key].push(entry);
  else contacts[ key ] = [entry];
  saveContacts();
  return dataCallback();
}

function addressWithoutPrefix (data) {
  if (data.address.startsWith(data.symbol.toLowerCase() + ':')) { // automatically remove prefix 'symbol:address' -> 'address'
    return data.address.substr(data.symbol.length + 1);
  } else return data.address;
}

function validateAddress (data, dataCallback, errorCallback) {
  const address = addressWithoutPrefix(data);
  hybrix.lib.rout(
    {query: `/a/${data.symbol.toLowerCase()}/validate/${address}`, channel: 'y'},
    valid => {
      if (valid !== 'valid') return errorCallback(`Address "${address}" is not a valid  ${data.symbol} address!`);
      return dataCallback();
    },
    error => {
      if (typeof error === 'string' && (error.endsWith('is unknown.') || error.endsWith('not found!'))) return errorCallback(error);
      return dataCallback(); // TODO: fixme -> ignores error and save just in case
    }
  );
}

function saveToContacts (data, dataCallback, errorCallback) {
  if (typeof data !== 'object' || data === null) return errorCallback('Expected data object.');
  if (typeof data.symbol !== 'string' || data.symbol === '') return errorCallback('Expected symbol.');
  if (typeof data.address !== 'string' || data.address === '') return errorCallback('Expected address.');
  if (typeof data.contactId !== 'string' || data.contactId === '') return errorCallback('Expected contact name.');
  return validateAddress(data, () => saveContact(data, dataCallback, errorCallback), errorCallback);
}

/**
 * get the entries for a given contactId
 * @param  {string} contactId
 * @return {Array}   contact entries
 */
function getContact (contactId) {
  return contacts.hasOwnProperty(contactId) ? contacts[contactId] : [];
}

function renameContact (oldContactId, newContactId) {
  const key = newContactId.trim().substring(0, stringLengthLimit);
  if (oldContactId === key) return false;
  if (!contacts.hasOwnProperty(key) && contacts.hasOwnProperty(oldContactId)) {
    contacts[key] = contacts[oldContactId];
    delete contacts[oldContactId];
    setTimeout( () => { saveContacts(); }, 2000);  // TODO: race condition fix, should be done differently
    return key;
  } else return false;
}

/**
 * updates a contact entry
 * @param  {string} oldEntry.contactId      [description]
 * @param  {string} oldEntry.address      [description]
 * @param  {string} oldEntry.symbol      [description]
 * @param  {string} oldEntry.label      [description]
 * @param  {string} newEntry.contactId      [description]
 * @param  {string} newEntry.address      [description]
 * @param  {string} newEntry.symbol      [description]
 * @param  {string} newEntry.label      [description]
 * @param  {function} dataCallback  [description]
 * @param  {function} errorCallback [description]
 */
function updateEntry (oldEntry, newEntry, dataCallback, errorCallback) { // {[symbolInUpperCase]: {[label]:[address]}}
  if (typeof oldEntry !== 'object' || oldEntry === null || typeof newEntry !== 'object' || newEntry === null) return errorCallback();

  const contactId = newEntry.contactId.trim().substring(0, stringLengthLimit);
  const oldContactId = oldEntry.contactId;
  if (contactId !== oldContactId) return errorCallback();

  const newAddress = newEntry.address.trim().substring(0, addressLengthLimit);
  const newLabel = newEntry.label.trim().substring(0, stringLengthLimit);
  const newSymbol = newEntry.symbol.toUpperCase().split('.')[0];// only base symbol

  const oldAddress = oldEntry.address;
  const oldLabel = oldEntry.label;
  const oldSymbol = oldEntry.symbol.toUpperCase().split('.')[0];// only base symbol

  if (newAddress === oldAddress && newLabel === oldLabel && newSymbol === oldSymbol) return dataCallback(); // nothing changes? do nothing

  validateAddress(newEntry, () => {
    const contact = getContact(contactId);
    for (const entry of contact) { // remove oldEntry
      if (entry.hasOwnProperty(oldSymbol) && entry[oldSymbol].hasOwnProperty(oldLabel) && entry[oldSymbol][oldLabel] === oldAddress) {
        delete entry[oldSymbol][oldLabel];
        if (Object.keys(entry[oldSymbol]).length === 0) delete entry[oldSymbol]; // clean up entirely if empty
        if (Object.keys(entry).length === 0) contact.splice(contact.indexOf(entry),1); // remove contact completely if empty
      }
    }
    // insert new newEntry
    contact.push({[newSymbol]: {[newLabel]: newAddress}});
    saveContacts();
    return dataCallback();
  }, errorCallback);
}

/**
 * Find contactId and label for given symbol and address
 * @param  {string} symbol  [description]
 * @param  {string} address [description]
 * @return {string|null}    returns '$contactId:$label' or null if not found
 */
function findContact (symbol, address) {
  if (typeof symbol !== 'string' || !symbol) return null;
  if (typeof address !== 'string' || !address) return null;
  address = cleanPrefix( address.trim().toLowerCase() ); // don't include symbol to purge all prefixes!
  for (const contactId in contacts) {
    const contact = contacts[contactId];
    for (const entry of contact) {
      for (const xsymbol in entry) {
        for (const label in entry[xsymbol]) {
          const entryAddress = entry[xsymbol][label].toLowerCase();
          const multiAddress = parseMultiAddress(entryAddress);
          if ( entryAddress.toLowerCase() === address ||
               (address.length > 5 && entryAddress.toLowerCase().split('-')[0] === address.split('-')[0]) || // handle XRP lables
               (multiAddress.isMultiAddress && multiAddress.subAddresses.join(',').indexOf(address) > -1) ) {
            return {contactId, label, symbol: xsymbol, address: entryAddress};
          }
        }
      }
    }
  }
  return null;
}

function findEntries (needle, symbol = '') {
  if (typeof needle !== 'string') return [];
  symbol = symbol.split('.')[0];
  const matchingEntries = [];
  for (const contactId in contacts) {
    const contact = contacts[contactId];
    for (const entry of contact) { // remove oldEntry
      for (const xsymbol in entry) {
        if (!symbol || xsymbol === symbol.toUpperCase()) {
          for (const label in entry[xsymbol]) { // remove oldEntry
            if (label.toLowerCase().includes(needle.toLowerCase()) || contactId.toLowerCase().includes(needle.toLowerCase())) {
              matchingEntries.push({contactId, label, symbol: xsymbol, address: entry[xsymbol][label] });
            }
          }
        }
      }
    }
  }
  return matchingEntries;
}

function removeEntry (symbol, contactId, label, address, dataCallback) {
  if (!contacts.hasOwnProperty(contactId)) return;
  symbol = symbol.toUpperCase();
  const contact = contacts[contactId];

  for (const entry of contact) { // remove oldEntry
    if (entry.hasOwnProperty(symbol) && entry[symbol].hasOwnProperty(label) && entry[symbol][label] === address) {
      delete entry[symbol][label];
      if (Object.keys(entry[symbol]).length === 0) delete entry[symbol];// trim empty containers
      if (Object.keys(entry).length === 0) contact.splice(contact.indexOf(entry), 1); // trim empty containers
      saveContacts();
      return dataCallback();
    }
  }
}

function removeContact (contactId, dataCallback) {
  if (contacts.hasOwnProperty(contactId)) {
    delete contacts[contactId];
    saveContacts();
    return dataCallback();
  }
}

function displayContact (contact) {
  return `👤 ${contact.contactId}${contact.label && contact.label !== '-' ? ' ➤ ' + contact.label : ''}`;
}

exports.removeContact = removeContact;
exports.removeEntry = removeEntry;
exports.contacts = contacts;
exports.loadContacts = loadContacts;
exports.saveToContacts = saveToContacts;
exports.getContact = getContact;
exports.renameContact = renameContact;
exports.updateEntry = updateEntry;
exports.findContact = findContact;
exports.findEntries = findEntries;
exports.displayContact = displayContact;
