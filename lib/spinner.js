exports.apply = apply;
exports.remove = remove;

function apply (BUTTON) {
  const SPAN = BUTTON.querySelector('DIV.spinner-loader > span');
  BUTTON.classList.add('disabled');
  BUTTON.style.pointerEvents = 'none';
  if (SPAN) return; // already has a spinner
  const SPAN_ = document.createElement('SPAN');
  SPAN_.style.display = 'none';
  for (const ELEMENT of BUTTON.childNodes) SPAN_.appendChild(ELEMENT);
  BUTTON.innerHTML = '';
  const DIV = document.createElement('DIV');
  DIV.className = 'spinner-loader';
  DIV.innerHTML = '...';
  DIV.appendChild(SPAN_);
  BUTTON.appendChild(DIV);
}

function remove (BUTTON) {
  const SPAN = BUTTON.querySelector('DIV.spinner-loader > span');
  BUTTON.classList.remove('disabled');
  BUTTON.style.pointerEvents = 'all';
  if (!SPAN) return; // no spinner to remove
  const DIV = BUTTON.querySelector('DIV.spinner-loader');
  for (const ELEMENT of SPAN.childNodes) BUTTON.appendChild(ELEMENT);

  BUTTON.removeChild(DIV);
}
