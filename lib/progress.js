
/**
 * [progress description]
 * @param  {Element} ELEMENT     The html container for the progress elements
 * @param  {Object} steps       The step labels {'StepName': ()=>breadCrumbOnclick}
 * @param  {integer} currentStep [description]
 * @return {void}
 */
exports.progress = (ELEMENT, steps, currentStep) => {
  const labels = Object.keys(steps);
  if (labels.length < 3) ELEMENT.classList.add('progress-steps2');
  if (labels.length === 3) ELEMENT.classList.add('progress-steps3');
  if (labels.length === 4) ELEMENT.classList.add('progress-steps4');
  if (labels.length >= 5) ELEMENT.classList.add('progress-steps5');
  ELEMENT.innerHTML = '';
  for (let step = 0; step < labels.length; ++step) {
    if (step > 0) {
      const DIV_line = document.createElement('DIV');
      DIV_line.classList.add('progress-line');
      if (step <= currentStep) DIV_line.classList.add('active');
      ELEMENT.appendChild(DIV_line);
    }

    const label = labels[step];
    const breadCrumbOnclick = steps[label];
    const DIV_step = document.createElement('DIV');
    DIV_step.classList.add('progress-step');

    const DIV_dot = document.createElement('DIV');
    DIV_dot.classList.add('dot');
    const DIV_label = document.createElement('DIV');
    DIV_label.classList.add('label');
    DIV_label.innerText = label;

    if (typeof breadCrumbOnclick === 'function') {
      DIV_step.style.cursor = 'pointer';
      DIV_step.onclick = breadCrumbOnclick;
    } else if (breadCrumbOnclick === 'error') {
      DIV_label.classList.add('failed');
      DIV_step.classList.add('failed');
    }

    if (step <= currentStep) DIV_step.classList.add('active');
    if (step === currentStep) {
      DIV_dot.classList.add('pulse');
    }
    DIV_step.appendChild(DIV_dot);
    DIV_step.appendChild(DIV_label);
    ELEMENT.appendChild(DIV_step);
  }
};
/**
 * [progress description]
 * @param  {Element} ELEMENT     The html container for the progress elements
 * @return {Function}
 */
exports.radial = (ELEMENT, label, progress = 0) => {
  const x = 150;
  const y = 150;
  const r = 125;
  const dr = 25;
  let currentProgress;
  let interval;
  const updateGradual = (progress, label) => {
    if (progress > 1) progress = 1;
    if (progress === false) ELEMENT.classList.remove('progress-radial-pulse');
    else ELEMENT.classList.add('progress-radial-pulse');

    currentProgress = progress;
    const a = progress === false ? 359 : 359 * progress;
    const x2 = x + r * Math.cos((a - 90) * Math.PI / 180);
    const y2 = y + r * Math.sin((a - 90) * Math.PI / 180);
    const x3 = x + (r - dr) * Math.cos((a - 90) * Math.PI / 180);
    const y3 = y + (r - dr) * Math.sin((a - 90) * Math.PI / 180);
    const x4 = x;
    const y4 = y - r + dr;
    if (typeof label === 'undefined') label = Math.floor(progress * 100) + '%';
    ELEMENT.classList.add('progress-radial-wrapper');
    ELEMENT.innerHTML = `
    <svg width="300px" height="300px" style="margin: auto; display: block;">
    <text x="50%" y="50%" dominant-baseline="middle" text-anchor="middle" font-size="14px" fill="black">${label}</text>

      <clipPath id="clip">
        <path d="M ${x} ${y - r}
                 A ${r} ${r} 0 ${a > 180 ? 1 : 0} 1 ${x2} ${y2}
                 L ${x3} ${y3}
                 A ${r - dr} ${r - dr} 0 ${a > 180 ? 1 : 0} 0 ${x4} ${y4}" />
      </clipPath>
      <foreignObject width="300px" height="300px" clip-path="url(#clip)">
        <div class="${progress === false ? 'progress-radial-failed' : 'progress-radial'}"/>
      </foreignObject>
    </svg>`;
  };
  // update uses updateGradual to gradually increment to re requested progress
  const update = (progress, label) => {
    if (progress === false) return updateGradual(progress, label);

    const startProgress = currentProgress || 0;
    const delta = progress - startProgress;
    let i = 0;
    if (interval) clearInterval(interval);
    interval = setInterval(() => {
      ++i;
      updateGradual(startProgress + delta * i / 10, label);
      if (i === 10) {
        clearInterval(interval);
        interval = null;
      }
    }, 50);
  };

  update(progress, label);
  return update;
};
