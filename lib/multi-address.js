const { decode } = require('../common/compress-unified-address');

// symbol variable is optional, without this method will try to determine if valid without strictly checking symbol
function parseMultiAddress (address, symbol) {
  if (typeof address === 'string') {
    if ( (typeof symbol === 'string' && symbol && address.startsWith(symbol + ':')) || (!symbol && address.indexOf(':') > 0) ) {
      address = address.split(':')[1];
    }
    const decodedAddress = decode(address);
    if (typeof decodedAddress !== 'string') return {isMultiAddress: false};
    return {isMultiAddress: true, subAddresses: decodedAddress.split(',')};
  } else return {isMultiAddress: false};
}

function cleanPrefix (address, symbol) {
  let result;
  if (!address.includes(',') && address.startsWith(symbol + ':')) { // automatically remove prefix 'symbol:address' -> 'address'
    result = address.substr(symbol.length + 1);
  } else {
    result = address.substr( address.indexOf(':')+1 );
  }
  return result;
}

exports.parseMultiAddress = parseMultiAddress;
exports.cleanPrefix = cleanPrefix;
