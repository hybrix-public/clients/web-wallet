function createNode (text) {
  const node = document.createElement('span');
  node.style.position = 'absolute';
  node.style.left = '-10000px';
  node.textContent = text;
  return node;
}

function copyNode (node) {
  let selection = document.getSelection();
  selection.removeAllRanges();

  let range = document.createRange();
  range.selectNodeContents(node);
  selection.addRange(range);

  document.execCommand('copy');
  selection.removeAllRanges();
}

function copyText (text) {
  const node = createNode(text);
  document.body.appendChild(node);
  copyNode(node);
  document.body.removeChild(node);
}

exports.onclickCopyToClipboard = (target, text, onSuccess, onError) => {
  const ELEMENT = typeof target === 'string'
    ? document.querySelector(target)
    : target;

  const onclick = () => {
    try {
      copyText(text);
      if (onSuccess) onSuccess();
    } catch (err) {
      if (onError) onError({err});
    }
  };

  ELEMENT.onclick = onclick;

  return {
    destroy: () => ELEMENT.removeEventListener('click', click),
    element: ELEMENT
  };
};
