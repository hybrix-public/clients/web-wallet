const ASSET_BALANCE_REFRESH_INTERVAL = 60 * 1000;

exports.addAsset = addAsset;
exports.removeAsset = removeAsset;
exports.clearAssets = clearAssets;
exports.clearRefreshEventListeners = clearRefreshEventListeners;

exports.getTags = getTags;
exports.getSymbols = getSymbols;
exports.getAsset = getAsset;
exports.isAdded = isAdded;

exports.addEventListener = addEventListener;
exports.removeEventListener = removeEventListener;

exports.getBalance = getBalance;
exports.getAddress = getAddress;
exports.hasPending = hasPending;
exports.hasSufficientFuel = hasSufficientFuel;

exports.isUnified = isUnified;
exports.isSubSymbolOf = isSubSymbolOf;

exports.setStarred = setStarred;
exports.isStarred = isStarred;
exports.getOrder = getOrder;

exports.isPreference = isPreference;
exports.setPreference = setPreference;
exports.getAssetPreferences = getAssetPreferences;
exports.importAssetPreferences = importAssetPreferences;
exports.storeAssetPreferences = storeAssetPreferences;

const hybrix = require('./hybrix').hybrix;
const listeners = {}; // {`${eventType}:${tag}`: {callback,viewId}}

const eventTypes = [
  'initAsset',
  'addAsset',
  'failAsset',
  'removeAsset',
  'starAsset',
  'unstarAsset',
  'refreshAsset',
  'preferenceAsset',
  'unpreferenceAsset'
];

function addEventListener (eventTypeAndTag, callback, viewId) {
  let [eventType, tag] = eventTypeAndTag.split(':');
  if (!tag) tag = '*';
  eventTypeAndTag = eventType + ':' + tag;
  if (eventType === 'refreshAsset') { // ensure that refresh calls are scheduled
    if (tag === '*') {
      for (const tag of getTags()) {
        if (typeof getAsset(tag).balance !== 'undefined' && getAsset(tag).balance !== 'n/a') callback(tag);
        startRefreshIfNotYetRefreshing(tag); // refresh all assets
      }
    } else {
      if (!isAdded(tag)) addAsset(tag);
      else if (typeof getAsset(tag).balance !== 'undefined' && getAsset(tag).balance !== 'n/a') callback(tag);
      startRefreshIfNotYetRefreshing(tag);
    }
  }
  if (!eventTypes.includes(eventType)) {
    console.error(`Invalid event type "${eventType}"`);
    return;
  }
  if (!listeners.hasOwnProperty(eventTypeAndTag)) listeners[eventTypeAndTag] = [];
  if (!listeners[eventTypeAndTag].includes(callback))listeners[eventTypeAndTag].push({callback, viewId});
}

function removeEventListener (eventTypeAndTag, callback) {
  let [eventType, tag] = eventTypeAndTag.split(':');
  if (!tag) tag = '*';
  if (!eventTypes.includes(eventType)) {
    console.error(`Invalid event type "${eventType}"`);
    return;
  }
  if (!listeners.hasOwnProperty(eventTypeAndTag)) return;
  const index = listeners[eventTypeAndTag].indexOf(callback);
  if (index === -1) return;
  listeners[eventTypeAndTag].splice(index, 1);
}

function callEventListeners (eventType, tag, ...parameters) {
  if (!eventTypes.includes(eventType)) {
    console.error(`Invalid event type "${eventType}"`);
    return;
  }
  if (listeners.hasOwnProperty(eventType + ':*')) {
    for (const {callback} of listeners[eventType + ':*']) {
      if (typeof callback === 'function') callback(tag, ...parameters);
    }
  }
  if (listeners.hasOwnProperty(eventType + ':' + tag)) {
    for (const {callback} of listeners[eventType + ':' + tag]) {
      if (typeof callback === 'function') callback(tag, ...parameters);
    }
  }
}

const assets = {};
/*
assets = {[tag = '$symbol#$offset'] : {
  starred: true|false,
  preference: true|false,
  balance,
  ...
}, ... }

 */

function isUnified (tag) {
  return isAdded(tag) && assets[tag].symbols;
}

function isAdded (tag) { return assets.hasOwnProperty(tag); }

function isStarred (tag) { return isAdded(tag) && assets[tag].starred; }

function isPreference (tag) { return isAdded(tag) && assets[tag].preference; }

function getOrder (tag) {
  return Object.keys(assets).indexOf(tag) * 100 + (isStarred(tag) ? 0 : 10000);
}

function isSubSymbolOf (symbol, superSymbol) {
  return isAdded(superSymbol) && assets[superSymbol].symbols && assets[superSymbol].symbols.hasOwnProperty(symbol);
}

function getBalance (tag) {
  return isAdded(tag) && assets[tag].hasOwnProperty('balance')
    ? assets[tag].balance
    : 'n/a';
}

function getAddress (tag) {
  return isAdded(tag) && assets[tag].hasOwnProperty('address')
    ? assets[tag].address
    : null;
}

function hasPending (tag) {
  return isAdded(tag) && assets[tag].hasOwnProperty('pending') &&
    assets[tag].pending instanceof Array && assets[tag].pending > 0;
}

function hasSufficientFuel (tag) {
  return isAdded(tag) && assets[tag].hasOwnProperty('balance')
    ? assets[tag].sufficientFuel
    : undefined;
}

function getTags () {
  return Object.keys(assets);
}

const unique = (value, index, self) => {
  return self.indexOf(value) === index;
};

function getSymbols () {
  return getTags()
    .map(tag => tag.split('#')[0])
    .filter(unique)
  ;
}

function getAsset (tag) {
  return assets[tag];
}

function removeAsset (tag) {
  if (!isAdded(tag)) return;
  clearRefreshLoop(tag);
  delete assets[tag];
  callEventListeners('removeAsset', tag);
}

function getAssetPreferences () {
  const assetPreferences = {};
  for (const tag of getTags()) {
    if (isPreference(tag)) assetPreferences[tag] = isStarred(tag) ? 1 : 0;
  }
  return assetPreferences;
}

function storeAssetPreferences () {
  const assetPreferences = getAssetPreferences();
  hybrix.lib.save({key: hybrix.constants.storagePrefixes.assetPreferences, value: JSON.stringify(assetPreferences), sessionKey: true, channel: 'y'}, () => {}, () => {});
}

function clearRefreshEventListeners (viewIdToBeCleared) {
  for (const eventTypeAndTag in listeners) {
    if (eventTypeAndTag.startsWith('refreshAsset:')) {
      listeners[eventTypeAndTag] = listeners[eventTypeAndTag].filter(({viewId}) => viewId !== viewIdToBeCleared);
      if (listeners[eventTypeAndTag].length === 0) delete listeners[eventTypeAndTag];
    }
  }
  // clean up refresh loops that are no longer needed
  if (listeners.hasOwnProperty('refreshAsset:*')) return; // all asset refreshes will be needed
  for (const tag of getTags()) {
    if (!listeners.hasOwnProperty('refreshAsset:' + tag)) clearRefreshLoop(tag); // no longer needed, clean up
  }
}

function clearAssets () {
  for (const tag of getTags()) removeAsset(tag);
  for (const eventTypeAndTag in listeners) delete listeners[eventTypeAndTag];
}

function setStarred (tag, starred, store = true) {
  if (!isAdded(tag)) return;
  const asset = getAsset(tag);
  if (asset.starred === starred) return; // nothing to do
  asset.starred = starred;
  if (store) storeAssetPreferences();
  if (starred) callEventListeners('starAsset', tag);
  else callEventListeners('unstarAsset', tag);
}

function setPreference (tag, preference, store = true) {
  if (!isAdded(tag)) return;
  const asset = getAsset(tag);
  if (asset.preference === preference) return; // nothing to do
  asset.preference = preference;
  if (store) storeAssetPreferences();
  if (preference) callEventListeners('preferenceAsset', tag);
  else callEventListeners('unpreferenceAsset', tag);
}

/*
Options
- {bool} [storeAssetPreference=true] : true|false store changes to asset preferences
- {bool} [starred=false] : true|false   whether to star asset
- {bool} [preference=false] whether to add to preferences
 */
async function addAsset (tag, dataCallback, errorCallback, options = {}) {
  const storeAssetPreference = !options.hasOwnProperty('storeAssetPreference') || options.storeAssetPreference;
  const symbol = tag.split('#')[0];

  // LEGACY: hard removal/conversion of old unified assets
  if (symbol === 'hy.usd' || symbol === 'hy.cny' || symbol === 'hy.eur') {
    symbol = symbol.split('.')[1];
  };

  const offset = Number(tag.split('#')[1] || 0);
  const preference = !!options.preference;
  const starred = !!options.starred;
  if (tag.endsWith('#0')) tag = symbol;
  if (isAdded(tag)) {
    if (options.hasOwnProperty('starred')) setStarred(tag, starred, storeAssetPreference);
    if (options.hasOwnProperty('preference')) setPreference(tag, preference, storeAssetPreference);
    if (typeof dataCallback === 'function') return dataCallback(getAsset(tag));
    return;
  }
  assets[tag] = {symbol, offset, starred, preference};
  if (preference && storeAssetPreference) storeAssetPreferences();

  callEventListeners('initAsset', tag);
  if (starred) callEventListeners('starAsset', tag);
  if (preference) callEventListeners('preferenceAsset', tag);

  return hybrix.lib.addAsset({symbol, offset},
    data => {
      const assetData = Object.values(data)[0];
      const asset = assets[tag];
      for (const key in assetData) asset[key] = assetData[key];
      if (asset.balance === 'undefined') asset.balance = 'n/a';
      callEventListeners('addAsset', tag);
      if (asset.balance !== 'n/a') callEventListeners('refreshAsset', tag, getAsset(tag));
      if (typeof dataCallback === 'function') return dataCallback(getAsset(tag));
      starRefreshLoop(tag);
    }, error => {
      callEventListeners('failAsset', tag, error);
      removeAsset(tag);
      if (typeof errorCallback === 'function') return errorCallback(error);
    }
  );
}

function startRefreshIfNotYetRefreshing (tag) {
  if (!isAdded(tag)) return;
  const asset = getAsset(tag);
  if (!asset.timeout) starRefreshLoop(tag); // start refresh loop
  // else already set to refresh
}

function clearRefreshLoop (tag) {
  if (!isAdded(tag)) return;
  const asset = getAsset(tag);
  if (asset.timeout) {
    clearTimeout(asset.timeout);
    asset.timeout = null;
  }
}

function scheduleNextRefresh (tag, interval) {
  if (!isAdded(tag)) return;
  const asset = getAsset(tag);
  clearRefreshLoop(tag);
  asset.timeout = setTimeout(() => starRefreshLoop(tag, interval), interval);
}

function starRefreshLoop (tag, interval = ASSET_BALANCE_REFRESH_INTERVAL) {
  const symbol = tag.split('#')[0];
  const offset = Number(tag.split('#')[1] || 0);
  hybrix.lib.refreshAsset({symbol, offset}, data => {
    const assetData = Object.values(data)[0];
    const asset = getAsset(tag);
    for (const key in assetData) {
      if (key === 'balance' && (assetData.balance === 'n/a' || typeof assetData.balance === 'undefined')) {
        if (asset.balance === 'undefined') asset.balance = 'n/a';
      } else asset[key] = assetData[key];
    }

    callEventListeners('refreshAsset', tag, getAsset(tag));
    // use randomness to stagger balance calls
    scheduleNextRefresh(tag, (1 + 0.1 * Math.random()) * ASSET_BALANCE_REFRESH_INTERVAL);
  }, error => {
    console.error(`Failed to refresh ${tag} asset`, error);
    scheduleNextRefresh(tag, interval * 2); // On error reduce update frequency
  });
}

function importAssetPreferences (assetPreferences, dataCallback, errorCallback) {
  // example: assetPreferences = {btc: 0, hy: 1, 'eth.hy': 0, 'vic.hy': 1};
  const sortedTags = Object.keys(assetPreferences).sort((tag1, tag2) => tag1.localeCompare(tag2));
  for (const tag of sortedTags) {
    const starred = !!assetPreferences[tag];
    addAsset(tag, dataCallback, errorCallback, {starred, preference: true, storeAssetPreference: false});
  }
}
