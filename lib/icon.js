exports.getFallbackIcon = getFallbackIcon;
exports.getIcon = getIcon;

const hybrix = require('./hybrix').hybrix;
const whenIconsReadyCallbacks = [];
let iconsRequested = false;
const icons = {};

function getFallbackIcon (symbol) {
  if (icons.hasOwnProperty(symbol)) return icons[symbol];
  if (symbol.endsWith('.hy') && icons.hy) return icons.hy;
  for (const iconSymbol in icons) {
    if (symbol.endsWith('.' + iconSymbol)) return icons[iconSymbol];
  }
  const char = (symbol.includes('.') ? symbol.split('.')[1] : symbol).charAt(0).toUpperCase(); // 'abc'->A 'abc.def' -> 'D'
  return `<svg width="50px" height="50px" viewBox="0 0 50 50" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="Asset-view" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="Symbols" transform="translate(-367.000000, -248.000000)" fill-rule="nonzero" fill="#000000"> <g id="error" transform="translate(367.000000, 248.000000)"> <path d="M25.016,0.016 C38.8656595,0.016 50.016,11.1663405 50.016,25.016 C50.016,38.8656595  38.8656595,50.016 25.016,50.016 C11.1663405,50.016 0.016,38.8656595 0.016,25.016 C0.016,11.1663405 11.1663405,0.016 25.016,0.016 Z" id="Shape"></path><text x="50%" y="72%" text-anchor="middle" fill="white" style="font-size: 32px; font-weight: 200;">${char}</text></g> </g> </g> </svg>`;
}

// will be called after icons retrieved from host
function handleIconsReadyCallbacks () {
  for (const {symbol, callback} of whenIconsReadyCallbacks) {
    const icon = icons.hasOwnProperty(symbol) ? icons[symbol] : getFallbackIcon(symbol);
    if (typeof callback === 'function') callback(icon);
  }
}

const getIcons = () => {
  if (iconsRequested) return;
  iconsRequested = true;
  hybrix.lib.rout({query: '/list/asset/icons', channel: false}, icons_ => {
    for (const symbol in icons_) icons[symbol] = icons_[symbol];
    handleIconsReadyCallbacks();
  }, error => {
    console.error('Failed to retrieve icons', error);
    handleIconsReadyCallbacks();
  });
};

function getIcon (symbol, callback) {
  if (Object.keys(icons).length > 0) { // icons already retrieved from host
    const icon = getFallbackIcon(symbol);
    if (typeof callback === 'function') callback(icon);
    return icon; // return fallback for direct usage
  } else { // icons not yet retrieved, retrieve them and provide fallback for now
    getIcons();
    if (typeof callback === 'function') whenIconsReadyCallbacks.push({symbol, callback});
    return getFallbackIcon(symbol); // return fallback for direct usage, queued callback will update when real icon is availabble
  }
}
