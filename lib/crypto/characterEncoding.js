// Crypto character encodings

// Binary encoding
const Binary = {

  // Convert a string to a byte array
  stringToBytes: function (str) {
    for (let bytes = [], i = 0; i < str.length; i++) { bytes.push(str.charCodeAt(i) & 0xFF); }
    return bytes;
  },

  // Convert a byte array to a string
  bytesToString: function (bytes) {
    for (let str = [], i = 0; i < bytes.length; i++) { str.push(String.fromCharCode(bytes[i])); }
    return str.join('');
  }

};

exports.Binary = Binary;
// UTF-8 encoding
exports.UTF8 = {

  // Convert a string to a byte array
  stringToBytes: function (str) {
    return Binary.stringToBytes(unescape(encodeURIComponent(str)));
  },

  // Convert a byte array to a string
  bytesToString: function (bytes) {
    return decodeURIComponent(escape(Binary.bytesToString(bytes)));
  }

};
