function SecureRandom () {
  const poolSize = 256;
  //  let state = null;
  const pool = [];
  let pptr = Math.floor(poolSize * Math.random());
  //  let poolCopyOnInit = null;

  this.seedTime = function () {
    this.seedInt(new Date().getTime());
  };

  // Mix in a 32-bit integer into the pool
  this.seedInt = function (x) {
    this.seedInt8(x);
    this.seedInt8(x >> 8);
    this.seedInt8(x >> 16);
    this.seedInt8(x >> 24);
  };

  // Mix in a 16-bit integer into the pool
  this.seedInt16 = function (x) {
    this.seedInt8(x);
    this.seedInt8(x >> 8);
  };

  // Mix in a 8-bit integer into the pool
  this.seedInt8 = function (x) {
    pool[pptr++] ^= x & 255;
    if (pptr >= poolSize) pptr -= poolSize;
  };

  this.getPool = () => pool;
}

exports.SecureRandom = new SecureRandom();
