const QRCode = require('./easy.qrcode.min'); // https:// github.com/ushelp/EasyQRCodeJS#options

exports.renderQR = function (text, ELEMENT) {
  ELEMENT.innerHTML = '';
  new QRCode(ELEMENT, {
    text,
    width: 200,
    height: 200,
    colorDark: '#000000',
    colorLight: '#ffffff',
    correctLevel: QRCode.CorrectLevel.H,
    logo: './png/logo-hybrix-qr.png', // Relative address, relative to `easy.qrcode.min.js`
    logoBackgroundTransparent: true // Whether use transparent image, default is false
  });
};
