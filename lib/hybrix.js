const branding = require('./branding');
const configuration = require('./configuration');
const {decompressFromEncodedURIComponent} = require('../common/crypto/lz-string.js');
const views = {}; // will be initialized from tmp/index.js
const openViewStack = [];
const parameterStack = [];
const alreadyOpenedViewIds = {};

function preLoadImage (src) {
  const image = document.createElement('img');
  image.src = src;
}

const hybrix = {
  view: {
    init: sources => { // will be called from tmp/index.js
      for (const viewId in sources) views[viewId] = sources[viewId];
      delete hybrix.view.init;
    }
    // pop
    // exec
    // open
    // close
    // closeAll
  },
  constants: configuration.constants,
  version: {} // wil be populated automatically using tmp/version.js
  // lib: hybrix-jslib interface will be initialized on load
  // login
};

exports.hybrix = hybrix;

const {importAssetPreferences, clearAssets, clearRefreshEventListeners} = require('./assets');
const {loadContacts} = require('./contacts');

const loadAssets = assetPreferences => {
  if (typeof assetPreferences !== 'object' || assetPreferences === null) assetPreferences = hybrix.constants.WALLET_ASSET_PREFERENCES;
  importAssetPreferences(assetPreferences,
    hybrix.view.exec('settings', 'loadCurrencyPreference'),
    console.error
  );
};

hybrix.login = (username, password, dataCallback, errorCallback, symbols, universalLoginSymbol) => {
  preLoadImage('./svg/radial-backdrop.svg');
  // todo refactor to respective views
  const credentials = universalLoginSymbol
    ? {symbol: universalLoginSymbol, privateKey: password}
    : {username, password};
  hybrix.lib.session(credentials, username => {
    document.getElementById('userID-nav').innerHTML = username.indexOf(':')>-1?`${username.split(':')[0]}:${username.split(':')[1].substr(0,3)}...${username.substr(-3)}`:`${username.substr(0,3)}...${username.substr(-3)}`;
    document.getElementById('userID').innerHTML = username;

    hybrix.lib.load({key: hybrix.constants.storagePrefixes.assetPreferences, sessionKey: true, channel: 'y'},
      savedAssetsString => {
        let savedAssetPreferences = null;
        try {
          console.log('Loading wallet assets and preferences.');
          savedAssetPreferences = JSON.parse(savedAssetsString);
        } catch (e) {
          console.error('Cannot load asset preferences! '+e);
        }
        loadAssets(savedAssetPreferences);
        dataCallback(); // the remaining tasks are kicked of
      },
      _ => {
        if (typeof symbols === 'string') symbols = symbols.split(',');
        let assetPreferences;
        if (symbols instanceof Array) {
          assetPreferences = Object.fromEntries(symbols.map(symbol => [symbol, 0]));
        } else {
          if (universalLoginSymbol) {
            console.log('Initializing universal login assets.');
            assetPreferences = {};
            assetPreferences[universalLoginSymbol] = 1;
          } else {
            console.log('Initializing new wallet assets.');
            assetPreferences = null;
          }
        }
        loadAssets(assetPreferences);
        dataCallback(); // the remaining tasks are kicked of
      });

    setLogOutTimeout();
    loadContacts();

  }, errorCallback);
};

let logOutTimeoutHandle = null;
let logOutFinalIntervalHandle = null;
let logOutTimeStamp = null;

function getLogOutMessage (counter) {
  return '{{LOGOUT:MESSAGE_LOGOUT_TIMEOUT_A}}<br>{{LOGOUT:MESSAGE_LOGOUT_TIMEOUT_B}}' + (hybrix.constants.LOGOUT_FINAL_TIMEOUT_S - counter) + '{{LOGOUT:MESSAGE_LOGOUT_TIMEOUT_C}}';
}

function doLogOut () {
  const DIV_popup = document.getElementById('popup');
  DIV_popup.style.display = 'none';
  hybrix.logout('{{LOGOUT:LOGGED_OUT_AUTO}}');
}

function setLogOutFinalInterval () {
  const DIV_popup = document.getElementById('popup');
  DIV_popup.style.display = 'block';

  let counter = logOutTimeStamp === null ? 0 : Math.floor((Date.now() - logOutTimeStamp - hybrix.constants.LOGOUT_TIMEOUT_MS) / 1000);
  DIV_popup.innerHTML = getLogOutMessage(counter);
  if (counter >= hybrix.constants.LOGOUT_FINAL_TIMEOUT_S) doLogOut();
  else if (logOutFinalIntervalHandle !== null) clearInterval(logOutFinalIntervalHandle);
  logOutFinalIntervalHandle = setInterval(
    () => {
      ++counter;
      if (counter >= hybrix.constants.LOGOUT_FINAL_TIMEOUT_S) doLogOut();
      else DIV_popup.innerHTML = getLogOutMessage(counter);
    }, 1000);
}

function setLogOutTimeout () {
  if (logOutTimeoutHandle !== null) clearTimeout(logOutTimeoutHandle);
  if (logOutFinalIntervalHandle !== null) {
    clearInterval(logOutFinalIntervalHandle);
    logOutFinalIntervalHandle = null;
    const DIV_popup = document.getElementById('popup');
    DIV_popup.style.display = 'none';
  }
  logOutTimeStamp = Date.now();
  logOutTimeoutHandle = setTimeout(() => {
    setLogOutFinalInterval();
  }, hybrix.constants.LOGOUT_TIMEOUT_MS);
}

function updateLogOutTimeout () {
  if (logOutTimeoutHandle !== null) setLogOutTimeout();
}

window.addEventListener('mousemove', updateLogOutTimeout);
window.addEventListener('mousedown', updateLogOutTimeout);

hybrix.logout = message => {
  if (logOutTimeoutHandle !== null) clearTimeout(logOutTimeoutHandle);
  logOutTimeoutHandle = null;
  if (logOutFinalIntervalHandle !== null) clearInterval(logOutFinalIntervalHandle);
  logOutFinalIntervalHandle = null;
  logOutTimeStamp = null;
  clearAssets();
  hybrix.view.exec('listAssets', 'clear');
  hybrix.view.swap('login', {message});
  hybrix.lib.logout();
  // todo refactor to respective views
  document.getElementById('userID-nav').innerHTML = '';
  document.getElementById('userID').innerHTML = '';
};

function isLoggedIn () {
  return document.getElementById('userID-nav').innerHTML !== '';
}
/**
 * based on url has rout to specific module with parameters using the module's rout method
 */
function handleRouting (viewId, parameters) {
  if (!viewId) viewId = hybrix.constants.DEFAULT_LANDING_PAGE;

  if (!views.hasOwnProperty(viewId)) { // Check if viewId exists and has a rout method
    hybrix.view.swap('login', {errorMessage: `Requested view ${viewId} does not exist!`});
    return;
  } else if (!views[viewId].hasOwnProperty('rout')) {
    hybrix.view.swap('login', {errorMessage: `Requested view ${viewId} cannot be opened!`});
    return;
  }

  const routingAction = () => {
    hybrix.view.exec(viewId, 'rout', parameters);
  };
  /* DEPRECATED: INSTANT ID LOGIN
  if (parameters.login) { // do login and rout
    const username = parameters.login.substr(0, 16);
    const password = parameters.login.substr(16);
    hybrix.login(username, password, routingAction, errorMessage => {
      console.error(errorMessage);
      hybrix.view.swap('login', {errorMessage});
    });
  } else */
  if (viewId === 'createWallet' || viewId === 'login' || isLoggedIn()) { // no need to login  TODO: let apps route from here?
    routingAction();
  } else { // first request a login, then rout
    const loginParameters = {message: parameters.message, hideCreateWallet: true};
    if (viewId === 'transaction') loginParameters.next = 'transaction';
    hybrix.view.swap('login', {callback: routingAction, ...loginParameters});
  }
}

exports.handleRouting = handleRouting;

function parseHash (hash) {
  const parameters = {};
  if (typeof hash === 'string' && hash.startsWith('#/')) { //  '#/viewId/parameter1=value1&...'
    let [viewId, parametersString] = decodeURI( hash.substr(2) ).split('/');//  '#/viewId/parameter1=value1&...' -> 'parameter1=value1&...' ->  ['viewId','parameter1=value1&...']
    if (viewId === 'z') { // #/z/abc indicates a compressed url,
      const compressedRoutingHash = hash.substr(4); // '#/z/abc' -> 'abc'
      const decompressedRoutingHash = decompressFromEncodedURIComponent(compressedRoutingHash); // 'abc' -> 'viewId/parameter1=value1&...'
      [viewId, parametersString] = decompressedRoutingHash.split('/');
    }
    if (typeof parametersString === 'string') {
      for (const parameterString of parametersString.split('&')) {
        const [key, value] = parameterString.split('=');
        if (key !== '') parameters[key] = value;
      }
    }
    return {viewId, parameters};
  } else return {viewId: null, parameters};
}
exports.parseHash = parseHash;

function handleRoutingHash () {
  const hash = window.location.hash;
  const {viewId, parameters} = parseHash(hash);
  if (viewId !== null) handleRouting(viewId, parameters);
  else hybrix.view.swap('login');
}

window.addEventListener('load', () => {
  const host = window.location.origin !== 'null' ? window.location.origin : 'https://wallet.hybrix.io'; // fallback to public wallet if no origin/localhost
  // TODO: make host selectable using a view!!!
  hybrix.lib = new Hybrix.Interface();

  for (const DIV_view of document.getElementsByClassName('view')) DIV_view.style.display = 'none';

  const DIV_gray = document.getElementById('gray');
  DIV_gray.onclick = () => hybrix.view.pop();

  branding.init();
  branding.logo(document.querySelector('#login_container .logo'));

  hybrix.lib.addHost({host: host + '/api/', encryptByDefault: true}, handleRoutingHash,
    error => {
      console.error(error);
      hybrix.view.open('alert', {icon:'cross', title: 'Fatal Failure', text: `Failed to connect to host "${host}\"! Please make sure you have an internet connection, and then try again.`, textMore: 'This is a fatal failure. Please contact hybrix support for assistance if possible.'});
      document.getElementById('alert-close-button').style.display = "none"; // hide the Okay button, since it is of no use here!
    }
  );
});

//  handle back button actions
window.onpopstate = function (e) {
  if (e.state) {
    const {viewId, parameters} = e.state;
    if (viewId === 'login') {
      hybrix.view.open('logout'); // prevent returning to login screen, ask to logout instead
    } else {
      hybrix.view.closeAll();
      handleRouting(viewId, parameters);
    }
  }
};

function setUrl (viewId, parameters) {
  let url = window.location.href.split('#')[0] + '#/' + viewId;
  let first = true;
  window.location.replace(url); // destroy any login identification first!
  const stateParameters = {};
  for (const key in parameters) {
    if (key !== 'id' && key !== 'user' && key !== 'pass') {
      const value = parameters[key];
      if (['string', 'number', 'boolean'].includes(typeof value)) {
        stateParameters[key] = value;
        url += (first ? '/' : '&') + key + '=' + value;
        first = false;
      }
    }
  }
  const newState = {viewId, parameters: stateParameters};
  const currentStateString = JSON.stringify(window.history.state);
  const newStateString = JSON.stringify(newState);
  if (newStateString !== currentStateString) {
    window.history.pushState(newState, '{{LOGIN:WALLET}} | hybrix - ' + viewId, url);
  }
}

function open (viewId, parameters = {}) {
  setUrl(viewId, parameters);

  try {
    if (!alreadyOpenedViewIds.hasOwnProperty(viewId)) {
      hybrix.view.exec(viewId, 'once');
      alreadyOpenedViewIds[viewId] = true;
    }
    hybrix.view.exec(viewId, 'open', parameters);
  } catch (e) {
    console.error(`Error opening ${viewId} view.`, e);
    if (viewId !== hybrix.constants.DEFAULT_LANDING_PAGE) {
      hybrix.view.swap(hybrix.constants.DEFAULT_LANDING_PAGE, {errorMessage: `Error opening ${viewId}!`});
    }
  }
}

hybrix.view.swap = function (viewId, parameters) { // swap base view
  if (!views.hasOwnProperty(viewId)) return;
  for (let index = openViewStack.length - 1; index >= 0; --index) {
    const viewId = openViewStack[index];
    close(viewId);
  }

  const DIV_gray = document.getElementById('gray');
  DIV_gray.style.display = 'none';

  for (const DIV_view of document.getElementsByClassName('view')) {
    if (DIV_view.id === 'view-' + viewId) {
      DIV_view.style.display = 'block';
      DIV_view.style.overflow = 'auto'; // reset scroll in base view
      const DIV_modalBody = DIV_view.querySelector('.modal-body');
      if (DIV_modalBody) DIV_modalBody.scrollTop = 0; // reset scroll in base view
    } else DIV_view.style.display = 'none';
  }
  openViewStack.length = 0;
  parameterStack.length = 0;
  openViewStack.push(viewId);
  parameterStack.push(parameters);
  const DIV_baseView = document.getElementById('view-' + openViewStack[0]);
  DIV_baseView.style.overflow = 'auto';
  open(viewId, parameters);
};

hybrix.view.open = function (viewId, parameters) {
  if (!views.hasOwnProperty(viewId)) return;
  for (const openViewId of openViewStack) {
    if (openViewId === viewId) hybrix.view.close(viewId);
  }

  const DIV_gray = document.getElementById('gray');
  DIV_gray.style.display = 'block';

  const DIV_view = document.getElementById('view-' + viewId);
  DIV_view.style.display = 'block';
  const DIV_modalBody = DIV_view.querySelector('.modal-body');
  if (DIV_modalBody)DIV_modalBody.scrollTop = 0; // reset scroll

  const BUTTONs_cancel = DIV_view.getElementsByClassName('BtnCancel'); // handle corner X button
  for (const BUTTON_cancel of [...BUTTONs_cancel]) {
    BUTTON_cancel.onclick = () => hybrix.view.close(viewId);
  }
  const BUTTONs_cancelCloseAll = DIV_view.getElementsByClassName('BtnCancelCloseAll'); // handle corner X button
  for (const BUTTON_cancelCloseAll of [...BUTTONs_cancelCloseAll]) {
    BUTTON_cancelCloseAll.onclick = () => hybrix.view.closeAll();
  }

  if (openViewStack.length > 0) { // disable scroll in base view
    const DIV_baseView = document.getElementById('view-' + openViewStack[0]);
    DIV_baseView.style.overflow = 'visible';
  }
  openViewStack.push(viewId);
  parameterStack.push(parameters);

  if (DIV_view.firstElementChild.classList.contains('modal')) { // push modal to front
    DIV_gray.style.zIndex = openViewStack.length * 10 - 1;
    DIV_view.firstElementChild.style.zIndex = openViewStack.length * 10;
  }
  open(viewId, parameters);
};

function close (viewId, ...parameters) {
  clearRefreshEventListeners(viewId);
  if (!views.hasOwnProperty(viewId)) return;
  const DIV_view = document.getElementById('view-' + viewId);
  DIV_view.style.display = 'none';

  // reorder z indices
  const index = openViewStack.indexOf(viewId);
  if (index === -1) return; // closing a view that's not open
  openViewStack.splice(index, 1);
  parameterStack.splice(index, 1);
  for (let i = 1; i < openViewStack.length; ++i) {
    const viewId = openViewStack[i];
    const DIV_view = document.getElementById('view-' + viewId);
    if (DIV_view.firstElementChild.classList.contains('modal')) { // push modal to front
      DIV_view.firstElementChild.style.zIndex = (i + 1) * 10;
    }
  }
  const DIV_gray = document.getElementById('gray');
  DIV_gray.style.zIndex = openViewStack.length * 10 - 1;
  if (openViewStack.length === 1) DIV_gray.style.display = 'none';

  if (DIV_view.firstElementChild.classList.contains('modal')) DIV_view.firstElementChild.style.zIndex = 0; // push modal to background

  hybrix.view.exec(viewId, 'close', ...parameters);
}

hybrix.view.close = function (viewId, ...parameters) {
  close(viewId, ...parameters);
  if (openViewStack.length === 1) { // reset scroll in base view
    const DIV_baseView = document.getElementById('view-' + openViewStack[0]);
    DIV_baseView.style.overflow = 'auto';
  }
  if (openViewStack.length > 0) {
    const newTopViewId = openViewStack[openViewStack.length - 1];
    setUrl(newTopViewId, parameterStack[openViewStack.length - 1]);

    if (openViewStack.length === 1) { // re-enanble scroll in base view
      const DIV_baseView = document.getElementById('view-' + openViewStack[0]);
      DIV_baseView.style.overflow = 'auto';
    }
  }
};

hybrix.view.exec = function (viewId, func, ...parameters) {
  if (!views.hasOwnProperty(viewId)) return; // TODO handle ?
  const view = views[viewId];
  if (view.hasOwnProperty(func) && typeof view[func] === 'function') return view[func](...parameters);
};

hybrix.view.pop = () => { // Close the top view
  if (openViewStack.length > 1) hybrix.view.close(openViewStack[openViewStack.length - 1]);
  if (openViewStack.length > 0) {
    const viewId = openViewStack[openViewStack.length - 1];
    setUrl(viewId, parameterStack[openViewStack.length - 1]);
  }
};

hybrix.view.closeAll = () => { // Close all views but the base view
  for (let index = openViewStack.length - 1; index >= 1; --index) {
    const viewId = openViewStack[index];
    close(viewId);
  }
  const viewId = openViewStack[0];
  setUrl(viewId, parameterStack[0]);

  if (openViewStack.length === 1) { // re-enanble scroll in base view
    const DIV_baseView = document.getElementById('view-' + openViewStack[0]);
    DIV_baseView.style.overflow = 'auto';
  }
};
