// (C) 2024 hybrix / hy.company / Joachim de Koning
// hybrixd module - web-wallet/module.js
// Module to provide the web wallet

// exec
function web_wallet (proc) {
  const source = 'web-wallet';
  const command = proc.command.slice(1).map((x) => x.split('?version=')[0]); // use version arg to do cache busting!
  const basePath = 'modules/' + source + '/files/';
  const routePath = command.length === 0 ? 'index.html' : command.join('/') +
    (command[0] === 'app' && command.length === 2 ? '/index.html' : '');
  proc.serv(basePath + routePath);
}

function version (proc) {
  proc.done(proc.peek('version'));
}

// exports
exports.web_wallet = web_wallet;
exports.version = version;
