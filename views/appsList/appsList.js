const hybrix = require('../../lib/hybrix').hybrix;
const {createElement} = require('../../lib/html');

exports.rout = () => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('appsList');
};

function renderApplication(appId) {
  const DIV_container = document.getElementById('appButtons');
  let mode = appId.split(':')[0];
  let id = appId.split(':')[1];
  const title = appId.split(':')[2]?appId.split(':')[2]:appId.split(':')[1];
  const properties = {app:id.toLowerCase(),title:title};

  // set some TODOs first!
  /*
  switch (properties.app) {
    case 'xdao':
      mode = 'modal';
      id = 'TODO';
    break;
  }*/
  let DIV_appButton;
  if(mode === 'modal') {
    DIV_appButton = createElement({
      classList: 'app modal-app'+(id==='TODO'?' app-todo':''),
      id: `modal-${properties.app}`,
      onclick: event => {
        hybrix.view.open(id,properties);
        event.stopPropagation();
      },
      tagName: 'DIV',
      innerHTML: `<img src="./svg/app-${properties.app}.svg" class="icon"></img><span>${title}</span>`
    });
  } else if(mode === 'link') {
    const links = {
      "explorer" : "https://explorer.hybrix.io"
    }
    DIV_appButton = createElement({
      classList: 'app link-app',
      id: `app-${properties.app}`,
      onclick: event => {
        window.open(links[properties.app], "_blank");
        event.stopPropagation();
      },
      tagName: 'DIV',
      innerHTML: `<img src="./svg/app-${properties.app}.svg" class="icon"></img><span>${title} ↝</span>`
    });
  } else {
    DIV_appButton = createElement({
      classList: 'app view-app',
      id: `app-${properties.app}`,
      onclick: event => {
        hybrix.view.swap(id,properties);
        event.stopPropagation();
      },
      tagName: 'DIV',
      innerHTML: `<img src="./svg/app-${properties.app}.svg" class="icon"></img><span>${title}</span>`
    });
  }
  DIV_container.appendChild(DIV_appButton);
}

exports.open = () => {
  // make pro-Mode available under hidden button/setting
  const H2_Applications = document.getElementById('appsList-title');
  const proModeMax = 4;
  let proModeClicks = 0;
  H2_Applications.onclick = () => {
    if (proModeClicks === proModeMax) {
      console.log(`Pro mode activated!`);
      const apps = ['app:allocate:Allocate'];
      for (app of apps) {
        renderApplication(app);
      }
    } else {
      if(proModeClicks < proModeMax) console.log(`Activating pro mode in ${proModeMax-proModeClicks} click${proModeMax-proModeClicks===1?'':'s'}...`);
      proModeClicks++;
    }
  };
};

exports.once = () => {
  // render available applications
  // DEPRECATED APP: app:xDAO:xDAO
  const apps = ['modal:qrScanner:{{APPSLIST:APP_SCANNER}}','modal:banking:{{APPSLIST:APP_BANKING}}','modal:contacts:{{APPSLIST:APP_CONTACTS}}','modal:pending:{{APPSLIST:APP_PENDING}}','modal:manageAssets:{{APPSLIST:APP_ASSETS}}','link:explorer:{{APPSLIST:APP_EXPLORER}}','modal:makePass:{{APPSLIST:APP_MAKEPASS}}','modal:settings:{{APPSLIST:APP_SETTINGS}}'];
  for (app of apps) {
    renderApplication(app);
  }
};
