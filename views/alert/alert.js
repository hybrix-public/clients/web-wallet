const hybrix = require('../../lib/hybrix').hybrix;

exports.rout = () => {
  hybrix.view.swap('listAssets');
}

function open (parameters) {
  const H2_title = document.getElementById('alert-title');
  const P_message = document.getElementById('alert-message');
  const P_message_more = document.getElementById('alert-message-more');
  const BUTTON_close = document.getElementById('alert-close-button');
  const IMG_checkmark = document.getElementById('alert-image-checkmark');
  const IMG_cross = document.getElementById('alert-image-cross');
  const IMG_info = document.getElementById('alert-image-info');

  if (typeof parameters.title === 'string') H2_title.innerText =  parameters.title;
  if (typeof parameters.text === 'string') P_message.innerHTML =  parameters.text;
  if (typeof parameters.textMore === 'string') P_message_more.innerHTML =  parameters.textMore;
  if (typeof parameters.button === 'string') BUTTON_close.innerText =  parameters.button;

  if (parameters.icon === 'cross') {
    IMG_cross.style.display = 'block';
    IMG_checkmark.style.display = 'none';
    IMG_info.style.display = 'none';
  } else if (parameters.icon === 'checkmark') {
    IMG_cross.style.display = 'none';
    IMG_checkmark.style.display = 'block';
    IMG_info.style.display = 'none';
  } else {
    IMG_cross.style.display = 'none';
    IMG_checkmark.style.display = 'none';
    IMG_info.style.display = 'block';
  }

  BUTTON_close.onclick = () => {
    if (typeof parameters.callback === 'function') {
      parameters.callback();
    } else {
      hybrix.view.pop();
    }
  }
}

exports.open = open;
