const {onclickCopyToClipboard} = require('../../lib/clipboard');
const hybrix = require('../../lib/hybrix').hybrix;
const {renderQR} = require('../../lib/qr');
const {parseMultiAddress} = require('../../lib/multi-address');
const {createElement, createCustomCheckBox} = require('../../lib/html');

exports.rout = parameters => {
  // TODO what if no symbol defined?
  const {symbol} = parameters;
  hybrix.view.swap('viewAsset', {symbol});
  hybrix.view.open('exportAsset', parameters);
};

const render = (asset, index = 0) => fullPrivateKey => {
  const DIV_subAddresses = document.getElementById('exportAsset-subAddresses');
  const multiAddress = parseMultiAddress(asset.address, asset.symbol);
  let privateKey;
  const INPUTsBySubSymbol = {};
  let i = 0;
  DIV_subAddresses.innerHTML = '';
  if (multiAddress.isMultiAddress) {
    for (const subAddress of multiAddress.subAddresses) {
      const subSymbol = subAddress.split(':')[0];
      if (i === index) {
        privateKey = fullPrivateKey.split(',') // 'a:1,b:2' -> // ['a:1','b:2']
          .filter(subPrivateKey => subPrivateKey.startsWith(subSymbol + ':'))[0] //  ['a:1','b:2'] > ['a:1'] -> 'a:1'
          .split(':')[1]; // 'a:1' -> '1'
      }
      const INPUT = createCustomCheckBox({
        parentNode: DIV_subAddresses,
        label: subSymbol,
        checked: i === index,
        onclick: (function (c) { return () => render(asset, c)(fullPrivateKey); }(i)),
        id: 'exportAsset_' + subSymbol,
        style: {display: 'inline-block'}
      });
      INPUTsBySubSymbol[subSymbol] = INPUT;
      ++i;
    }
  } else privateKey = fullPrivateKey;

  const DIV_privatekey = document.getElementById('export-privatekey');
  DIV_privatekey.innerText = privateKey;
  const DIV_QR = document.getElementById('export-qrcode');
  renderQR(privateKey, DIV_QR);

  const BUTTON_toggle = document.getElementById('export-toggle-privatekey-button');
  const SPAN_toggle = document.getElementById('export-toggle-visibility');
  const BUTTON_copy = document.getElementById('export-copy-privatekey-button');

  const hide = () => {
    DIV_privatekey.style.display = 'none';
    DIV_QR.style.display = 'none';
    SPAN_toggle.innerHTML = 'Show';
  };

  hide();

  BUTTON_toggle.onclick = () => {
    const DIV_copy = document.getElementById('exportAsset-copySuccess');
    DIV_copy.classList.remove('active');
    const visible = SPAN_toggle.innerHTML === 'Hide';

    onclickCopyToClipboard(BUTTON_copy, privateKey, () => {}, console.error); // TODO define onSuccess/ onError

    if (visible) hide();
    else {
      DIV_privatekey.style.display = 'inline-block';
      DIV_QR.style.display = 'inline-block';
      SPAN_toggle.innerHTML = 'Hide';
    }
  };

  onclickCopyToClipboard(BUTTON_copy, privateKey, () => {
    const DIV_copy = document.getElementById('exportAsset-copySuccess');
    DIV_copy.classList.add('active');
  }, console.error); // TODO define onError
};

function fail () {
// TODO
}

exports.open = parameters => {
  const {symbol} = parameters;
  const DIV_copy = document.getElementById('exportAsset-copySuccess');
  DIV_copy.classList.remove('active');
  hybrix.lib.addAsset(
    {symbol},
    data => hybrix.lib.getPrivateKey({symbol: symbol}, render(data[symbol]), fail),
    console.error
  );
};
