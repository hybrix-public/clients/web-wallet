const hybrix = require('../../lib/hybrix').hybrix;
const {radial} = require('../../lib/progress');
const {getIcon} = require('../../lib/icon');
const {addAsset, storeAssetPreferences} = require('../../lib/assets');

exports.rout = () => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('scanAssets');
};

const assetsFound = [];

const checkBalance = symbol => balance => {
  if (balance > 0 && symbol !== 'dummy') {
    assetsFound.push(symbol);
    addAsset(symbol, null, console.error, {preference: true, storeAssetPreference: false});
    const A_action = document.getElementById('manageAssets-symbol-' + symbol);
    if (A_action) {
      A_action.className = 'pure-button changeManageBtn selectedAsset';
      const DIV_tr = A_action.parentNode.parentNode;
      DIV_tr.classList.add('added');
    }
    return true;
  }
  return false;
};

function scanDone () {
  const DIV_progress = document.getElementById('scanAssets-progress');
  let message = '<p>{{SCANASSETS:MESSAGE_FOUND_A}}' + assetsFound.length + '{{SCANASSETS:MESSAGE_FOUND_B}}</p><p style="text-align:left; margin-left:1cm;">';
  for (const symbol of assetsFound) {
    const icon = getIcon(symbol);
    message += '<span class="scanAssets-icon">' + icon + '</span>&nbsp;' + symbol + '<br/>';
  }
  message += '</p>';
  DIV_progress.innerHTML = message;
  storeAssetPreferences();
}

const getBalances = updateRadial => symbols => {
  const steps = Object.fromEntries(symbols.map(symbol => [symbol, [{symbol}, 'getBalance', checkBalance(symbol)]]));
  hybrix.lib.parallel(steps, scanDone, console.error, progress => {
    updateRadial(progress, Math.floor(progress * 100) + '% (' + assetsFound.length + ' {{SCANASSETS:FOUND}})');
  });
};

function scan (updateRadial) {
  const BUTTON_start = document.getElementById('scanAssets-start');
  assetsFound.length = 0; // reset
  BUTTON_start.classList.add('disabled');
  updateRadial(0.1, '{{SCANASSETS:INIT_SCAN}}');
  hybrix.lib.rout({query: '/a'}, getBalances(updateRadial), console.error);
}

exports.open = function () {
  const BUTTON_start = document.getElementById('scanAssets-start');
  const DIV_progress = document.getElementById('scanAssets-progress');
  const DIV_message = document.getElementById('scanAssets-message');

  if (window.location.hostname === 'localhost') {
    const updateRadial = radial(DIV_progress, '{{SCANASSETS:PRESS_START}}', 0);
    BUTTON_start.classList.remove('disabled');
    BUTTON_start.onclick = () => scan(updateRadial);
    DIV_message.innerHTML = '';
  } else {
    BUTTON_start.classList.add('disabled');
    const updateRadial = radial(DIV_progress, '{{SCANASSETS:FEATURE_NOT_AVAILABLE}}', false);
    DIV_message.innerHTML = '{{SCANASSETS:SCAN_ONLY_AVAILABLE_LOCALLY}} <br><a target="_blank" href="https://api.hybrix.io/help/hybrixd">{{SCANASSETS:LINK_DOCUMENTATION}}</a> {{SCANASSETS:FOR_MORE_INFORMATION}}';
  }
};
