const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;
const {setReporting} = require('../settings/settings');

let bankingMethods;

exports.rout = parameters => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('banking');
};

function grabEmail(callbackFollowup) {
  // grab user's e-mail if needed ahead of anything else!
  hybrix.lib.load({key: hybrix.constants.storagePrefixes.email, sessionKey: true, channel: 'y'}, email => {
    callbackFollowup(email);
  }, () => { setReporting(callbackFollowup); });
}

function once (parameters) {
  const DIV_message = document.getElementById('banking-message');
  const DIV_message_more = document.getElementById('banking-message-more');
  const BUTTON_deposit = document.getElementById('banking-deposit');
  const BUTTON_withdraw = document.getElementById('banking-withdraw');
  const BUTTON_transfer = document.getElementById('banking-transfer');

  function failBanking() {
    DIV_message.innerHTML = '{{BANKING:NOT_AVAILABLE_A}}';
    DIV_message_more.innerHTML = '{{BANKING:NOT_AVAILABLE_B}}';
    BUTTON_deposit.setAttribute('disabled',true);
    BUTTON_withdraw.setAttribute('disabled',true);
    BUTTON_transfer.setAttribute('disabled',true);
  }
  
  hybrix.lib.rout({query: '/e/banking/methods', regular: false}, (result) => {
    bankingMethods = result;
    if (typeof bankingMethods === 'object' && bankingMethods !== null && Object.keys(bankingMethods).length > 0) {
      BUTTON_deposit.removeAttribute('disabled');
      BUTTON_withdraw.removeAttribute('disabled');
      BUTTON_transfer.removeAttribute('disabled');
    } else failBanking();
  }, (err) => {
    failBanking();
  });
}

function open (parameters) {
  const BUTTON_deposit = document.getElementById('banking-deposit');
  const BUTTON_withdraw = document.getElementById('banking-withdraw');
  const BUTTON_transfer = document.getElementById('banking-transfer');
  const DIV_progress = document.getElementById('banking-progress');

  progress(DIV_progress, {
    '{{BANKINGAMOUNT:PROGRESS_DETAILS}}': () => hybrix.view.pop(),
    '{{BANKINGAMOUNT:PROGRESS_AMOUNT}}': null,
    '{{BANKINGAMOUNT:PROGRESS_REVIEW}}': null,
    '{{BANKINGAMOUNT:PROGRESS_TRANSACT}}': null
  }, 0);

  grabEmail(
    email => {
      BUTTON_deposit.onclick = () => hybrix.view.open('bankingAsset', {action: 'deposit', email, bankingMethods});
      BUTTON_withdraw.onclick = () => hybrix.view.open('bankingAsset', {action: 'withdraw', email, bankingMethods});
      BUTTON_transfer.onclick = () => hybrix.view.open('bankingAsset', {action: 'transfer', email, bankingMethods});
    }
  );
}

exports.once = once
exports.open = open;
