const hybrix = require('../../lib/hybrix').hybrix;

exports.rout = () => {
  hybrix.view.swap('listAssets');
}

function open (parameters) {
  const BUTTON_close = document.getElementById('todo-close-button');
  BUTTON_close.onclick = () => hybrix.view.pop();
};

exports.open = open;
