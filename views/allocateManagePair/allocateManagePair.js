const spinner = require('../../lib/spinner');
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const hybrix = require('../../lib/hybrix').hybrix;
const {getSignatureSteps, refreshPairs, DEFAULT_ALLOCATION_OFFSET} = require('../allocate/allocate.js');
const {addAsset, addEventListener, getBalance} = require('../../lib/assets');

const SWAP_STATS_CACHE_TIME = 5 * 60 * 1000;
exports.SWAP_STATS_CACHE_TIME = SWAP_STATS_CACHE_TIME;
exports.setPair = setPair;
exports.getPair = getPair;
exports.deletePair = deletePair;

exports.rout = parameters => {
  hybrix.view.swap('allocate');
  hybrix.view.open('allocateManagePair', parameters);
};

function getPair (fromSymbol, toSymbol, dataCallback, errorCallback) {
  hybrix.lib.sequential([
    ...getSignatureSteps('getPair', [fromSymbol, toSymbol]),
    ({accountID, signature}) => ({query: '/e/swap/allocation/pair/get/' + accountID + '/' + fromSymbol + '/' + toSymbol + '/' + signature, regular: false}), 'rout'
  ], dataCallback, errorCallback);
}

function setPair (fromSymbol, toSymbol, feePercent, riskPercent, balanceWeightPercent, dataCallback, errorCallback) {
  hybrix.lib.sequential([
    ...getSignatureSteps('setPair', [fromSymbol, toSymbol, feePercent, riskPercent, balanceWeightPercent]),
    ({accountID, signature}) => ({query: '/e/swap/allocation/pair/set/' + accountID + '/' + fromSymbol + '/' + toSymbol + '/' + feePercent + '/' + riskPercent + '/' + balanceWeightPercent + '/' + signature, regular: false}), 'rout'
  ], dataCallback, errorCallback);
}

function deletePair (fromSymbol, toSymbol, dataCallback, errorCallback) {
  hybrix.lib.sequential([
    ...getSignatureSteps('deletePair', [fromSymbol, toSymbol]),
    ({accountID, signature}) => ({query: '/e/swap/allocation/pair/delete/' + accountID + '/' + fromSymbol + '/' + toSymbol + '/' + signature, regular: false}), 'rout'
  ], dataCallback, errorCallback);
}

function startDeletePair (parameters) {
  const DIV_spinner = document.getElementById('allocateManagePair-spinner');
  const DIV_spinner_message = document.getElementById('allocateManagePair-spinner-message');
  const DIV_message = document.getElementById('allocateManagePair-message');
  DIV_message.innerHTML = '';
  DIV_spinner_message.innerText = 'Deleting pair...';
  DIV_spinner.style.display = 'block';
  deletePair(parameters.deleteParams.ask, parameters.deleteParams.bid, () => {
    refreshPairs();
    DIV_spinner.style.display = 'none';
    hybrix.view.closeAll();
  },
  error => {
    DIV_message.innerHTML = 'Failed to delete pair! '+error;
    DIV_spinner.style.display = 'none';
    console.error(error);
  });
}

function setDeleteButton(balance,ask,bid) {
  const BUTTON_withdraw = document.getElementById('allocateManagePair-withdraw');
  const BUTTON_delete = document.getElementById('allocateManagePair-delete');
  BUTTON_withdraw.removeAttribute('disabled');
  BUTTON_delete.removeAttribute('disabled');
  if (balance > 0.05) {
    BUTTON_delete.onclick = () => {
      hybrix.view.open('alert', {
        title:'Delete Pair',
        text:'Before you can delete a pair, you must withdraw all funds from it first!',
        textMore:'Please use the Withdraw button to remove your allocated funds from this pair. Then click on Delete again.',
        button:'Got it!'
      });
    };
  } else {
    deleteParams = {ask, bid}
    BUTTON_delete.onclick = () => {
      hybrix.view.open('confirm', {
        text:'Are you sure you want to delete this pair?',
        textMore:'It can be created again later on.',
        yesCallback: startDeletePair,
        deleteParams
      });
    };
  }
}

function renderPair (pairData) {
  const BUTTON_save = document.getElementById('allocateManagePair-save');
  const BUTTON_delete = document.getElementById('allocateManagePair-delete');
  const BUTTON_withdraw = document.getElementById('allocateManagePair-withdraw');
  const DIV_message = document.getElementById('allocateManagePair-message');
  const DIV_allocationBalance = document.getElementById('allocateManagePair-allocationBalance');
  const DIV_risk = document.getElementById('allocateManagePair-risk');
  const DIV_sufficiency = document.getElementById('allocateManagePair-sufficiency');
  const INPUT_feeContainer = document.getElementById('allocateManagePair-feeContainer');
  const INPUT_fee = document.getElementById('allocateManagePair-fee');
  DIV_allocationBalance.innerHTML = prettyPrintAmount(pairData.balance, pairData.bid, false, true);
  DIV_risk.innerHTML = pairData.risk; // TODO make adjustable
  DIV_sufficiency.innerHTML = prettyPrintAmount(pairData.sufficiency, pairData.bid, false, true);
  INPUT_fee.value = parseFloat(pairData.fee);
  INPUT_fee.onchange = INPUT_fee.oninput = INPUT_fee.onpaste = () => {
    const value = INPUT_fee.value;
    if (!isNaN(value) && value > 0) {
      BUTTON_save.removeAttribute('disabled');
    }
  }
  if (!pairData.active) {
    DIV_message.innerHTML = 'Insufficient allocated balance or sufficiency!';
  }
  setDeleteButton(pairData.balance,pairData.ask,pairData.bid);
}

function render (parameters) {
  const {pair} = parameters;
  const DIV_spinner = document.getElementById('allocateManagePair-spinner');
  const DIV_spinner_message = document.getElementById('allocateManagePair-spinner-message');
  const DIV_message = document.getElementById('allocateManagePair-message');
  const DIV_pair = document.getElementById('allocateManagePair-pair');
  const DIV_explain = document.getElementById('allocateManagePair-explain');
  const DIV_risk = document.getElementById('allocateManagePair-risk');
  const DIV_mainBalance = document.getElementById('allocateManagePair-mainBalance');
  const DIV_allocationBalance = document.getElementById('allocateManagePair-allocationBalance');
  const DIV_feeRange = document.getElementById('allocateManagePair-feeRange');
  const DIV_sufficiency = document.getElementById('allocateManagePair-sufficiency');
  const DIV_rate = document.getElementById('allocateManagePair-rate');
  const INPUT_fee = document.getElementById('allocateManagePair-fee');
  const INPUT_feeContainer = document.getElementById('allocateManagePair-feeContainer');
  const BUTTON_withdraw = document.getElementById('allocateManagePair-withdraw');
  const BUTTON_allocate = document.getElementById('allocateManagePair-allocate');
  const BUTTON_save = document.getElementById('allocateManagePair-save');
  const BUTTON_delete = document.getElementById('allocateManagePair-delete');
  const BUTTON_virt = document.getElementById('allocateManagePair-virt');

  DIV_spinner.style.display = 'none';
  DIV_message.innerHTML = '';
  DIV_risk.innerHTML = '...'; // TODO make adjustable
  DIV_allocationBalance.innerHTML = '...';
  DIV_mainBalance.innerHTML = '...';
  DIV_sufficiency.innerHTML = '...';
  INPUT_fee.value = '';
  DIV_feeRange.innerHTML = '...';
  DIV_message.innerHTML = '';
  BUTTON_withdraw.setAttribute('disabled', true);
  BUTTON_allocate.setAttribute('disabled', true);
  BUTTON_save.setAttribute('disabled', true);
  BUTTON_delete.setAttribute('disabled', true);

  if (pair) {
    const [fromSymbol, toSymbol] = pair.split(':');
    DIV_pair.innerHTML = pair.toUpperCase().replace(':',' ⊳ ');
    DIV_explain.innerHTML = `This allocation pair accepts ${fromSymbol.toUpperCase()} in exchange for ${toSymbol.toUpperCase()}.`;
    BUTTON_save.onclick = () => {
      BUTTON_save.setAttribute('disabled', true);
      DIV_spinner.style.display = 'block';
      DIV_spinner_message.innerText = 'Saving changes...';
      setTimeout(() => {  // give UI some time to update
        const feePercent = INPUT_fee.value;
        const riskPercent = 15;            // TODO: make configurable
        const balanceWeightPercent = 5; // TODO: implementation
        setPair(fromSymbol, toSymbol, feePercent, riskPercent, balanceWeightPercent,
          () => {
            BUTTON_save.removeAttribute('disabled');
            DIV_spinner.style.display = 'none';
            hybrix.view.pop();
          },
          console.error); // TODO better error
      },500);
    };
    if (parameters.hasVirtual) {
      BUTTON_virt.style.display = 'block';
      BUTTON_virt.removeAttribute('disabled');
      BUTTON_virt.onclick = () => hybrix.view.open('allocateManageVirt', {toSymbol,fromSymbol,createNew:false});
    } else {
      BUTTON_virt.style.display = 'none';
      BUTTON_virt.setAttribute('disabled', true);
    }
    BUTTON_withdraw.onclick = () => {
      hybrix.view.open('transaction', {
        text: 'This transaction will withdraw funds from your allocation back to your main wallet.',
        fromOffset: DEFAULT_ALLOCATION_OFFSET,
        toOffset: 0,
        symbol: toSymbol
      });
    };
    BUTTON_allocate.onclick = () => {
      hybrix.view.open('transaction', {
        text: 'This transaction will allocate funds from your main wallet to make them available for autonomous swaps.',
        fromOffset: 0,
        toOffset: DEFAULT_ALLOCATION_OFFSET,
        symbol: toSymbol
      });
    };
    hybrix.lib.rout({query: '/e/swap/allocation/pair/stats', regular: false, cache: SWAP_STATS_CACHE_TIME },
      pairs => {
        if (pairs.hasOwnProperty(pair)) {
          DIV_feeRange.innerHTML = pairs[pair].fee;
        } else {
          DIV_feeRange.innerHTML = 'N/A';
        }
      },
      error => {
        console.error(error);
        DIV_feeRange.innerHTML = 'N/A';
      }
    );
    spinner.apply(DIV_mainBalance);
    addEventListener('refreshAsset:' + toSymbol, () => {
      spinner.remove(DIV_mainBalance);
      const balance = getBalance(toSymbol);
      DIV_mainBalance.innerHTML = prettyPrintAmount(balance, toSymbol, false, true);
      if (balance > 0) BUTTON_allocate.removeAttribute('disabled');
      else BUTTON_allocate.setAttribute('disabled', true);
    }, 'allocateManagePair');
    spinner.apply(DIV_allocationBalance);
    getPair(fromSymbol, toSymbol, renderPair, error => {
      spinner.remove(DIV_allocationBalance);
      DIV_message.innerHTML = 'Error Failed to retrieve pair data!';
      console.error(error);
    });
    addEventListener('refreshAsset:' + toSymbol + '#' + DEFAULT_ALLOCATION_OFFSET, () => {
      spinner.remove(DIV_allocationBalance);
      const balance = getBalance(toSymbol + '#' + DEFAULT_ALLOCATION_OFFSET);
      DIV_allocationBalance.innerHTML = prettyPrintAmount(balance, toSymbol);
      if (balance > 0) {
        BUTTON_withdraw.removeAttribute('disabled');
      } else {
        BUTTON_withdraw.setAttribute('disabled', true);
      }
      setDeleteButton(balance,fromSymbol,toSymbol);
    }, 'allocateManagePair');
    if(parameters.hasVirtual) {
      DIV_rate.innerHTML = `1 ${fromSymbol.toUpperCase()} = 1 ${toSymbol.toUpperCase()}`;
    } else {
      hybrix.lib.getValuation({fromSymbol, toSymbol}, rate => {
        DIV_rate.innerHTML = `${prettyPrintAmount(1, fromSymbol, false, true)} = ${prettyPrintAmount(rate, toSymbol, false, true)}`;
      }, error => {
        DIV_rate.innerHTML = 'N/A';
        console.error(error);
      });
    }
  } else {
    hybrix.view.pop();
    hybrix.view.open('allocatePreparePair', {type:'crypto'});
  }
}

exports.open = parameters => {
  setTimeout(render(parameters), 1000);  // give UX time to display
}
