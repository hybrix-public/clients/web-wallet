const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const {getIcon} = require('../../lib/icon');
const {getBalance, hasSufficientFuel} = require('../../lib/assets');
const VIRTCHAR = '~';

exports.rout = parameters => {
  if (typeof parameters !== 'object' || parameters === null) {
    hybrix.view.swap('listAssets');
    hybrix.view.open('swapAsset', {});
  } else if (parameters.hasOwnProperty('moveSymbol') && parameters.hasOwnProperty('fromSymbol')) {
    const {fromSymbol} = parameters;
    const symbol = fromSymbol.split(',')[0];
    hybrix.view.swap('viewAsset', {symbol});
    hybrix.view.open('swapAsset', parameters);
  } else if (parameters.hasOwnProperty('moveSymbol')) {
    const {moveSymbol} = parameters;
    const symbol = moveSymbol.split(',')[0];
    hybrix.view.swap('viewAsset', {symbol});
    hybrix.view.open('swapAsset', parameters);
  } else if (parameters.hasOwnProperty('fromSymbol') && parameters.hasOwnProperty('toSymbol') && parameters.hasOwnProperty('amount')) {
    const {fromSymbol} = parameters;
    const symbol = fromSymbol.split(',')[0];
    hybrix.view.swap('viewAsset', {symbol});
    hybrix.view.open('swapReview', parameters);
  } else if (
    parameters.hasOwnProperty('fromSymbol') &&
    parameters.hasOwnProperty('toSymbol') &&
    (parameters.fromSymbol.includes(',') || parameters.toSymbol.includes(','))
  ) {
    const {fromSymbol} = parameters;
    const symbol = fromSymbol.split(',')[0];
    hybrix.view.swap('viewAsset', {symbol});
    hybrix.view.open('swapAsset', parameters);
  } else if (parameters.hasOwnProperty('fromSymbol') && parameters.hasOwnProperty('toSymbol')
  ) {
    const {fromSymbol} = parameters;
    const symbol = fromSymbol.split(',')[0];
    hybrix.view.swap('viewAsset', {symbol});
    hybrix.view.open('swapAmount', parameters);
  } else if (parameters.hasOwnProperty('fromSymbol')) {
    const {fromSymbol} = parameters;
    const symbol = fromSymbol.split(',')[0];
    hybrix.view.swap('viewAsset', {symbol});
    hybrix.view.open('swapAsset', parameters);
  } else if (parameters.hasOwnProperty('toSymbol')) {
    const {toSymbol} = parameters;
    const symbol = toSymbol.split(',')[0];
    hybrix.view.swap('viewAsset', {symbol});
    hybrix.view.open('swapAsset', parameters);
  } else if (parameters.hasOwnProperty('symbol')) {
    const symbol = parameters.symbol.split(',')[0];
    hybrix.view.swap('viewAsset', {symbol});
    hybrix.view.open('swapStart', parameters);
  } else {
    hybrix.view.swap('listAssets');
    hybrix.view.open('swapAsset', {});
  }
};

const unique = (value, index, self) => {
  return self.indexOf(value) === index;
};

const renderUnifiedAsset = parameters => asset => {
  hybrix.lib.rout({query: '/e/swap/deal/pairs', regular: false}, renderAvailablePairs(parameters, asset), console.error);
};

function filterPairFrom (pair, symbolList) {
  const symbols = symbolList.split(',');
  for (const symbol of symbols) {
    if (pair.indexOf(VIRTCHAR)===-1 && pair.startsWith(symbol + ':')) return true;
  }
  return false;
}

function filterPairTo (pair, symbolList) {
  const symbols = symbolList.split(',');
  for (const symbol of symbols) {
    if (pair.indexOf(VIRTCHAR)===-1 && pair.endsWith(':' + symbol)) return true;
  }
  return false;
}

const renderAvailablePairs = (parameters, asset) => pairs => {
  // TODO pairs = ['eth.hy:tomo.hy', 'tomo.hy:eth.hy'];
  if (parameters.hasOwnProperty('moveSymbol')) {
    asset = Object.values(asset)[0];
    if (typeof asset.symbols !== 'object' || asset.symbols === null) {
      // TODO provide UI feedback
      console.error('Not a unifed asset: ' + parameters.moveSymbol);
      return;
    }

    const symbols = Object.keys(asset.symbols);
    if (parameters.hasOwnProperty('fromSymbol')) {
      pairs = pairs.filter(pair => filterPairFrom(pair, parameters.fromSymbol) && symbols.includes(pair.split(':')[1]));
    } else {
      pairs = pairs.filter(pair => symbols.includes(pair.split(':')[0]) && symbols.includes(pair.split(':')[1]));
    }
    //
  } else if (parameters.hasOwnProperty('fromSymbol') && parameters.hasOwnProperty('toSymbol')) {
    pairs = pairs.filter(pair => filterPairFrom(pair, parameters.fromSymbol) && filterPairTo(pair, parameters.toSymbol));
  } else if (parameters.hasOwnProperty('fromSymbol')) {
    pairs = pairs.filter(pair => filterPairFrom(pair, parameters.fromSymbol));
  } else if (parameters.hasOwnProperty('toSymbol')) {
    pairs = pairs.filter(pair => filterPairTo(pair, parameters.toSymbol));
  } else if (parameters.hasOwnProperty('symbol')) {
    pairs = pairs.filter(pair => filterPairFrom(pair, parameters.symbol) || filterPairTo(pair, parameters.symbol));
  }

  const DIV_spinner = document.getElementById('swapAsset-spinner');
  const DIV_message = document.getElementById('swapAsset-message');
  const SPAN_action = document.getElementById('swapAsset-action');
  const BUTTON_back = document.getElementById('swapAsset-back');
  const BUTTON_next = document.getElementById('swapAsset-next');
  const TD_from = document.getElementById('swapAsset-from');
  const TD_to = document.getElementById('swapAsset-to');

  let translatedAction = parameters.hasOwnProperty('action') && parameters.action ? parameters.action : '{{SWAPASSET:ACTION_TRANSFER}}';
  switch (translatedAction) {
    case 'buy': translatedAction='{{SWAPASSET:ACTION_BUY}}'; break;
    case 'sell': translatedAction='{{SWAPASSET:ACTION_SELL}}'; break;
    case 'move': translatedAction='{{SWAPASSET:ACTION_MOVE}}'; break;
    case 'fuel': translatedAction='{{SWAPASSET:ACTION_FUEL}}'; break;
  }
  SPAN_action.innerText = translatedAction;

  if (pairs.length === 0) {
    let message;
    if (parameters.fromSymbol) message = `{{SWAPASSET:WARN_NO_PAIRS_A}}${parameters.fromSymbol}:*{{SWAPASSET:WARN_NO_PAIRS_B}}`;
    else if (parameters.toSymbol) message = `{{SWAPASSET:WARN_NO_PAIRS_A}}*:${parameters.toSymbol}{{SWAPASSET:WARN_NO_PAIRS_B}}`;
    else message = '{{SWAPASSET:WARN_NO_PAIRS_C}}';
    TD_from.innerHTML = message;
    TD_to.innerHTML = '-';
  } else {
    const prefixToTop = 'hy'; // this sorts HY assets to the top
    const fromSymbols =
      pairs.map(pair => pair.split(':')[0])
      .filter(unique)
      .sort((a, b) => a.localeCompare(b))
      .sort((a, b) => { if (a.indexOf(prefixToTop) === -1) return -1; else return 1; })
      .sort((a, b) => { if (b.indexOf(prefixToTop) === -1) return -1; else return 1; });
    const toSymbols =
      pairs.map(pair => pair.split(':')[1])
      .filter(unique)
      .sort((a, b) => a.localeCompare(b))
      .sort((a, b) => { if (a.indexOf(prefixToTop) === -1) return -1; else return 1; })
      .sort((a, b) => { if (b.indexOf(prefixToTop) === -1) return -1; else return 1; });
    TD_to.innerHTML = '';
    TD_from.innerHTML = '';
    let selectedFromSymbol = parameters.fromSymbol;
    let selectedToSymbol = parameters.ToSymbol;
    const toggleNextButton = () => {
      if (selectedFromSymbol && selectedToSymbol) BUTTON_next.classList.remove('disabled');
      else BUTTON_next.classList.add('disabled');
    };

    for (const fromSymbol of fromSymbols) {
      const DIV = document.createElement('DIV');
      DIV.symbol = fromSymbol;
      const SPAN_icon = document.createElement('SPAN');
      const DIV_symbolAndBalance = document.createElement('DIV');
      const SPAN_symbol = document.createElement('SPAN');
      SPAN_symbol.innerHTML = fromSymbol +
      (hasSufficientFuel(fromSymbol) === false
        ? '<img src="./svg/fuel-red.svg" width="30px" style="vertical-align: middle;padding-left: 5px;width: 25px;"/>'
        : ''
      );

      SPAN_icon.innerHTML = getIcon(fromSymbol, icon => { SPAN_icon.innerHTML = icon; }); // get fallback icon direclty, update real icon later
      DIV.appendChild(SPAN_icon);
      DIV_symbolAndBalance.appendChild(SPAN_symbol);
      DIV_symbolAndBalance.style.display = 'inline-block';
      DIV_symbolAndBalance.style.verticalAlign = 'middle';
      const balance = getBalance(fromSymbol);
      const SPAN_balance = document.createElement('SPAN');
      SPAN_balance.innerHTML = isNaN(balance)?'':prettyPrintAmount(balance, null, false, true);
      SPAN_balance.style.display = 'block';
      SPAN_balance.style.fontSize = '10px';
      SPAN_balance.style.marginTop = '-8px';
      DIV_symbolAndBalance.appendChild(SPAN_balance);
      DIV.appendChild(DIV_symbolAndBalance);

      if (parameters.fromSymbol === fromSymbol ||
        (fromSymbols.length === 1 && hasSufficientFuel !== false) ||
        (!parameters.hasOwnProperty('fromSymbol') && fromSymbol === 'hy' && hasSufficientFuel !== false)
      ) {
        selectedFromSymbol = fromSymbol;
        DIV.classList.add('swapAsset-selected');
      }
      DIV.onclick = () => {
        let count = 0;
        for (const DIV_to of TD_to.children) {
          const pair = fromSymbol + ':' + DIV_to.symbol;
          if (!pairs.includes(pair)) {
            DIV_to.classList.add('swapAsset-disabled');
            if (DIV_to.symbol === selectedToSymbol) { // unselect selected to symbol if there's no matching pair
              DIV_to.classList.remove('swapAsset-selected');
              selectedToSymbol = undefined;
            }
          } else {
            DIV_to.classList.remove('swapAsset-disabled');
            ++count;
          }
        }
        if (count === 1) { // auto select if only one target available
          for (const DIV_to of TD_to.children) {
            const pair = fromSymbol + ':' + DIV_to.symbol;
            if (pairs.includes(pair)) {
              selectedToSymbol = DIV_to.symbol;
              DIV_to.classList.add('swapAsset-selected');
              break;
            }
          }
        }

        for (const DIV_other of TD_from.children) {
          DIV_other.classList.remove('swapAsset-disabled');
          if (DIV === DIV_other) DIV.classList.add('swapAsset-selected');
          else DIV_other.classList.remove('swapAsset-selected');
          selectedFromSymbol = fromSymbol;
        }
        toggleNextButton();
      };
      TD_from.appendChild(DIV);
    }
    for (const toSymbol of toSymbols) {
      const DIV = document.createElement('DIV');
      DIV.symbol = toSymbol;
      const SPAN_icon = document.createElement('SPAN');
      const SPAN_symbol = document.createElement('SPAN');
      SPAN_symbol.innerText = toSymbol;
      SPAN_icon.innerHTML = getIcon(toSymbol, icon => { SPAN_icon.innerHTML = icon; }); // get fallback icon directly, update real icon later
      DIV.appendChild(SPAN_icon);
      DIV.appendChild(SPAN_symbol);

      if (parameters.toSymbol === toSymbol || toSymbols.length === 1) {
        selectedToSymbol = toSymbol;
        DIV.classList.add('swapAsset-selected');
      }

      DIV.onclick = () => {
        let count = 0;
        for (const DIV_from of TD_from.children) {
          const pair = DIV_from.symbol + ':' + toSymbol;
          if (!pairs.includes(pair)) {
            DIV_from.classList.add('swapAsset-disabled');
            if (DIV_from.symbol === selectedFromSymbol) { // unselect selected from symbol if there's no matching pair
              DIV_from.classList.remove('swapAsset-selected');
              selectedFromSymbol = undefined;
            }
          } else {
            DIV_from.classList.remove('swapAsset-disabled');
            ++count;
          }
        }
        if (count === 1) { // auto select if only one target available
          for (const DIV_from of TD_from.children) {
            const pair = DIV_from.symbol + ':' + toSymbol;
            if (pairs.includes(pair)) {
              selectedFromSymbol = DIV_from.symbol;
              DIV_from.classList.add('swapAsset-selected');
              break;
            }
          }
        }
        for (const DIV_other of TD_to.children) {
          DIV_other.classList.remove('swapAsset-disabled');
          if (DIV === DIV_other) DIV.classList.add('swapAsset-selected');
          else DIV_other.classList.remove('swapAsset-selected');
          selectedToSymbol = toSymbol;
        }
        toggleNextButton();
      };
      TD_to.appendChild(DIV);
    }
    toggleNextButton();
    BUTTON_next.onclick = () => {
      if (!BUTTON_next.classList.contains('disabled')) { // make sure user cannot mash button while rout call is ongoing
        BUTTON_back.classList.add('disabled');
        BUTTON_next.classList.add('disabled');
        DIV_spinner.style.display = 'block';
        // if an amount has already been given (like from Voting app modal), skip the swapAmount modal
        if (parameters.hasOwnProperty('amount')) {
          hybrix.lib.getBalance({symbol:selectedFromSymbol}, balance => {
            parameters.fromSymbol = selectedFromSymbol;
            BUTTON_back.classList.remove('disabled');
            BUTTON_next.classList.remove('disabled');
            DIV_spinner.style.display = 'none';
            hybrix.view.open('swapReview', {...parameters, balance});
          },
          error => {
            console.error(error);
            BUTTON_back.classList.remove('disabled');
            BUTTON_next.classList.remove('disabled');
            DIV_spinner.style.display = 'none';
            hybrix.view.closeAll();
            hybrix.view.open('swapAsset', parameters);
            DIV_message.innerHTML = `{{SWAPASSET:ERROR_GET_BALANCE}}<br>${error}`;
          });
        } else {
          hybrix.lib.rout({query: `/e/swap/deal/estimate/${selectedFromSymbol}/${selectedToSymbol}`, regular: false},
            estimate => {
              BUTTON_back.classList.remove('disabled');
              BUTTON_next.classList.remove('disabled');
              DIV_spinner.style.display = 'none';
              hybrix.view.open('swapAmount', {action: parameters.action, fuelForSymbol: parameters.fuelForSymbol, fromSymbol: selectedFromSymbol, toSymbol: selectedToSymbol, estimate});
            },
            error => {
              console.error(error);
              BUTTON_back.classList.remove('disabled');
              BUTTON_next.classList.remove('disabled');
              DIV_spinner.style.display = 'none';
              hybrix.view.closeAll();
              hybrix.view.open('swapAsset', parameters);
              DIV_message.innerHTML = `{{SWAPASSET:ERROR_GET_ESTIMATE}}<br>${error}`;
            });
        }
      }
    };
  }
};

exports.open = function (parameters) {
  const spinnerHtml = '<div style="text-align: center;"><div class="spinner-loader">...</div></div>';
  const TD_from = document.getElementById('swapAsset-from');
  const TD_to = document.getElementById('swapAsset-to');

  const BUTTON_cancel = document.getElementById('swapAsset-cancel');
  const BUTTON_back = document.getElementById('swapAsset-back');
  const BUTTON_next = document.getElementById('swapAsset-next');
  const DIV_message = document.getElementById('swapAsset-message');
  const DIV_spinner = document.getElementById('swapAsset-spinner');
  DIV_spinner.style.display = 'none';

  const DIV_progress = document.getElementById('swapAsset-progress');
  progress(DIV_progress, {
    '{{SWAPASSET:PROGRESS_DETAILS}}': null,
    '{{SWAPASSET:PROGRESS_AMOUNT}}': null,
    '{{SWAPASSET:PROGRESS_REVIEW}}': null,
    '{{SWAPASSET:PROGRESS_SWAP}}': null
  }, 0);

  DIV_message.innerHTML = '';
  TD_from.innerHTML = spinnerHtml;
  TD_to.innerHTML = spinnerHtml;

  BUTTON_back.classList.remove('disabled');
  BUTTON_next.classList.add('disabled');
  BUTTON_cancel.onclick = () => hybrix.view.closeAll();
  BUTTON_back.onclick = () => {
    if (!BUTTON_back.classList.contains('disabled')) { // make sure user cannot mash button while rout call is ongoing
      hybrix.view.pop();
    }
  };

  if (parameters.hasOwnProperty('moveSymbol')) {
    const symbol = parameters.moveSymbol;
    hybrix.lib.asset({symbol}, renderUnifiedAsset(parameters), console.error); // TODO
  } else {
    hybrix.lib.rout({query: '/e/swap/deal/pairs', regular: false}, renderAvailablePairs(parameters), console.error); // TODO
  }
};
