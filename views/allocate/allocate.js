const hybrix = require('../../lib/hybrix').hybrix;
const spinner = require('../../lib/spinner');
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const {addAsset, addEventListener, getAsset, getBalance} = require('../../lib/assets');
const VIRTCHAR = '~';

const MINIMAL_DEPOSIT = 10;
const DEFAULT_ALLOCATION_OFFSET = 100;

exports.getSignatureSteps = getSignatureSteps;
exports.refreshPairs = refreshPairs;
exports.DEFAULT_ALLOCATION_OFFSET = DEFAULT_ALLOCATION_OFFSET;
exports.VIRTCHAR = VIRTCHAR;

function stringify (x) {
  return typeof x === 'undefined' ? 'undefined' : x;
}
function getSignature (qrtzMethodId, accountID, parameters, secretKey) {
  return [qrtzMethodId, accountID, ...parameters, secretKey].map(stringify).join('-');
}
// Nb: the qrtzMethodId is the qrtz method name so 'deletePair' and not 'pairDelete' or 'pair/delete'
function getSignatureSteps (qrtzMethodId, parameters) {
  return [
    {offset: DEFAULT_ALLOCATION_OFFSET}, 'getLoginKeyPair',
    {
      keys: [keys => keys],
      accountID: [
        keys => ({data: 'account ' + keys.secretKey}), 'hash',
        hash => ({data: hash, source: 'hex', target: 'base58'}), 'code'
      ]
    },
    'parallel',
    {
      accountID: [({accountID}) => accountID],
      signature: [({keys, accountID}) => getSignature(qrtzMethodId, accountID, parameters, keys.secretKey), 'hash']
    },
    'parallel'
  ];
}

exports.rout = parameters => hybrix.view.swap('allocate', parameters);

function accountDetails (dataCallback, errorCallback) {
  hybrix.lib.sequential([
    ...getSignatureSteps('detailsAccount', []),
    ({accountID, signature}) => ({query: '/e/swap/allocation/account/details/' + accountID + '/' + signature, regular: false}), 'rout'
  ], dataCallback, errorCallback);
}

function getPairs (dataCallback, errorCallback) {
  hybrix.lib.sequential([
    ...getSignatureSteps('detailsAccount', []),
    ({accountID}) => ({query: '/e/swap/allocation/pair/list/' + accountID, regular: false}), 'rout'
  ], dataCallback, errorCallback);
}

function create (dataCallback, errorCallback) {
  hybrix.lib.sequential([
    {offset: DEFAULT_ALLOCATION_OFFSET}, 'getLoginKeyPair',
    keys => ({
      secretKey: {data: keys.secretKey, step: 'id'},
      accountId: [{data: 'account ' + keys.secretKey}, 'hash', hash => ({data: hash, source: 'hex', target: 'base58'}), 'code']
    }), 'parallel',
    data => ({query: '/e/swap/allocation/account/init/' + data.accountId + '/' + data.secretKey, chan: 'y', regular: false}), 'rout'
  ], dataCallback, errorCallback);
}

function getBalances (symbol) {
  const SPAN_mainBalance = document.getElementById('allocate-mainBalance');
  const SPAN_allocationBalance = document.getElementById('allocate-allocationBalance');
  const SPAN_securityBalance = document.getElementById('allocate-securityBalance');
  const BUTTON_transfer = document.getElementById('allocate-transfer');
  const BUTTON_securityReserve = document.getElementById('allocate-securityReserve');
  const BUTTON_withdraw = document.getElementById('allocate-withdraw');
  BUTTON_withdraw.setAttribute('disabled', true);
  BUTTON_transfer.setAttribute('disabled', true);
  BUTTON_securityReserve.setAttribute('disabled', true);

  addEventListener('refreshAsset:' + symbol, () => {
    spinner.remove(SPAN_mainBalance);
    const asset = getAsset(symbol);
    const balance = getBalance(symbol);
    SPAN_mainBalance.innerHTML = prettyPrintAmount(balance, symbol.replace('tomo.',''), true);
    if (balance > 0) BUTTON_transfer.removeAttribute('disabled');
    if (asset.sufficientFuel === false && balance !== 'n/a' && Number(balance) > 0) {
      console.log(` >>> insufficient fuel for main balance ${symbol} !!!`);
    }
  }, 'allocate');
  addAsset(symbol, () => {}, console.error);

  const tagAllocation = symbol + '#' + DEFAULT_ALLOCATION_OFFSET;
  addEventListener('refreshAsset:' + tagAllocation, () => {
    spinner.remove(SPAN_allocationBalance);
    const asset = getAsset(symbol);
    const balance = getBalance(tagAllocation);
    document.getElementById('allocate-allocationBalanceNumber').innerText = balance;
    SPAN_allocationBalance.innerHTML = prettyPrintAmount(balance, symbol.replace('tomo.',''), true);
    if (balance > 0) {
      if (SPAN_securityBalance.innerHTML !== 'N/A') BUTTON_securityReserve.removeAttribute('disabled');
      BUTTON_withdraw.removeAttribute('disabled');
    }
    if (asset.sufficientFuel === false && balance !== 'n/a' && Number(balance) > 0) {
      console.log(` >>> insufficient fuel for allocation balance ${symbol} !!!`);
    }
  }, 'allocate');
  addAsset(tagAllocation, () => {}, console.error);
}

function refreshAccountDetails () {
  const DIV_message = document.getElementById('allocate-message');
  const DIV_allocateTransferButtons = document.getElementById('allocate-transfer-buttons');
  const DIV_allocateSecurityButtons = document.getElementById('allocate-security-buttons');
  const BUTTON_withdraw = document.getElementById('allocate-withdraw');
  const BUTTON_transfer = document.getElementById('allocate-transfer');
  const BUTTON_securityReserve = document.getElementById('allocate-securityReserve');
  const BUTTON_securityExtract = document.getElementById('allocate-securityExtract');
  const BUTTON_createAccount = document.getElementById('allocate-createAccount');
  const BUTTON_createPair = document.getElementById('allocate-createPair');
  const SPAN_mainBalance = document.getElementById('allocate-mainBalance');
  const SPAN_allocationBalance = document.getElementById('allocate-allocationBalance');
  const SPAN_securityBalance = document.getElementById('allocate-securityBalance');
  const SPAN_allocationBalanceContainer = document.getElementById('allocate-allocationBalanceContainer');
  const SPAN_securityBalanceContainer = document.getElementById('allocate-securityBalanceContainer');
  const BUTTON_allocateReportingEdit = document.getElementById('allocate-reporting-edit');
  const DIV_allocateReportingTarget = document.getElementById('allocate-reporting-target');
  const DIV_pairs = document.getElementById('allocate-pairs');
  const P_error = document.getElementById('allocate-error-message');
  DIV_pairs.innerHTML = '';
  DIV_pairs.style.display = 'none';
  BUTTON_createAccount.style.display = 'none';
  BUTTON_securityReserve.setAttribute('disabled', true);
  BUTTON_securityExtract.setAttribute('disabled', true);
  BUTTON_createPair.setAttribute('disabled', true);
  BUTTON_withdraw.setAttribute('disabled', true);
  BUTTON_transfer.setAttribute('disabled', true);
  DIV_message.innerHTML = '';
  spinner.apply(SPAN_mainBalance);
  spinner.apply(SPAN_allocationBalance);
  spinner.apply(SPAN_securityBalance);

  const transferSymbol = 'tomo.hy'; // TODO: override until we have created hyde
  const symbol = 'hy'; // TODO: override until we have created hyde
  accountDetails(details => {
    getBalances(transferSymbol);
    if (typeof details.securityReserve !== 'undefined') {
      DIV_allocateReportingTarget.innerText = typeof details.contact !== 'undefined' && details.contact !== 'undefined' && details.contact?details.contact:'N/A';

      const reportingEditSubmit = (target) => {
        hybrix.lib.sequential([
          ...getSignatureSteps('setContactAccount', [target]),
          ({accountID, signature}) => ({query: '/e/swap/allocation/account/setContact/' + accountID + '/' + target + '/' + signature, regular: false}), 'rout'
        ], (result) => {
          DIV_allocateReportingTarget.innerText = typeof target !== 'undefined' && target ? target : 'N/A';
          hybrix.view.pop();
          hybrix.view.open('alert', {icon: 'checkmark', title: 'Success', text: 'A test e-mail has been sent to: ' + target, textMore: 'Please check to make sure that you have received it!'});
        }, (e) => {
          DIV_allocateReportingTarget.innerText = 'N/A';
          hybrix.view.pop();
          hybrix.view.open('alert', {icon: 'cross', title: 'Error', text: 'Something went wrong when saving the contact information. <br> Error: ' + e});
        });
      }

      BUTTON_allocateReportingEdit.onclick = () => hybrix.view.open('input',
        {
          title:'Reporting Email',
          text:'Enter a valid email address where you can receive notification messages.',
          textMore:'You will receive emails when swaps on your allocation are initiated or disputes are raised.',
          textPending:'Updating contact email address...',
          placeholder:'someone@email.domain',
          pattern:'^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$',
          fieldValue:DIV_allocateReportingTarget.innerText?DIV_allocateReportingTarget.innerText:'',
          callback: reportingEditSubmit
        });

      spinner.remove(SPAN_securityBalance);
      if (details.securityReserve.balance > MINIMAL_DEPOSIT) {
        BUTTON_createPair.removeAttribute('disabled');
      } else {
        DIV_message.innerHTML = `A minimal scurity deposit reserve of ${prettyPrintAmount(MINIMAL_DEPOSIT, symbol)} is required!`;
      }
      if (details.securityReserve.balance > 0 && details.securityReserve.balance > details.securityReserve.locked) BUTTON_securityExtract.removeAttribute('disabled');
      DIV_pairs.style.display = 'block';

      let html = prettyPrintAmount(details.securityReserve.balance, symbol, true);
      if (details.securityReserve.locked > 0) html += `&nbsp;&nbsp;&nbsp;<small>(${prettyPrintAmount(details.securityReserve.locked, symbol, true)} locked)</small>`;
      SPAN_securityBalance.innerHTML = html;
      SPAN_securityBalanceContainer.removeAttribute('disabled');
      SPAN_allocationBalanceContainer.removeAttribute('disabled');

      BUTTON_securityReserve.onclick = () => {
        const balance = document.getElementById('allocate-allocationBalanceNumber').innerText || 0;
        hybrix.view.open('allocateReserve', {action: 'reserve', symbol, balance, min: MINIMAL_DEPOSIT, details:details.securityReserve, callback:refreshAccountDetails});
      };

      BUTTON_securityExtract.onclick = () => {
        hybrix.view.open('allocateReserve', {action: 'extract', symbol, details:details.securityReserve, callback:refreshAccountDetails});
      };

      BUTTON_withdraw.onclick = () => {
        hybrix.view.open('transaction', {
          text: 'When submitting this transaction you will withdraw funds from your allocation back to your wallet main balance.',
          fromOffset: DEFAULT_ALLOCATION_OFFSET,
          toOffset: 0,
          symbol: transferSymbol,
        });
      };

      BUTTON_transfer.onclick = () => {
        hybrix.view.open('transaction', {
          text: 'When submitting this transaction you will transfer funds from your main wallet to your allocation balance.',
          fromOffset: 0,
          toOffset: DEFAULT_ALLOCATION_OFFSET,
          symbol: transferSymbol,
        });
      };
    } else {
      getBalances(transferSymbol);
      spinner.remove(SPAN_securityBalance);
      SPAN_securityBalanceContainer.setAttribute('disabled', true);
      SPAN_allocationBalanceContainer.setAttribute('disabled', true);
      BUTTON_withdraw.removeAttribute('disabled');
      DIV_allocateReportingTarget.innerText = 'N/A';
      SPAN_securityBalance.innerText = 'N/A';
      P_error.innerHTML = 'ERROR: CANNOT DETERMINE SECURITY BALANCE!<br>PLEASE LOGOUT AND TRY AGAIN.';
    }
  }, error => {
    getBalances(transferSymbol);
    spinner.remove(SPAN_securityBalance);
    SPAN_securityBalance.innerText = 'N/A';
    DIV_allocateReportingTarget.innerText = 'N/A';
    if (error === 'Account does not exist!') {
      BUTTON_createAccount.style.display = 'block';
      DIV_allocateTransferButtons.style.height = '0px';
      DIV_allocateTransferButtons.style.opacity = 0;
      DIV_allocateSecurityButtons.style.height = '0px';
      DIV_allocateSecurityButtons.style.opacity = 0;
      SPAN_securityBalanceContainer.setAttribute('disabled', true);
      SPAN_allocationBalanceContainer.setAttribute('disabled', true);
      setTimeout( () => {
        BUTTON_createAccount.removeAttribute('disabled');
        hybrix.lib.rout({query: '/e/swap/allocation/security-symbol', chan: 'y', regular: false, cache: Infinity}, symbol => {
          SPAN_securityBalance.innerHTML = prettyPrintAmount(0, symbol.replace('tomo.',''), true, true);
        }, console.error);
      }, 3000);
    } else {
      P_error.innerHTML = 'ERROR: CANNOT RETRIEVE SECURITY BALANCE!<br>PLEASE LOGOUT AND TRY AGAIN.';
    }
  });

  refreshPairs();
}

function refreshPairs () {
  const DIV_pairs = document.getElementById('allocate-pairs');
  const BUTTON_createPair = document.getElementById('allocate-createPair');
  spinner.apply(DIV_pairs);
  getPairs(pairs_ => {
    spinner.remove(DIV_pairs);
    BUTTON_createPair.removeAttribute('disabled');
    DIV_pairs.innerHTML = '';
    if (pairs_.length === 0) {
      DIV_pairs.innerHTML = 'No allocation pairs are available!';
    }
    for (const pair of pairs_) {
      const DIV_pair = document.createElement('DIV');
      if(pair.indexOf(VIRTCHAR)>-1) {
        DIV_pair.className = 'allocate-pair allocate-fiat pure-button pure-button-large pure-button-light';
        if(pair.split(':')[1].charAt(0)===VIRTCHAR) {
          DIV_pair.onclick = () => hybrix.view.open('allocateManageVirt', {pair});
        } else {
          const hasVirtual = true;
          DIV_pair.onclick = () => hybrix.view.open('allocateManagePair', {pair,hasVirtual});
        }
      } else {
        DIV_pair.className = 'allocate-pair allocate-crypto pure-button pure-button-large pure-button-light';
        DIV_pair.onclick = () => hybrix.view.open('allocateManagePair', {pair});
      }
      DIV_pair.setAttribute('role', 'button');
      DIV_pair.setAttribute('data-toggle', 'modal');
      DIV_pair.innerHTML = pair.toUpperCase().replace(":", " ⊳ ");
      DIV_pairs.appendChild(DIV_pair);
    }
  }, error => {
    spinner.remove(DIV_pairs);
    DIV_pairs.innerText = 'Failed to retrieve pairs:' + error;
  });
}

exports.open = () => {
  const BUTTON_back = document.getElementById('allocate-back-button');
  const BUTTON_pending = document.getElementById('allocate-pending-button');
  const BUTTON_apps = document.getElementById('allocate-apps-button');
  BUTTON_back.onclick = () => hybrix.view.swap('listAssets');
  BUTTON_pending.onclick = () => hybrix.view.open('pending');
  BUTTON_apps.onclick = () => hybrix.view.open('appsList');

  const SPAN_mainBalance = document.getElementById('allocate-mainBalance');
  const SPAN_allocationBalance = document.getElementById('allocate-allocationBalance');
  const SPAN_securityBalance = document.getElementById('allocate-securityBalance');
  SPAN_mainBalance.innerHTML = '';
  SPAN_allocationBalance.innerHTML = '';
  SPAN_securityBalance.innerHTML = '';
  spinner.apply(SPAN_mainBalance);
  spinner.apply(SPAN_allocationBalance);
  spinner.apply(SPAN_securityBalance);

  const BUTTON_createAccount = document.getElementById('allocate-createAccount');
  const BUTTON_createPair = document.getElementById('allocate-createPair');
  const DIV_message = document.getElementById('allocate-message');

  const DIV_allocateTransferButtons = document.getElementById('allocate-transfer-buttons');
  const DIV_allocateSecurityButtons = document.getElementById('allocate-security-buttons');

  DIV_message.innerHTML = '';
  BUTTON_createPair.onclick = () => hybrix.view.open('allocateStart', {});

  BUTTON_createAccount.onclick = () => {
    const buttonText = BUTTON_createAccount.innerHTML;
    spinner.apply(BUTTON_createAccount);
    setTimeout( () => {  // give UX/UI chance to update
      create(() => {
        spinner.remove(BUTTON_createAccount);
        BUTTON_createAccount.innerHTML = buttonText;
        DIV_allocateTransferButtons.style.height = '120px';
        DIV_allocateTransferButtons.style.opacity = 1;
        DIV_allocateSecurityButtons.style.height = '120px';
        DIV_allocateSecurityButtons.style.opacity = 1;
        refreshAccountDetails();
      }, error => {
        spinner.remove(BUTTON_createAccount);
        BUTTON_createAccount.innerHTML = buttonText;
        DIV_message.innerHTML = 'Failed to create account: ' + error;
      });
    }, 1000);
  };

  setTimeout(refreshAccountDetails, 2000);  // hacky, but necessary timeout for UI to be able to draw
};
