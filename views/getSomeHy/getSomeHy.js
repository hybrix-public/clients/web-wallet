const hybrix = require('../../lib/hybrix').hybrix;

exports.rout = parameters => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('getSomeHy');
};

function open (parameters) {
  const BUTTON_banking = document.getElementById('getSomeHy-banking');
  const BUTTON_scan = document.getElementById('getSomeHy-scan');
  BUTTON_banking.onclick = () => {
    hybrix.view.pop();
    hybrix.view.open('banking');
  }
  BUTTON_scan.onclick = () => {
    hybrix.view.open('getSomeHyScan');
  }
}

exports.open = open;
