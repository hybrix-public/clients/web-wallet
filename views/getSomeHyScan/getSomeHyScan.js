const hybrix = require('../../lib/hybrix').hybrix;

exports.rout = parameters => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('getSomeHyScan');
};

function list (symbols) {
  let result = '';
  if (symbols.length > 0) {
    result=symbols.length === 1
      ? `<b>${symbols[0]}</b>`
      : `<b>${symbols.slice(1).join(', ')}</b> {{GETSOMEHYSCAN:AND}} <b>${symbols[0]}</b>`;
  }
  return result;
}

function renderMessage (balancePerSymbol) {
  const DIV_message = document.getElementById('getSomeHyScan-message');
  const BUTTON_next = document.getElementById('getSomeHyScan-next');

  const symbolsWithBalance = [];
  for (const symbol in balancePerSymbol) {
    if (balancePerSymbol[symbol] > 0) symbolsWithBalance.push(symbol);
  }
  if (symbolsWithBalance.length > 0) {
    DIV_message.innerHTML = `<p>{{GETSOMEHYSCAN:LOOKS_LIKE_A}}${list(symbolsWithBalance)}{{GETSOMEHYSCAN:LOOKS_LIKE_B}}</p>`;
    BUTTON_next.removeAttribute('disabled');
    BUTTON_next.onclick = () => hybrix.view.open('swapAsset', {action: 'receive', toSymbol: 'hy'});
  } else {
    const assetsAvailable = list(Object.keys(balancePerSymbol));
    DIV_message.innerHTML = `<p>{{GETSOMEHYSCAN:NO_ASSETS}}${assetsAvailable?' {{GETSOMEHYSCAN:SWAP_MUST_HAVE}} '+assetsAvailable:''}</p>`;
  }
}

function checkBalances (pairs) {
  const symbols = pairs
    .filter(pair => pair.endsWith(':hy'))
    .map(pair => pair.split(':')[0]);
  const balanceSteps = {};
  for (const symbol of symbols) {
    balanceSteps[symbol] = {data: {symbol}, step: 'getBalance'};
  }
  hybrix.lib.parallel(balanceSteps, renderMessage, console.error);
}

function open (parameters) {
  const DIV_message = document.getElementById('getSomeHyScan-message');
  const BUTTON_cancel = document.getElementById('getSomeHyScan-cancel');
  const BUTTON_next = document.getElementById('getSomeHyScan-next');
  BUTTON_next.setAttribute('disabled',true);
  BUTTON_cancel.onclick = () => hybrix.view.pop();
  DIV_message.innerHTML = '<p>{{GETSOMEHYSCAN:PLEASE_WAIT}}</p><div class="spinner-loader" style="margin-top: 24px;">...</div>';
  hybrix.lib.rout({query: '/e/swap/deal/pairs', regular: false}, checkBalances, console.error);
}

exports.open = open;
