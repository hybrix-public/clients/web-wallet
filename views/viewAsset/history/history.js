const hybrix = require('../../../lib/hybrix').hybrix;
const {decode} = require('../../../common/compress-unified-address');
const {createElement} = require('../../../lib/html');
const Decimal = require('../../../lib/decimal').Decimal;
const sha256 = require('js-sha256');
const {prettyPrintAmount, renderDate} = require('../../../lib/prettyPrintAmount');
const {findContact} = require('../../../lib/contacts');
const PAGE_SIZE = 12;
let pendingTransactionsPreviousLength = 0;

function displayContact (contact) {
  return `👤 ${contact.contactId}${contact.label && contact.label !== '-' ? ' ➤ ' + contact.label : ''}`;
}

// ensure that "a,b" matches "a,c" and "b,d" but not "c,f"
function matchAddress (address1, address2, symbol) {
  if (typeof address1 === 'number') address1 = address1.toString();
  if (typeof address2 === 'number') address2 = address2.toString();
  if (typeof address1 !== 'string' || typeof address2 !== 'string') return false;

  if (address1.startsWith(symbol + ':')) { // check for packed unified addresses
    const decodedAddress1 = decode(address1.split(':')[1]);
    if (decodedAddress1 !== null && matchAddress(decodedAddress1, address2, symbol)) return true;
  }
  if (address2.startsWith(symbol + ':')) { // check for packed unified addresses
    const decodedAddress2 = decode(address2.split(':')[1]);
    if (decodedAddress2 !== null && matchAddress(address1, decodedAddress2, symbol)) return true;
  }

  const subAddresses2 = address2.split(',');
  for (const subAddress1 of address1.split(',')) {
    if (subAddresses2.includes(subAddress1)) return true;
  }
  return false;
}

function renderAmount (asset, transaction) {
  const amount = typeof transaction.amount === 'undefined' ? '{{VIEWASSET:N/A}}' : transaction.amount;
  const sentToSelf = matchAddress(asset.address, transaction.source) && matchAddress(asset.address, transaction.target, asset.symbol);
  const toOrFrom = matchAddress(asset.address, transaction.target, asset.symbol) ? 'received' : 'sent';
  const feeOrDefault = isNaN(Number(transaction.fee)) ? ( isNaN(Number(asset.fee)) ? 0 : asset.fee ) : transaction.fee;

  if (sentToSelf) {
    const feeWithoutTrailingZeros = Number(feeOrDefault);
    return `- ${prettyPrintAmount(feeWithoutTrailingZeros.toString(), asset.symbol)}`;
  } else if (toOrFrom === 'received') {
    return `+ ${prettyPrintAmount(amount, asset.symbol)}`;
  } else {
    const amountWithFee = isNaN(amount) ? new Decimal(feeOrDefault) : new Decimal(amount).plus(new Decimal(feeOrDefault));
    return `- ${prettyPrintAmount(amountWithFee.toString(), asset.symbol)}`;
  }
}

function renderAddress (asset, transaction) {
  const source = !transaction.source || transaction.source === 'null' || transaction.source === 'unknown' || transaction.source === null ? '[unknown]' : transaction.source;
  const target = !transaction.target || transaction.target === 'null' || transaction.target === 'unknown' || transaction.target === null ? '[unknown]' : transaction.target;

  const sent = matchAddress(asset.address, source, asset.symbol);
  const received = matchAddress(asset.address, target, asset.symbol);

  let contact = null;
  if (sent && received) {
    return {
      addressString: `{{VIEWASSET:HISTORY_INTERNAL}} <span class="history-contact">👤 {{VIEWASSET:HISTORY_SELF}}</span>, ${source}`,
      directionString: '{{VIEWASSET:HISTORY_TO}}',
      direction: 'self'
    };
  } else if (received) {
    contact = findContact(asset.symbol, source);
    return {
      addressString: `{{VIEWASSET:HISTORY_RECEIVED}} ${contact ? '<span class="history-contact">' + displayContact(contact) + '</span>, ': ''}${source}`,
      directionString: '{{VIEWASSET:HISTORY_FROM}}',
      direction: 'from'
    };
  } else {
    contact = findContact(asset.symbol, target);
    return {
      addressString: `{{VIEWASSET:HISTORY_SENT}} ${contact ? '<span class="history-contact">' + displayContact(contact) + '</span>, ': ''}${target}`,
      directionString: '{{VIEWASSET:HISTORY_TO}}',
      direction: 'to'
    };
  }
}

function renderExplorerLink (symbol, transactionId) {
  const currency = hybrix.view.exec('settings', 'getCurrencyPreference');
  return `<a title="{{VIEWASSET:HISTORY_EXPLORE}}" target="_blank" class="viewAsset-exploreTransaction" href="https://explorer.hybrix.io/?symbol=${symbol}&transactionId=${transactionId}&currency=${currency}"></a>`;
}

const renderTransaction = (asset, transactionId, DIV) => transaction => {
  const {addressString, direction} = renderAddress(asset, transaction);
  DIV.style.cursor = 'default';
  DIV.onclick = undefined;
  DIV.getElementsByClassName('tx-history-date')[0].innerHTML = renderDate(transaction.timestamp);
  const DIV_amount = DIV.getElementsByClassName('tx-history-amount')[0];
  DIV_amount.innerHTML = renderAmount(asset, transaction);
  DIV_amount.className = `tx-history-amount ${direction}`;
  DIV.getElementsByClassName('tx-history-address')[0].innerHTML = addressString;
  DIV.getElementsByClassName('tx-history-id')[0].innerHTML = `{{VIEWASSET:HISTORY_TXID}} ${transactionId} ${renderExplorerLink(asset.symbol, transactionId)}`;
};

const failTransaction = (asset, transactionId, DIV) => error => {
  console.error(error);
  DIV.onclick = () => retrieveTransaction(asset, transactionId, DIV);
  DIV.id = `tx-history-${transactionId}`;
  DIV.style.cursor = 'pointer';
  DIV.getElementsByClassName('tx-history-date')[0].className = 'tx-history-fail';
  DIV.getElementsByClassName('tx-history-fail')[0].innerHTML = '{{VIEWASSET:HISTORY_TXDATA_UNAVAILABLE}}<br>{{VIEWASSET:HISTORY_RETRY}}';
  const DIV_amount = DIV.getElementsByClassName('tx-history-amount')[0];
  DIV_amount.innerHTML = prettyPrintAmount('{{VIEWASSET:N/A}}', asset.symbol);
  DIV_amount.className = 'tx-history-amount';
  DIV.getElementsByClassName('tx-history-address')[0].innerHTML = '{{VIEWASSET:N/A}}';
  DIV.getElementsByClassName('tx-history-id')[0].innerHTML = `{{VIEWASSET:HISTORY_TXID}} ${transactionId} ${renderExplorerLink(asset.symbol, transactionId)}`;
};

const retrieveTransaction = (asset, transactionId, DIV) => {
  const DIV_history = document.getElementById('viewAsset-history');
  let description;
  if (!DIV) {
    DIV = createElement({id: `tx-history-${transactionId}`, className: 'tx-history-entry', parentNode: DIV_history});
    description = '...';
  } else {    
    const descriptionValue = DIV.getElementsByClassName('tx-history-description')[0].value;
    description = typeof descriptionValue === 'string' ? descriptionValue : '';
  }
  DIV.onclick = undefined;
  DIV.style.cursor = 'normal';
  DIV.innerHTML = `<p class="tx-history-date">...</p>
      <p class="tx-history-amount">... ${asset.symbol.toUpperCase()}</p>
      <p class="tx-history-description">${description}</p>
      <p class="tx-history-address">...</p>
      <p class="tx-history-id">{{VIEWASSET:HISTORY_TXID}} ${transactionId} ${renderExplorerLink(asset.symbol, transactionId)}</p>`; // TODO spinner
  hybrix.lib.rout({query: '/a/' + asset.symbol + '/transaction/' + transactionId, channel: 'y'}, renderTransaction(asset, transactionId, DIV), failTransaction(asset, transactionId, DIV));
  removePendingTransaction(transactionId); // if it's in history, do not show in double in pending
};

const renderHistory = (asset, page) => (history) => {
  const DIV_history = document.getElementById('viewAsset-history');
  if (page === 0) DIV_history.innerHTML = ''; // reset
  else { // remove previous load more button
    const DIV_more = document.getElementById('viewAsset-history-more');
    if (DIV_more) DIV_more.parentNode.removeChild(DIV_more);
  }

  // load and render transactions
  for (const transactionId of history) retrieveTransaction(asset, transactionId);

  // load and render descriptions
  for (const prefixedTransactionID of history) {
    const transactionArray = prefixedTransactionID.split(':');
    const transactionID = transactionArray.length > 1 ? transactionArray[1] : transactionArray[0];
    const hashedTransactionID = sha256.sha256(transactionID);
    const P_description = document.getElementById(`tx-history-${prefixedTransactionID}`).getElementsByClassName('tx-history-description')[0];
    hybrix.lib.load({key:hashedTransactionID,fallback:'',encrypted:false},
      description => {
        P_description.innerText = description;
      }
    );
  }

  const DIV_more = createElement({
    id: 'viewAsset-history-more',
    parentNode: DIV_history
  });

  if (history.length >= PAGE_SIZE) {
    DIV_more.innerHTML = '{{VIEWASSET:HISTORY_BUTTON_MORE}}';
    DIV_more.onclick = () => {
      DIV_more.classList.add('disabled');
      DIV_more.innerHTML = '<div class="spinner-loader">...</div>';
      retrieveHistory(asset, page + 1);
    };
    DIV_more.classList.remove('disabled');
  } else if (page === 0 && history.length === 0) {
    DIV_more.innerHTML = '{{VIEWASSET:HISTORY_NO_TX}}';
    DIV_more.classList.add('disabled');
    DIV_more.onclick = null;
  } else {
    DIV_more.classList.add('disabled');
    DIV_more.innerHTML = '{{VIEWASSET:HISTORY_NO_MORE_TX}}';
    DIV_more.onclick = null;
  }
};

const failHistory = (asset, page) => history => {
  const DIV_history = document.getElementById('viewAsset-history');
  if (page === 0) DIV_history.innerHTML = ''; // reset spinner
  else { // remove previous load more button
    const DIV_more = document.getElementById('viewAsset-history-more');
    DIV_more.parentNode.removeChild(DIV_more);
  }

  const DIV_more = createElement({
    id: 'viewAsset-history-more',
    innerHTML: '<div>{{VIEWASSET:HISTORY_DATA_UNAVAILABLE}} {{VIEWASSET:HISTORY_RETRY}}</div>',
    parentNode: DIV_history,
    onclick: () => retrieveHistory(asset, page + 1)
  });
};

const retrieveHistory = (asset, page = 0) => {
  const DIV_history = document.getElementById('viewAsset-history');
  const DIV_more = document.getElementById('viewAsset-history-more');
  if (DIV_more) DIV_more.style.backgroundColor = undefined;
  if (page === 0 && DIV_history.innerHTML === '') {
    DIV_history.innerHTML = '<div class="content"><br /><br /><div class="spinner-loader">...</div><div id="viewAsset-spinner-message">&nbsp;&nbsp;&nbsp;{{VIEWASSET:HISTORY_GETTING_DATA}}</div></div>';
  }
  hybrix.lib.rout({query: '/a/' + asset.symbol + '/history/' + asset.address + '/' + PAGE_SIZE + '/' + page * PAGE_SIZE, channel: 'y'}, renderHistory(asset, page), failHistory(asset, page));
};

const renderPending = asset => pendingTransactions => {
  pendingTransactions = Object.values(pendingTransactions).filter(pendingTransaction => pendingTransaction.type === 'regular' && pendingTransaction.status < 0.55 && pendingTransaction.meta.symbol === asset.symbol);
  const DIV_pendingTransactions = document.getElementById('viewAsset-pending');
  if (pendingTransactions.length !== pendingTransactionsPreviousLength) {
    const NOP = () => {};
    hybrix.lib.getPending({remove: false, sync: true}, NOP, NOP);
    if (pendingTransactions.length > 0) {
      pendingTransactionsPreviousLength = pendingTransactions.length;
      DIV_pendingTransactions.innerHTML = ''; // empty the list before building using pending object elements
      for (const pendingTransaction of pendingTransactions) {
        for (const pendingTransactionID of pendingTransaction.meta.id) {
          if (!(pendingTransaction.status === 1 && isTransactionInHistory(prefixedTransactionID))) {
            // DEBUG: console.log('    -> '+JSON.stringify(pendingTransaction));
            const date = renderDate(pendingTransaction.timestamp / 1000);
            const amount = renderAmount(asset, pendingTransaction.meta);
            const {addressString, direction} = renderAddress(asset, pendingTransaction.meta);
            const DIV_pendingTransaction = document.createElement('DIV');
            DIV_pendingTransaction.className = 'tx-history-entry viewAsset-pendingTransaction';  // TODO: pending/failed?
            const transactionArray = pendingTransactionID.split(':');
            const transactionID = transactionArray.length > 1 ? transactionArray[1] : transactionArray[0];
            const hashedTransactionID = sha256.sha256(transactionID);
            DIV_pendingTransaction.innerHTML = `
              <p class="tx-history-date">${date}</p>
              <p class="tx-history-amount ${direction}">${amount}</p>
              <p class="tx-history-description">...</p>
              <p class="tx-history-address">${addressString}</p>
              <p class="tx-history-id">{{VIEWASSET:HISTORY_TXID}} ${transactionID} ${renderExplorerLink(asset.symbol, transactionID)} <span class="viewAsset-history-pending">{{VIEWASSET:HISTORY_PENDING}}</span></p>`;
            // DEPRECATED: DIV_pendingTransactions.appendChild(DIV_pendingTransaction);
            DIV_pendingTransactions.insertBefore(DIV_pendingTransaction, DIV_pendingTransactions.firstChild)
            hybrix.lib.load({key:hashedTransactionID,fallback:'',encrypted:false}, description => {
              const DIV_pendingTransactionDescription = DIV_pendingTransaction.getElementsByClassName("tx-history-description")[0];
              DIV_pendingTransactionDescription.innerHTML = `${description}`;
            }, NOP);
          }
        }
      }
    } else {
      const DIV_history = document.getElementById('viewAsset-history');
      pendingTransactionsPreviousLength = pendingTransactions.length;
      // empty the list if no pending transactions
      if(pendingTransactions.length === 0) DIV_pendingTransactions.innerHTML = '';
      // refresh history if it is populated
      if (DIV_history.innerHTML !== '') {
        retrieveHistory(asset,0);
      }
    }
  }
};

function removePendingTransaction (transactionId) {
  const DIV_pendingTransactions = document.getElementById('viewAsset-pending');
  for (const DIV_pendingTransaction of DIV_pendingTransactions.children) {
    if (String(DIV_pendingTransactions.transactionId) === String(transactionId)) {
      DIV_pendingTransactions.removeChild(DIV_pendingTransaction);
      return;
    }
  }
}

function isTransactionInHistory (transactionId) {
  const DIV_history = document.getElementById('viewAsset-history');
  for (const DIV_transaction of DIV_history.children) {
    if (String(DIV_transaction.transactionId) === String(transactionId)) return true;
  }
  return false;
}

function failPending (error) {
  console.error(error);
  document.getElementById('viewAsset-pending').innerHTML = '{{VIEWASSET:HISTORY_FAILED_GET_PENDING}}';
}

function retrievePending (asset, sync) {
  if (sync !== true) sync = false;
  hybrix.lib.getPending({remove: false, sync}, renderPending(asset), failPending);
}
exports.retrieveHistory = retrieveHistory;
exports.retrievePending = retrievePending;
