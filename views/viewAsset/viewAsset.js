const hybrix = require('../../lib/hybrix').hybrix;
const {retrieveHistory, retrievePending} = require('./history/history');
//const branding = require('../../lib/branding');
const {parseMultiAddress} = require('../../lib/multi-address');
const {createElement} = require('../../lib/html');
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const {addAsset, getAsset, addEventListener, getBalance} = require('../../lib/assets');
const {getIcon} = require('../../lib/icon');
const {getCurrencyPreference} = require('../settings/settings');

let currentSymbol;
const VIEWASSET_PENDING_REFRESH = 8000;

const fuelSymbols = [];

exports.rout = parameters => {
  if (!parameters.hasOwnProperty('symbol')) hybrix.view.swap('listAssets', parameters);
  else hybrix.view.swap('viewAsset', parameters);
};

exports.once = () => {
  const BUTTON_back = document.getElementById('viewAsset-back-button');
  BUTTON_back.onclick = () => hybrix.view.swap('listAssets');

  const BUTTON_pending = document.getElementById('viewAsset-pending-button');
  BUTTON_pending.onclick = () => hybrix.view.open('pending');

  const BUTTON_apps = document.getElementById('viewAsset-apps-button');
  BUTTON_apps.onclick = () => hybrix.view.open('appsList');
};

function fuelLock (symbol) {
  const asset = getAsset(symbol);

  fuelSymbols.length = 0; // reset fuel symbols
  const DIV_locked = document.getElementById('viewAsset-lock');
  const BUTTON_send = document.getElementById('sendAssetButton');
  const BUTTON_fuel = document.getElementById('fuelAssetButton');
  const multiAddress = parseMultiAddress(asset.address, symbol);
  let message = '';
  if (multiAddress.isMultiAddress) {
    message = '<span id="asset-overview-fuel-warning">{{VIEWASSET:WARN_CANNOT_SEND}}<br>';
    for (const subSymbol in asset.subSufficientFuels) {
      let subBalance = 'n/a';
      for (const subAddress in asset.subBalances) {
        if (subAddress.startsWith(subSymbol + ':')) {
          subBalance = asset.subBalances[subAddress];
          break;
        }
      }

      if (asset.subSufficientFuels[subSymbol] === false && subBalance !== 'n/a' && Number(subBalance) > 0) {
        message += `Sending ${subSymbol.toUpperCase()} requires `;
        for (const feeSymbol in asset.feePerSubAsset[subSymbol]) {
          const fee = asset.feePerSubAsset[subSymbol][feeSymbol];
          message += ` ${prettyPrintAmount(fee, feeSymbol, false, true)} `;
          const baseBalance = asset.baseBalances[feeSymbol];
          if (baseBalance === 'n/a') message += `&nbsp;${feeSymbol.toUpperCase()} {{VIEWASSET:BALANCE_UNKNOWN}}`;
          else {
            message += `&nbsp;{{VIEWASSET:ONLY_AVAILABLE_A}}${prettyPrintAmount(baseBalance, feeSymbol, false, true)}{{VIEWASSET:ONLY_AVAILABLE_B}}`;
            fuelSymbols.push(feeSymbol);
          }
        }
        message += '<br/>';
      }
    }
  } else {
    message = `<span id="asset-overview-fuel-warning">{{VIEWASSET:WARN_CANNOT_SEND}}<br>{{VIEWASSET:SENDING_REQUIRES_A}}${prettyPrintAmount(asset.fee, asset['fee-symbol'], false, true)}{{VIEWASSET:SENDING_REQUIRES_B}}`;
    for (const baseSymbol in asset.baseBalances) {
      const baseBalance = asset.baseBalances[baseSymbol];
      if (baseBalance === 'n/a') message += `&nbsp;${baseSymbol.toUpperCase()} {{VIEWASSET:BALANCE_UNKNOWN}}`;
      else {
        fuelSymbols.push(baseSymbol);
        message += `&nbsp;{{VIEWASSET:ONLY_AVAILABLE_A}}${prettyPrintAmount(baseBalance, baseSymbol, false, true)}{{VIEWASSET:ONLY_AVAILABLE_B}}`;
      }
    }
    message += '</span>';
  }

  const baseSymbols = fuelSymbols
    .map(baseSymbol => baseSymbol.toUpperCase())
    .join(' or ');

  BUTTON_send.setAttribute('disabled', true);
  if (fuelSymbols.length > 0) {
    message += `<br><br>{{VIEWASSET:CLICK_FUEL_A}}${baseSymbols}{{VIEWASSET:CLICK_FUEL_B}}`;

    BUTTON_send.style.display = 'none';
    BUTTON_fuel.style.display = 'inline-block';
  } else {
    BUTTON_send.style.display = 'inline-block';
    BUTTON_fuel.style.display = 'none';
  }

  DIV_locked.firstElementChild.innerHTML = message;
  DIV_locked.style.display = 'block';
}

function updateAsset (symbol, first = false) {
  const asset = getAsset(symbol);
  const DIV_balance = document.getElementById('asset-overview-asset-balance');
  const DIV_valuation = document.getElementById('asset-overview-asset-valuation');
  const DIV_locked = document.getElementById('viewAsset-lock');
  const BUTTON_send = document.getElementById('sendAssetButton');
  const BUTTON_fuel = document.getElementById('fuelAssetButton');
  const DIV_icon = document.getElementById('asset-overview-asset-icon');
  
  const balance = getBalance(symbol);

  DIV_balance.innerHTML = balance === 'n/a' && first
    ? '...' // no 'n/a' if still loading
    : prettyPrintAmount(balance, symbol);

  if (DIV_balance.lastKnownBalance !== balance) { // only refresh if balance has changed
    DIV_valuation.innerHTML = '...';
    DIV_balance.lastKnownBalance = balance;
    const currency = getCurrencyPreference();

    hybrix.lib.getValuation({fromSymbol: symbol, toSymbol: currency, amount: balance}, rate => {
      DIV_valuation.innerHTML = prettyPrintAmount(rate, currency, true);
    }, error => {
      console.error('Could not retrieve valuation for ' + symbol, error);
      DIV_valuation.innerHTML = '<small style="opacity:0.5;">{{VIEWASSET:N/A}}</small>';
    });
  }

  if ((asset.features && asset.features.send !== false) && asset.sufficientFuel === false && balance !== 'n/a' && Number(balance) > 0) {
    fuelLock(symbol);
  } else {
    if (symbol.substr(0,5) === 'test_') {
      DIV_locked.firstElementChild.innerHTML = '<span id="asset-overview-fuel-warning">{{VIEWASSET:WARN_TEST_MODE}}</span>';
      DIV_locked.style.display = 'block';
    } else DIV_locked.style.display = 'none';
    BUTTON_send.style.display = 'inline-block';
    BUTTON_send.removeAttribute('disabled');
    BUTTON_fuel.style.display = 'none';
    if(asset.features && asset.features.send === false) BUTTON_send.setAttribute('disabled', true);
    else BUTTON_send.removeAttribute('disabled');
  }

  if (typeof asset.subBalances === 'object' && asset.subBalances !== null) {
    const TABLE_subAddresses = document.getElementById('viewAsset-subAddresses');
    for (const TR of [...TABLE_subAddresses.children]) {
      if (asset.subBalances.hasOwnProperty(TR.subAddress)) {
        const TD_subBalance = TR.lastChild;
        TD_subBalance.innerHTML = prettyPrintAmount(asset.subBalances[TR.subAddress], symbol);
      }
    }
  }
  DIV_icon.innerHTML = getIcon(symbol, icon => { DIV_icon.innerHTML = icon; });
}

function checkSwapPairs (symbol) {
  const asset = getAsset(symbol);

  const BUTTON_send = document.getElementById('sendAssetButton');
  const BUTTON_fuel = document.getElementById('fuelAssetButton');
  const BUTTON_swap = document.getElementById('swapAssetButton');

  hybrix.lib.rout({query: '/e/swap/deal/pairs', encryptByDefault: true, regular: false, cache: 5 * 60 * 1000}, pairs => {
    let swapTo = false;
    let swapFrom = false;
    let swapToBase = false;
    const swappableFeeSymbols = [];
    for (const pair of pairs) {
      if (typeof pair === 'string') {
        if (pair.startsWith(symbol + ':')) swapFrom = true;
        if (pair.endsWith(':' + symbol)) swapTo = true;
        for (const feeSymbol in asset.baseBalances) {
          if (pair.endsWith(':' + feeSymbol)) {
            if (!swappableFeeSymbols.includes(feeSymbol)) swappableFeeSymbols.push(feeSymbol);
            swapToBase = true;
          }
        }
      }
    }
    if (swapTo || swapFrom) {
      BUTTON_send.removeAttribute('disabled');
      BUTTON_swap.removeAttribute('disabled');
      BUTTON_fuel.setAttribute('disabled', true);
    }
    if (swapToBase) {
      BUTTON_fuel.removeAttribute('disabled');

      BUTTON_fuel.onclick = () => {
        const toSymbol = fuelSymbols.length > 0
          ? swappableFeeSymbols.filter(feeSymbol => fuelSymbols.includes(feeSymbol)).join(',')
          : swappableFeeSymbols.join(',');
        hybrix.view.open('swapAsset', {toSymbol, action: 'fuel', fuelForSymbol: symbol});
      };
    }
  }, error => {
    BUTTON_swap.setAttribute('disabled', true);
    BUTTON_fuel.setAttribute('disabled', true);
    console.error('Failed to retrieve swap pairs.', error);
  });
}

function renderViewAsset (symbol) {
  const BUTTON_history = document.getElementById('viewAsset-loadHistoryButton');
  const BUTTON_send = document.getElementById('sendAssetButton');
  const BUTTON_export = document.getElementById('exportAssetButton');
  const BUTTON_receive = document.getElementById('receiveAssetButton');

  BUTTON_send.onclick = () => hybrix.view.open('transaction', {symbol});
  BUTTON_receive.onclick = () => hybrix.view.open('receiveAsset', {symbol});
  BUTTON_export.onclick = () => hybrix.view.open('exportAsset', {symbol});

  const asset = getAsset(symbol);
  currentSymbol = symbol;
  checkSwapPairs(symbol);

  document.getElementById('asset-overview-asset-id').innerHTML = symbol;
  document.getElementById('asset-overview-asset-name').innerHTML = asset.name ? asset.name : '';

  if(asset.features && asset.features.receive === false) BUTTON_receive.setAttribute('disabled', true);
  else BUTTON_receive.removeAttribute('disabled');

  const DIV_historyNote = document.getElementById('viewAsset-history-note');
  DIV_historyNote.innerHTML = asset.notes && asset.notes.history ? asset.notes.history : '';

  const TABLE_subAddresses = document.getElementById('viewAsset-subAddresses');
  TABLE_subAddresses.innerHTML = '';
  const multiAddress = parseMultiAddress(asset.address, symbol);

  if (multiAddress.isMultiAddress) {
    for (const subAddress of multiAddress.subAddresses) {
      const subSymbol = subAddress.split(':')[0];
      const TR = createElement({tagName: 'TR', subAddress});
      const TD_subSymbol = createElement({tagName: 'TD', parentNode: TR, innerHTML: subSymbol});
      const TD_subBalance = createElement({tagName: 'TD', parentNode: TR, innerHTML: '...'});
      TABLE_subAddresses.appendChild(TR);
    }
  }
  const A_explore = document.getElementById('viewAsset-explore');
  const currency = hybrix.view.exec('settings', 'getCurrencyPreference');
  A_explore.href = `https://explorer.hybrix.io/?symbol=${symbol}&address=${asset.address}&currency=${currency}`;
  if (asset.features && asset.features.history === false) BUTTON_history.setAttribute('disabled',true);
  else {
    BUTTON_history.removeAttribute('disabled');
    BUTTON_history.onclick = () => {
      BUTTON_history.setAttribute('disabled',true);
      setTimeout( () => {
        BUTTON_history.style.display = 'none';
      },5000);
      retrieveHistory(asset);
      retrievePending(asset);
    };
  } 

  updateAsset(symbol, true);
  addEventListener('refreshAsset:' + symbol, () => updateAsset(symbol, false), 'viewAsset');
}

exports.open = parameters => {
  const {symbol} = parameters;
  const BUTTON_scanner = document.getElementById('viewAsset-scanner-button');  // the QRscanner button must be reinitialized on open, depending on the symbol that was clicked!
  BUTTON_scanner.onclick = () => hybrix.view.open('qrScanner', {
    type: 'transaction',
    dataCallback: result => {
      if (result.symbol !== null && result.symbol !== symbol) {  // QR code is for a different asset than this view, so we go back to listAssets
        hybrix.lib.addAsset({symbol: result.symbol}, () => {
          hybrix.view.swap('listAssets', {});
          hybrix.view.open('transaction', {symbol: result.symbol, target: result.address, amount: result.amount, description: result.description, message: result.message});
        },
        error => console.error(`Failed to add ${result.symbol} symbol.`, error));
      } else {
        hybrix.lib.addAsset({symbol}, asset => {
          hybrix.view.open('transaction', {symbol, target: result.address, amount: result.amount, description: result.description, message: result.message});
        },
        error => console.error(`Failed to add ${result.symbol} symbol.`, error));
      }
    }
  });

  const BUTTON_fuel = document.getElementById('fuelAssetButton');
  const BUTTON_swap = document.getElementById('swapAssetButton');
  const BUTTON_history = document.getElementById('viewAsset-loadHistoryButton');
  const DIV_balance = document.getElementById('asset-overview-asset-balance');
  const DIV_valuation = document.getElementById('asset-overview-asset-valuation');

  DIV_balance.lastKnownBalance = null;
  DIV_valuation.innerHTML = '...';

  document.getElementById('viewAsset-history-note').innerHTML = '';
  document.getElementById('viewAsset-pending').innerHTML = '';
  document.getElementById('viewAsset-history').innerHTML = '';

  BUTTON_history.setAttribute('disabled', true);
  BUTTON_history.style.display = 'inline-block';
  BUTTON_swap.setAttribute('disabled', true);
  BUTTON_fuel.setAttribute('disabled', true);
  BUTTON_swap.onclick = () => hybrix.view.open('swapStart', {symbol});

  addAsset(symbol, () => renderViewAsset(symbol), console.error);

  if (window.viewAssetPendingInterval) { clearInterval(window.viewAssetPendingInterval); }
  window.viewAssetPendingInterval = setInterval( () => {
      retrievePending(getAsset(symbol));
    }, VIEWASSET_PENDING_REFRESH);
  retrievePending(getAsset(symbol), true);  // on open, refresh pending, while syncing with remote node once!

};

exports.close = () => {
  currentSymbol = undefined;
  if (window.viewAssetPendingInterval) { clearInterval(window.viewAssetPendingInterval); }
};

exports.refreshPending = symbol => {
  if (currentSymbol === symbol) retrievePending(getAsset(symbol));
};
