const hybrix = require('../../lib/hybrix').hybrix;
const {prettyPrintAmount, renderDate} = require('../../lib/prettyPrintAmount');
const PENDING_REFRESH = 15000; // refesh rate of pending transactions
const MAX_DISPUTE_THRESHOLD = 1209600; // 14 days maximum time to dispute
let pendingCachedContent;
let pendingModalMode = 0;

exports.rout = parameters => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('pending', parameters);
};

function sortPendingTransactions (pendingTransactions) {
  return Object.fromEntries(
    Object.entries(pendingTransactions).sort(([key1, tx1], [key2, tx2]) => tx2.timestamp - tx1.timestamp)
  );
}

function renderDescription (tx, cropped = true) {
  const currency = hybrix.view.exec('settings', 'getCurrencyPreference');
  let html;
  if (tx.type === 'deal') {
    const symbol = tx.meta.ask.symbol;
    if (cropped) html = `{{PENDING:SWAP_ITEM}} ${tx.meta.ask.symbol.toUpperCase()} {{PENDING:FOR}} ${tx.meta.bid.symbol.toUpperCase()}`;
    else {
      html = `{{PENDING:SWAP_ITEM}} ${prettyPrintAmount(tx.meta.ask.amount, tx.meta.ask.symbol)} {{PENDING:FOR}} ${prettyPrintAmount(tx.meta.bid.amount, tx.meta.bid.symbol)}.`;
    }
  } else {
    html = `{{PENDING:SEND_ITEM}} ${prettyPrintAmount(tx.meta.amount, tx.meta.symbol)}`;
  }
  return html;
}

function renderStatus (tx) {
  const progressPercent = isNaN(tx.progress)?0:Math.round(tx.progress*1000)/10;
  let status, className;
  switch (tx.status) {
    case 1: status = '{{PENDING:STATUS_COMPLETE}} (100%)'; className = `pending-complete`; break;
    case 0: status = '{{PENDING:STATUS_PENDING}} (' + progressPercent + '%)'; className = `pending-pending`; break;
    case -1: status = '{{PENDING:STATUS_FAILED}}'; className = `pending-failed`; break;
    default: status = '{{PENDING:STATUS_UNKNOWN}}'; className = `pending-failed`; break;
  }
  // we multiply the progress to make sure a 100% progress meter spans the entire bar width, including negative left margin of -20px;
  return '<div class="' + className + '"><span class="progress-meter" style="width: ' + (progressPercent * 1.25) + '%;margin-left:-20px;"></span><span class="progress-text">' + status + '</span></div>';
}

function render (pendingTransactions) {
  pendingCachedContent = pendingTransactions;
  if (pendingModalMode === 0) {
    pendingTransactions = sortPendingTransactions(pendingTransactions);
    const BUTTON_clear = document.getElementById('pending-clear');
    let foundItemThatCanBeCleared = false;
    const DIV = document.getElementById('pending-transactions');

    if (Object.keys(pendingTransactions).length === 0) {
      DIV.innerHTML = '<div style="color: #AAA;width: 100%;text-align: center;margin-top: 20%;margin-bottom: 20%;font-size: 20px;">{{PENDING:NO_PENDING}}</div>';
    } else {
      DIV.innerHTML = '';
      for (const id in pendingTransactions) {
        const tx = pendingTransactions[id];
        const DIV_subelement = document.createElement('DIV');
        DIV_subelement.className = 'pending-container';
        let date = renderDate(tx.timestamp / 1000).split(' ').join('<br/>');
        date = date.substr(0, date.length - 3); // '2021-03-25 10:29:06' -> '2021-03-25 10:29'
        DIV_subelement.innerHTML = `<span class="pending-date">${date}</span><span class="pending-details">${renderDescription(tx)}</span>${renderStatus(tx)}`;
        DIV_subelement.onclick = () => {
          //console.log(' Opening '+JSON.stringify({id})+'...');
          DIV.innerHTML = '';
          hybrix.view.open('pending', {id});
          clearInterval(window.pendingInterval);
        }
        DIV.appendChild(DIV_subelement);
        if (tx.status === -1 || tx.status === 1) foundItemThatCanBeCleared = true;
      }
    }
    //BUTTON_clear.style.display = foundItemThatCanBeCleared ? 'inline-block' : 'none';
  }
}

const renderSingle = id => pendingTransactions => {
  const DIV = document.getElementById('pending-transactions');

  if (!pendingTransactions.hasOwnProperty(id)) {
    DIV.innerHTML = `Failed to find pending object "${id}"!`;
  } else {
    const tx = pendingTransactions[id];
    // TODO: const exploreLink = `<a title="Explore..." target="_blank" class="viewAsset-exploreTransaction" href="https://explorer.hybrix.io/?symbol=${tx.meta.symbol}&transactionId=${tx.meta.id}&currency=${currency}"></a>`;
    exploreLink = '';
    let idDiv = id.split(':'); idDiv.shift(); idDiv = idDiv.join(': ');
    const target = tx.meta.target?tx.meta.target:tx.meta.bid.target;
    DIV.innerHTML = `<div id="pending-detail">
        <div id="pending-detail-id">{{PENDING:ID}}&nbsp;${idDiv}</div>
        <div id="pending-detail-action">{{PENDING:ACTION}} ${renderDescription(tx, false)}</div>
        <div id="pending-detail-date">{{PENDING:TIME}} ${renderDate(tx.timestamp / 1000)}</div>
        <div id="pending-detail-status">{{PENDING:STATUS}} <div class="pending-progress">${renderStatus(tx)}</div></div>
        <div id="pending-detail-target">{{PENDING:TARGET}} ${target} ${exploreLink}</div>
        <br /><div id="pending-detail-object"><span id="pending-detail-object-title">{{PENDING:TECHNICAL_DETAILS}}</span><br/>${JSON.stringify(tx)}</div>
        <div id="pending-disputeButton" class="pure-button pure-button-large" style="display:none;"><div class="icon"></div> {{PENDING:BUTTON_DISPUTE}}</div>
      </div>`;
    // when looking at the details, add a dispute button only if this is a swap and the threshold is proper
    const BUTTON_dispute = document.getElementById('pending-disputeButton');
    const disputeThreshold = Date.now() - MAX_DISPUTE_THRESHOLD;
    if(!tx.meta.target) {
      BUTTON_dispute.style.display = 'block';
      if (tx.timestamp > disputeThreshold) {
        BUTTON_dispute.disabled = false;
        BUTTON_dispute.onclick = () => hybrix.view.open('swapDispute', {proposal: tx.meta});
      } else {
        BUTTON_dispute.style.display = 'none';
        BUTTON_dispute.disabled = true;
        BUTTON_dispute.onclick = null;
      }
    } else BUTTON_dispute.style.display = 'none';
    const DIV_details = document.getElementById('pending-detail-object');
    const SPAN_details_title = document.getElementById('pending-detail-object-title');
    SPAN_details_title.onclick = () => {
      if(DIV_details.style.height === 'auto') {
        DIV_details.style.height = '1.5em';
      } else {
        DIV_details.style.height = 'auto';
      }
    }
  }
};

function errorCallback (error) {
  const DIV = document.getElementById('pending-transactions');
  DIV.innerHTML = '{{PENDING:ERROR_FAILED_TO_RETRIEVE}}';
  console.error(error);
}

exports.open = (parameters) => {
  const DIV = document.getElementById('pending-transactions');
  const spinner = '<div id="pending-spinner"><div class="spinner-loader">...</div><div id="pending-spinner-message">{{PENDING:SPINNER_RETRIEVING}}</div></div>';
  DIV.innerHTML = spinner;
  const BUTTON_close = document.getElementById('pending-close');
  // DISABLED: const BUTTON_clear = document.getElementById('pending-clear');

  if (window.pendingInterval) { clearInterval(window.pendingInterval); }

  if (parameters.hasOwnProperty('id')) {
    pendingModalMode = 1;
    BUTTON_close.style.display = 'block';
    BUTTON_close.onclick = () => {
      DIV.innerHTML = '';
      hybrix.view.open('pending', {});
    }
    if (pendingCachedContent) renderSingle(parameters.id)(pendingCachedContent);
    else hybrix.lib.getPending({remove: false}, renderSingle(parameters.id), errorCallback);
  } else {
    pendingModalMode = 0;
    BUTTON_close.style.display = 'none';
    window.pendingInterval = setInterval( () => {
        hybrix.lib.getPending({remove: false, sync: false}, render, errorCallback);  // disable sync on refresh to avoid loading remote meta object repeatedly
    }, PENDING_REFRESH);
    BUTTON_close.onclick = () => {
      clearInterval(window.pendingInterval);
      hybrix.view.pop();
    }
    if (pendingCachedContent) render(pendingCachedContent);
    hybrix.lib.getPending({remove: false}, render, errorCallback);

    /*
    BUTTON_clear.style.display = 'none';
    BUTTON_clear.onclick = () => {
      DIV.innerHTML = spinner;
      hybrix.lib.getPending({remove: true}, render, errorCallback);
    };
    */
  }
};

exports.close = () => {
  clearInterval(window.pendingInterval);
}

exports.clearPendingCache = () => {
  pendingCachedContent = null;
}
