const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;

exports.rout = parameters => {
  hybrix.view.swap('allocate');
  hybrix.view.open('allocateStart', parameters);
}

exports.open = parameters => {
  const DIV_progress = document.getElementById('allocateStart-progress');
  const BUTTON_crypto = document.getElementById('allocateStart-crypto');
  const BUTTON_fiat = document.getElementById('allocateStart-fiat');

  progress(DIV_progress, {
    Prepare: null,
    Allocate: null,
    Create: null
  }, 0);

  BUTTON_crypto.onclick = () => {
    hybrix.view.open('allocatePreparePair', {type:'crypto'});
  }

  BUTTON_fiat.onclick = () => {
    //hybrix.view.open('allocatePreparePair', {type:'fiat'});
    hybrix.view.open('TODO');
  }
}
