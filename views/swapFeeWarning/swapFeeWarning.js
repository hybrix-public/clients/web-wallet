const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;

exports.open = function (parameters) {
  const BUTTON_back = document.getElementById('swapFeeWarning-back');
  const BUTTON_accept = document.getElementById('swapFeeWarning-accept');

  BUTTON_back.onclick = () => hybrix.view.pop();
  BUTTON_accept.onclick = () => {
    hybrix.view.pop();
    hybrix.view.open('swapReview', {...parameters, amount: parameters.amount});
  };

  const DIV_progress = document.getElementById('swapFeeWarning-progress');
  progress(DIV_progress, {
    '{{SWAPFEEWARNING:PROGRESS_DETAILS}}': null,
    '{{SWAPFEEWARNING:PROGRESS_AMOUNT}}': () => hybrix.view.pop(),
    '{{SWAPFEEWARNING:PROGRESS_REVIEW}}': null,
    '{{SWAPFEEWARNING:PROGRESS_SWAP}}': null    
  }, 2);
};
