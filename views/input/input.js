const hybrix = require('../../lib/hybrix').hybrix;

exports.rout = () => {
  hybrix.view.swap('listAssets');
}

function open (parameters) {
  const H2_title = document.getElementById('input-title');
  const P_question = document.getElementById('input-question');
  const P_question_more = document.getElementById('input-question-more');
  const INPUT_field = document.getElementById('input-field');
  const BUTTON_submit = document.getElementById('input-submit-button');
  const DIV_spinner = document.getElementById('input-spinner');
  const DIV_spinner_message = document.getElementById('input-spinner-message');

  DIV_spinner.style.display = 'none';

  if (typeof parameters.title === 'string') H2_title.innerText =  parameters.title;
  if (typeof parameters.text === 'string') P_question.innerText =  parameters.text;
  if (typeof parameters.textMore === 'string') P_question_more.innerText =  parameters.textMore;
  if (typeof parameters.textPending === 'string') DIV_spinner_message.innerText = parameters.textPending;
  if (typeof parameters.placeholder === 'string') INPUT_field.placeholder = parameters.placeholder;
  if (typeof parameters.pattern === 'string') INPUT_field.pattern = parameters.pattern;
  if (typeof parameters.fieldValue === 'string') INPUT_field.value = parameters.fieldValue;

  INPUT_field.onchange = INPUT_field.oninput = INPUT_field.onpaste = () => {
    const fieldValue = INPUT_field.value;
    let regExp = false;
    if (typeof parameters.pattern === 'string') {
      regExp = new RegExp(parameters.pattern, 'i');
    }
    if ((regExp && regExp.test(fieldValue)) || !regExp) {
      BUTTON_submit.disabled = false;
    } else if(regExp) {
      BUTTON_submit.disabled = true;
    }
  }

  BUTTON_submit.onclick = () => {
    DIV_spinner.style.display = 'block';
    BUTTON_submit.disabled = true;
    const fieldValue = INPUT_field.value;
    if (typeof parameters.callback === 'function') {
      setTimeout( () => {
        parameters.callback(fieldValue);
      }, 500);
    }
  }
}

exports.open = open;
