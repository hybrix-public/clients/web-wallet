const {onclickCopyToClipboard} = require('../../lib/clipboard');
const {renderQR} = require('../../lib/qr');
const {parseMultiAddress} = require('../../lib/multi-address');
const {createElement, createCustomCheckBox} = require('../../lib/html');
const hybrix = require('../../lib/hybrix').hybrix;
const {addAsset} = require('../../lib/assets');

exports.rout = parameters => {
  // TODO what if no symbol defined?
  const {symbol} = parameters;
  hybrix.view.swap('viewAsset', {symbol});
  hybrix.view.open('paymentRequest', parameters);
};

function render (address, symbol) {
  const INPUT_amount = document.getElementById('paymentRequest-amount');
  const INPUT_description = document.getElementById('paymentRequest-description');
  const DIV_url = document.getElementById('paymentRequest-url');
  const DIV_paymentQRCode = document.getElementById('paymentRequest-qrcode');
  const BUTTON_copy = document.getElementById('paymentRequest-copyButton');
  const BUTTON_share = document.getElementById('paymentRequest-shareButton');

  const amount = INPUT_amount.value.replace(',','.');
  INPUT_amount.value = amount;
  
  const description = INPUT_description.value;
  if (isNaN(amount) || amount <= 0) {
    DIV_paymentQRCode.style.opacity=0;
    BUTTON_copy.setAttribute('disabled',true);
    BUTTON_share.setAttribute('disabled',true);
    if (amount !== '') {
      DIV_url.innerText = '{{PAYMENTREQUEST:ENTER_VALID_AMOUNT}}';
      DIV_url.style['text-decoration'] = 'none';
      DIV_url.style.display = 'block';
    } else {
      DIV_url.style.display = 'none';
    }
  } else {
    // NOTE: our description of the transaction is called message in the BIP0021 specification!
    const query = `${symbol}:${address}?amount=${(!isNaN(amount)?amount:'0.0')}${(description !== '' ? '&message='+description : '')}`;
    const urlStart = window.location.origin + window.location.pathname + '#/transaction/target=' + address + '&symbol=' + symbol + (amount !== '' ? '&amount=' + amount : '');
    const url = urlStart + (description !== '' ? '&description=' + description : '');
    const clipboardUrl = urlStart + (description !== '' ? '&description=' + encodeURIComponent(description) : '');
    renderQR(query, DIV_paymentQRCode);
    //const CANVAS_paymentQRCode = document.querySelector('#paymentRequest-qrcode canvas');
    //console.log(JSON.stringify(CANVAS_paymentQRCode));
    //CANVAS_paymentQRCode.width = 200;
    //CANVAS_paymentQRCode.height = 200;
    DIV_paymentQRCode.style.opacity=1;
    DIV_url.innerText = clipboardUrl;
    DIV_url.style.display = 'block';
    DIV_url.style['text-decoration'] = 'underline';
    BUTTON_copy.removeAttribute('disabled');
    BUTTON_share.removeAttribute('disabled');
    BUTTON_share.onclick = () => {
      if (navigator.share) {
        navigator.share({
          title: '{{PAYMENTREQUEST:REQUEST_HYBRIX}}: {{PAYMENTREQUEST:REQUEST_PLEASE_SEND}} ' + amount + ' ' + symbol.toUpperCase(),
          text: '{{PAYMENTREQUEST:REQUEST_SEND}} ' + amount + ' ' + symbol.toUpperCase() + ' {{PAYMENTREQUEST:REQUEST_TO}} ' + address + '. {{PAYMENTREQUEST:POWERED_BY}}',
          url
        })
          .then(() => {})
          .catch(error => console.error(error));
      } else {
        window.location.href = 'mailto:somebody@somewhere?subject={{PAYMENTREQUEST:REQUEST_HYBRIX}}' + amount + ' ' + symbol.toUpperCase() + '&body={{PAYMENTREQUEST:REQUEST_PLEASE_SEND}} ' + amount + ' ' + symbol.toUpperCase() + ' {{PAYMENTREQUEST:REQUEST_TO}} ' + address + '.%0d%0a%0d%0a{{PAYMENTREQUEST:REQUEST_THANK_YOU}}%0d%0a%0d%0a%0d%0a{{PAYMENTREQUEST:POWERED_BY}}';
      }
    };
    const copyClipboardAction = () => {
      const DIV_copy = document.getElementById('paymentRequest-copySuccess');
      DIV_copy.classList.add('active');
      setTimeout( () => { DIV_copy.classList.remove('active'); }, 5000);
    }
    onclickCopyToClipboard(BUTTON_copy, clipboardUrl, copyClipboardAction, console.error);
    onclickCopyToClipboard(DIV_url, clipboardUrl, copyClipboardAction, console.error);
  }
}

function getSubAddressBySubSymbol (multiAddress, subSymbol) {
  for (const subAddress of multiAddress.subAddresses) {
    if (subAddress.startsWith(subSymbol + ':')) return subAddress;
  }
  return undefined;
}

function renderPaymentRequest (asset) {
  const INPUT_amount = document.getElementById('paymentRequest-amount');
  const INPUT_description = document.getElementById('paymentRequest-description');
  let onchange;
  let address = asset.address;

  // DEPRECATED:
  //const TABLE_subAddresses = document.getElementById('paymentRequest-subAddresses');
  //const multiAddress = parseMultiAddress(asset.address, asset.symbol);
  /*
  if (multiAddress.isMultiAddress) {
    const INPUTsBySubSymbol = {};
    onchange = () => {
      let address = '';
      for (const subSymbol in INPUTsBySubSymbol) {
        if (INPUTsBySubSymbol[subSymbol].checked) {
          if (address !== '') address += ',';
          address += getSubAddressBySubSymbol(multiAddress, subSymbol);
        }
      }
      render(address, asset.symbol);
    };
    for (const subAddress of multiAddress.subAddresses) {
      const subSymbol = subAddress.split(':')[0];
      const TR = createElement({tagName: 'TR', subAddress});
      const TD = createElement({tagName: 'TD', parentNode: TR});
      const INPUT = createCustomCheckBox({
        parentNode: TD,
        label: subSymbol,
        checked: true,
        onclick: onchange,
        id: 'paymentRequest_' + subSymbol
      });
      TABLE_subAddresses.appendChild(TR);
      INPUTsBySubSymbol[subSymbol] = INPUT;
    }
    render(asset.address, asset.symbol);
  } else {*/
  //}
  
  const multiAddress = parseMultiAddress(asset.address, asset.symbol);
  if (multiAddress.isMultiAddress) {
    address = address.split(':')[1];
  }

  render(address, asset.symbol);
  onchange = () => render(address, asset.symbol);
  INPUT_amount.oninput = INPUT_amount.onpaste = INPUT_amount.onchange = INPUT_description.oninput = INPUT_description.onpaste = INPUT_description.onchange = onchange;
}

exports.open = parameters => {
  const {symbol} = parameters;
  
  // DEPRECATED
  //const TABLE_subAddresses = document.getElementById('paymentRequest-subAddresses');
  //TABLE_subAddresses.innerHTML = '';
  
  const INPUT_amount = document.getElementById('paymentRequest-amount');
  const INPUT_description = document.getElementById('paymentRequest-description');
  INPUT_amount.value = '';
  INPUT_description.value = '';
  const DIV_copy = document.getElementById('paymentRequest-copySuccess');
  DIV_copy.classList.remove('active');
  const DIV_symbol = document.getElementById('paymentRequest-symbol');
  DIV_symbol.innerHTML = symbol.toUpperCase();
  renderPaymentRequest(parameters);
  const BUTTON_copy = document.getElementById('paymentRequest-copyButton');
  const BUTTON_share = document.getElementById('paymentRequest-shareButton');
  BUTTON_copy.setAttribute('disabled',true);
  BUTTON_share.setAttribute('disabled',true);
  const DIV_url = document.getElementById('paymentRequest-url');
  DIV_url.innerText = '';
  DIV_url.style.display = 'none';
  DIV_url.style['text-decoration'] = 'none';
};
