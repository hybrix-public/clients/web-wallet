const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;

exports.rout = parameters => {
  // TODO what if no symbol exists
  const {symbol} = parameters;
  hybrix.view.swap('viewAsset', {symbol});
  hybrix.view.open('transactionComplete', parameters);
};

exports.open = function (parameters) {
  const DIV_progress = document.getElementById('transactionComplete-progress');
  progress(DIV_progress, {
    '{{TRANSACTIONCOMPLETE:PROGRESS_PREPARE}}': null,
    '{{TRANSACTIONCOMPLETE:PROGRESS_REVIEW}}': null,
    '{{TRANSACTIONCOMPLETE:PROGRESS_SEND}}': null    
  }, 2);

  const {transactionIDs, symbol} = parameters;

  const BUTTON_pending = document.getElementById('transactionComplete-pending');

  BUTTON_pending.onclick = () => hybrix.view.open('pending');

  const BUTTON_done = document.getElementById('transactionComplete-done');
  const SPAN_transactionID = document.getElementById('transactionComplete-transactionId');
  const DIV_message = document.getElementById('transactionComplete-message');
  const currency = hybrix.view.exec('settings', 'getCurrencyPreference');
  const exploreLink = `<a title="{{TRANSACTIONCOMPLETE:LINK_EXPLORE}}" target="_blank" class="viewAsset-exploreTransaction" href="https://explorer.hybrix.io/?symbol=${symbol}&transactionId=${transactionIDs}&currency=${currency}"></a>`;

  SPAN_transactionID.innerHTML = transactionIDs + ' ' + exploreLink;

  if (parameters.hasOwnProperty('referer') && parameters.referer) {
    const redirect = () => {
      const href = decodeURIComponent(parameters.referer) + '?txid=' + transactionIDs; // TODO decrypt & encrypt?
      window.location.href = href;
    };
    DIV_message.innerHTML = '{{TRANSACTIONCOMPLETE:REDIRECTION_NOTICE}}';
    setTimeout(redirect, 5000);
    BUTTON_done.onclick = redirect;
  } else {
    DIV_message.innerHTML = '';
    BUTTON_done.onclick = () => hybrix.view.pop();
  }
};
