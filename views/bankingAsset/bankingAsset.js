const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;
const {getIcon} = require('../../lib/icon');
const {getBalance,getAddress} = require('../../lib/assets');
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const {rout} = require('../banking/banking');
exports.rout = rout;

// const DEFAULT_OPTION = 'iban';
let bankingMethods = null;
let selectedCurrency;
let selectedMethod;
let selectedMethodDetails;
let selectedBalance = 0;
let selectedSource = null;

function addBankingMethods (object) {
  const SELECT_option = document.querySelector('.bankingAsset-option');
  let html = '';
  let cnt = 0;
  for (const key in object) {
    if (!selectedMethod && !cnt) { selectedMethod = object[key]; } // auto-select first entry
    html += `<option value="${key}" ${selectedMethod === key ? 'SELECTED' : ''}>${object[key].name}</option>`;
  }
  SELECT_option.innerHTML = html;
  SELECT_option.onchange = () => {
    const key = SELECT_option.value;
    selectedMethod = object[key];
  }
}

function render (bankingMethods) {
  const toggleNextButton = () => {
    const BUTTON_next = document.getElementById('bankingAsset-next');
    if (selectedCurrency) BUTTON_next.removeAttribute('disabled');
    else BUTTON_next.setAttribute('disabled', true);
  };
  const DIV_selectbox = document.getElementById('bankingAsset-selectbox');
  let cnt = 0;
  for (const currency in bankingMethods) {
    cnt++;
    const SPAN_icon = document.createElement('SPAN');
    SPAN_icon.innerHTML = getIcon(currency, icon => { SPAN_icon.innerHTML = icon; }); // get fallback icon directly, update real icon later
    const DIV_sub = document.createElement('DIV');
    const SPAN_currency = document.createElement('SPAN');
    SPAN_currency.innerHTML = currency;
    const SPAN_balance = document.createElement('SPAN');
    SPAN_balance.classList.add('bankingAsset-item-balance');
    DIV_sub.appendChild(SPAN_currency);
    DIV_sub.appendChild(SPAN_balance);
    const DIV = document.createElement('DIV');
    DIV.symbol = currency;
    DIV.classList.add('bankingAsset-item');
    DIV.appendChild(SPAN_icon);
    DIV.appendChild(DIV_sub);
    DIV_selectbox.appendChild(DIV);
    DIV.onclick = () => {
      let count = 0;
      for (const DIV_to of DIV_selectbox.children) {
        if (DIV.symbol !== DIV_to.symbol) {
          DIV_to.classList.remove('bankingAsset-selected');
        }
        ++count;
      }
      DIV.classList.add('bankingAsset-selected');
      selectedCurrency = DIV.symbol.toLowerCase();
      addBankingMethods(bankingMethods[DIV.symbol.toLowerCase()]);
      toggleNextButton();
    };
    // put balance in individual select boxes
    const balance = getBalance(currency);
    if (isNaN(balance)) hybrix.view.pop();
    SPAN_balance.innerHTML = isNaN(balance)?'':prettyPrintAmount(balance, currency);
    selectedBalance = balance;
    selectedSource = getAddress(currency);
  }
  // if there is only one currency in the list, auto click it after 1 second
  if (cnt === 1) {
    const DIV_to = DIV_selectbox.children[0];
    DIV_to.classList.add('bankingAsset-selected');
    selectedCurrency = DIV_to.symbol.toLowerCase();
    addBankingMethods(bankingMethods[DIV_to.symbol.toLowerCase()]);
    toggleNextButton();
  }
}

exports.open = (parameters) => {
  const DIV_message = document.getElementById('bankingAsset-message');
  const BUTTON_cancel = document.getElementById('bankingAsset-cancel');
  const BUTTON_next = document.getElementById('bankingAsset-next');
  const SPAN_action = document.getElementById('bankingAsset-action');
  const DIV_progress = document.getElementById('bankingAsset-progress');

  progress(DIV_progress, {
    '{{BANKINGASSET:PROGRESS_DETAILS}}': () => hybrix.view.pop(),
    '{{BANKINGASSET:PROGRESS_AMOUNT}}': null,
    '{{BANKINGASSET:PROGRESS_REVIEW}}': null,
    '{{BANKINGASSET:PROGRESS_TRANSACT}}': null
  }, 0);

  BUTTON_next.setAttribute('disabled', true);

  const DIV_selectbox = document.getElementById('bankingAsset-selectbox');
  DIV_selectbox.innerHTML = '';
  addBankingMethods({}); // empty the dropdown list

  let translatedAction = parameters.hasOwnProperty('action') && parameters.action ? parameters.action : '{{BANKINGASSET:ACTION_TRANSFER}}';
  switch (translatedAction) {
    case 'deposit': translatedAction='{{BANKINGASSET:ACTION_DEPOSIT}}'; break;
    case 'withdraw': translatedAction='{{BANKINGASSET:ACTION_WITHDRAW}}'; break;
    case 'transfer': translatedAction='{{BANKINGASSET:ACTION_TRANSFER}}'; break;
  }
  SPAN_action.innerText = translatedAction;

  render(parameters.bankingMethods);

  BUTTON_cancel.onclick = () => hybrix.view.pop();
  BUTTON_next.onclick = () => {
    const details = {bankingCurrency:selectedCurrency, bankingMethod:selectedMethod, balance: selectedBalance, ...parameters};
    if (selectedMethod.hasOwnProperty('type') && selectedMethod.type === 'website') {
      details.target = selectedSource;
      details.path = selectedMethod.hasOwnProperty('paths') && selectedMethod.paths.hasOwnProperty(parameters.action) ? selectedMethod.paths[parameters.action] : '';
      hybrix.view.open('bankingWebsite', details);
    } else {
      if (parameters.action === 'deposit') {
        details.source = '[{{BANKINGASSET:BANK_ACCOUNT}}]';
        details.target = selectedSource;
        hybrix.view.open('bankingAmount', details);
      } else {
        details.source = selectedSource;
        hybrix.view.open('bankingDetails', details);
      }
    }
  }
}

