const hybrix = require('../../lib/hybrix').hybrix;

exports.open = parameters => {
  const {credentials, symbols} = parameters;
  document.getElementById('createWalletConfirm-continue').onclick = () => hybrix.view.swap('login', {user:credentials.username, pass:credentials.password, symbols, newWallet: true});
  document.getElementById('createWalletConfirm-back').onclick = () => hybrix.view.swap('createWalletReview', {credentials, symbols});
};
