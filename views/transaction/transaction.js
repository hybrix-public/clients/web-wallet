const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;
const decode = require('../../common/compress-unified-address').decode;

const {saveToContacts, findContact, displayContact, findEntries} = require('../../lib/contacts');
const Decimal = require('../../lib/decimal').Decimal;
const {parseMultiAddress, cleanPrefix} = require('../../lib/multi-address');
const {createElement, createCustomCheckBox} = require('../../lib/html');
const {addEventListener} = require('../../lib/assets');
const {addAsset} = require('../../lib/assets');
let maxSendableAmount = 0;

exports.rout = parameters => {
  const {symbol} = parameters;
  if (symbol) hybrix.view.swap('viewAsset', {symbol});
  else hybrix.view.swap('listAssets', {symbol});
  hybrix.view.open('transaction', parameters);
};

function trimTrailingZeroes (x) {
  x = String(x);
  let [head, tail] = x.split('.');
  if (!tail) return head; // '123' -> '123'
  tail = tail.replace(/0+$/, ''); // '1230000...' -> '123'
  if (head === '') head = '0'; // '.123' -> '0.123'
  if (tail === '') return head; // '123.0...' -> '123'
  return head + '.' + tail; // '123.456000...' -> '123.456'
}

function resetSaveToContacts () {
  const DIV_saveToAddressBook = document.getElementById('saveToAddressBook');
  const INPUT_saveToAddressBookConfirm = document.getElementById('saveToAddressBookConfirm');
  const DIV_saveToAddressBookData = document.getElementById('saveToAddressBookData');
  const INPUT_contactName = document.getElementById('transaction-contact-name');
  const INPUT_contactLabel = document.getElementById('transaction-contact-label');
  DIV_saveToAddressBook.style.display = 'none';
  DIV_saveToAddressBookData.style.display = 'none';
  INPUT_saveToAddressBookConfirm.checked = false;
  INPUT_contactName.value = '';
  INPUT_contactLabel.value = '';
}

const isSwap = (target, multiAddress, asset) => {
  if (!target.includes(':')) return [];
  const subTargetSymbols = target.split(',').map(subTarget => subTarget.split(':')[0]);
  if (!multiAddress.isMultiAddress) return subTargetSymbols;
  const swapSubTargetSymbols = subTargetSymbols.filter(subTargetSymbol => !asset.symbols.includes(subTargetSymbol));
  if (swapSubTargetSymbols.length !== subTargetSymbols.length) return []; // there are dirctly transferable subsymbols that do not require a swap
  return swapSubTargetSymbols;
};

const getSubAddressFromUnifiedAddress = (symbol, target) => { // 'symbol:a,other:b' -> 'a' ,/ "other:a,symbol:b" -> 'b'
  const possiblePackedTarget = target.split(':').slice(1).join(':'); // 'unisymbol:packedmultitarget' -> 'packedmultitarget'
  const unpackedTarget = decode(possiblePackedTarget) || target; // tryUnpacking a compressed multi address, otherwise use target
  if (typeof unpackedTarget === 'string' && (unpackedTarget.startsWith(symbol + ':') || unpackedTarget.includes(',' + symbol + ':'))) {
    for (const subAddress of unpackedTarget.split(',')) {
      if (subAddress.startsWith(symbol + ':')) return subAddress;
    }
  } else return null;
};

const trySwap = (target, multiAddress, asset, symbol, amount) => {
  const DIV_contact = document.getElementById('transaction-contact');
  const DIV_generalError = document.getElementById('transaction-general-errorMessage');
  const BUTTON_send = document.getElementById('transaction-next');
  const INPUT_target = document.getElementById('modal-send-target');
  const INPUT_saveToAddressBookConfirm = document.getElementById('saveToAddressBookConfirm');
  const INPUT_contactName = document.getElementById('transaction-contact-name');
  const INPUT_contactLabel = document.getElementById('transaction-contact-label');

  const contact = {symbol, address: target, contactId: INPUT_contactName.value.trim(), label: INPUT_contactLabel.value.trim()};
  if (INPUT_saveToAddressBookConfirm.checked) {
    saveToContacts(contact, () => {}, () => {}); // TODO: callbacks needed here?
    resetSaveToContacts();
    DIV_contact.innerHTML = displayContact(contact);
  }

  const [targetSymbol, targetAddress] = target.split(':'); // TODO handle unified assets
  hybrix.lib.rout({query: `/e/swap/deal/estimate/${symbol}/${targetSymbol}`},
    estimate => {
      BUTTON_send.innerHTML = '{{TRANSACTION:BUTTON_NEXT}}';
      BUTTON_send.removeAttribute('disabled');
      hybrix.view.open('swapReview', {fromSymbol: symbol, toSymbol: targetSymbol, amount, target: targetAddress, estimate, swap: true});
    },
    error => {
      console.error('estimate error', error);
      INPUT_target.classList.add('error');
      DIV_generalError.innerHTML = '{{TRANSACTION:ERROR_SWAP_NOT_POSSIBLE}}'; // TODO support this
      DIV_generalError.style.display = 'block';
    }
  );
};

function sendTransaction (source, target, symbol, asset, multiAddress, description, parameters) {
  const DIV_spinner = document.getElementById('transaction-spinner');
  const DIV_contact = document.getElementById('transaction-contact');
  const INPUT_contactName = document.getElementById('transaction-contact-name');
  const INPUT_contactLabel = document.getElementById('transaction-contact-label');
  const INPUT_saveToAddressBookConfirm = document.getElementById('saveToAddressBookConfirm');
  const BUTTON_send = document.getElementById('transaction-next');
  const DIV_generalError = document.getElementById('transaction-general-errorMessage');
  const INPUT_fee = document.getElementById('transaction-fee');
  const INPUT_amount = document.getElementById('modal-send-amount');
  const INPUT_description = document.getElementById('modal-send-description');
  const INPUT_attachment = document.getElementById('transaction-attachment');
  const INPUT_fromOffset = document.getElementById('transaction-fromOffset');
  const INPUT_target = document.getElementById('modal-send-target');
  const DIV_fromAddressBook = document.getElementById('transaction-contacts-button');
  const DIV_fromQR = document.getElementById('transaction-qr-scanner-button');

  DIV_spinner.style.display = 'block';

  const amount = INPUT_amount.value;
  const offset = isNaN(INPUT_fromOffset.value) ? 0 : Number(INPUT_fromOffset.value);
  const transaction = {symbol, source, target, amount, offset};
  transaction.source = source;

  if (INPUT_fee.value !== '') transaction.fee = INPUT_fee.value;
  if (INPUT_description.value.trim() !== '') transaction.description = INPUT_description.value.trim();
  if (INPUT_attachment.value.trim() !== '') transaction.attachment = INPUT_attachment.value.trim();

  const contact = {symbol, address: target, contactId: INPUT_contactName.value.trim(), label: INPUT_contactLabel.value.trim()};
  if (INPUT_saveToAddressBookConfirm.checked) {
    saveToContacts(contact, () => {}, () => {}); // TODO: callbacks needed here?
    resetSaveToContacts();
    DIV_contact.innerHTML = displayContact(contact);
  }

  function rawTxFailed(errorMessage) {
    DIV_spinner.style.display = 'none';
    DIV_fromAddressBook.classList.remove('disabled');
    DIV_fromQR.classList.remove('disabled');
    BUTTON_send.removeAttribute('disabled');
    DIV_generalError.innerHTML = errorMessage;
    DIV_generalError.style.display = 'block';
  }
  
  function rawTxSuccess(rawTransaction, notice) {
    DIV_spinner.style.display = 'none';
    DIV_fromAddressBook.classList.remove('disabled');
    DIV_fromQR.classList.remove('disabled');
    BUTTON_send.removeAttribute('disabled');
    DIV_generalError.style.display = 'none';
    hybrix.view.open('transactionReview', {symbol, transaction, rawTransaction, referer: parameters.referer, toOffset: parameters.toOffset, notice});
  }
  
  hybrix.lib.rawTransaction(transaction, rawTransaction => {
    rawTxSuccess(rawTransaction);
  }, errorMessage => {
    const DECREASE_MAX = 0.995; // max amount to send is decreased by 0.5% if needed when user is trying to send out MAX wallet funds
    // These courses of action are tried if a normal rawTx generation fails.
    hybrix.view.close('transactionReview');
    if (errorMessage.includes('is not a valid address')) {
      const targetSubAddress = getSubAddressFromUnifiedAddress(symbol, target); // 'symbol:a,other:b' -> 'a'
      if (targetSubAddress) {
        // DEBUG: console.log(' >>> trying subAddress... ');
        sendTransaction(source, targetSubAddress, symbol, asset, multiAddress, description, parameters);
      } else if (isSwap(target, multiAddress, asset).length !== 0) {
        trySwap(target, multiAddress, asset, symbol, amount);
      } else rawTxFailed(errorMessage);
    } else if (errorMessage.includes('Insufficient funds available for transaction') &&
        amount >= maxSendableAmount && (maxSendableAmount / amount) >= DECREASE_MAX) { // MAX has been used, but fees are rising: try with slightly smaller amount
      transaction.amount = transaction.amount * DECREASE_MAX;
      hybrix.lib.rawTransaction(transaction, rawTransaction => {
        // DEBUG: console.log(' >>> trying lower MAX amount... ');
        rawTxSuccess(rawTransaction, '{{TRANSACTION:REVIEW_MAX_NOTICE}}');
      }, errorMessageRetried => {
        rawTxFailed(errorMessageRetried);
      });
    } else rawTxFailed(errorMessage);
  });
}

function validateAmount () {
  const INPUT_amount = document.getElementById('modal-send-amount');
  if (INPUT_amount.value === '') return {isValid: false, errorMessage: '{{TRANSACTION:ERROR_NO_EMPTY_AMOUNT}}'};
  let amount;
  try {
    amount = new Decimal(INPUT_amount.value);
  } catch (e) {
    return {isValid: false, errorMessage: '{{TRANSACTION:ERROR_EXPECT_VALID_AMOUNT}}'};
  }
  if (amount.lt(0)) return {isValid: false, errorMessage: '{{TRANSACTION:ERROR_NO_NEGATIVE_AMOUNT}}'};
  return {isValid: true};
}

function validateTarget (address) {
  const INPUT_target = document.getElementById('modal-send-target');
  return INPUT_target.value !== ''
    ? {isValid: true}
    : {isValid: false, errorMessage: '{{TRANSACTION:ERROR_NO_EMPTY_TARGET}}'};
}

function validateInput () {
  const BUTTON_send = document.getElementById('transaction-next');
  if (!validateAmount().isValid || !validateTarget().isValid) {
    BUTTON_send.setAttribute('disabled',true);
    return false;
  } else {
    BUTTON_send.removeAttribute('disabled');
    return true;
  }
}

const renderSendAsset = parameters => asset => {
  const symbol = asset.symbol;
  const INPUT_target = document.getElementById('modal-send-target');
  const INPUT_amount = document.getElementById('modal-send-amount');
  const INPUT_description = document.getElementById('modal-send-description');
  const INPUT_attachment = document.getElementById('transaction-attachment');
  const INPUT_fromOffset = document.getElementById('transaction-fromOffset');
  const BUTTON_max = document.getElementById('transaction-max-button');
  const DIV_contact = document.getElementById('transaction-contact');
  const DIV_saveToAddressBook = document.getElementById('saveToAddressBook');
  const DIV_targetError = document.getElementById('transaction-target-errorMessage');
  const DIV_generalError = document.getElementById('transaction-general-errorMessage');
  const DIV_sendNote = document.getElementById('transaction-send-note');
  const DIV_fromAddressBook = document.getElementById('transaction-contacts-button');
  const DIV_fromQR = document.getElementById('transaction-qr-scanner-button');
  const BUTTON_send = document.getElementById('transaction-next');
  const INPUT_fee = document.getElementById('transaction-fee');
  const TABLE_subAddresses = document.getElementById('transaction-subAddresses');
  const DIV_spinner = document.getElementById('transaction-spinner');
  DIV_spinner.style.display = 'block';

  INPUT_fromOffset.value = parameters.fromOffset?parameters.fromOffset:'';

  DIV_sendNote.innerHTML = asset.notes && asset.notes.send ? asset.notes.send : '';
  if(parameters.text) {
    DIV_sendNote.innerHTML = parameters.text + ' ' + DIV_sendNote.innerHTML;
  }

  INPUT_fee.placeholder = `{{TRANSACTION:INPUT_CUSTOM_FEE_A}}${trimTrailingZeroes(asset.fee)}{{TRANSACTION:INPUT_CUSTOM_FEE_B}}`;

  if (typeof asset.features === 'object' && asset.features.attachment) INPUT_attachment.removeAttribute('disabled');
  else INPUT_attachment.setAttribute('disabled', true); // Disable attachment option if feature not available.
  // clean errors
  DIV_targetError.style.display = 'none';
  DIV_generalError.style.display = 'none';
  INPUT_target.classList.remove('error');

  TABLE_subAddresses.innerHTML = '';
  let multiAddress;
  let INPUTsbySubSymbol = {};
  let subAddresses = {};

  // The following addresses a race condition. There may be a more elegant way to do this! (TODO: FIXME)
  const timeout = (ms) => new Promise(resolve => setTimeout(resolve, ms));
  (async () => {
    for(let i = 0; i <= 16; i++) {
      if (typeof asset.address !== 'undefined' && asset.address) {
        i = 99;
        DIV_spinner.style.display = 'none';
        multiAddress = parseMultiAddress(asset.address, symbol);
        if (multiAddress.isMultiAddress) {
          for (const subAddress of multiAddress.subAddresses) {
            const subSymbol = subAddress.split(':')[0];
            subAddresses[subSymbol] = subAddress;
            const TR = createElement({tagName: 'TR', subAddress});
            const TD = createElement({tagName: 'TD', parentNode: TR});
            const INPUT_subSymbol = createCustomCheckBox({parentNode: TD, label: subSymbol, checked: true, onclick: () => { BUTTON_max.removeAttribute('disabled'); }});
            INPUTsbySubSymbol[subSymbol] = INPUT_subSymbol;
            TABLE_subAddresses.appendChild(TR);
          }
          /* DEPRECATED: Used to be expanded addresses, now this is simply unified.
          let source = '';
          for (const subSymbol in INPUTsbySubSymbol) {
            if (INPUTsbySubSymbol[subSymbol].checked) {
              if (source !== '') source += ',';
              source += subAddresses[subSymbol];
            }
          }
          return source; */
          source = parameters.source ? parameters.source : asset.address;
        } else source = parameters.source ? parameters.source : asset.address;

        const target = INPUT_target.value || null;
        const offset = parameters.fromOffset?parameters.fromOffset:0;
        const bytes = INPUT_attachment.value.length; // attachment length bytes
        getTransactionDetails(symbol, source, target, offset, bytes,
          details => {
            maxSendableAmount = trimTrailingZeroes(details['max-amount']);
            SPAN_balance_value.innerText = maxSendableAmount;
            SPAN_balance_symbol.innerText = symbol.toUpperCase();
            BUTTON_max.removeAttribute('disabled');
          }, error => {
            console.error('Failed to determine max transferable amount!', error);
            SPAN_balance_value.innerText = '{{TRANSACTION:N/A}}';
            SPAN_balance_symbol.innerText = '';
          }
        )
      } else await timeout(1000);
    }
  })();
  
  const onChange = (inputTarget) => {
    const cleanTarget = multiAddress.isMultiAddress ? inputTarget : cleanPrefix(inputTarget, asset.symbol);
    if (cleanTarget !== INPUT_target.value) INPUT_target.value = cleanTarget;
    if (cleanTarget.length > 2) {
      const contact = cleanTarget.length > 2 ? findContact(symbol, cleanTarget) : null;
      if (multiAddress.isMultiAddress && !inputTarget.includes(':')) {
        INPUT_target.classList.add('error');
        let html = '{{TRANSACTION:MISSING_PREFIX}} {{TRANSACTION:DID_YOU_MEAN}}';
        for (const subSymbol in asset.symbols) {
          const subAddress = subSymbol + ':' + INPUT_target.value;
          html += `<br><a class="transaction-selectPrefix" onclick="document.getElementById('modal-send-target').value='${subAddress}';document.getElementById('modal-send-target').onchange();document.getElementById('transaction-target-errorMessage').style.display='none';"><span>{{TRANSACTION:SELECT}}</span>${subAddress}</a>`;
        }
        INPUT_target.classList.remove('error');
        DIV_targetError.innerHTML = html;
        DIV_targetError.style.display = 'block';
      }

      if (inputTarget === asset.address) {
        DIV_contact.innerHTML = '👤 {{TRANSACTION:CONTACT_SELF}}';
        DIV_saveToAddressBook.style.display = 'none';
        INPUT_target.classList.remove('error');
        DIV_targetError.style.display = 'none';
        DIV_targetError.innerHTML = '';
      } else if (!contact) {
        if (cleanTarget.length > 2) {
          const entries = findEntries(cleanTarget, symbol);
          DIV_contact.innerHTML = '';
          if (entries.length) {
            for (const entry of entries) {
              const DIV_entry = document.createElement('DIV');
              DIV_entry.innerText = displayContact(entry);
              DIV_entry.className = 'transaction-entrySuggestion';
              DIV_entry.onclick = () => {
                INPUT_target.value = entry.address;
                onChange(entry.address);
              };
              DIV_contact.appendChild(DIV_entry);
            }
            DIV_saveToAddressBook.style.display = 'none';
          } else {
            DIV_contact.innerHTML = '';
            DIV_saveToAddressBook.style.display = 'inline-block';
          }
        } else {
          DIV_contact.innerHTML = '';
          DIV_saveToAddressBook.style.display = 'none';
        }
        INPUT_target.classList.remove('error');
        DIV_targetError.style.display = 'none';
        DIV_targetError.innerHTML = '';
      } else { // attempt to match address to single entry or make clickable
        INPUT_target.classList.remove('error');
        DIV_targetError.style.display = 'none';
        DIV_targetError.innerHTML = '';
        DIV_saveToAddressBook.style.display = 'none';
        DIV_contact.innerHTML = '';
        const DIV_entry = document.createElement('DIV');
        DIV_entry.innerHTML = displayContact(contact);
        if(cleanTarget.toLowerCase() !== contact.address.toLowerCase()) {
          DIV_entry.className = 'transaction-entrySuggestion';
          DIV_entry.onclick = () => {
            INPUT_target.value = contact.address;
            onChange(contact.address);
          };
        }
        DIV_contact.appendChild(DIV_entry);
      }
    } else {
      resetSaveToContacts();
      DIV_contact.innerHTML = '';
      if (cleanTarget.length === 0) {
        INPUT_target.classList.add('error');
        DIV_targetError.innerHTML = '{{TRANSACTION:PLEASE_ENTER_TARGET}}';
        DIV_targetError.style.display = 'block';
      }
    }
    validateInput();
  };

  let prevValue = '';
  INPUT_target.onchange = INPUT_target.oninput = INPUT_target.onpaste = () => {
    const inputTargetValue = INPUT_target.value;
    if (inputTargetValue !== prevValue) {
      // check to see if we actually pasted a long value
      if (Math.abs(inputTargetValue.length-prevValue.length)>12) {
        INPUT_target.style.background = 'none';
        INPUT_target.style.backgroundClip = 'none';
        INPUT_target.style.webkitBackgroundClip = 'none';
        INPUT_target.style.webkitTextFillColor = 'black';
      }
      prevValue = inputTargetValue;
      BUTTON_max.removeAttribute('disabled');
      onChange(inputTargetValue);
    }
  };

  DIV_fromQR.onclick = () => {
    if (!DIV_fromQR.classList.contains('disabled')) {
      hybrix.view.open('qrScanner', {
        type: 'transaction',
        dataCallback: result => {
          onChange(result.address);
          if (result.amount !== null) INPUT_amount.value = result.amount;
          BUTTON_max.removeAttribute('disabled');
        }
      });
    }
  }

  BUTTON_send.onclick = () => {
    if (validateInput()) {
      BUTTON_max.setAttribute('disabled',true);
      BUTTON_send.setAttribute('disabled',true);
      DIV_fromAddressBook.classList.add('disabled');
      DIV_fromQR.classList.add('disabled');
      const target = INPUT_target.value;
      const description = INPUT_description.value.trim();
      sendTransaction(source, target, symbol, asset, multiAddress, description, parameters);
    }
  };

  INPUT_amount.onchange = INPUT_amount.oninput = INPUT_amount.onpaste = () => {
    INPUT_amount.value = INPUT_amount.value.replace(',','.');
    const {isValid, errorMessage} = validateAmount();
    const DIV_error = document.getElementById('transaction-amount-errorMessage');
    if (isValid) {
      INPUT_amount.classList.remove('error');
      DIV_error.style.display = 'none';
    } else {
      DIV_error.innerHTML = errorMessage;
      DIV_error.style.display = 'block';
      INPUT_amount.classList.add('error');
    }
    BUTTON_max.removeAttribute('disabled');
    validateInput();
  };

  const SPAN_balance_value = document.getElementById('transaction-balance-value');
  const SPAN_balance_symbol = document.getElementById('transaction-balance-symbol');
  SPAN_balance_value.innerText = '...';
  SPAN_balance_symbol.innerText = '';

  BUTTON_max.setAttribute('disabled',true);
  BUTTON_max.onclick = () => {
    const DIV_error = document.getElementById('transaction-amount-errorMessage');
    BUTTON_max.setAttribute('disabled',true);
    if (maxSendableAmount > 0) {
        DIV_error.style.display = 'none';
        INPUT_amount.value = maxSendableAmount;
        BUTTON_send.removeAttribute('disabled');
    } else {
        INPUT_amount.value = '';
        BUTTON_max.removeAttribute('disabled');
        DIV_error.innerHTML = '{{TRANSACTION:WARN_CANNOT_DETERMINE_MAX}}';
        DIV_error.style.display = 'block';
    }
  };

  validateInput();
};

function getTransactionDetails (symbol, source, target, offset, bytes, dataCallback, errorCallback) {
  hybrix.lib.getPublicKey({symbol, offset}, publicKey => {
    hybrix.lib.rout(
      {query: `/a/${symbol}/transaction-details/${source}/0/${target}/${bytes}/${publicKey}`, channel: 'y'},
      dataCallback, errorCallback
    );
  }, errorCallback);
}

function selectSymbol (symbol, parameters) {
  const DIV_symbol = document.getElementById('transaction-symbol');
  const DIV_contact = document.getElementById('transaction-contact');
  const INPUT_target = document.getElementById('modal-send-target');
  const DIV_fromAddressBook = document.getElementById('transaction-contacts-button');
  const DIV_saveToAddressBook = document.getElementById('saveToAddressBook');
  const DIV_targetError = document.getElementById('transaction-target-errorMessage');

  const target = INPUT_target.value;

  const contact = findContact(symbol, target);
  DIV_contact.innerHTML =  contact ? displayContact(contact) : '';
  DIV_symbol.innerHTML = symbol ? symbol.toUpperCase() : '...';

  if (symbol) addAsset(symbol, renderSendAsset(parameters), console.error);
  const contactSelected = (contact) => {
    DIV_contact.innerHTML = displayContact(contact);
    INPUT_target.value = contact.address;
    INPUT_target.onchange(); // hack: fire off the onchange event to update fields and buttons
    DIV_saveToAddressBook.style.display = 'none';
  };

  DIV_fromAddressBook.onclick = () => {
    if (!DIV_fromAddressBook.classList.contains('disabled')) {
      INPUT_target.classList.remove('error');
      DIV_targetError.innerHTML = '';
      DIV_targetError.style.display = 'none';
      hybrix.view.open('contactsSelect', {symbol, callback: contactSelected});
    }
  }
}

let symbols;

exports.once = function () {
  const SELECT_symbol = document.getElementById('transaction-selectSymbol');
  addEventListener('addAsset', symbol => {
    const OPTION = createElement({tagName: 'OPTION', innerHTML: symbol, parentNode: SELECT_symbol});
    if (!(symbols instanceof Array) || typeof symbol !== 'string' || symbols.includes(symbol)) OPTION.removeAttribute('disabled', true);
    else OPTION.setAttribute('disabled', true);
  });
  addEventListener('removeAsset', symbol => {
    for (const OPTION of SELECT_symbol.children) {
      if (OPTION.value === symbol) SELECT_symbol.removeChild(OPTION);
    }
  });
};

function render (parameters) {
  const {symbol, source, target, amount, description, attachment, referer, fromOffset, toOffset, targetHide, contact} = parameters;

  const DIV_sendNote = document.getElementById('transaction-send-note');
  const SELECT_symbol = document.getElementById('transaction-selectSymbol');
  const INPUT_target = document.getElementById('modal-send-target');
  const DIV_fromAddressBook = document.getElementById('transaction-contacts-button');
  const DIV_fromQR = document.getElementById('transaction-qr-scanner-button');
  const DIV_targetButtons = document.getElementById('transaction-field-elements-A');
  const INPUT_amount = document.getElementById('modal-send-amount');
  const INPUT_description = document.getElementById('modal-send-description');
  const BUTTON_max = document.getElementById('transaction-max-button');
  const DIV_error = document.getElementById('transaction-amount-errorMessage');
  const DIV_target_and_elements = document.getElementById('modal-send-target-and-elements');
  const BUTTON_more = document.getElementById('transaction-more-button');
  const DIV_more = document.getElementById('transaction-more-wrapper');
  const BUTTON_send = document.getElementById('transaction-next');
  const INPUT_fee = document.getElementById('transaction-fee');
  const INPUT_attachment = document.getElementById('transaction-attachment');
  const TABLE_subAddresses = document.getElementById('transaction-subAddresses');
  const DIV_saveToAddressBook = document.getElementById('saveToAddressBook');
  const DIV_saveToAddressBookData = document.getElementById('saveToAddressBookData');
  const INPUT_saveToAddressBookConfirm = document.getElementById('saveToAddressBookConfirm');
  const INPUT_contactName = document.getElementById('transaction-contact-name');
  const INPUT_contactLabel = document.getElementById('transaction-contact-label');
  const DIV_progress = document.getElementById('transaction-progress');
  const DIV_contact = document.getElementById('transaction-contact');

  const showSymbolSelect = typeof symbol !== 'string' || symbol.includes(',');

  progress(DIV_progress, {
    '{{TRANSACTION:PROGRESS_PREPARE}}': null,
    '{{TRANSACTION:PROGRESS_REVIEW}}': null,
    '{{TRANSACTION:PROGRESS_SEND}}': null
  }, 0);

  DIV_error.style.display = 'none';
  DIV_sendNote.innerHTML = '';
  DIV_fromAddressBook.classList.remove('disabled');
  DIV_fromQR.classList.remove('disabled');

  INPUT_amount.value = isNaN(amount) ? '' : amount;

  if (targetHide) {
    DIV_target_and_elements.style.display = 'none';
  } else {
    DIV_target_and_elements.style.display = 'block';
  }

  //BUTTON_more.style.display = attachment ? 'none' : 'block';
  //BUTTON_more.style.display = 'none';
  DIV_more.style.display = attachment ? 'block' : 'none';

  BUTTON_send.innerHTML = '{{TRANSACTION:BUTTON_NEXT}}';
  BUTTON_send.setAttribute('disabled',true);

  INPUT_fee.placeholder = '{{TRANSACTION:PLACEHOLDER_FEE}}';
  INPUT_fee.value = '';
  if (typeof description !== 'undefined') { INPUT_description.value = decodeURIComponent(description) || ''; } else INPUT_description.value = '';
  if (typeof attachment !== 'undefined') { INPUT_attachment.value = decodeURIComponent(attachment) || ''; } else INPUT_attachment.value = '';

  TABLE_subAddresses.innerHTML = '';

  BUTTON_more.onclick = () => {
    BUTTON_more.style.display = 'none';
    DIV_more.style.display = 'block';
  };

  resetSaveToContacts();
  DIV_saveToAddressBookData.style.display = 'none';
  INPUT_saveToAddressBookConfirm.onclick = () => {
    DIV_saveToAddressBookData.style.display = INPUT_saveToAddressBookConfirm.checked ? 'block' : 'none';
  };

  if (referer) {
    INPUT_amount.setAttribute('disabled', true);
    INPUT_target.style.setProperty('display', 'none', 'important');
    DIV_targetButtons.style.display = 'none';
    BUTTON_max.style.display = 'none';
  } else {
    INPUT_amount.removeAttribute('disabled');
    INPUT_target.style.display = 'block';
    BUTTON_max.style.display = 'inline-block';
    DIV_targetButtons.style.display = 'block';
    INPUT_target.style.display = 'block';
  }

  if (showSymbolSelect) {
    SELECT_symbol.style.display = 'block';
    symbols = typeof symbol === 'string' ? symbol.split(',') : [];
    let first = true;
    for (const OPTION of SELECT_symbol.children) {
      if (first) first = false;
      else {
        const symbol = OPTION.value;
        if (typeof symbol !== 'string' || symbols.includes(symbol)) OPTION.removeAttribute('disabled', true);
        else OPTION.setAttribute('disabled', true);
      }
    }
    SELECT_symbol.onchange = () => {
      const symbol = SELECT_symbol.value;
      const index = symbols.indexOf(symbol);
      INPUT_target.value = (target || '').split(',')[index];
      selectSymbol(symbol, parameters);
    };
    SELECT_symbol.value = symbols[0];
    selectSymbol(symbols[0], parameters);
    INPUT_target.value = (target || '').split(',')[0];
  } else {
    SELECT_symbol.style.display = 'none';
    INPUT_target.value = target || '';
    selectSymbol(symbol, parameters);
  }
  
  if (target) {
    let contactFind = findContact(symbol, target);
    if (contactFind) {
      contactHTML = displayContact(contactFind);
      DIV_contact.innerHTML = contactHTML;
    } else {
      DIV_saveToAddressBook.style.display = 'inline-block';
      if (contact) {
        INPUT_saveToAddressBookConfirm.checked = true;
        INPUT_contactName.value = contact.split(':')[0];
        INPUT_contactLabel.value = contact.split(':')[1]?contact.split(':')[1]:'';
        DIV_saveToAddressBookData.style.display = 'block';
      }
    }
  }

}

exports.open = function (parameters) {
  const {symbol, toOffset, fromOffset} = parameters;
  if (typeof toOffset === 'undefined' && typeof fromOffset === 'undefined') render(parameters);
  else { // TODO handle if symbol not defined
    hybrix.lib.getAddress({symbol, offset: fromOffset}, source => {
      hybrix.lib.getAddress({symbol, offset: toOffset}, target => {
        parameters.source = source;
        parameters.target = target;
        parameters.targetHide = true;
        render(parameters);
      }, console.error);
    }, console.error);
  }
};
