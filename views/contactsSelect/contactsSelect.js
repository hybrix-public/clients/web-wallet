const {contacts} = require('../../lib/contacts');
const {createElement} = require('../../lib/html');
const hybrix = require('../../lib/hybrix').hybrix;

function displayContact (contact) {
  return `👤 ${contact.contactId}${contact.label && contact.label !== '-' ? ' ➤ ' + contact.label : ''}`;
}

const onChange = (INPUT_search, DIV_contacts) => () => {
  const needle = INPUT_search.value.toLowerCase();
  for (let DIV_contact of [...DIV_contacts.children]) {
    DIV_contact.style.display = DIV_contact.getAttribute('data').toLowerCase().includes(needle) ? 'block' : 'none';
  }
};

exports.rout = parameters => {
  const {symbol} = parameters;
  hybrix.view.swap('viewAsset', {symbol});
  hybrix.view.open('contactsSelect', {
    symbol,
    callback: result => hybrix.view.open('transaction', {symbol, target: result.address})
  });
};

//  callback = (contactId,label,address) => {}
exports.open = parameters => {
  const {symbol, callback} = parameters;
  const DIV_contacts = document.getElementById('contactsSelect-contacts');
  DIV_contacts.innerHTML = '';

  const symbolToUpperCase = symbol.toUpperCase().split('.')[0]; // only use base symbol
  for (let contactId of Object.keys(contacts).sort()) {
    const contact = contacts[contactId];
    for (let entry of contact) {
      if (entry.hasOwnProperty(symbolToUpperCase)) {
        for (let label in entry[symbolToUpperCase]) {
          const address = entry[symbolToUpperCase][label];
          const DIV_contact = createElement({className: 'contactsSelect-contact',
            innerHTML: displayContact({contactId, label, address}),
            parentNode: DIV_contacts,
            onclick: () => {
              if (typeof callback === 'function') callback({contactId, label, address});
              hybrix.view.pop();
            }});
          DIV_contact.setAttribute('data', contactId + label);
        }
      }
    }
  }
  const INPUT_search = document.getElementById('contactsSelect-search');
  INPUT_search.keyup = INPUT_search.onchange = INPUT_search.oninput = INPUT_search.onpaste = onChange(INPUT_search, DIV_contacts);

  const BUTTON_cancel = document.getElementById('contactsSelect-cancel-button');
  BUTTON_cancel.onclick = () => hybrix.view.pop();
};
