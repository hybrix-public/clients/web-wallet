const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;
const {VIRTCHAR} = require('../allocate/allocate.js');

const placeholderText = 'Select symbol...';

exports.rout = parameters => {
  hybrix.view.swap('allocate');
  hybrix.view.open('allocatePreparePair', parameters);
};

function fillDropDowns(ledgers) {
  const SELECT_toSymbol = document.getElementById('allocatePreparePair-toSymbol');
  const SELECT_fromSymbol = document.getElementById('allocatePreparePair-fromSymbol');
  const placeholder = '<option disabled selected hidden>' + placeholderText + '</option>';
  SELECT_toSymbol.innerHTML = placeholder;
  SELECT_fromSymbol.innerHTML = placeholder;
  for (const symbol of ledgers) {
    const symbolUpperCase = symbol.toUpperCase();
    const OPTION1 = document.createElement('OPTION');
    OPTION1.innerHTML = OPTION1.value = symbolUpperCase;
    SELECT_fromSymbol.appendChild(OPTION1);
    const OPTION2 = document.createElement('OPTION');
    OPTION2.innerHTML = OPTION2.value = symbolUpperCase;
    SELECT_toSymbol.appendChild(OPTION2);
  }
}

exports.once = () => {
  const DIV_progress = document.getElementById('allocatePreparePair-progress');
  progress(DIV_progress, {
    Prepare: null,
    Allocate: null,
    Create: null
  }, 0);
};

exports.open = parameters => {
  const DIV_spinner = document.getElementById('allocatePreparePair-spinner');
  const DIV_mainUI = document.getElementById('allocatePreparePair');
  const DIV_message = document.getElementById('allocatePreparePair-message');
  const SELECT_toSymbol = document.getElementById('allocatePreparePair-toSymbol');
  const SELECT_fromSymbol = document.getElementById('allocatePreparePair-fromSymbol');
  const BUTTON_next = document.getElementById('allocatePreparePair-next');
  const BUTTON_cancel = document.getElementById('allocatePreparePair-cancel');

  SELECT_toSymbol.value = undefined;
  SELECT_toSymbol.selectedIndex = 0;
  SELECT_fromSymbol.value = undefined;
  SELECT_fromSymbol.selectedIndex = 0;
  DIV_message.innerText = '';

  DIV_spinner.style.display = 'block';
  DIV_mainUI.style.display = 'block';
  if (parameters.type === 'fiat') {
    hybrix.lib.rout({query: '/e/swap/deal/banking', regular: false, cache: Infinity },
      assetsObject => {
        const keys = Object.keys(assetsObject);
        let ledgers = [];
        for (const type of Object.keys(assetsObject)) {
          for (const symbol of assetsObject[type]) {
            ledgers.push(`${type} | ${symbol}`);
            if (ledgers.indexOf(`hybrix | ${symbol}`)===-1) {
              ledgers.push(`hybrix | ${symbol}`);
            }
          }
        }
        ledgers.sort();
        fillDropDowns(ledgers);
        DIV_spinner.style.display = 'none';
      },
      e => {
        DIV_mainUI.style.display = 'none';
        DIV_message.innerText = 'Error: Cannot retrieve supported virtual ledgers!';
        console.error(e);
      }
    );
  } else {
    hybrix.lib.rout({query: '/e/swap/deal/ledgers', regular: false, cache: Infinity },
      ledgers => {
        ledgers.sort();
        fillDropDowns(ledgers);
        DIV_spinner.style.display = 'none';
      },
      e => {
        DIV_mainUI.style.display = 'none';
        DIV_message.innerText = 'Error: Cannot retrieve supported digital ledgers!';
        console.error(e);
      }
    );
  }

  let toSelected, fromSelected;
  const handleChange = () => {
    const toVirtual = SELECT_toSymbol.value.split(' | ');
    const fromVirtual = SELECT_fromSymbol.value.split(' | ');
    if (parameters.type === 'fiat') {
      if (toVirtual[0].toLowerCase() !== 'hybrix' && fromVirtual[0].toLowerCase() !== 'hybrix') {
        if (toSelected === SELECT_toSymbol.value) {
          SELECT_toSymbol.value = `HYBRIX | ${fromVirtual[1]}`;
        } else if (fromSelected === SELECT_fromSymbol.value) {
          SELECT_fromSymbol.value = `HYBRIX | ${toVirtual[1]}`;
        }
      }
      if (toVirtual[0].toLowerCase() === 'hybrix' && fromVirtual[0].toLowerCase() === 'hybrix') {
        if (toSelected === SELECT_toSymbol.value) {
          SELECT_toSymbol.value = undefined;
          SELECT_toSymbol.selectedIndex = 0;
        } else if (fromSelected === SELECT_fromSymbol.value) {
          SELECT_fromSymbol.value = undefined;
          SELECT_fromSymbol.selectedIndex = 0;
        }
      }
    } else {
      if (toVirtual[0] === fromVirtual[0]) {
        if (toSelected === SELECT_toSymbol.value) {
          SELECT_toSymbol.value = undefined;
          SELECT_toSymbol.selectedIndex = 0;
        } else if (fromSelected === SELECT_fromSymbol.value) {
          SELECT_fromSymbol.value = undefined;
          SELECT_fromSymbol.selectedIndex = 0;
        }
      }
    }
    if (SELECT_toSymbol.value && SELECT_toSymbol.value !== placeholderText && SELECT_fromSymbol.value && SELECT_fromSymbol.value !== placeholderText && SELECT_toSymbol.value !== SELECT_fromSymbol.value) {
      BUTTON_next.removeAttribute('disabled');
      DIV_message.innerText = 'Nice pair! This means you will be accepting '+SELECT_fromSymbol.value+' in exchange for '+SELECT_toSymbol.value+'.';
    } else {
      BUTTON_next.setAttribute('disabled', true);
      DIV_message.innerText = 'Please select two different symbols to create an allocation pair.';
    }
    toSelected = SELECT_toSymbol.value;
    fromSelected = SELECT_fromSymbol.value;
  };

  SELECT_toSymbol.onchange = handleChange;
  SELECT_fromSymbol.onchange = handleChange;

  BUTTON_next.onclick = () => {
    let toSymbol = toSelected.toLowerCase().replace(/ /g,'');
    let fromSymbol = fromSelected.toLowerCase().replace(/ /g,'');
    if(parameters.type === 'fiat') {
      if (toSymbol.split('|')[0] === 'hybrix') {
        toSymbol = toSymbol.split('|')[1];
        fromSymbol = `${VIRTCHAR}${fromSymbol.replace('|','.')}`;
        hybrix.view.open('allocateManageVirt', {fromSymbol, toSymbol, createNew:true});
      } else {
        toSymbol = `${VIRTCHAR}${toSymbol.replace('|','.')}`;
        fromSymbol = fromSymbol.split('|')[1];
        hybrix.view.open('allocateManageVirt', {fromSymbol, toSymbol, createNew:true});
      }
    } else {
      hybrix.view.open('allocateCreatePair', {fromSymbol, toSymbol, type:parameters.type});
    }
  };
  BUTTON_cancel.onclick = () => {
    hybrix.view.pop();
  };
};
