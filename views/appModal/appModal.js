const hybrix = require('../../lib/hybrix').hybrix;
const spinner = require('../../lib/spinner');
const { activate } = require('../../common');

exports.rout = (properties) => {
  hybrix.view.open('appModal',properties);
  hybrix.view.swap('app',properties.parent);
}

exports.open = (properties) => {
  // put a title on the app or modal, in case that is not done by the app maker
  const DIV_content = document.getElementById('app-modal-content');
  spinner.apply(DIV_content);

  if(properties.parent && properties.parent.app) {
    const DIV_header = document.getElementById('app-modal-header');
    DIV_header.innerText = properties.title;
    hybrix.lib.rout({query: `/e/${properties.parent.app}/modal.css`, channel: false, regular: false}, result => {
      const STYLE_css = document.getElementById('app-modal-stylesheet');
      STYLE_css.innerText = result;
      hybrix.lib.rout({query: `/e/${properties.parent.app}/modal.js`, channel: false, regular: false}, result => {
        // export the functionality to a pre-prepared var
        const windowActivation = '; if (typeof window !== "undefined") { window.deterministic = startApplication; }';
        let startApplication = activate(result + windowActivation);
        spinner.remove(DIV_content);
        if (startApplication) {
          window.request = (url,dataCallback,errorCallback,progressCallback,debug = false,retries) => {
            hybrix.lib.rout({query: url, channel: false, regular: false, retries},dataCallback,errorCallback,progressCallback);
          };
          window.hybrix = hybrix;
          window.spinner = spinner;
          startApplication('wallet',properties);
        } else {
          DIV_content.innerHTML = `<h2 style="color: red;">Error: Cannot activate application modal!</h2>`;
          console.log('Could not activate application modal: '+result);
        }
      }, (e) => {
        DIV_content.innerHTML = `<h2 style="color: red;">Error: Cannot load application modal data!</h2>`;
        console.log('Could not load application modal: '+e);
      });
    }, (e) => {
      DIV_content.innerHTML = `<h2 style="color: red;">Error: Cannot load application modal stylesheet!</h2>`;
      console.log('Could not load application modal: '+e);
    });
  } else {
    DIV_content.innerHTML = `<h2 style="color: red;">Error: Application ${properties.app} does not exist!</h2>`;
    console.log('Application does not exist: '+properties.app);
  }
};

exports.once = () => {
  const BUTTON_close = document.getElementById('app-modal-BtnClose');
  BUTTON_close.onclick = () => hybrix.view.closeAll();
};
