const {saveToContacts, getContact, renameContact, updateEntry, removeEntry} = require('../../lib/contacts');
const {createElement} = require('../../lib/html');
const hybrix = require('../../lib/hybrix').hybrix;
const {getSymbols} = require('../../lib/assets');

exports.rout = parameters => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('contactsEdit', parameters);
};

function removeEntryAndRefresh(parameters) {
  let {contactId, label, symbol, address} = parameters;
  removeEntry(symbol, contactId, label, address, () => {
    hybrix.view.exec('contactsEdit', 'open'); // refresh contacts list
  });
}

const open = parameters => {
  let {contactId, label, symbol, address} = parameters;
  if (typeof symbol === 'string') symbol = symbol.split('.')[0]; // only use base symbol to classify
  const DIV_error = document.getElementById('contactsEdit-error');
  DIV_error.innerHTML = ''; // reset
  const INPUT_name = document.getElementById('contactsEdit-name');
  const SELECT_symbol = document.getElementById('contactsEdit-symbol');
  const INPUT_address = document.getElementById('contactsEdit-address');
  const INPUT_label = document.getElementById('contactsEdit-label');

  const symbols = getSymbols();
  const uniqueBaseSymbols = [...new Set(symbols.map(s => s.split('.')[0]))];
  let html = '', selectSymbol = false, inList = false;
  for (const s of uniqueBaseSymbols) {
    selectSymbol = typeof symbol === 'string' && s.toLowerCase() === symbol.toLowerCase();
    html += `<option value="${s}" ${selectSymbol ? 'SELECTED' : ''}>${s.toUpperCase()}</option>`;
    if (selectSymbol) { inList = true; }
  }
  if (!inList) {
    if (typeof symbol === 'string') {
      html += `<option value="${symbol.toLowerCase()}" SELECTED>${symbol}</option>`;
    } else {
      html = '<option DISABLED SELECTED HIDDEN>{{CONTACTSEDIT:SELECT_SYMBOL}}</option>' + html;
    }
  }
  SELECT_symbol.innerHTML = html;

  const BUTTON_save = document.getElementById('contactsEdit-save');
  const DIV_entries = document.getElementById('contactsEdit-entries');

  INPUT_name.value = contactId || '';
  let close;
  if (contactId && !label && !symbol && !address) { // edit contact top-level
    INPUT_name.classList.remove('disabled');
    INPUT_name.readOnly = false;
    INPUT_address.style.display = 'none';
    INPUT_label.style.display = 'none';
    SELECT_symbol.style.display = 'none';
    DIV_entries.style.display = 'block';
    const contact = getContact(contactId);
    DIV_entries.innerHTML = '';
    for (const entry of contact) {
      for (const symbol in entry) {
        for (const label in entry[symbol]) {
          const address = entry[symbol][label];
          const DIV_entry = createElement({
            className: 'contactsEdit-entry',
            parentNode: DIV_entries,
            innerHTML: `<div class="contactsEdit-label">${address}${label && label !== '-' ? ' ➤ ' + label : ''}</div>`
          });
          const BUTTON_edit = createElement({
            className: 'contactsEdit-edit',
            parentNode: DIV_entry,
            style: 'display:inline-block;',
            onclick: () => open({contactId, label, symbol, address})
          });
          const BUTTON_delete = createElement({
            className: 'contactsEdit-delete',
            parentNode: DIV_entry,
            style: 'display:inline-block;',
            onclick: () =>
              hybrix.view.open('confirm', {
                text:'{{CONTACTSEDIT:SURE_DELETE}}',
                textMore:'{{CONTACTSEDIT:NO_RETURN}}',
                yesCallback: removeEntryAndRefresh,
                contactId,
                symbol,
                label,
                address
            })

          });
        }
      }
    }
    // TODO render entries ?
    close = () => {
      hybrix.view.exec('contacts', 'open'); // refresh contacts
      hybrix.view.pop();
    };
    BUTTON_save.onclick = () => {
      DIV_error.innerHTML = ''; // reset
      const newContactId = INPUT_name.value;
      renameContact(contactId, newContactId); // returns contactId key on success, false on failure -> TODO catch errors?
      close();
    };
  } else { // edit wallet entry or create new contact
    const newContact = !contactId;
    if (newContact) {
      INPUT_name.classList.remove('disabled');
      INPUT_name.readOnly = false;
    } else {
      INPUT_name.classList.add('disabled');
      INPUT_name.readOnly = true;
    }

    INPUT_address.value = address || '';
    INPUT_label.value = label && label !== '-' ? label : '';
    INPUT_address.style.display = 'block';
    INPUT_label.style.display = 'block';
    SELECT_symbol.style.display = 'block';
    DIV_entries.style.display = 'none';

    close = () => {
      if (!newContact) open({contactId}); // back to contact view
      else {
        hybrix.view.exec('contacts', 'open'); // refresh contacts
        hybrix.view.pop();
      }
    };

    BUTTON_save.onclick = () => {
      DIV_error.innerHTML = ''; // reset
      if (uniqueBaseSymbols.includes(SELECT_symbol.value.toLowerCase())) {
        const inputAddress = INPUT_address.value;
        const addressSanitized = inputAddress.includes(':') ? inputAddress.split(':')[1].trim() : inputAddress.trim();
        const newEntry = {
          address: addressSanitized,
          contactId: INPUT_name.value,
          label: INPUT_label.value.trim(),
          symbol: SELECT_symbol.value
        };
        if (newContact) saveToContacts(newEntry, close, error => { DIV_error.innerHTML = error; });
        else updateEntry({contactId, address, symbol, label}, newEntry, close, error => { DIV_error.innerHTML = error; });
      } else {
        DIV_error.innerHTML = '{{CONTACTSEDIT:PLEASE_SELECT_SYMBOL}}';
      }
    };
  }

  const BUTTON_cancel = document.getElementById('contactsEdit-cancel');
  BUTTON_cancel.onclick = close;
};

exports.open = open;
