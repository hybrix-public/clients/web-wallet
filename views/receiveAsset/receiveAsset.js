const {onclickCopyToClipboard} = require('../../lib/clipboard');
const {renderQR} = require('../../lib/qr');
const {parseMultiAddress} = require('../../lib/multi-address');
const {createElement, createCustomCheckBox} = require('../../lib/html');
const hybrix = require('../../lib/hybrix').hybrix;

exports.rout = parameters => {
  const {symbol} = parameters;
  if (symbol) hybrix.view.swap('viewAsset', {symbol});
  else hybrix.view.swap('listAssets');
  hybrix.view.open('receiveAsset', parameters);
};

function render (address) {
  const DIV_address = document.getElementById('receiveAsset-address');
  DIV_address.innerText = address;

  renderQR(address, document.getElementById('receiveAsset-qrcode'));

  const BUTTON_copy = document.getElementById('receiveAsset-copyButton');
  // TODO optimize
  onclickCopyToClipboard(BUTTON_copy, address, () => {
    const DIV_copy = document.getElementById('receiveAsset-copySuccess');
    DIV_copy.classList.add('active');
    setTimeout( () => { DIV_copy.classList.remove('active'); }, 5000);
  }, console.error);
  onclickCopyToClipboard(DIV_address, address, () => {
    const DIV_copy = document.getElementById('receiveAsset-copySuccess');
    DIV_copy.classList.add('active');
    setTimeout( () => { DIV_copy.classList.remove('active'); }, 5000);
  }, console.error);
}

function getSubAddressBySubSymbol (multiAddress, subSymbol) {
  for (const subAddress of multiAddress.subAddresses) {
    if (subAddress.startsWith(subSymbol + ':')) return subAddress;
  }
  return undefined;
}

function renderReceive (asset, checkOnlyOne = false) {
  const DIV_subAddresses = document.getElementById('receiveAsset-subAddresses');
  const multiAddress = parseMultiAddress(asset.address, asset.symbol);
  const CHECKBOX_prefix = document.getElementById('receiveAsset-usePrefix');

  DIV_subAddresses.innerHTML = '';

  const INPUTsBySubSymbol = {};

  CHECKBOX_prefix.onchange = () => {
    DIV_subAddresses.style.display = CHECKBOX_prefix.checked?'block':'none';
    renderReceive(asset, CHECKBOX_prefix.checked ? 0 : false);
  }

  if (multiAddress.isMultiAddress) {
    const renderMultiAddress = () => {
      let address = '';
      let complete = true;
      for (const subSymbol in INPUTsBySubSymbol) {
        if (INPUTsBySubSymbol[subSymbol].checked) {
          if (address !== '') address += ',';
          address += getSubAddressBySubSymbol(multiAddress, subSymbol);
        } else complete = false;
      }
      if (complete) render(asset.address);
      else if (address.includes(',')) render(address); // 'btc:123,eth:456' -> 'btc:123,eth:456'
      else if (CHECKBOX_prefix.checked) render(address.split(':').slice(1).join(':')); // 'btc:123' -> '123'
      else if (address.split(':').length > 2) render(address.split(':')[0] + ':' + address.split(':')[2]); // 'bch:bitcoincash:123' -> 'bch:123'
      else render(address);
    };

    const onclick = clickedSubSymbol => () => {
      if (CHECKBOX_prefix.checked && checkOnlyOne !== false) { // redraw with only one asset checked
        renderReceive(asset, Object.keys(INPUTsBySubSymbol).indexOf(clickedSubSymbol));
      } else renderMultiAddress();
    };
    // only show checkboxes when receiving from external wallet
    let i = 0;
    for (const subAddress of multiAddress.subAddresses) {
      const subSymbol = subAddress.split(':')[0];
      const SPAN = createElement({tagName: 'SPAN', subAddress});
      const INPUT = createCustomCheckBox({
        parentNode: SPAN,
        label: subSymbol,
        checked: checkOnlyOne === false ? true : i === checkOnlyOne,
        onclick: onclick(subSymbol),
        id: 'receiveAsset_' + subSymbol
      });
      DIV_subAddresses.appendChild(SPAN);
      INPUTsBySubSymbol[subSymbol] = INPUT;
      ++i;
    }
    renderMultiAddress();
  } else if (CHECKBOX_prefix.checked) render(asset.address); // '0x...' -> '0x...' , 'bitcoincash:...'  -> 'bitcoincash:...'
  else if (typeof asset.address === 'string' && asset.address.includes(':')) {
    render(asset.symbol + ':' + asset.address.split(':')[1]); // 'bitcoincash:...' -> 'bch:...'
  } else render(asset.symbol + ':' + asset.address); // '0x...' -> 'eth:0x...'

  const DIV_paymentRequest = document.getElementById('receiveAsset-paymentRequest');
  DIV_paymentRequest.onclick = () => hybrix.view.open('paymentRequest', {symbol:asset.symbol,address:asset.address});

  /* DEPRECATED
  if (asset.symbol.startsWith('mock')) {
    const BUTTON_mine = document.getElementById('receiveAsset-mineButton');
    BUTTON_mine.onclick = () => {
      const contract = asset.symbol.split('.')[1];
      const query = `/e/mockchain/mine/${contract}/${asset.address}/100`;
      hybrix.lib.rout({query}, console.log, console.error); // mining log
    };
  }
  */
}

exports.open = parameters => {
  const {symbol} = parameters;

  const CHECKBOX_prefix = document.getElementById('receiveAsset-usePrefix');
  CHECKBOX_prefix.checked = 0;
  const DIV_subAddresses = document.getElementById('receiveAsset-subAddresses');
  DIV_subAddresses.style.display = 'none'; // hide on opening of modal
  DIV_subAddresses.innerHTML = '';

  /* DEPRECATED
  const BUTTON_mine = document.getElementById('receiveAsset-mineButton');
  BUTTON_mine.style.display = symbol && symbol.startsWith('mock') ? 'block' : 'none';
  */
  
  const SELECT_symbol = document.getElementById('receiveAsset-selectSymbol');
  if (!symbol) {
    SELECT_symbol.style.display = 'none';
    // TODO show select box and fill with symbols
  } else {
    SELECT_symbol.style.display = 'none';
    hybrix.lib.addAsset({symbol}, data => renderReceive(data[symbol]), console.error);
  }

  const DIV_copy = document.getElementById('receiveAsset-copySuccess');
  DIV_copy.classList.remove('active');
  const DIV_symbol = document.getElementById('receiveAsset-symbol');
  DIV_symbol.innerHTML = symbol.toUpperCase();
};
