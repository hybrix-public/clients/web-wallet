const {handleRouting, hybrix} = require('../../lib/hybrix');
const {version} = require('../../tmp/version');
const quotes = require(`../../tmp/login-quotes.json`);

exports.rout = parameters => hybrix.view.swap('login', parameters);
const SELECT_universalLoginSymbol_DEFAULT = '{{LOGIN:SELECT_ASSET}}';

function initMakePass (userid, passwd) {
  hybrix.view.exec('makePass', 'setCredentials', {userid, passwd});
}

function resetFlipOverAnimation () {
  document.querySelector('.flipper').classList.remove('active'); // FLIP LOGIN FORM BACK
  document.getElementById('login-button').classList.remove('disabled');
  document.querySelector('#generateform').classList.remove('inactive');
  document.querySelector('#poweredby').classList.remove('inactive');
  document.getElementById('login-universalLogin').style.display = 'block';
}

function doFlipOverAnimation () {
  document.getElementById('login-button').classList.add('disabled');
  document.querySelector('.flipper').classList.add('active'); // FLIP LOGIN FORM
  document.querySelector('#generateform').classList.add('inactive');
  document.querySelector('#poweredby').classList.add('inactive');
  document.getElementById('login-universalLogin').style.display = 'none';
  window.getComputedStyle(document.querySelector('.flipper')); // force DOM redraw
}

const doLogin = (symbols, callback, newWallet) => () => {
  doFlipOverAnimation();
  const DIV_error = document.getElementById('login-error-message');
  const INPUT_username = document.getElementById('inputUserID');
  const INPUT_password = document.getElementById('inputPasscode');
  const SELECT_universalLoginSymbol = document.getElementById('login-universalLogin-symbol');

  DIV_error.style.display = 'none';
  const username = INPUT_username.value;
  const password = INPUT_password.value;
  
  initMakePass(username,password); // retain credentials to enable card pass making

  const errorCallback = error => {
    resetFlipOverAnimation();
    DIV_error.style.display = 'block';
    DIV_error.innerHTML = error;
    INPUT_username.value = '';
    INPUT_password.value = '';
  };

  const universalLoginSymbol = SELECT_universalLoginSymbol.style.display === 'block'
    ? SELECT_universalLoginSymbol.value
    : null;

  hybrix.login(username, password,
    () => {
      if (typeof callback === 'function') {
        try {
          callback();
        } catch (error) {
          console.error(error);
          const errorMessage = '{{LOGIN:ERROR_REDIRECTION}}';
          hybrix.view.swap(hybrix.constants.DEFAULT_LANDING_PAGE, {errorMessage});
        }
      } else {
        const parameters = {};
        if (newWallet) parameters.message = '{{LOGIN:WELCOME_NEW_WALLET}}';
        hybrix.view.swap(hybrix.constants.DEFAULT_LANDING_PAGE, parameters);
      }
      INPUT_username.value = '';
      INPUT_password.value = '';
      resetFlipOverAnimation();
    },
    errorCallback, symbols, universalLoginSymbol);
};

function showUniversalLogin () {
  const BUTTON_universalLogin = document.getElementById('login-universalLogin');
  const SELECT_universalLoginSymbol = document.getElementById('login-universalLogin-symbol');
  const INPUT_username = document.getElementById('inputUserID');
  const INPUT_password = document.getElementById('inputPasscode');
  const BUTTON_qr = document.getElementById('login-qr-scanner-button');

  BUTTON_universalLogin.innerHTML = '🔓';

  INPUT_password.value = '';

  SELECT_universalLoginSymbol.innerHTML = `<OPTION SELECTED disabled>${SELECT_universalLoginSymbol_DEFAULT}</OPTION>`;
  hybrix.lib.rout('/list/asset/names', names => {
    for (const symbol in names) {
      if (symbol.includes('.') || symbol === 'mock' || symbol === 'dummy') continue;
      const OPTION = document.createElement('option');
      OPTION.value = symbol;
      OPTION.innerHTML = names[symbol] + ' (' + symbol.toUpperCase() + ')';
      SELECT_universalLoginSymbol.appendChild(OPTION);
    }
  }, console.error);
  SELECT_universalLoginSymbol.style.display = 'block';
  INPUT_username.style.display = 'none';
  INPUT_password.placeholder = '{{LOGIN:PRIVATE_KEY}}';
  BUTTON_qr.style.display = 'none';
}

function showRegularLogin () {
  const BUTTON_universalLogin = document.getElementById('login-universalLogin');
  const SELECT_universalLoginSymbol = document.getElementById('login-universalLogin-symbol');
  const INPUT_username = document.getElementById('inputUserID');
  const INPUT_password = document.getElementById('inputPasscode');
  const BUTTON_qr = document.getElementById('login-qr-scanner-button');

  INPUT_password.value = '';

  BUTTON_universalLogin.innerHTML = '🔒';
  SELECT_universalLoginSymbol.style.display = 'none';
  INPUT_username.style.display = 'inline-block';
  INPUT_password.placeholder = '{{LOGIN:PASSWORD}}';
  BUTTON_qr.style.display = 'inline-block';
}

exports.open = parameters => {
  let hideCreateWallet = parameters.hideCreateWallet;
  const {id, user, pass, errorMessage, callback, newWallet, symbols} = parameters;
  let message = parameters.message ? parameters.message : '';
  
  const INPUT_username = document.getElementById('inputUserID');
  const INPUT_password = document.getElementById('inputPasscode');
  const FORM_createWallet = document.getElementById('generateform');
  const BUTTON_login = document.getElementById('login-button');
  const DIV_error = document.getElementById('login-error-message');
  const DIV_message = document.getElementById('login-logged-out-message');
  const BUTTON_createWallet = document.getElementById('login-create-wallet-button');
  const BUTTON_universalLogin = document.getElementById('login-universalLogin');
  const SELECT_universalLoginSymbol = document.getElementById('login-universalLogin-symbol');

  const BUTTON_qr = document.getElementById('login-qr-scanner-button');
  // On MacOS there is apparently some weird extra button on input fields by default, so move the QR icon to the left in that edge-case.
  if (navigator.userAgent.indexOf("Mac") > -1 || navigator.userAgent.indexOf("like Mac") > -1 || navigator.appVersion.indexOf("Mac") > -1) BUTTON_qr.style['margin-right'] = '35px';

  /*
   formats when login using QR:
   [$HOSTNAME]/#/$VIEWID/user=...&pass=...[&symbols=hy,eth,xrp][&new=true]
   [$HOSTNAME]/#/z/$COMPRESSED_VIEWID_AND_PARAMETERS
   */

  BUTTON_qr.onclick = () => {
    hybrix.view.open('qrScanner', {
      type: 'login',
      title: '{{LOGIN:SCAN_QR}}',
      dataCallback: (scannedViewId, scannedParameters) => {
        if (scannedParameters.id) { // short URI login
          INPUT_username.value = scannedParameters.id.substr(0, 16);
          INPUT_password.value = scannedParameters.id.substr(16);
        } else {
          INPUT_username.value = scannedParameters.user || '';
          INPUT_password.value = scannedParameters.pass || '';
        }
        // NOTE: scannedViewId or scannedParameters are not directly passed during the login procedure of qrScanner!
        //       This is because we want to route back to the original callback if available
        setTimeout( doLogin(parameters.symbols || symbols, callback, false), 1000); // login with a second delay to let the user see what is going on and prevent race condition
      },
      cancelCallback: (e) => {
        console.log('{{LOGIN:FAIL_READ_QR}} {{LOGIN:ERROR_RESULT}}' + e);
      }
    });
  };
  if (parameters.hasOwnProperty('universal')) showUniversalLogin();
  else showRegularLogin();

  BUTTON_universalLogin.onclick = () => {
    DIV_error.style.display = 'none';
    if (BUTTON_universalLogin.innerHTML === '🔓') showRegularLogin();
    else showUniversalLogin();
  };

  SELECT_universalLoginSymbol.onchange = () => checkInput();

  BUTTON_createWallet.onclick = () => hybrix.view.swap('createWallet', {symbols});

  if (id) { // shorthand login
    INPUT_username.value = id.substr(0, 16);
    INPUT_password.value = id.substr(16);
  } else {
    INPUT_username.value = user || '';
    INPUT_password.value = pass || '';
  }
  if (id || (user && pass)) { hideCreateWallet = true; }

  FORM_createWallet.style.display = newWallet || hideCreateWallet ? 'none' : 'block';

  if(parameters.next === 'transaction') message = '{{LOGIN:MESSAGE_TRANSACTION}} ' + message;

  if (message) {
    DIV_message.style.display = 'block';
    DIV_message.innerHTML = '<p>' + decodeURIComponent(message) + '</p>';
  } else {
    DIV_message.style.display = 'none';
  }

  if (typeof errorMessage === 'string') {
    console.error(errorMessage);
    DIV_error.style.display = 'block';
    DIV_error.innerHTML = errorMessage;
  } else DIV_error.style.display = 'none';

  checkInput();
  BUTTON_login.onclick = doLogin(symbols, callback, newWallet);

  INPUT_username.focus();
};

function sanitizeInput (stringInput) {
  // this sanitizes the RFC4648 compatible string to be more flexible and auto-convert easily confused characters to to correct input
  return stringInput.toUpperCase().replace(/1/g, 'I').replace(/0/g, 'O').replace(/8/g, 'B').replace(/9/g, 'G').replace(' ', '');
}

function checkInput () {
  const BUTTON_login = document.getElementById('login-button');
  const SELECT_universalLoginSymbol = document.getElementById('login-universalLogin-symbol');

  const INPUT_username = document.getElementById('inputUserID');
  const INPUT_password = document.getElementById('inputPasscode');

  if (INPUT_username.value !== '') INPUT_username.value = sanitizeInput(INPUT_username.value);
  if (INPUT_username.value !== '' && INPUT_password.value !== '') {
    INPUT_password.value = sanitizeInput(INPUT_password.value);
    BUTTON_login.classList.remove('disabled');
  } else if (SELECT_universalLoginSymbol.value !== SELECT_universalLoginSymbol_DEFAULT && INPUT_password.value !== '') BUTTON_login.classList.remove('disabled');
  else BUTTON_login.classList.add('disabled');
}

function daysIntoYear(date){
    return (Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()) - Date.UTC(date.getFullYear(), 0, 0)) / 24 / 60 / 60 / 1000;
}

exports.once = () => {
  const BUTTON_login = document.getElementById('login-button');
  const SPAN_quoteOfTheDay = document.getElementById('quote-of-the-day-quote');
  const SPAN_quoteOfTheDayAuthor = document.getElementById('quote-of-the-day-author');  

  // daily quote
  if (quotes instanceof Array && quotes !== []) {
    const quoteN = (daysIntoYear(new Date()) % quotes.length);
    if (isNaN(quoteN) || typeof quotes[quoteN] === 'undefined') quoteN = 0;
    if (typeof quotes[quoteN] !== 'undefined') {
      const quoteSelect = quotes[quoteN].split('|');
      SPAN_quoteOfTheDay.innerText = quoteSelect[1];
      SPAN_quoteOfTheDayAuthor.innerText = quoteSelect[0];
    }
  }
  
  hybrix.lib.rout({query: '/version'}, version => {
    hybrix.version.hybrixd = version;
    const SPAN_hybrixdVersion = document.getElementById('login-hybrixd-version');
    SPAN_hybrixdVersion.innerText = version;
    const SPAN_hybrixdVersion2 = document.getElementById('settings-hybrixd-version'); // TODO refactor to better place
    SPAN_hybrixdVersion2.innerText = version;
  }, console.error);

  const SPAN_walletVersion = document.getElementById('login-wallet-version');
  hybrix.version.wallet = version;
  SPAN_walletVersion.innerText = version;

  const INPUT_username = document.getElementById('inputUserID');
  const INPUT_password = document.getElementById('inputPasscode');

  INPUT_username.onchange = INPUT_username.onpaste = INPUT_username.oninput = INPUT_password.onchange = INPUT_username.password = INPUT_password.oninput = checkInput;

  INPUT_username.onkeyup = event => {
    if (event.keyCode === 13) {
      if (INPUT_password.value) {
        if (INPUT_username.value) {
          BUTTON_login.onclick();
        }
      } else INPUT_password.focus();
    }
  };

  INPUT_password.onkeyup = event => {
    if (event.keyCode === 13) {
      if (INPUT_username.value) {
        if (INPUT_password.value) {
          BUTTON_login.onclick();
        }
      } else INPUT_username.focus();
    }
  };
};

