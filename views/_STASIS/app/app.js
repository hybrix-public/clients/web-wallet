const hybrix = require('../../lib/hybrix').hybrix;
const branding = require('../../lib/branding');
const spinner = require('../../lib/spinner');
const { activate } = require('../../common');
const LZString = require('../../../common/crypto/lz-string');

exports.rout = (properties) => {
  hybrix.view.swap('app',properties);
}

exports.open = (properties) => {
  // put a title on the app or modal, in case that is not done by the app maker
  const DIV_content = document.getElementById('app-content');
  spinner.apply(DIV_content);

  if(properties.app) {
    const DIV_header = document.getElementById('app-header');
    DIV_header.innerText = properties.title;
console.log(` >>>> /e/${properties.app}/app.css`);
    hybrix.lib.rout({query: `/e/${properties.app}/app.css`, channel: false, regular: false}, result => {
      const STYLE_css = document.getElementById('app-stylesheet');
      STYLE_css.innerText = result;
      hybrix.lib.rout({query: `/e/${properties.app}/app.js`, channel: false, regular: false}, result => {
        // export the functionality to a pre-prepared var
        const windowActivation = '; if (typeof window !== "undefined") { window.deterministic = startApplication; }';
        let startApplication = activate(result + windowActivation);
        spinner.remove(DIV_content);
        if (startApplication) {
          window.request = (url,dataCallback,errorCallback,progressCallback,debug = false,retries) => {
            hybrix.lib.rout({query: url, channel: false, regular: false, retries},dataCallback,errorCallback,progressCallback);
          };
          window.hybrix = hybrix;
          window.spinner = spinner;
          window.LZString = LZString;
          startApplication('wallet',properties);
        } else {
          DIV_content.innerHTML = `<h2 style="color: red;">Error: Cannot activate application!</h2>`;
          console.log('Could not activate application: '+result);
        }
      }, (e) => {
        DIV_content.innerHTML = `<h2 style="color: red;">Error: Cannot load application data!</h2>`;
        console.log('Could not load application: '+e);
      });
    }, (e) => {
      DIV_content.innerHTML = `<h2 style="color: red;">Error: Cannot load application stylesheet!</h2>`;
      console.log('Could not load application: '+e);
    });
  } else {
    DIV_content.innerHTML = `<h2 style="color: red;">Error: Application ${properties.app} does not exist!</h2>`;
    console.log('Application does not exist: '+properties.app);
  }
};

exports.once = () => {
  //const DIV_LOGO = document.getElementById('app-logo');
  //branding.logo(DIV_LOGO);

  const BUTTON_back = document.getElementById('app-back-button');
  BUTTON_back.onclick = () => hybrix.view.swap('listAssets');
  const BUTTON_pending = document.getElementById('app-pending-button');
  BUTTON_pending.onclick = () => hybrix.view.open('pending');
  const BUTTON_qrscanner = document.getElementById('app-qrscanner-button');
  // TODO: do we need to add the QR button action here as well?
  const BUTTON_menu = document.getElementById('app-menu-button');
  BUTTON_menu.onclick = () => hybrix.view.open('appsList');
};
