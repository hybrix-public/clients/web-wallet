/*
 * hybrix DAO - multi-ledger distributed autonomous organization
 * first example of a hybrix application that can render both in
 * wallet, cli-wallet as well as a normal browser tab
 *
 */
function startApplication (context,properties) {
  if (context === 'wallet' || context === 'gui') {
    const hashelementIdx = window.location.href.indexOf('#');
    const editMode = (hashelementIdx > 0 && window.location.href.substr(hashelementIdx+1,9) === 'password=')?true:false;
    const authcode = window.location.href.substr(hashelementIdx+10);

    if(context !== 'wallet') {
      setTimeout( () => {
        const A_loginButton = document.getElementById('loginButton');
        A_loginButton.href = 'https://wallet.hybrix.io/#/app/app='+properties.app+'&title='+properties.title;
        A_loginButton.style.opacity = 1;
        A_loginButton.style.background = '#22BB22';
      }, 3000);
    }
    const DIV_header = document.getElementById('app-header');
    DIV_header.innerHTML = `<h1>${properties.title}</h1>`;

    renderFrame = (data) => {
      const DIV_content = document.getElementById('app-content');
      let output = '';
      let proposalButton = {};
      let elementHtml = '';
      let support, percent, targetPercent, equalizer, barPercent, barYesPercent, barNoPercent, statsLegend;
      // 1. calculate basic statistics and totals
      for (const idx in data) {
        data[idx].stats = {}
        switch (data[idx].type) {
          case 'boolean':
            data[idx].balances = data[idx].balances?data[idx].balances:{'yes':0,'no':0};
            data[idx].stats.support = Number(data[idx].balances.yes)+Number(data[idx].balances.no);
            data[idx].stats.percent = (data[idx].stats.support/Number(data[idx].target))*100;
            data[idx].stats.equalizer = data[idx].stats.support < data[idx].target ? data[idx].stats.support/data[idx].target : 1;
          break;
          default:
            data[idx].balances = data[idx].balances?data[idx].balances:{'default':0};
            data[idx].stats.support = Number(data[idx].balances.default);
            data[idx].stats.percent = (data[idx].stats.support/data[idx].target)*100;
            data[idx].stats.equalizer = data[idx].stats.percent <= 100 ? 0.9 : data[idx].stats.percent <= 145 ? 0.75 : data[idx].stats.percent <= 395 ? 0.5 : data[idx].stats.percent <= 795 ? 0.25 : 0.1;
        }
      }
      // 2. sort the entries by certain criteria
      data.sort((a, b) => {
        return a.support - b.support;
      });
      data.sort((a, b) => {
        const timeNow = new Date().getTime() / 1000;
        const zeroToBottom = 99999999999999999;
        return (a.deadline&&a.deadline>timeNow?a.deadline:zeroToBottom) - (b.deadline&&b.deadline>timeNow?b.deadline:zeroToBottom);
      });
      // 3. render sorted and calculated data into DOM
      for (const element of data) {
        switch (element.type) {
          case 'boolean':
            targetPercent = (Number(element.balances.yes)/element.stats.support)*100;
            barYesPercent = (Number(element.balances.yes)/element.stats.support)*100 * element.stats.equalizer;
            barNoPercent = (Number(element.balances.no)/element.stats.support)*100 * element.stats.equalizer;
            elementHtml = `<div class="title">${element.title}</div><div class="description">${element.description}</div><div class="bar"><span class="bar-unfill"><span class="bar-fill-yes" style="width: ${barYesPercent}%;"></span><span class="target-wrap"><span class="target" style="margin-left:${targetPercent}%;"></span></span><span class="bar-fill-no" style="width: ${barNoPercent}%;"></span></div>`;
            statsLegend = '<div class="proposal-stats"><span class="bullet-yes">▪</span> Accept: '+Math.round(element.balances.yes)+' <br /><span class="bullet-no">▪</span> Oppose: '+Math.round(element.balances.no)+' <br /><span class="bullet-required">▪</span> Required: '+Math.round(element.target)+' </div>';
          break;
          default:
            targetPercent = element.stats.equalizer * 100;
            barPercent = element.stats.percent * element.stats.equalizer;
            elementHtml = `<div class="title">${element.title}</div><div class="description">${element.description}</div><div class="bar"><span class="bar-unfill"><span class="bar-fill" style="width: ${barPercent}%;"></span><span class="target-wrap"><span class="target" style="margin-left:${targetPercent}%;"></span></span></span></div>`;
            statsLegend = '<div class="proposal-stats"><span class="bullet-support">▪</span> Support: '+Math.round(element.stats.support)+' <br /><span class="bullet-required">▪</span> Required: '+Math.round(element.target)+' </div>';
        }
        const deadline = !element.deadline || isNaN(element.deadline) ? 'none' : new Date(element.deadline*1000).toISOString().slice(0, -5).replace('T',' ');
        elementHtml += statsLegend+'<div class="proposal-stats">Type: '+element.type+' <br />Deadline: '+deadline+'</div>';
        // add edit/delete buttons if in edit mode
        if(editMode) {
          elementHtml += `<br /><br /><button type="button" id="proposal-${element.id}-button-edit">EDIT</button> <button type="button" id="proposal-${element.id}-button-delete">DELETE</button>`;
        }
        output += `<div class="proposal" id="proposal-${element.id}">${elementHtml}</div>`;
      }
      // render into DOM
      if(editMode) {
        DIV_content.innerHTML = `<button type="button" id="proposal-button-new">+ NEW</button><br /><br />`+output;
      } else {
        let introText;
        if (context === 'wallet') {
          introText = 'Proposals below may be voted on by clicking on them.';
        } else {
          introText = 'Please first connect to hybrix wallet to vote on proposals.';
        }
        DIV_content.innerHTML = '<div style="padding-left: 12px; width: 100%; color: #111;"><p>The hybrix DAO shows proposals that can be voted on to direct the developments of hybrix.<br />'+introText+'</p></div>'+output;
      }
      // add button events if in wallet context
      if(editMode) {
        proposalButton['new'] = document.getElementById(`proposal-button-new`);
        proposalButton['new'].onclick = event => {
          event.stopPropagation();
          const newId = 'newId'; // TODO: make multi-editable?
          data[newId] = {id:null,title:'',description:'',type:'cumulative',target:100};
          const newProposal = `<div class="proposal" id="proposal-new-${newId}"></div>`;
          DIV_content.innerHTML = newProposal;
          // make new entry editable...
          DIV_edit = document.getElementById(`proposal-new-${newId}`);
          //id:1,title:'Some cool initiative',description:'This is some cool initiative',type:'cumulative',target:50,addresses:{'default':'hy:02CnbfWxh1WNpoC9FmnkPtXUMtDXPa1uycXK5qHL33eevjWVFpi7Q6K9mpnGv3MyzQWkLSmAfHjdYUGciGVg8zRzab1j7EQ3'}
          DIV_edit.innerHTML = `<form class="pure-form pure-form-stacked">
          Title: <input id="editproposal-${newId}-title" value="" /><br />
          Description: <input id="editproposal-${newId}-description" value="" /><br />
          Type: <input id="editproposal-${newId}-type" value="cumulative" placeholder="cumulative" /><br />
          Target:  <input id="editproposal-${newId}-target" value="100" /><br />
          Deadline:  <input id="editproposal-${newId}-deadline" value="" placeholder="YYYY-MM-DD [T00:00:00]"/><br />
          <br />
          <button type="button" id="editproposal-${newId}-button">CREATE NEW</button>
          </form>`;
          BUTTON_edit_submit[newId] = document.getElementById(`editproposal-${newId}-button`);
          BUTTON_edit_submit[newId].onclick = event => {
            const editedId = newId;
            // some AJAx call here posting all changes back to mothership
            let saveObject = {};
            saveObject.id = null;
            saveObject.title = document.getElementById(`editproposal-${editedId}-title`).value;
            saveObject.description = document.getElementById(`editproposal-${editedId}-description`).value;
            saveObject.type = document.getElementById(`editproposal-${editedId}-type`).value;
            saveObject.target = document.getElementById(`editproposal-${editedId}-target`).value;
            const newDeadline = document.getElementById(`editproposal-${editedId}-deadline`).value;
            saveObject.deadline = newDeadline ? Date.parse(newDeadline) / 1000 : null;
            DIV_edit.innerHTML = `Creating record...`;
            const compressedData = LZString.compressToEncodedURIComponent(JSON.stringify(saveObject));
            const signature = sha256(compressedData+authcode);
            request('/e/xdao/save/'+compressedData+'/'+signature,
                data => {
                  DIV_edit.innerHTML = `NEW RECORD CREATED: ${JSON.stringify(data)}`;
                },
                error => {
                  DIV_edit.innerHTML = `ERROR CREATING RECORD! (${error}) `;
                }
              );
            event.stopPropagation();
          };
        };
      }
      // edit and delete buttons
      let BUTTON_edit_submit = [];
      let BUTTON_delete_submit = [];
      for (const idx in data) {
        const element = data[idx];
        const elementId=element.id;
        proposalButton[elementId] = document.getElementById(`proposal-${elementId}`);
        if (!editMode) {
          if (context === 'wallet') {
            function openModal(event,id) {
              hybrix.view.open('appModal',{parent:properties,modal:'vote',title:'Vote',voteObject:data[id]});
              event.stopPropagation();
            };
            proposalButton[elementId].onclick = event => openModal(event,idx);
          } else {
            let DIV_edit;
            proposalButton[elementId].onclick = event => {
              alert('Please connect to hybrix wallet if you would like to vote!');
              event.stopPropagation();
            };
          }
        } else {
          // handle edit/delete buttons if in edit mode
          proposalButton['edit'+elementId] = document.getElementById(`proposal-${elementId}-button-edit`);
          proposalButton['edit'+elementId].onclick = event => {
            DIV_edit = document.getElementById(`proposal-${elementId}`);
            //id:1,title:'Some cool initiative',description:'This is some cool initiative',type:'cumulative',target:50,addresses:{'default':'hy:02CnbfWxh1WNpoC9FmnkPtXUMtDXPa1uycXK5qHL33eevjWVFpi7Q6K9mpnGv3MyzQWkLSmAfHjdYUGciGVg8zRzab1j7EQ3'}
            const deadline = !data[idx].deadline || isNaN(data[idx].deadline) ? '' : new Date(data[idx].deadline*1000).toISOString().slice(0, -5);
            DIV_edit.innerHTML = `<form class="pure-form pure-form-stacked">
            ID: <span id="editproposal-${elementId}-id">${idx}</span><br />
            Title: <input id="editproposal-${elementId}-title" value="${data[idx].title}"/><br />
            Description: <input id="editproposal-${elementId}-description" value="${data[idx].description}"/><br />
            Type: <input id="editproposal-${elementId}-type" value="${data[idx].type}"/><br />
            Target:  <input id="editproposal-${elementId}-target" value="${data[idx].target}"/><br />
            Deadline:  <input id="editproposal-${elementId}-deadline" value="${deadline}" placeholder="YYYY-MM-DD [T00:00:00]"/><br />
            <br />
            <button type="button" id="editproposal-${elementId}-button">SUBMIT</button>
            </form>`;
            BUTTON_edit_submit[idx] = document.getElementById(`editproposal-${elementId}-button`);
            BUTTON_edit_submit[idx].onclick = event => {
              const DIV_editproposal = DIV_edit;
              const editedId = elementId;
              // some AJAx call here posting all changes back to mothership
              let saveObject = {};
              saveObject.id = editedId;
              saveObject.title = document.getElementById(`editproposal-${editedId}-title`).value;
              saveObject.description = document.getElementById(`editproposal-${editedId}-description`).value;
              saveObject.type = document.getElementById(`editproposal-${editedId}-type`).value;
              saveObject.target = document.getElementById(`editproposal-${editedId}-target`).value;
              const newDeadline = document.getElementById(`editproposal-${editedId}-deadline`).value;
              saveObject.deadline = newDeadline ? Date.parse(newDeadline) / 1000 : null;
              DIV_edit.innerHTML = `Saving record...`;
              const compressedData = LZString.compressToEncodedURIComponent(JSON.stringify(saveObject));
              const signature = sha256(compressedData+authcode);
              request('/e/xdao/save/'+compressedData+'/'+signature,
                  data => {
                    DIV_edit.innerHTML = `RECORD EDITED: ${JSON.stringify(data)} `; // DEBUG: +'/e/xdao/save/'+compressedData+'/'+signature;
                  },
                  error => {
                    DIV_edit.innerHTML = `ERROR EDITING RECORD! (${error})`;
                  }
                );
              event.stopPropagation();
            };
          };
          proposalButton['delete'+elementId] = document.getElementById(`proposal-${elementId}-button-delete`);
          proposalButton['delete'+elementId].onclick = event => {
            DIV_edit = document.getElementById(`proposal-${elementId}`);
            //id:1,title:'Some cool initiative',description:'This is some cool initiative',type:'cumulative',target:50,addresses:{'default':'hy:02CnbfWxh1WNpoC9FmnkPtXUMtDXPa1uycXK5qHL33eevjWVFpi7Q6K9mpnGv3MyzQWkLSmAfHjdYUGciGVg8zRzab1j7EQ3'}
            DIV_edit.innerHTML = `ARE YOU SURE YOU WISH TO DELETE THIS PROPOSAL? (TODO) <br /><button type="button" id="deleteproposal-${idx}-button">YES</button>`;
            BUTTON_delete_submit[idx] = document.getElementById(`deleteproposal-${idx}-button`);
            BUTTON_delete_submit[idx].onclick = event => {
              const DIV_editproposal = DIV_edit;
              const deletedId = elementId;
              const signature = sha256(deletedId+authcode);
              DIV_edit.innerHTML = `Deleting record...`;
              request('/e/xdao/delete/'+deletedId+'/'+signature,
                  data => {
                    DIV_edit.innerHTML = `RECORD DELETED: ${JSON.stringify(data)}`;
                  },
                  error => {
                    DIV_edit.innerHTML = `ERROR DELETING RECORD! (${error})`;
                  }
                );
              event.stopPropagation();
            };
            event.stopPropagation();
          };
        }
      }
    }
  } else {
    renderFrame = (data) => { console.log(data) };
  }


  request('/e/xdao/stats',
      data => renderFrame(data),
      error => renderFrame(error),
      (progress,data) => {}
    );

}
