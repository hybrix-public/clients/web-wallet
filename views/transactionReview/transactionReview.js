const hybrix = require('../../lib/hybrix').hybrix;
const Decimal = require('../../lib/decimal').Decimal;
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const progress = require('../../lib/progress').progress;
const {findContact, displayContact} = require('../../lib/contacts');
const sha256 = require('js-sha256');

const renderReview = (symbol, transaction, rawTransaction, referer, toOffset) => assets => {
  const asset = assets[symbol];
  const DIV_source = document.getElementById('transaction-overview-source');
  const DIV_sourceWrapper = document.getElementById('transaction-overview-sourceWrapper');

  if (transaction.hasOwnProperty('source') && transaction.source) {
    if (transaction.source !== asset.address) {
      const sourceContact = findContact(symbol, transaction.source);
      const sourceContactString = sourceContact ? displayContact(sourceContact) : '';
      DIV_source.innerHTML = sourceContactString ? `<contact>${sourceContactString}</contact><br>${transaction.source}` : transaction.source;
      if (transaction.offset) {
        DIV_source.innerHTML += `<br>{{TRANSACTIONREVIEW:LABEL_OFFSET}} ${transaction.offset}`;
      }
    } else {
      DIV_source.innerHTML = `<contact>👤 <i>{{TRANSACTIONREVIEW:CONTACT_SELF}}</i></contact><br>${asset.address}`;
    }
  } else if (asset.address) {
    DIV_source.innerHTML = `<contact>👤 <i>{{TRANSACTIONREVIEW:CONTACT_SELF}}</i></contact><br>${asset.address}`;
  } else DIV_sourceWrapper.style.display = 'none';

  const DIV_attachment = document.getElementById('transaction-overview-attachment');
  const DIV_attachmentWrapper = document.getElementById('transaction-overview-attachmentWrapper');

  if (transaction.hasOwnProperty('attachment')) {
    DIV_attachmentWrapper.style.display = 'block';
    DIV_attachment.innerHTML = transaction.attachment;
  } else DIV_attachmentWrapper.style.display = 'none';

  const DIV_spinner = document.getElementById('transactionReview-spinner');
  const DIV_target = document.getElementById('transaction-overview-target');
  const DIV_amount = document.getElementById('transaction-overview-amount');
  const DIV_fee = document.getElementById('transaction-overview-fee'); // TODO multi fee
  const DIV_description = document.getElementById('transaction-overview-description');
  const DIV_total = document.getElementById('transaction-overview-total');
  const DIV_showRawTransaction = document.getElementById('transactionReview-showRawTransaction');
  const DIV_rawTransaction = document.getElementById('transactionReview-rawTransaction');
  const BUTTON_back = document.getElementById('transactionReview-back');
  const BUTTON_send = document.getElementById('transactionReview-send');
  const fee = transaction.hasOwnProperty('fee') ? transaction.fee : asset.fee;
  const total = new Decimal(transaction.amount).add(new Decimal(fee)).toString();
  const description = transaction.hasOwnProperty('description') ? transaction.description : '';
  DIV_showRawTransaction.onclick = () => {
    DIV_showRawTransaction.style.display = 'none';
    DIV_rawTransaction.style.display = 'block';
    DIV_rawTransaction.innerText = rawTransaction;
  };

  const contact = findContact(symbol, transaction.target);
  const contactString = contact ? displayContact(contact) : '';
  DIV_target.innerHTML = contact ? `<contact>${contactString}</contact><br>${transaction.target}` : transaction.target;
  if (typeof toOffset !== 'undefined') DIV_target.innerHTML += `<br>offset: ${toOffset}`;
  else if (transaction.target === asset.address) DIV_target.innerHTML = `<contact>👤 <i>{{TRANSACTIONREVIEW:CONTACT_SELF}}</i></contact><br>${DIV_target.innerHTML}`;
  DIV_amount.innerHTML = prettyPrintAmount(transaction.amount, symbol);

  if (transaction.hasOwnProperty('fee')) {
    DIV_fee.innerHTML = prettyPrintAmount(fee, symbol) + ' {{TRANSACTIONREVIEW:CUSTOMIZED_FEE}}';
    // TODO normal fee is x, please be aware that your transaction might be slower / more expensive than required
  } else {
    DIV_fee.innerHTML = prettyPrintAmount(fee, symbol); // TODO multi fee
  }

  if (transaction.hasOwnProperty('description')) {
    DIV_description.innerText = transaction.description;
  } else {
    DIV_description.innerText = '';
  }
  
  DIV_total.innerHTML = prettyPrintAmount(total, symbol); // TODO multi fee
  BUTTON_back.onclick = () => hybrix.view.pop();
  BUTTON_send.onclick = () => {
    DIV_spinner.style.display = 'block';
    BUTTON_send.setAttribute('disabled',true);
    BUTTON_back.setAttribute('disabled',true);
    // here we push a previously prepared rawTransaction, so not using the hybrix-lib transaction API directly
    hybrix.lib.rout({query: '/a/' + symbol + '/push/_', data: rawTransaction},
      transactionResult => {
        // save description: split up in case of unified tx
        const NOP = () => {};
        const transactionIDs = transactionResult.split(',');
        if (typeof transaction.description === 'string' && transaction.description !== '') {
          for (const prefixedTransactionID of transactionIDs) {
            const transactionArray = prefixedTransactionID.split(':');
            const transactionID = transactionArray.length > 1 ? transactionArray[1] : transactionArray[0];
            const hashedTransactionID = sha256.sha256(transactionID);
            hybrix.lib.save({key:hashedTransactionID,value:transaction.description,signed:true,encrypted:false}, NOP, NOP);
            // DEBUG: console.log(JSON.stringify({key:hashedTransactionID,value:transaction.description,signed:true,encrypted:false}));
          }
        }
        // add to pending
        hybrix.lib.addPending(
          {transaction: {ref: symbol + ':' + transactionIDs, type: 'regular', meta: {id: transactionIDs, ...transaction}, sync:false}},
          () => { setTimeout( hybrix.view.exec('viewAsset', 'refreshPending', symbol), 2000); }, NOP);
        hybrix.view.close('transactionReview');
        hybrix.view.close('transaction');
        const txIDs = transactionResult.split(',');
        hybrix.view.open('transactionComplete', {symbol, transactionIDs, referer});
        DIV_spinner.style.display = 'none';
        BUTTON_send.removeAttribute('disabled');
        BUTTON_back.removeAttribute('disabled');
      },
      errorMessage => {
        hybrix.view.open('transactionFailed', {transaction, asset, errorMessage, rawTransaction, referer});
        DIV_spinner.style.display = 'none';
        BUTTON_send.removeAttribute('disabled');
        BUTTON_back.removeAttribute('disabled');
      });
  };
};

const renderPending = symbol => pendingTransactions => {
  const DIV_pending = document.getElementById('transactionReview-pending');
  pendingTransactions = Object.values(pendingTransactions)
    .filter(transaction => transaction.type === 'regular' && transaction.status < 0.55 && (transaction.meta.symbol === symbol || transaction.meta.symbol.startsWith(symbol + ':')));

  if (pendingTransactions.length === 0) DIV_pending.innerHTML = `{{TRANSACTIONREVIEW:NO_PENDING_A}}${symbol.toUpperCase()}{{TRANSACTIONREVIEW:NO_PENDING_B}}`;
  else {
    let html = '<table>';
    for (const transaction of pendingTransactions) {
      html += '<tr><td>' + transaction.meta.amount + ' ' + transaction.meta.symbol.toUpperCase() + '</td><td class="transactionReview-pending">{{TRANSACTIONREVIEW:STATUS_PENDING}}</td></tr>';
    }

    DIV_pending.innerHTML = html + '</table>';
  }
};

function failPending (error) {
  console.error('failPending', error);
  const DIV_pending = document.getElementById('transactionReview-pending');
  DIV_pending.innerHTML = '{{TRANSACTIONREVIEW:WARN_CANNOT_GET_PENDING}}';
}

exports.open = function (parameters) {
  const DIV_progress = document.getElementById('transactionReview-progress');
  progress(DIV_progress, {
    '{{TRANSACTIONREVIEW:PROGRESS_PREPARE}}': null,
    '{{TRANSACTIONREVIEW:PROGRESS_REVIEW}}': null,
    '{{TRANSACTIONREVIEW:PROGRESS_SEND}}': null
  }, 1);

  const P_notice = document.getElementById('transactionReview-notice');
  if (parameters.notice) P_notice.innerText = parameters.notice;
  const DIV_rawTransaction = document.getElementById('transactionReview-rawTransaction');
  DIV_rawTransaction.innerText = '...';
  DIV_rawTransaction.style.display = 'none';
  const DIV_showRawTransaction = document.getElementById('transactionReview-showRawTransaction');
  DIV_showRawTransaction.style.display = 'block';
  const DIV_pending = document.getElementById('transactionReview-pending');
  DIV_pending.innerHTML = '...';
  const {symbol, transaction, rawTransaction, referer, toOffset, fromOffset} = parameters;

  const BUTTON_send = document.getElementById('transactionReview-send');
  const BUTTON_back = document.getElementById('transactionReview-back');
  BUTTON_back.removeAttribute('disabled');
  BUTTON_send.removeAttribute('disabled');

  hybrix.lib.asset({symbol}, renderReview(symbol, transaction, rawTransaction, referer, toOffset, fromOffset), console.error);
  hybrix.lib.getPending({remove: false, sync: false}, renderPending(symbol), failPending);
};
