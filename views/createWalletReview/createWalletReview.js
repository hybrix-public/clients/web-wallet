const hybrix = require('../../lib/hybrix').hybrix;

exports.rout = parameters => hybrix.view.swap('createWalletReview', parameters);

exports.open = parameters => {
  const {credentials, symbols} = parameters;
  document.getElementById('createWalletReview-makepass-button').onclick = () => {
    // workaround to set return callback for fullscreen view
    hybrix.view.exec('makePass', 'setCloseCallback',
      () => {
        hybrix.view.swap('createWalletReview', parameters);
      }
    );
    hybrix.view.swap('empty');
    hybrix.view.open('makePass', {userid:credentials.username, passwd:credentials.password});
  }
  document.getElementById('createWalletReview-continue-button').onclick = () => hybrix.view.swap('createWalletConfirm', {credentials, symbols});
  document.getElementById('createWalletReview-username').innerHTML = credentials.username;
  document.getElementById('createWalletReview-password').innerHTML = credentials.password;
};
