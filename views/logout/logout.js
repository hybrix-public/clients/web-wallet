const hybrix = require('../../lib/hybrix').hybrix;

exports.rout = () => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('logout');
};

window.addEventListener('load', () => {
  const BUTTON_confirm = document.getElementById('logout-confirm');

  BUTTON_confirm.onclick = () => {
    hybrix.logout('{{LOGOUT:LOGGED_OUT}}');
  };
  const BUTTON_cancel = document.getElementById('logout-cancel');

  BUTTON_cancel.onclick = () => hybrix.view.close('logout');
});
