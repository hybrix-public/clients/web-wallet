const hybrix = require('../../lib/hybrix').hybrix;
const {version} = require('../../tmp/version');
const sha256 = require('js-sha256');
var makePassData = null;
var makePassCallback = null;

const doObfuscate = (salt, text) => {
  const textToChars = (text) => text.split('').map((c) => c.charCodeAt(0));
  const byteHex = (n) => ('0' + Number(n).toString(16)).substr(-2);
  const applySaltToChar = (code) => textToChars(salt).reduce((a, b) => a ^ b, code);

  return text
    .split('')
    .map(textToChars)
    .map(applySaltToChar)
    .map(byteHex)
    .join('');
};

const deObfuscate = (salt, encoded) => {
  const textToChars = (text) => text.split('').map((c) => c.charCodeAt(0));
  const applySaltToChar = (code) => textToChars(salt).reduce((a, b) => a ^ b, code);
  return encoded
    .match(/.{1,2}/g)
    .map((hex) => parseInt(hex, 16))
    .map(applySaltToChar)
    .map((charCode) => String.fromCharCode(charCode))
    .join('');
};

function setCredentials (passCredentials) {
  makePassData = doObfuscate( sha256.sha256(new Date().toDateString()), JSON.stringify({userid:passCredentials.userid, passwd:passCredentials.passwd}) );
}

function getCredentials () {
  var result = null;
  try {
    if (makePassData) {
      result = JSON.parse( deObfuscate( sha256.sha256(new Date().toDateString()), makePassData) );
    }
  } catch (e) {}
  return result;
}

function setCloseCallback (passCallback) {
  if (passCallback) makePassCallback = passCallback; // workaround to pass the callback to close procedure
}

function close () {
  if (makePassCallback) {
    makePassCallback(); // workaround to be able to pass callback during create access
    makePassCallback = null;
  } else hybrix.view.pop();
}

function open (parameters) {
  window.location.hash = '/makePass'; // immediately remove arguments from visible URI
  
  const INPUT_field = document.getElementById('makePass-field');
  const BUTTON_submit = document.getElementById('makePass-submit-button');
  const BUTTON_back = document.getElementById('makePass-back-button');
  const passCredentials = parameters.hasOwnProperty('userid') && parameters.hasOwnProperty('passwd')?parameters:getCredentials();

  INPUT_field.value = '';
  if (passCredentials) {
    BUTTON_submit.removeAttribute('disabled');
    BUTTON_submit.onclick = () => {
      const fieldValue = INPUT_field.value;
      const shortenedID = passCredentials.userid.substr(0,3)+'...'+passCredentials.userid.substr(-3);
      const newTabURI = window.location.href.split('#')[0]+`app/cardpass?version=${version}#id=${passCredentials.userid}&pass=${passCredentials.passwd}&name=`+(fieldValue?`${fieldValue}`:shortenedID);
      window.open(newTabURI, '_blank');
    }
  } else BUTTON_submit.setAttribute('disabled', true);
  
  BUTTON_back.onclick = () => { close(); }
}

exports.open = open;
exports.close = close;
exports.setCredentials = setCredentials;
exports.setCloseCallback = setCloseCallback;
