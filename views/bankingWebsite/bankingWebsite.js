const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;

exports.rout = parameters => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('banking');
};

function open (parameters) {
  console.log(` >>> ${JSON.stringify(parameters)}`);
  const DIV_message = document.getElementById('bankingWebsite-message');
  const A_continue = document.getElementById('bankingWebsite-continue');
  const BUTTON_back = document.getElementById('bankingWebsite-back');
  const BUTTON_done = document.getElementById('bankingWebsite-done');
  const DIV_progress = document.getElementById('bankingWebsite-progress');
  const SPAN_target = document.getElementById('bankingWebsite-target');

  progress(DIV_progress, {
    '{{BANKINGASSET:PROGRESS_DETAILS}}': () => hybrix.view.pop(),
    '{{BANKINGASSET:PROGRESS_AMOUNT}}': () => hybrix.view.pop(),
    '{{BANKINGASSET:PROGRESS_REVIEW}}': () => hybrix.view.pop(),
    '{{BANKINGASSET:PROGRESS_TRANSACT}}': null
  }, 3);
  
  const bankingMethod = parameters.bankingMethod;
  SPAN_target.innerText = bankingMethod.hasOwnProperty('name') && bankingMethod.name ? bankingMethod.name : '{{BANKINGASSET:WEBSITE}}';
  A_continue.href = `${bankingMethod.hasOwnProperty('target') && bankingMethod.target ? bankingMethod.target : '#'}${bankingMethod.hasOwnProperty('path') && bankingMethod.path ? '/' + bankingMethod.path : ''}`;

  BUTTON_close.setAttribute('disabled','true');
  A_continue.removeAttribute('disabled');
  
  A_continue.onclick = () => {
    BUTTON_close.removeAttribute('disabled');
    A_continue.setAttribute('disabled','true');
  }
  
  BUTTON_back.onclick = () => hybrix.view.pop();
  BUTTON_done.onclick = () => hybrix.view.closeAll();
}

exports.open = open;
