const hybrix = require('../../lib/hybrix').hybrix;
const {createElement} = require('../../lib/html');
const {getIcon} = require('../../lib/icon');
const {addAsset, setPreference, setStarred, isPreference, isStarred, getAssetPreferences, getSymbols, addEventListener} = require('../../lib/assets');

exports.rout = () => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('manageAssets');
};

const DIV_assetsPerSymbol = {};

function renderPreferenceAsset (symbol) {
  const DIV_asset = DIV_assetsPerSymbol[symbol];
  const A_action = document.getElementById('manageAssets-symbol-' + symbol);
  if (!DIV_asset || !A_action) return;
  A_action.className = 'pure-button changeManageBtn selectedAsset';
  DIV_asset.classList.add('added');
}
function renderUnPreferenceAsset (symbol) {
  const DIV_asset = DIV_assetsPerSymbol[symbol];
  const A_action = document.getElementById('manageAssets-symbol-' + symbol);
  if (!DIV_asset || !A_action) return;
  A_action.className = 'pure-button changeManageBtn unselectedAsset';
  DIV_asset.classList.remove('added');
}

function renderStarredAsset (symbol) {
  const DIV_asset = DIV_assetsPerSymbol[symbol];
  if (!DIV_asset) return;
  const A_star = DIV_asset.querySelector('.manageAssets-star');
  if (!A_star) return;
  A_star.classList.add('starred');
  A_star.classList.remove('unstarred');
}

function renderUnstarredAsset (symbol) {
  const DIV_asset = DIV_assetsPerSymbol[symbol];
  if (!DIV_asset) return;
  const A_star = DIV_asset.querySelector('.manageAssets-star');
  if (!A_star) return;
  A_star.classList.remove('starred');
  A_star.classList.add('unstarred');
}

function renderManageButton (symbol, name) {
  const preference = isPreference(symbol);
  const starred = isStarred(symbol);

  const isAddedClass = preference ? 'added' : '';
  const DIV_asset = createElement({className: 'tr ' + isAddedClass, symbol});
  DIV_assetsPerSymbol[symbol] = DIV_asset;
  DIV_asset.setAttribute('data', (symbol + name).toLowerCase()); // for search

  const DIV_col1 = createElement({className: 'td col1', parentNode: DIV_asset});

  const btnClass = preference ? 'selectedAsset' : 'unselectedAsset';
  const A_action = createElement({tagName: 'A', id: 'manageAssets-symbol-' + symbol, className: 'pure-button changeManageBtn ' + btnClass, role: 'button', parentNode: DIV_col1});

  const DIV_icon = createElement({className: 'icon', innerText: '...', parentNode: DIV_col1});

  const DIV_symbol = createElement({className: 'asset', innerText: symbol.toUpperCase(), parentNode: DIV_col1});

  const DIV_name = createElement({className: 'full-name', innerText: name, parentNode: DIV_col1});

  const DIV_col2 = createElement({className: 'td col2 actions', parentNode: DIV_asset});

  const maybeStarActiveClass = starred ? ' starred' : ' unstarred';

  const A_star = createElement({
    tagName: 'A',
    className: 'pure-button manageAssets-star ' + maybeStarActiveClass,
    role: 'button',
    parentNode: DIV_col2
  });

  const DIV_action = createElement({
    className: 'actions-icon',
    parentNode: A_action
  });

  const icon = getIcon(symbol, icon => { DIV_icon.innerHTML = icon; }); // get fallback icon directly, update real icon later
  DIV_icon.innerHTML = icon;

  A_action.onclick = () => {
    if (isPreference(symbol)) setPreference(symbol, false);
    else addAsset(symbol, null, console.error, {preference: true, storeAssetPreference: true});
  };

  A_star.onclick = () => {
    if (isStarred(symbol)) {
      A_star.classList.add('unstarred');
      A_star.classList.remove('starred');
      setStarred(symbol, false);
    } else {
      A_star.classList.remove('unstarred');
      A_star.classList.add('starred');
      setStarred(symbol, true);
    }
  };

  return DIV_asset;
}

const onChange = () => {
  const DIV_assets = document.getElementById('manageAssets-list');
  const INPUT_search = document.getElementById('manageAssets-search-assets');
  const showChains = !!document.getElementById('manageAssets-chains').checked;
  const showTokens = !!document.getElementById('manageAssets-tokens').checked;
  const showTest = !!document.getElementById('manageAssets-test').checked;
  const needle = INPUT_search.value.toLowerCase();
  if (!needle) INPUT_search.focus();  // focus input field at start
  for (const DIV_asset of [...DIV_assets.children]) {
    const symbol = DIV_asset.symbol;
    const isTest = symbol === 'dummy' || symbol.startsWith('mock.') || symbol.startsWith('test_');
    const isToken = symbol.includes('.') && !isTest;
    const isChain = !symbol.includes('.') && !isTest;
    const show = (needle !== '' && DIV_asset.getAttribute('data').includes(needle)) ||
     (needle === '' && ((isChain && showChains) || (isToken && showTokens) || (isTest && showTest)));
    DIV_asset.style.display = show ? 'block' : 'none';
    let order;
    if (DIV_asset.symbol.toLowerCase() === needle) order = 0; // "needle"
    else if (DIV_asset.symbol.toLowerCase().startsWith(needle)) order = 1; // "needlebla"
    else if (DIV_asset.symbol.toLowerCase().split('.').includes(needle)) order = 2; // "bla.needle"
    else if (DIV_asset.symbol.toLowerCase().includes(needle)) order = 3; // "blaneedle"
    else order = 4; // "bla"
    DIV_asset.style.order = order;
  }
};

exports.once = () => {
  const DIV_assets = document.getElementById('manageAssets-list');
  hybrix.lib.rout({query: '/list/asset/details'}, assets => {
    assets.sort((a, b) => a.symbol.localeCompare(b.symbol));
    DIV_assets.innerHTML = '';
    for (const asset of assets) {
      const symbol = asset.symbol;
      const A = renderManageButton(symbol, asset.name);
      DIV_assets.appendChild(A);
    }
    onChange();
  });

  const INPUT_search = document.getElementById('manageAssets-search-assets');
  const INPUT_showChains = document.getElementById('manageAssets-chains');
  const INPUT_showTokens = document.getElementById('manageAssets-tokens');
  const INPUT_showTest = document.getElementById('manageAssets-test');
  INPUT_showChains.onchange = INPUT_showTokens.onchange = INPUT_showTest.onchange =
  INPUT_search.onpaste = INPUT_search.keyup = INPUT_search.onchange = INPUT_search.oninput = onChange;

  /* DISABLED
  const BUTTON_scan = document.getElementById('manageAssets-scan');
  BUTTON_scan.classList.remove('disabled');
  BUTTON_scan.onclick = () => hybrix.view.open('scanAssets');
  */

  const DIV_starTip = document.getElementById('manageAssets-star-tip');
  DIV_starTip.style.display = 'block';
};

exports.open = function () {
  const INPUT_search = document.getElementById('manageAssets-search-assets');
  INPUT_search.value = '';
  
  const assetPreferences = getAssetPreferences();
  for (const symbol of getSymbols()) {
    if (assetPreferences.hasOwnProperty(symbol)) {
      renderPreferenceAsset(symbol);
      if (assetPreferences[symbol]) renderStarredAsset(symbol);
      else renderStarredAsset(symbol);
    } else renderUnPreferenceAsset(symbol);
  }
  onChange();
  addEventListener('preferenceAsset', renderPreferenceAsset);
  addEventListener('unpreferenceAsset', renderUnPreferenceAsset);
  addEventListener('starAsset', renderStarredAsset);
  addEventListener('unstarAsset', renderUnstarredAsset);
};
