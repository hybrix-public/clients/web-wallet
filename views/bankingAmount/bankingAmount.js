const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const {getSignatureSteps} = require('../allocate/allocate.js');
const {rout} = require('../banking/banking');
const {getAsset} = require('../../lib/assets');
exports.rout = rout;

const VIRTCHAR = '~';
let maxAmount = 0;
let transferFee = 0;
let transactionFee = 0;
let decimals = 2;

function translatedAction(action) {
  let translatedAction;
  switch (action) {
    case 'deposit': translatedAction='{{BANKINGAMOUNT:ACTION_DEPOSIT}}'; break;
    case 'withdraw': translatedAction='{{BANKINGAMOUNT:ACTION_WITHDRAW}}'; break;
    default: translatedAction='{{BANKINGAMOUNT:ACTION_TRANSFER}}';
  }
  return translatedAction;
}

function renderFee (action, parameters) {
  const DIV_fee = document.getElementById('bankingAmount-fee');
  const INPUT_amount = document.getElementById('bankingAmount-amount');
  const BUTTON_confirm = document.getElementById('bankingAmount-confirm');
  const BUTTON_max = document.getElementById('bankingAmount-max-button');
  const DIV_balanceAmount = document.getElementById('bankingAmount-balance-value');
  const amount = Number(INPUT_amount.value);
  if (isNaN(maxAmount) || maxAmount <= 0) maxAmount = maxAmount;
  BUTTON_max.removeAttribute('disabled');
  if (action === 'deposit') {
    if (isNaN(amount) || amount === 0) {
      DIV_fee.innerHTML = '';
      BUTTON_confirm.setAttribute('disabled', true);
    } else if (isNaN(amount) || amount <= 0) {
      DIV_fee.innerHTML = '{{BANKINGAMOUNT:ENTER_POSITIVE_AMOUNT}}';
      BUTTON_confirm.setAttribute('disabled', true);
    } else if (amount < minAmount) {
      BUTTON_confirm.setAttribute('disabled', true);
      DIV_fee.innerHTML = `{{BANKINGAMOUNT:ENTER_MINIMUM_AMOUNT_A}}${prettyPrintAmount(minAmount, parameters.bankingCurrency)}{{BANKINGAMOUNT:ENTER_MINIMUM_AMOUNT_B}}`;
    } else if (amount > maxAmount) {
      DIV_fee.innerHTML = `{{BANKINGAMOUNT:ENTER_NOT_MORE_A}}${translatedAction(action)}{{BANKINGAMOUNT:ENTER_NOT_MORE_B}}${prettyPrintAmount(maxAmount, parameters.bankingCurrency)}{{BANKINGAMOUNT:ENTER_NOT_MORE_C}}`;
      BUTTON_confirm.setAttribute('disabled', true);
    } else {
      transferFee = calculateTransferFee(parameters.bankingMethod, action, amount);
      if (transferFee > 0) {
        DIV_fee.innerHTML = `{{BANKINGAMOUNT:NOTE_TRANSFER_FEES_A}} {{BANKINGAMOUNT:NOTE_ASKED_TO_REMIT}}${prettyPrintAmount( Math.round((amount + transferFee)*(10**decimals))/(10**decimals) , parameters.bankingCurrency, false, true)}{{BANKINGAMOUNT:NOTE_TRANSFER_FEES_B}}${translatedAction(action)}{{BANKINGAMOUNT:NOTE_TRANSFER_FEES_C}}`;
      } else {
        DIV_fee.innerHTML = `{{BANKINGAMOUNT:NOTE_DEPOSIT_NO_COST}}`;
      }
      BUTTON_confirm.removeAttribute('disabled');
    }
  } else {
    if (isNaN(amount) || amount === 0) {
      DIV_fee.innerHTML = '';
      BUTTON_confirm.setAttribute('disabled', true);
    } else if (isNaN(amount) || amount <= 0) {
      BUTTON_confirm.setAttribute('disabled', true);
      DIV_fee.innerHTML = '{{BANKINGAMOUNT:ENTER_POSITIVE_AMOUNT}}';
    } else if (amount < minAmount) {
      BUTTON_confirm.setAttribute('disabled', true);
      DIV_fee.innerHTML = `{{BANKINGAMOUNT:ENTER_MINIMUM_AMOUNT_A}}${prettyPrintAmount(minAmount, parameters.bankingCurrency)}{{BANKINGAMOUNT:ENTER_MINIMUM_AMOUNT_B}}`;
    } else if (amount > maxAmount) {
      DIV_fee.innerHTML = `{{BANKINGAMOUNT:ENTER_NOT_MORE_A}}${translatedAction(action)}{{BANKINGAMOUNT:ENTER_NOT_MORE_B}}${prettyPrintAmount(maxAmount, parameters.bankingCurrency)}{{BANKINGAMOUNT:ENTER_NOT_MORE_C}}`;
      BUTTON_confirm.setAttribute('disabled', true);
    } else {
      transferFee = calculateTransferFee(parameters.bankingMethod, action, amount);
      if (transferFee > 0) {
        DIV_fee.innerHTML = `{{BANKINGAMOUNT:NOTE_TRANSFER_FEES_A}} {{BANKINGAMOUNT:NOTE_SEND_TOTAL_OF}}${prettyPrintAmount( Math.round((amount + transferFee)*(10**decimals))/(10**decimals) , parameters.bankingCurrency, false, true)}{{BANKINGAMOUNT:NOTE_TRANSFER_FEES_B}}${translatedAction(action)}{{BANKINGAMOUNT:NOTE_TRANSFER_FEES_C}}`;
      } else {
        DIV_fee.innerHTML = `{{BANKINGAMOUNT:NOTE_WITHDRAW_NO_COST}}`;
      }      
      BUTTON_confirm.removeAttribute('disabled');
    }
  }
}

function calculateTransferFee (bankingMethod, action, balance) {
  // TODO const transactionFee = bankingMethod.fees.transaction; // blockchain fee
  const serviceFee = Number(bankingMethod.fees[action].fixed); // flat fee
  const allocatorFee = Number(bankingMethod.fees[action].percent); // percentage fee
  return !isNaN(balance) ? Number(transactionFee) + serviceFee + (allocatorFee * Number(balance)) : 0;
}

exports.open = parameters => {
  const DIV_progress = document.getElementById('bankingAmount-progress');
  const DIV_spinner = document.getElementById('bankingAmount-spinner');
  const BUTTON_max = document.getElementById('bankingAmount-max-button');
  const BUTTON_confirm = document.getElementById('bankingAmount-confirm');
  const BUTTON_cancel = document.getElementById('bankingAmount-cancel');

  const DIV_symbol = document.getElementById('bankingAmount-symbol');
  const INPUT_amount = document.getElementById('bankingAmount-amount');
  const DIV_fee = document.getElementById('bankingAmount-fee');
  const DIV_balanceAmount = document.getElementById('bankingAmount-balance-value');
  const DIV_balanceSymbol = document.getElementById('bankingAmount-balance-symbol');
  const SPAN_action = document.getElementById('bankingAmount-action');

  const [action,bankingMethod,toSymbol] = [parameters.action,parameters.bankingMethod,parameters.bankingCurrency];
  // const fromSymbol = `${VIRTCHAR}${parameters.bankingOption}.${toSymbol}`;
  const decimals = parameters.bankingMethod.hasOwnProperty('factor') && !isNaN(parameters.bankingMethod.factor) ? Number(parameters.bankingMethod.factor) : 2;

  DIV_fee.innerHTML = '';
  BUTTON_confirm.setAttribute('disabled', true);
  BUTTON_max.setAttribute('disabled', true);
  progress(DIV_progress, {
    '{{BANKINGAMOUNT:PROGRESS_DETAILS}}': () => hybrix.view.pop(),
    '{{BANKINGAMOUNT:PROGRESS_AMOUNT}}': null,
    '{{BANKINGAMOUNT:PROGRESS_REVIEW}}': null,
    '{{BANKINGAMOUNT:PROGRESS_TRANSACT}}': null
  }, 1);

  let translatedAction = action ? action : '{{BANKINGAMOUNT:ACTION_TRANSFER}}';
  switch (translatedAction) {
    case 'deposit': translatedAction='{{BANKINGAMOUNT:ACTION_DEPOSIT}}'; break;
    case 'withdraw': translatedAction='{{BANKINGAMOUNT:ACTION_WITHDRAW}}'; break;
    case 'transfer': translatedAction='{{BANKINGAMOUNT:ACTION_TRANSFER}}'; break;
  }
  SPAN_action.innerText = translatedAction;
  DIV_spinner.style.display = 'block';

  /*
  hybrix.lib.rout({query: '/e/deal/estimate/'+fromSymbol+'/'+toSymbol, regular: false},
    estimate => {
      allocatorFee = Number(estimate.fees.allocator);
      serviceFee = Number(estimate.fees.network);
      if (action === 'deposit') {
        maxAmount = Number(estimate.bid.sufficiency);
      } else {
        // withdraw or transfer: here we already know the user's own fiat balance from the asset selection modal
        maxAmount = Number(parameters.fiatBalance<estimate.bid.sufficiency && !isNaN(parameters.fiatBalance) ? parameters.fiatBalance : estimate.bid.sufficiency);
      }
      DIV_balanceAmount.innerText = maxAmount * 0.95;
      DIV_balanceSymbol.innerText = toSymbol.toUpperCase();
      if (maxAmount > 0) BUTTON_max.removeAttribute('disabled'); else BUTTON_max.setAttribute('disabled', true);
      DIV_spinner.style.display = 'none';
    },
    error => {
      console.log(error);
      DIV_balanceAmount.innerText = 'N/A';
      DIV_balanceSymbol.innerText = '';
      BUTTON_max.setAttribute('disabled', true);
      DIV_spinner.style.display = 'none';
    }
  );
  DIV_symbol.innerHTML = toSymbol.toUpperCase() || '';
  */

  minAmount = bankingMethod.minimum;
  if (action === 'deposit') {
    transactionFee = 0;
    maxAmount = bankingMethod.sufficiency;
  } else {
    const asset = getAsset(toSymbol);
    const balance = isNaN(parameters.balance) ? getBalance(toSymbol) : parameters.balance;
    transactionFee = asset.fee;
    const maxAvailableBalance = Math.floor((balance - transactionFee - calculateTransferFee(parameters.bankingMethod, action, balance))*(10**decimals))/(10**decimals);
    maxAmount = maxAvailableBalance < bankingMethod.sufficiency ? maxAvailableBalance : bankingMethod.sufficiency;
  }

  DIV_balanceAmount.innerText = maxAmount ? maxAmount : 'N/A';
  DIV_balanceSymbol.innerText = maxAmount ? toSymbol.toUpperCase() : '';
  if (maxAmount) BUTTON_max.removeAttribute('disabled');
  
  DIV_symbol.innerHTML = toSymbol.toUpperCase() || '';
  INPUT_amount.value = '';

  DIV_spinner.style.display = 'none';

  BUTTON_max.onclick = () => {
    BUTTON_max.setAttribute('disabled', true);
    INPUT_amount.value = maxAmount;
    DIV_balanceAmount.innerText = maxAmount;
    BUTTON_confirm.removeAttribute('disabled');
    renderFee(action, parameters);
  };

  INPUT_amount.onchange = INPUT_amount.oninput = INPUT_amount.onpaste = () => renderFee(action, parameters);

  BUTTON_cancel.onclick = () => hybrix.view.pop();

  BUTTON_confirm.onclick = () => {
    const amount = Number(INPUT_amount.value);
    let payAmount = Math.round(( amount + transferFee )*(10**decimals))/(10**decimals);
    if (parameters.action !== 'deposit') payAmount = Math.round(( payAmount - transactionFee )*(10**decimals))/(10**decimals);
    hybrix.view.open('bankingReview', {amount, payAmount, transactionFee, ...parameters});
  };
};
