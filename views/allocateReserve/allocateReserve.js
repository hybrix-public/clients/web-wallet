const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const {getSignatureSteps} = require('../allocate/allocate.js');
let maxSendableAmount = 0;

function securityExtract (amount, dataCallback, errorCallback) {
  hybrix.lib.sequential([
    ...getSignatureSteps('securityReserveAccount', ['extract', amount]),
    ({accountID, signature}) => ({query: '/e/swap/allocation/account/securityReserve/' + accountID + '/extract/' + amount + '/' + signature, regular: false}), 'rout'
  ], dataCallback, errorCallback);
}

function securityReserve (amount, dataCallback, errorCallback) {
  hybrix.lib.sequential([
    ...getSignatureSteps('securityReserveAccount', ['reserve', amount]),
    ({accountID, signature}) => ({query: '/e/swap/allocation/account/securityReserve/' + accountID + '/reserve/' + amount + '/' + signature, regular: false}), 'rout'
  ], dataCallback, errorCallback);
}

exports.rout = parameters => {
  hybrix.view.swap('allocate');
};

function renderFee (action, details) {
  const DIV_fee = document.getElementById('allocateReserve-fee');
  const INPUT_amount = document.getElementById('allocateReserve-amount');
  const BUTTON_confirm = document.getElementById('allocateReserve-confirm');
  const BUTTON_max = document.getElementById('allocateReserve-max-button');
  BUTTON_max.removeAttribute('disabled');
  const amount = INPUT_amount.value;
  if (action === 'reserve' && typeof details === 'object') {
    if (amount === '') {
      BUTTON_confirm.setAttribute('disabled', true);
      DIV_fee.innerHTML = `Note that a ${details.fees.transfer} fee will be deducted.`;
    } else if (isNaN(amount) || amount <= 0) {
      DIV_fee.innerHTML = 'Please enter a positive amount.';
      BUTTON_confirm.setAttribute('disabled', true);
    } else if (amount > maxSendableAmount) {
      DIV_fee.innerHTML = `You cannot ${action} more than ${prettyPrintAmount(maxSendableAmount, details.symbol)}!`;
      BUTTON_confirm.setAttribute('disabled', true);
    } else {
      DIV_fee.innerHTML = `Note that a ${details.fees.transfer} fee will be deducted.`;
      BUTTON_confirm.removeAttribute('disabled');
    }
  } else {
    if (amount === '') {
      DIV_fee.innerHTML = '';
      BUTTON_confirm.setAttribute('disabled', true);
    } else if (isNaN(amount) || amount <= 0) {
      BUTTON_confirm.setAttribute('disabled', true);
      DIV_fee.innerHTML = 'Please enter a positive amount.';
    } else if (amount > maxSendableAmount) {
      DIV_fee.innerHTML = `You cannot ${action} more than ${prettyPrintAmount(maxSendableAmount, details.symbol)}!`;
      BUTTON_confirm.setAttribute('disabled', true);
    } else {
      DIV_fee.innerHTML = '';
      BUTTON_confirm.removeAttribute('disabled');
    }
  }
}

exports.open = parameters => {
  const DIV_open = document.getElementById('allocateReserve-open');
  const DIV_done = document.getElementById('allocateReserve-done');

  const DIV_spinner = document.getElementById('allocateReserve-spinner');
  const DIV_progress = document.getElementById('allocateReserve-progress');
  const BUTTON_max = document.getElementById('allocateReserve-max-button');
  const BUTTON_confirm = document.getElementById('allocateReserve-confirm');

  const DIV_symbol = document.getElementById('allocateReserve-symbol');
  const INPUT_amount = document.getElementById('allocateReserve-amount');
  const DIV_fee = document.getElementById('allocateReserve-fee');
  const DIV_balanceAmount = document.getElementById('allocateReserve-balance-value');
  const DIV_balanceSymbol = document.getElementById('allocateReserve-balance-symbol');

  const {balance, min, symbol, details} = parameters;
  const callback = parameters.callback;
  let action = parameters.action;

  DIV_open.style.display = 'block';
  DIV_done.style.display = 'none';

  // action = 'extract'|'reserve'\'transfer'|'withdraw'
  BUTTON_max.setAttribute('disabled', true);
  if (action === 'reserve') {
    progress(DIV_progress, { Prepare: null, Reserve: null }, 0);
    const transferFee = parseFloat(details.fees.transfer) * Number(balance) / 100.05;
    maxSendableAmount = balance - transferFee;
  } else {
    progress(DIV_progress, { Prepare: null, Extract: null }, 0);
    maxSendableAmount = details.balance - details.locked;
  }
  if (maxSendableAmount > 0) BUTTON_max.removeAttribute('disabled');
  DIV_balanceAmount.innerText = maxSendableAmount;
  DIV_balanceSymbol.innerText = details.symbol.toUpperCase();

  BUTTON_confirm.innerText = action==='reserve'?'Reserve':'Extract';
  DIV_symbol.innerHTML = symbol.toUpperCase() || '';
  INPUT_amount.value = min || '';
  DIV_fee.innerHTML = '';
  INPUT_amount.oninput = INPUT_amount.onpaste = () => renderFee(action, details);

  BUTTON_max.onclick = () => {
    BUTTON_max.setAttribute('disabled', true);
    INPUT_amount.value = maxSendableAmount;
    if (maxSendableAmount > 0) BUTTON_confirm.removeAttribute('disabled');
  };

  BUTTON_confirm.onclick = () => {
    if (action==='done') {
      hybrix.view.closeAll();
    } else {
      DIV_spinner.style.display = 'block';
      BUTTON_confirm.setAttribute('disabled', true);
      BUTTON_max.setAttribute('disabled', true);
      if (action==='reserve') {
        progress(DIV_progress, { Prepare: null, Reserve: null }, 1);
      } else {
        progress(DIV_progress, { Prepare: null, Extract: null }, 1);
      }
      const amount = Number(INPUT_amount.value);
      const errorCallback = error => {
        if (action==='reserve') {
          progress(DIV_progress, { Prepare: null, Reserve: null }, 0);
        } else {
          progress(DIV_progress, { Prepare: null, Extract: null }, 0);
        }
        DIV_spinner.style.display = 'none';
        DIV_fee.innerHTML = 'Error: ' + error;
        console.error(error);
      };
      const dataCallback = () => {
        action = 'done';
        BUTTON_confirm.innerText = 'Close';
        DIV_spinner.style.display = 'none';
        DIV_open.style.display = 'none';
        DIV_done.style.display = 'block';
        BUTTON_confirm.removeAttribute('disabled');
        BUTTON_max.removeAttribute('disabled');
        callback();
      };
      if (action === 'extract') securityExtract(amount, dataCallback, errorCallback);
      else if (action === 'reserve') securityReserve(amount, dataCallback, errorCallback);
    }
  };
};
