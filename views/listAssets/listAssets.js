const hybrix = require('../../lib/hybrix').hybrix;
const {createElement} = require('../../lib/html');
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const {getCurrencyPreference} = require('../settings/settings');
const {getIcon} = require('../../lib/icon');
const {setPreference} = require('../../lib/assets');
//const branding = require('../../lib/branding');

const {
  addEventListener, getAsset, getAssetPreferences,
  getBalance, isStarred, hasSufficientFuel, isPreference,
  isSubSymbolOf, getTags, getOrder, isUnified, hasPending
} = require('../../lib/assets');

const DIV_containers = {};

exports.rout = () => hybrix.view.swap('listAssets');

function checkUnifiedAsset (symbol) {
  const order = getOrder(symbol);
  const DIV_container = DIV_containers[symbol];
  if (!DIV_container) return;
  const DIV_content = document.getElementById('asset-list');
  if (isUnified(symbol)) { // if unified find subAssets
    for (const DIV_subContainer of DIV_content.children) {
      const subSymbol = DIV_subContainer.symbol;
      if (isSubSymbolOf(subSymbol, symbol)) {
        const subStarred = isStarred(subSymbol);
        DIV_subContainer.classList.add('listAssets-subAsset');
        DIV_subContainer.style.order = order + (subStarred ? 1 : 2);
      }
    }
  } else {
    for (const DIV_superContainer of DIV_content.children) {
      const superSymbol = DIV_superContainer.symbol;
      if (isSubSymbolOf(symbol, superSymbol)) {
        const superOrder = getOrder(superSymbol);
        const starred = isStarred(symbol);
        DIV_container.classList.add('listAssets-subAsset');
        return superOrder + (starred ? 1 : 2);
      }
    }
  }
  return order;
}

function updateAssetValuation (symbol) {
  const DIV_container = DIV_containers[symbol];
  if (!DIV_container) return;
  const DIV_valuation = DIV_container.querySelector('.valuation');
  const balance = getBalance(symbol);
  if (balance === 'n/a' || typeof balance === 'undefined') DIV_valuation.innerHTML = '';
  else {
    const currency = getCurrencyPreference();
    if (Number(balance) === 0) DIV_valuation.innerHTML = prettyPrintAmount(0, currency, true);
    else {
      DIV_valuation.innerHTML = '...';
      hybrix.lib.getValuation({fromSymbol: symbol, toSymbol: currency, amount: balance}, rate => {
        DIV_valuation.innerHTML = prettyPrintAmount(rate, currency, true);
      }, error => {
        console.error('Could not retrieve valuation for ' + symbol, error);
        DIV_valuation.innerHTML = '<small style="opacity:0.5;">n/a</small>';
      });
    }
  }
}

exports.updateAssetValuations = () => {
  for (const tag of getTags()) updateAssetValuation(tag);
};

function updateAsset (symbol) {
  const DIV_container = DIV_containers[symbol];
  if (!DIV_container) return;
  DIV_container.classList.remove('loading');

  const asset = getAsset(symbol);
  const SPAN_asset = DIV_container.querySelector('.asset > span');
  if (asset.name) {
    SPAN_asset.innerText = asset.name;
  }

  const DIV_balance = DIV_container.querySelector('.asset-balance-info > .balance');
  const DIV_pending = DIV_container.querySelector('.listAssets-pending-symbol');
  const balance = getBalance(symbol);
  if (
    balance === 'n/a' || Number(balance) === 0 || // No lock if no balance available
    hasSufficientFuel(symbol) !== false // if true or undefined : show as unlocked
  ) {
    DIV_container.classList.remove('listAssets-locked');
  } else {
    DIV_container.classList.add('listAssets-locked');
  }
  DIV_balance.innerHTML = prettyPrintAmount(balance, symbol, false, true);
  if (DIV_balance.balance !== balance) updateAssetValuation(symbol);
  DIV_balance.balance = balance;
  DIV_pending.style.display = hasPending(symbol) ? null : 'none';
}

function renderMessage (message, isError = false) {
  const DIV_content = document.getElementById('asset-list');
  const DIV_error = createElement({
    innerHTML: message,
    className: 'listAssets-message' + (isError ? ' error' : '')
  });
  DIV_error.onclick = () => DIV_content.removeChild(DIV_error);
  DIV_content.prepend(DIV_error);
}

function updateOrder (symbol) {
  const DIV_container = DIV_containers[symbol];
  if (!DIV_container) return;
  const starred = isStarred(symbol);
  const order = checkUnifiedAsset(symbol);
  DIV_container.style.order = order;
  const A_star = DIV_container.querySelector('.listAssets-starred');
  A_star.style.display = starred ? 'inline-block' : 'none';
}

function renderPreferenceAsset (symbol) {
  if (DIV_containers[symbol]) return;
  const DIV_content = document.getElementById('asset-list');
  const DIV_container = createElement({classList: 'asset-container loading'});
  const DIV_asset = createElement({classList: 'asset', parentNode: DIV_container});
  const DIV_icon = createElement({classList: 'icon', innerHTML: '', parentNode: DIV_asset});
  const SPAN_asset = createElement({tagName: 'SPAN', innerText: symbol.toUpperCase(), parentNode: DIV_asset});
  const DIV_star = createElement({classList: 'star', parentNode: DIV_asset});
  const A_star = createElement({tagName: 'A', className: 'listAssets-starred', role: 'button', parentNode: DIV_star});
  const DIV_balanceInfo = createElement({classList: 'asset-balance-info', parentNode: DIV_container});
  const DIV_balance = createElement({classList: 'balance', parentNode: DIV_balanceInfo, innerHTML: '...'});
  const DIV_pending = createElement({classList: 'listAssets-pending-symbol', parentNode: DIV_balanceInfo, style: {display: 'none'}});
  const DIV_valuation = createElement({classList: 'valuation', parentNode: DIV_balanceInfo});
  // const SPAN_symbol = createElement({tagName: 'SPAN', innerHTML: prettyPrintSymbol(symbol), parentNode: DIV_asset});
  if (symbol === 'hy') {
    const SPAN_getSomeHy = createElement({
      onclick: event => {
        hybrix.view.open('getSomeHy');
        event.stopPropagation();
      },
      tagName: 'SPAN',
      id: 'listAssets-getSomeHy',
      innerText: '{{LISTASSETS:GET_SOME_HY}}',
      parentNode: DIV_container
    });
  }

  DIV_container.symbol = symbol; // to determine sub symbol for unified
  DIV_container.onclick = () => hybrix.view.swap('viewAsset', {symbol});

  DIV_content.appendChild(DIV_container);

  DIV_containers[symbol] = DIV_container;

  const icon = getIcon(symbol, icon => { DIV_icon.innerHTML = icon; }); // get fallback icon direclty, update real icon later
  DIV_icon.innerHTML = icon;

  DIV_pending.onclick = () => hybrix.view.open('pending');

  updateOrder(symbol);
}

function renderAddAsset (symbol) {
  if (!isPreference(symbol)) return;
  const DIV_container = DIV_containers[symbol];
  if (!DIV_container) return;
  DIV_container.classList.remove('loading');
  updateOrder(symbol);
}

/*
function renderBranding () {
  branding.logo(document.querySelector('.nav-logo'));
}
*/

function removeAssetContainer (symbol) {
  const DIV_container = DIV_containers[symbol];
  if (DIV_container) {
    if (DIV_container.parentNode) DIV_container.parentNode.removeChild(DIV_container);
  }
  delete DIV_containers[symbol];
}

function renderFailAsset (symbol, error) {
  const DIV_container = DIV_containers[symbol];
  removeAssetContainer(symbol);
  if (DIV_container) {
    if (error.includes('not found')) {
      renderMessage(`{{LISTASSETS:ERROR_ASSET_NO_SUPPORT_A}}${symbol.toUpperCase()}{{LISTASSETS:ERROR_ASSET_NO_SUPPORT_B}}`, true);
      setPreference(symbol, false);
    } else {
      console.error(`Failed to add ${symbol.toUpperCase()} asset!`, error);
      renderMessage(`{{LISTASSETS:ERROR_FAILED_ADD_A}}${symbol.toUpperCase()}{{LISTASSETS:ERROR_FAILED_ADD_B}}`, true);
    }
  }
}

exports.once = () => {
  const BUTTON_pending = document.getElementById('listAssets-pending-button');
  BUTTON_pending.onclick = () => hybrix.view.open('pending');

  const BUTTON_scanner = document.getElementById('listAssets-scanner-button');
  BUTTON_scanner.onclick = () => hybrix.view.open('qrScanner', {
    type: 'transaction',
    dataCallback: result => {
      if (result.symbol !== null) {
        hybrix.lib.addAsset({symbol: result.symbol}, () => {
          hybrix.view.open('transaction', {symbol: result.symbol, target: result.address, amount: result.amount, message: result.message});
        },
        error => console.error(`Failed to add ${result.symbol} symbol.`, error));
      } else { // no symbol selected, this can be done in sendAsset ui
        hybrix.view.open('transaction', {symbol: undefined, target: result.address, amount: result.amount, message: result.message});
      }
    }
  });

  const BUTTON_apps = document.getElementById('listAssets-apps-button');
  BUTTON_apps.onclick = () => hybrix.view.open('appsList');

  const BUTTON_addWallet = document.getElementById('listAssets-addWallet');
  BUTTON_addWallet.onclick = () => hybrix.view.open('manageAssets');

  const BUTTON_banking = document.getElementById('listAssets-banking');
  BUTTON_banking.onclick = () => hybrix.view.open('banking');
};

exports.open = parameters => {
  const {errorMessage, message} = parameters;
  if (message) renderMessage(message, false);
  if (errorMessage) renderMessage(errorMessage, true);
  const assetPreferences = getAssetPreferences();
  for (const symbol in assetPreferences) renderPreferenceAsset(symbol);

  addEventListener('refreshAsset', updateAsset, 'listAssets');
  addEventListener('preferenceAsset', renderPreferenceAsset);
  addEventListener('addAsset', renderAddAsset);
  addEventListener('removeAsset', removeAssetContainer);
  addEventListener('unpreferenceAsset', removeAssetContainer);
  addEventListener('failAsset', renderFailAsset);
  addEventListener('starAsset', symbol => updateOrder(symbol));
  addEventListener('unstarAsset', symbol => updateOrder(symbol));
};

exports.clear = () => {
  const DIV_assets = document.getElementById('asset-list');
  DIV_assets.innerHTML = '';
};
