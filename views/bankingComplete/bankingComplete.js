const hybrix = require('../../lib/hybrix').hybrix;
const {progress, radial} = require('../../lib/progress');
const sha256 = require('js-sha256');

/*
const MAX_CLAIM_ATTEMPTS = 5;
const CLAIM_RETRY_DELAY_MS = 2000;
const remitDeal = (rawTx, proposal, updateRadial) => (retries = 0) => {
  const dealID = proposal.id;

  hybrix.lib.rout({query: `/e/swap/deal/remit/${dealID}/${rawTx}`, regular: false},
    transactionID => {
      hybrix.lib.addPending({transaction: {ref: 'deal:' + dealID, type: 'deal', meta: {transactionID, ...proposal}}},()=>{},console.error);
      const BUTTON_done = document.getElementById('bankingComplete-done');
      BUTTON_done.classList.remove('disabled');
      updateRadial(1, 'Swap deal active.');
      const SWAP_message = document.getElementById('bankingComplete-message');
      SWAP_message.innerHTML = `You made a deal! The remote transfer of your swap is pending. Check your <span id="bankingComplete-pending">pending transactions</span> to see the progress of deal <b>${dealID}</b>. Your transaction ID is: ${transactionID}.`;
      const SPAN_pending = document.getElementById('bankingComplete-pending');
      if (SPAN_pending) SPAN_pending.onclick = () => hybrix.view.open('pending');
      hybrix.view.exec('viewAsset', 'refreshPending', proposal.ask.symbol);
    },
    error => {
      console.error('Failed to claim swap deal!', dealID, error);
      if (retries > MAX_CLAIM_ATTEMPTS) { // retry attempts exhausted
        setTimeout(
          () => failProposal(updateRadial, `Failed to initiate swap deal!', 'Something went wrong while claiming your swap deal with id ${dealID}. Please contact hybrix support!`)(error),
          Math.pow(2, retries) * CLAIM_RETRY_DELAY_MS);
      } else { // another attempt
        remitDeal(rawTx, proposal, updateRadial)(retries + 1);
      }
    },
    progress => {
      if (retries > 0) updateRadial(0.6 + 0.2 * progress, `Claiming swap deal (Attempt (${retries+1}/${MAX_CLAIM_ATTEMPTS})...`);
      else updateRadial(0.6 + 0.2 * progress, 'Remitting swap...');
    }
  );
};
*/

const handleProposal = (parameters, updateRadial) => proposal => {
  const BUTTON_done = document.getElementById('bankingComplete-done');
  const MODAL_message = document.getElementById('bankingComplete-message');
  if (parameters.action === 'deposit') {
    const bankAmount = `<div style="color: #AAA">{{BANKINGCOMPLETE:AMOUNT}}</div>${parameters.payAmount} ${parameters.bankingCurrency.toUpperCase()}`;
    const bankTarget = '<div style="color: #AAA">{{BANKINGCOMPLETE:BANK_ACCOUNT}}</div>'+proposal.target.replaceAll('^','<br />');
    const bankDescription = `<div style="color: #AAA">{{BANKINGCOMPLETE:DESCRIPTION}}</div>{{BANKINGCOMPLETE:VOUCHER}} ${proposal.id}`;
    hybrix.view.open('alert', {icon:'checkmark', title: '{{BANKINGCOMPLETE:SEND_DEPOSIT}}', text: '{{BANKINGCOMPLETE:SEND_DEPOSIT_TEXT}}', textMore: `${bankAmount}<br><br>${bankTarget}<br><br>${bankDescription}`, button: '{{BANKINGCOMPLETE:BUTTON_USER_DONE}}',
      callback: () => {
        hybrix.view.pop();
        updateRadial(1, '{{BANKINGCOMPLETE:DEPOSIT_SUBMITTED}}');
        MODAL_message.innerHTML = `{{BANKINGCOMPLETE:TRANSFER_PENDING}} <b>${proposal.id}</b>.`;
        BUTTON_done.classList.remove('disabled');
      }
    });
  } else {
    const description = parameters.description.trim().replaceAll('{ID}',proposal.id);
    const transaction = {
      symbol: parameters.bankingCurrency,
      amount: parameters.payAmount,
      target: proposal.target,
      addPending: true // TODO: set to false if you want to be added to pending as a swap/banking tx later on.
    };
    hybrix.lib.transaction(transaction,
      transactionID => {
        const hashedTransactionID = sha256.sha256(transactionID);
        if (description !== '') hybrix.lib.save({key:hashedTransactionID,value:description,signed:true,encrypted:false}, () => {}, () => {});
        // TODO? : hybrix.lib.addPending({transaction: {ref: 'bank:' + dealID, type: 'bank', meta: {transactionID, ...proposal}}},()=>{},console.error);
        hybrix.view.exec('viewAsset', 'refreshPending', parameters.bankingCurrency);
        updateRadial(1, (parameters.action === 'withdraw' ? '{{BANKINGCOMPLETE:WITHDRAWAL}}' : '{{BANKINGCOMPLETE:TRANSFER}}')+' {{BANKINGCOMPLETE:SUBMITTED}}');
        MODAL_message.innerHTML = `{{BANKINGCOMPLETE:TRANSFER_PENDING}} <b>${proposal.id}</b>.<br><br>{{BANKINGCOMPLETE:YOUR_TXID}}: ${transactionID}.`;
        BUTTON_done.classList.remove('disabled');
      },
      failProposal(updateRadial, '{{BANKINGCOMPLETE:FAILED_TX}}', '{{BANKINGCOMPLETE:AMOUNT_UNAVAILABLE}}'),
      progress => updateRadial(0.4 + 0.2 * progress, '{{BANKINGCOMPLETE:SENDING_TX}}')
    );
  }
};

const failProposal = (updateRadial, message, description) => error => {
  const DIV_progress = document.getElementById('bankingComplete-progress');
  const BUTTON_done = document.getElementById('bankingComplete-done');
  BUTTON_done.classList.remove('disabled');
  updateRadial(false, message);
  DIV_progress.children[5].classList.add('failed');
  DIV_progress.children[6].classList.add('failed');
  const SWAP_message = document.getElementById('bankingComplete-message');
  SWAP_message.innerHTML = description + (error ? `<br/><br/>${error}` : '');
};

const bankingCall = (parameters, updateRadial) => target => {
  const method = parameters.bankingMethod.type.split(':')[1];
  const description = parameters.action === 'transfer' ? '/'+parameters.description.replaceAll(' ','_') : '';
  hybrix.lib.rout({query: `/e/banking/${parameters.action}/${method}/${parameters.bankingCurrency}/${parameters.amount}/${target}/${parameters.email}${description}`, regular: false},
    handleProposal(parameters, updateRadial),
    failProposal(updateRadial, 'Failed to contact gateway!', `The ${method} request gave an unexpected response! Please close this window and try again.`),
    progress => updateRadial(0.2 + 0.2 * progress, '{{BANKINGCOMPLETE:GETTING_PROPOSAL}}')
  );
};

exports.open = function (parameters) {
  const BUTTON_done = document.getElementById('bankingComplete-done');
  BUTTON_done.classList.add('disabled');

  const SWAP_message = document.getElementById('bankingComplete-message');
  SWAP_message.innerHTML = '';

  const DIV_progress = document.getElementById('bankingComplete-progress');
  progress(DIV_progress, {
    '{{BANKINGCOMPLETE:PROGRESS_DETAILS}}': null,
    '{{BANKINGCOMPLETE:PROGRESS_AMOUNT}}': null,
    '{{BANKINGCOMPLETE:PROGRESS_REVIEW}}': null,
    '{{BANKINGCOMPLETE:PROGRESS_TRANSACT}}': null
  }, 3);
  const DIV_radial = document.getElementById('bankingComplete-radial');

  const updateRadial = radial(DIV_radial, '{{BANKINGCOMPLETE:REQUEST_PROPOSAL}}', 0.2);

  if (!isNaN(parameters.amount) && parameters.amount > 0) {
    if (parameters.action === 'deposit' && typeof parameters.bankingCurrency === 'string') {
      hybrix.lib.getAddress({symbol: parameters.bankingCurrency}, bankingCall(parameters, updateRadial), console.error);
    } else if (parameters.target instanceof Array) {
      const target = parameters.target.join('^').replaceAll(' ','_');
      bankingCall(parameters, updateRadial)(target);
    } else hybrix.view.pop();
  } else hybrix.view.pop();

  BUTTON_done.onclick = () => {
    if(!BUTTON_done.classList.contains('disabled')) {
      hybrix.view.closeAll();
    }
  }
};
