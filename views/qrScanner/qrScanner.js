const {parseHash, hybrix} = require('../../lib/hybrix');
const validations = require('../../common/qr-validation.js');

// The QR-scanner is now loaded in the header to stay outside of webpack.
// DEPRECATED: const QrScanner = require('../../files/js/qr-scanner.umd.min.js');
// DEPRECATED: QrScanner.WORKER_PATH = './js/qr-scanner-worker.min.js';

const qrWrapper = {};

exports.rout = parameters => {
  if (parameters.hasOwnProperty('symbol')) {
    const {symbol} = parameters;
    hybrix.view.swap('viewAsset', {symbol});
    hybrix.view.open('qrScanner', {
      symbol,
      dataCallback: result => hybrix.view.open('transaction', {symbol, target: result.address, amount: result.amount, description: result.description, message: result.message})
    });
  } else {
    hybrix.view.swap('listAssets');
    hybrix.view.open('qrScanner', {
      dataCallback: result => hybrix.view.open('transaction', {symbol: result.symbol, target: result.address, amount: result.amount, description: result.description, message: result.message})
    });
  }
};

async function stopQrScanner () {
  if (qrWrapper.hasOwnProperty('qrScanner')) {
    await qrWrapper.qrScanner.stop();
    await qrWrapper.qrScanner.destroy();
    qrWrapper.qrScanner = null;
  }
}

const checkQrData = (QRType, scanResult, dataCallback, errorCallback) => {
  const resultString = scanResult.data;
  if (!qrWrapper.hasOwnProperty('qrScanner')) return errorCallback('{{QRSCANNER:ERROR_FAILED_GET_DATA}} ');
  if (QRType === 'login') { // expects 'https://wallet.hybrix.io/#/viewId/parameter1=...&...'
    const hash = '#' + resultString.split('#')[1]; // 'https://wallet.hybrix.io/#/viewId/parameter1=...&...'  -> '#/viewId/parameter1=...&...'
    const {viewId, parameters} = parseHash(hash);
    if (typeof parameters === 'object' && (parameters.hasOwnProperty('id') || (parameters.hasOwnProperty('user') && parameters.hasOwnProperty('pass')))) return dataCallback(viewId?viewId:'login', parameters);
    else return errorCallback('{{QRSCANNER:ERROR_NO_CREDENTIALS}} ');
  } else if (QRType === 'transaction') { // asset transfer
    let parsedResult;
    try {
      parsedResult = validateQrData(resultString);
    } catch (error) {
      return errorCallback(error);
    }
    if (parsedResult.error) return errorCallback(parsedResult.data);
    else return dataCallback(parsedResult.data); // {symbol, address, [amount|null], [timestamp|null], [message|null]}
  } else return errorCallback('{{QRSCANNER:ERROR_FAILED_PARSE_LOGIN}} ');
};

function validateQrData (string) {
  const resParsed = validations.parseToObject(string);
  if (resParsed === null || typeof resParsed !== 'object') return {error: 1, data: `{{QRSCANNER:ERROR_FAILED_PARSE_DATA}} {{QRSCANNER:DATA_READ}} ${string}`};
  const result = validations.validate(resParsed.symbol, resParsed.amount, resParsed.address, resParsed.timestamp);
  if (result.error === 0) return {error: 0, data: resParsed};
  else {
    result.data += ` {{QRSCANNER:DATA_READ}} ${string}`;
    return result;
  }
}

// dataCallback  ({symbol,address,amount,message})=>{...}
// cancelCallback  ()=>{...}
exports.open = function ({dataCallback, cancelCallback, title, type}) {
  const DIV_message = document.getElementById('qrScanner-message');
  if (title) {
    const H2_title = document.getElementById('qrScanner-title');
    H2_title.innerText = title;
  }
  DIV_message.innerHTML = '';

  const wrappedDataCallback = (resultA, resultB) => {
    stopQrScanner();
    if (typeof dataCallback === 'function') {
      hybrix.view.pop();
      return dataCallback(resultA, resultB);
    } else DIV_message.innerHTML = JSON.stringify(resultA);
  };

  const errorCallback = error => {
    if (error === 'No QR code found') return; // ignore default error thrown while still busy
    DIV_message.innerHTML += error;
  };

  QrScanner.hasCamera().then(hasCamera => {
    if (!hasCamera) errorCallback('{{QRSCANNER:ERROR_NO_CAMERA}} {{QRSCANNER:SHOULD_ALLOW_ACCESS}} ');
  });

  qrWrapper.qrScanner = new QrScanner(document.getElementById('qrScanner-video'), scanResult => { checkQrData(type, scanResult, wrappedDataCallback, errorCallback); },
                            {
                              maxScansPerSecond:4,
                              highlightScanRegion:true,
                              highlightCodeOutline:true
                            });

  qrWrapper.qrScanner._onDecodeError = e => {} /* console.log(`QR-code scanner decoding error: ${e}`); */
  qrWrapper.qrScanner.setInversionMode('both');
  qrWrapper.qrScanner.start().catch(e => errorCallback('{{QRSCANNER:ERROR_CANNOT_START_SCAN}}'));

  // DEBUG: to test 'scan' without actual QR-code
  /*
    setTimeout( ()=>{
      checkQrData('login', {data:'https://wallet.hybrix.io/#/login/user=...&pass=...'}, wrappedDataCallback, errorCallback);
    }, 3000);
  */
  
  const BUTTON_cancel = document.getElementById('qrScanner-cancel');
  BUTTON_cancel.onclick = () => {
    stopQrScanner();
    hybrix.view.pop();
    if (typeof cancelCallback === 'function') cancelCallback();
  };
};

exports.close = () => stopQrScanner();
