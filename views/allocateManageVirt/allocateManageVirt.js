const spinner = require('../../lib/spinner');
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const hybrix = require('../../lib/hybrix').hybrix;
const {setPair, SWAP_STATS_CACHE_TIME} = require('../allocateManagePair/allocateManagePair.js');
const progress = require('../../lib/progress').progress;
const {VIRTCHAR, getSignatureSteps, refreshPairs} = require('../allocate/allocate.js');
const {deletePair, getPair} = require('../allocateManagePair/allocateManagePair.js');
const { addEventListener, getBalance} = require('../../lib/assets');
let virtualBalance = 0;
let virtualAddress = null;

exports.rout = parameters => {
  hybrix.view.swap('allocate');
  hybrix.view.open('allocateManageVirt', parameters);
};

const setAddressVirtual = (symbol, address, dataCallback, errorCallback) => {
  if (address) {
    address = address.replace(' ','_').replace(/[#\/~@$%^&*=+]/,'');
    hybrix.lib.sequential([
      ...getSignatureSteps('setAddressAccount', [symbol,address]),
      ({accountID, signature}) => ({query: '/e/swap/allocation/account/setAddress/' + accountID + '/' + symbol + '/' + address + '/' + signature, regular: false}), 'rout'
    ], dataCallback, errorCallback);
  } else dataCallback();
}

const addressVirtual = (symbol, dataCallback, errorCallback) => {
  hybrix.lib.sequential([
    ...getSignatureSteps('addressAccount', [symbol]),
    ({accountID, signature}) => ({query: '/e/swap/allocation/account/address/' + accountID + '/' + symbol + '/' + signature, regular: false}), 'rout'
  ], dataCallback, errorCallback);
}

const depositVirtual = (symbol, amount, dataCallback, errorCallback) => {
  if (amount) {
    hybrix.lib.sequential([
      ...getSignatureSteps('rebalancePair', [symbol, String(amount)]),
      ({accountID, signature}) => ({query: '/e/swap/allocation/pair/rebalance/' + accountID + '/' + symbol + '/' + amount + '/' + signature}), 'rout'
    ], dataCallback, errorCallback);
  } else dataCallback();
}

const balanceVirtual = (symbol, dataCallback, errorCallback) => {
  hybrix.lib.sequential([
    ...getSignatureSteps('balanceAccount', [symbol]),
    ({accountID, signature}) => ({query: '/e/swap/allocation/account/balance/' + accountID + '/' + symbol + '/' + signature, regular: false}), 'rout'
  ], dataCallback, errorCallback);
}

exports.open = parameters => {
  let {pair, fromSymbol, toSymbol, createNew} = parameters;
  if (!createNew && typeof pair !== 'undefined' && pair) {
    fromSymbol = pair.split(':')[0];
    toSymbol = pair.split(':')[1];
  } else {
    pair = fromSymbol + ':' + toSymbol;
  }

  const H2_title = document.getElementById('allocateManageVirt-title');
  const DIV_progress = document.getElementById('allocateManageVirt-progress');
  const DIV_pair = document.getElementById('allocateManageVirt-pair');
  const DIV_explain = document.getElementById('allocateManageVirt-explain');
  const DIV_spinner = document.getElementById('allocateManageVirt-spinner');
  const DIV_spinner_message = document.getElementById('allocateManageVirt-spinner-message');
  const DIV_mainSymbol = document.getElementById('allocateManageVirt-symbol');
  const SPAN_mainSymbol = document.getElementById('allocateManageVirt-symbolSpan');
  const SPAN_mainAddress = document.getElementById('allocateManageVirt-addressSpan');
  const DIV_message = document.getElementById('allocateManageVirt-message');
  const DIV_expanded = document.getElementById('allocateManageVirt-expanded');
  const SPAN_feeRange = document.getElementById('allocateManageVirt-feeRange');
  const INPUT_fee = document.getElementById('allocateManageVirt-fee');
  const SPAN_risk = document.getElementById('allocateManageVirt-risk');
  const SPAN_rate = document.getElementById('allocateManageVirt-rate');
  const SPAN_sufficiency = document.getElementById('allocateManageVirt-sufficiency');

  const INPUT_address = document.getElementById('allocateManageVirt-address');
  const INPUT_balance = document.getElementById('allocateManageVirt-amount');

  const BUTTON_delete = document.getElementById('allocateManageVirt-delete');
  const BUTTON_cancel = document.getElementById('allocateManageVirt-cancel');
  const BUTTON_save = document.getElementById('allocateManageVirt-save');

  DIV_spinner.style.display = 'block';
  DIV_spinner_message.innerText = 'Loading virtual pair data...';
  BUTTON_save.setAttribute('disabled', true);
  BUTTON_cancel.removeAttribute('disabled');
  DIV_mainSymbol.innerText = toSymbol;
  INPUT_address.value = '';
  INPUT_address.innerHTML = '';
  INPUT_balance.value = '';
  INPUT_balance.innerHTML = '';
  DIV_message.innerHTML = '';
  SPAN_feeRange.innerHTML = '...';
  SPAN_risk.innerHTML = '...';
  SPAN_sufficiency.innerHTML = '...';

  let editToSymbol;
  if(toSymbol.charAt(0) === VIRTCHAR) {
    editToSymbol = toSymbol;
    editFromSymbol = fromSymbol;
    expanded = true;
  } else {
    editToSymbol = fromSymbol;
    editFromSymbol = toSymbol;
    expanded = false;
  }

  SPAN_mainSymbol.innerText = editToSymbol.split('.')[1];
  SPAN_mainAddress.innerText = editToSymbol.split('.')[0].substr(1);

  function disableSpinner() {   // remove spinner if both fields loaded
    if (!INPUT_balance.getAttribute('disabled') && !INPUT_address.getAttribute('disabled')) DIV_spinner.style.display = 'none';
  }

  BUTTON_cancel.style.display = 'none';
  BUTTON_delete.style.display = 'inline-block';
  BUTTON_delete.removeAttribute('disabled');
  setTimeout( () => {  // give UI chance to update first
    INPUT_address.setAttribute('disabled',true);
    INPUT_balance.setAttribute('disabled',true);
    addressVirtual(editToSymbol, (address) => {
      if (address) {
        INPUT_address.removeAttribute('disabled');
        INPUT_address.value = address;
        virtualAddress = address;
      } else {
        INPUT_address.value = '';
      }
      disableSpinner();
    }, error => {
      INPUT_address.removeAttribute('disabled');
      disableSpinner();
      console.error('Could not get virtual address! '+error);
    });
    getPair(editFromSymbol, editToSymbol, pairData => {
      if (!isNaN(pairData.fee) && pairData.fee>0 && pairData.fee<5) {
        INPUT_fee.value = pairData.fee;
      } else {
        INPUT_fee.value = 0.35;
      }
      const balance = pairData.balance;
      if (balance > 0) {
        hasAllocatedBalance = true;
        INPUT_balance.value = Number(balance);
        virtualBalance = Number(balance);
      } else {
        INPUT_balance.value = 0;
      }
      INPUT_balance.removeAttribute('disabled');
      SPAN_sufficiency.innerHTML = prettyPrintAmount(pairData.sufficiency, pairData.bid, false, true);
      SPAN_risk.innerHTML = pairData.risk;
      disableSpinner();
    }, error => {
      INPUT_balance.removeAttribute('disabled');
      SPAN_sufficiency.innerHTML = 'N/A';
      SPAN_risk.innerHTML = 'N/A';
      disableSpinner();
      console.error(error);
    });
    SPAN_rate.innerHTML = `1 ${fromSymbol.toUpperCase()} = 1 ${toSymbol.toUpperCase()}`;
    function saveEnable() {
      if (INPUT_address.value &&
          !isNaN(INPUT_balance.value) && INPUT_balance.value>0 &&
          !isNaN(INPUT_fee.value) && INPUT_fee.value > 0 && INPUT_fee.value < 5) {
        BUTTON_save.removeAttribute('disabled');
      } else {
        BUTTON_save.setAttribute('disabled',true);
        DIV_message.innerHTML = 'Please set a proper address, balance and fee!';
      }
    }
    INPUT_address.onchange = INPUT_address.oninput = INPUT_address.onpaste = () => {
      saveEnable();
    }
    INPUT_balance.onchange = INPUT_balance.oninput = INPUT_balance.onpaste = () => {
      saveEnable();
    }
    INPUT_fee.onchange = INPUT_fee.oninput = INPUT_fee.onpaste = () => {
      saveEnable();
    }
    if (!INPUT_address.getAttribute('disabled')) DIV_spinner.style.display = 'none'; // remove spinner if both fields loaded

  }, 500);

  if (!createNew) {
    H2_title.innerHTML = 'Manage Pair';
    DIV_progress.style.display = 'none';
    DIV_pair.innerHTML = `${fromSymbol.toUpperCase()} ⊳ ${toSymbol.toUpperCase()}`;
    DIV_explain.innerText = `This allocation pair accepts ${fromSymbol.toUpperCase()} in exchange for ${toSymbol.toUpperCase()}.`;
    function startDeletePair(parameters) {
      BUTTON_delete.setAttribute('disabled',true);
      DIV_spinner.style.display = 'block';
      DIV_message.innerHTML = '';
      deletePair(parameters.fromSymbol, parameters.toSymbol, () => {
        DIV_spinner.style.display = 'none';
        hybrix.view.closeAll();
        refreshPairs();
      },
      error => {
        DIV_message.innerHTML = 'Failed to delete pair!';
        DIV_spinner.style.display = 'none';
        console.error(error);
      });
    }
    BUTTON_delete.style.display = 'inline-block';
    BUTTON_delete.removeAttribute('disabled');
    BUTTON_delete.onclick = () => {
      hybrix.view.open('confirm', {
        text:'Are you sure you want to delete this pair?',
        textMore:'It can be created again later on.',
        yesCallback: startDeletePair,
        fromSymbol,
        toSymbol
      });
    };
  } else {
    DIV_progress.style.display = 'block';
    progress(DIV_progress, {
      Prepare: () => hybrix.view.pop(),
      Allocate: null,
      Create: null
    }, 1);

    H2_title.innerHTML = 'Create Pair';
    DIV_pair.innerHTML = '';
    DIV_explain.innerText = 'This swap pair will require you to manually send currency to the recipient.';
    BUTTON_cancel.style.display = 'inline-block';
    BUTTON_delete.style.display = 'none';
    BUTTON_delete.setAttribute('disabled',true);
    BUTTON_save.style.width = '62%';
  }

  /*
  INPUT_address.onchange = INPUT_address.oninput = INPUT_address.onpaste = () => {
    if (INPUT_address.value && (!expanded || (expanded && INPUT_fee.value > 0 && INPUT_fee.value <= 5))) {
      BUTTON_save.removeAttribute('disabled');
    } else {
      BUTTON_save.setAttribute('disabled', true);
      DIV_message.innerHTML = 'Please set an address for sending/recieving!';
    }
  }
  INPUT_balance.onchange = INPUT_balance.oninput = INPUT_balance.onpaste = () => {
    if (INPUT_address.value && (!expanded || (expanded && INPUT_fee.value > 0 && INPUT_fee.value <= 5))) {
      BUTTON_save.removeAttribute('disabled');
    } else {
      BUTTON_save.setAttribute('disabled', true);
      DIV_message.innerHTML = 'Please set a virtual balance above zero !';
    }
  }
  */

  if (expanded) {
    DIV_expanded.style.display = 'block';
    BUTTON_save.style.width = '62%';
    BUTTON_delete.removeAttribute('disabled');
    INPUT_fee.onchange = INPUT_fee.oninput = INPUT_fee.onpaste = () => {
      if (INPUT_fee.value > 0 && INPUT_fee.value <= 5 && INPUT_address.value && INPUT_balance.value) {
        BUTTON_save.removeAttribute('disabled');
      } else BUTTON_save.setAttribute('disabled', true);
    };
    hybrix.lib.rout({query: '/e/swap/allocation/pair/stats', regular: false, cache: SWAP_STATS_CACHE_TIME },
      pairs => {
        if (pairs.hasOwnProperty(pair)) {
          SPAN_feeRange.innerHTML = pairs[pair].fee;
        } else {
          SPAN_feeRange.innerHTML = 'N/A';
        }
      },
      error => {
        SPAN_feeRange.innerHTML = 'Error: Failed to retrieve fee range information!';
        console.error(error);
      }
    );
  } else {
    DIV_expanded.style.display = 'none';
    BUTTON_delete.style.display = 'none';
    BUTTON_delete.setAttribute('disabled', true);
    BUTTON_cancel.style.display = 'inline-block';
  }

  BUTTON_cancel.onclick = () => hybrix.view.pop();

  BUTTON_save.onclick = () => {
    DIV_spinner.style.display = 'block';
    if(createNew && expanded) {
      DIV_spinner_message.innerText = 'Creating new swap pair...';
    } else {
      DIV_spinner_message.innerText = 'Saving changes...';
    }
    BUTTON_save.setAttribute('disabled', true);
    BUTTON_cancel.setAttribute('disabled', true);
    function returnError(error) {
      BUTTON_save.removeAttribute('disabled');
      BUTTON_cancel.removeAttribute('disabled');
      DIV_spinner.style.display = 'none';
      DIV_message.innerHTML = 'Error: ' + error;
      console.error(error);
    }

    setTimeout( () => {  // give UI chance to update first
      const address = INPUT_address.value !== virtualAddress ? INPUT_address.value : null;
      const balance = INPUT_balance.value;
      const fee = INPUT_fee.value;
      const risk = 50;  // TODO customizable!
      const balanceWeight = 5;  // TODO customizable!
      setAddressVirtual(editToSymbol, address, () => {
        const newBalance = balance - virtualBalance;
        depositVirtual(editToSymbol, newBalance, () => {
          DIV_spinner.style.display = 'none';
          BUTTON_save.removeAttribute('disabled');
          BUTTON_cancel.removeAttribute('disabled');
          if (createNew) { // if target is not crypto, save virtual pair and close modal
            hybrix.view.open('allocateCreatePair', {fromSymbol, toSymbol});
          } else {
            setPair(fromSymbol, toSymbol, fee, risk, balanceWeight, () => {
              if(expanded) {
                hybrix.view.closeAll();
                refreshPairs();
              } else hybrix.view.pop();
            }, returnError);
          }
        }, returnError);
      }, returnError);
    }, 500);
  };
};
