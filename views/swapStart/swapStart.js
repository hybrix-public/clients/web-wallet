const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;
const {rout} = require('../swapAsset/swapAsset');

exports.rout = rout;

function isUnifiedAsset (asset) {
  return typeof asset.symbols === 'object' && asset.symbols !== null;
}

const render = parameters => asset => {
  asset = Object.values(asset)[0];
  const {symbol} = parameters;

  const DIV_symbol = document.getElementById('swapStart-symbol');
  DIV_symbol.innerHTML = symbol ? symbol.toUpperCase() : '...';

  const DIV_progress = document.getElementById('swapStart-progress');
  progress(DIV_progress, {
    '{{SWAPSTART:PROGRESS_DETAILS}}': null,
    '{{SWAPSTART:PROGRESS_AMOUNT}}': null,
    '{{SWAPSTART:PROGRESS_REVIEW}}': null,
    '{{SWAPSTART:PROGRESS_SWAP}}': null
  }, 0);

  const BUTTON_buy = document.getElementById('swapStart-buy');
  const BUTTON_sell = document.getElementById('swapStart-sell');
  const BUTTON_move = document.getElementById('swapStart-move');

  BUTTON_sell.onclick = () => hybrix.view.open('swapAsset', {action: 'sell', fromSymbol: symbol});
  BUTTON_buy.onclick = () => hybrix.view.open('swapAsset', {action: 'buy', toSymbol: symbol});

  if (isUnifiedAsset(asset)) { // Nb. hardcoded exception for hy token
    BUTTON_move.onclick = () => hybrix.view.open('swapAsset', {action: 'move', moveSymbol: symbol});
    BUTTON_move.style.display = null;
  } else if (symbol.split('.')[1] === 'hy') { // Nb. hardcoded exception for hy token
    BUTTON_move.onclick = () => hybrix.view.open('swapAsset', {action: 'move', moveSymbol: 'hy', fromSymbol: symbol});
    BUTTON_move.style.display = null;
  } else BUTTON_move.style.display = 'none';
};

exports.open = function (parameters) {
  const {symbol} = parameters;
  hybrix.lib.asset({symbol}, render(parameters), console.error); // TODO
};
