const hybrix = require('../../lib/hybrix').hybrix;
const Crypto = require('../../lib/crypto/utils');
const {SecureRandom} = require('../../lib/crypto/secureRandom');

let seedPointsCounter = 0; // counter
let totalSeedPointsRequired;
let lastInputTimeStamp;
let symbols;

exports.rout = () => hybrix.view.swap('createWallet');

exports.open = parameters => {
  symbols = parameters.symbols;
  seedPointsCounter = 0;
  lastInputTimeStamp = new Date().getTime();
  totalSeedPointsRequired = 200 + Math.floor(Crypto.utils.randomBytes(12)[11]);
  window.addEventListener('mousedown', addSeedPointFromMouseEvent);
  window.addEventListener('touchstart', addSeedPointFromMouseEvent);
  window.addEventListener('touchmove', addSeedPointFromMouseEvent);
  window.addEventListener('mousemove', addSeedPointFromMouseEvent);
  window.addEventListener('keypress', addSeedPointFromKeyEvent);
};

function removePoints () {
  const DIV_seedCanvas = document.getElementById('view-createWallet');
  for (const DIV_seedPoint of [...document.getElementsByClassName('seedpoint')]) DIV_seedCanvas.removeChild(DIV_seedPoint);
}

function cleanUp () {
  window.removeEventListener('mousedown', addSeedPointFromMouseEvent);
  window.removeEventListener('touchstart', addSeedPointFromMouseEvent);
  window.removeEventListener('touchmove', addSeedPointFromMouseEvent);
  window.removeEventListener('mousemove', addSeedPointFromMouseEvent);
  window.removeEventListener('keypress', addSeedPointFromKeyEvent);
  removePoints();
}

function drawSeedPoint (x, y) {
  const DIV_seedPoint = document.createElement('div');
  const DIV_seedCanvas = document.getElementById('view-createWallet');
  DIV_seedPoint.className = 'seedpoint';
  DIV_seedPoint.style.top = y + 'px';
  DIV_seedPoint.style.left = x + 'px';
  DIV_seedCanvas.appendChild(DIV_seedPoint);

  const percentSeeded = Math.floor(seedPointsCounter / totalSeedPointsRequired * 100) + '%';
  document.getElementById('createWallet-percentage').innerHTML = percentSeeded;
  document.getElementById('createWalletFormContent').style.position = 'fixed';
}

function finalizeSeeding () {
  cleanUp();
  const entropy = Crypto.utils.bytesToHex(SecureRandom.getPool());
  hybrix.lib.createWallet({entropy, offset: 0}, credentials => {
    hybrix.view.swap('createWalletReview', {credentials, symbols});
  }, error => {
    hybrix.view.swap('login', {symbols});
    hybrix.view.open('alert', {icon:'cross', title: '{{CREATEWALLET:FAILURE_TITLE}}', text: '{{CREATEWALLET:FAILURE_TEXT}}'});
    console.error(error);
  });
}

function addSeedPointFromMouseEvent (evt) {
  evt = evt || window.event;

  if (seedPointsCounter >= totalSeedPointsRequired) { // seeding is over now we generate and display the address
    finalizeSeeding();
  } else {
    const timeStamp = new Date().getTime();
    if (timeStamp - lastInputTimeStamp > 40) { // seed mouse position X and Y when mouse movements are greater than 40ms apart.
      SecureRandom.seedTime();

      let clientX, clientY;
      if (evt.type === 'mousemove') {
        clientX = evt.clientX;
        clientY = evt.clientY;
      } else {
        clientX = evt.touches[0].pageX;
        clientY = evt.touches[0].pageY;
      }
      SecureRandom.seedInt16((clientX * clientY));
      drawSeedPoint(clientX, clientY);
      evt.preventDefault();
      lastInputTimeStamp = timeStamp;
      ++seedPointsCounter;
    }
  }
  return false;
}

function addSeedPointFromKeyEvent (evt) {
  evt = evt || window.event;
  if (seedPointsCounter >= totalSeedPointsRequired) { // seeding is over now we generate and display the address
    finalizeSeeding();
  } else {
    const timeStamp = new Date().getTime();
    if (evt.which) { // seed key press data
      SecureRandom.seedTime();
      SecureRandom.seedInt8(evt.which);
      const keyPressTimeDiff = timeStamp - lastInputTimeStamp;
      SecureRandom.seedInt8(keyPressTimeDiff);
      drawSeedPoint(Math.random() * window.innerWidth, Math.random() * window.innerHeight);
      lastInputTimeStamp = timeStamp;
      ++seedPointsCounter;
    }
  }
}
