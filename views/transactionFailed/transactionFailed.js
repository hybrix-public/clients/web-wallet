const hybrix = require('../../lib/hybrix').hybrix;
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const progress = require('../../lib/progress').progress;

exports.open = function (parameters) {
  const {transaction, asset, errorMessage, rawTransaction} = parameters;
  const BUTTON_back = document.getElementById('transactionFailed-back');
  const BUTTON_close = document.getElementById('transactionFailed-close');

  const DIV_progress = document.getElementById('transactionFailed-progress');
  progress(DIV_progress, {
    '{{TRANSACTIONFAILED:PROGRESS_PREPARE}}': null,
    '{{TRANSACTIONFAILED:PROGRESS_REVIEW}}': null,
    '{{TRANSACTIONFAILED:PROGRESS_SEND}}': 'error'
  }, 2);

  const SPAN_errorMessage = document.getElementById('transactionFailed-errorMessage');
  SPAN_errorMessage.innerHTML = JSON.stringify(errorMessage);

  const SPAN_amount = document.getElementById('transactionFailed-amount');
  SPAN_amount.innerHTML = prettyPrintAmount(transaction.amount, transaction.symbol);

  const SPAN_fee = document.getElementById('transactionFailed-fee');
  SPAN_fee.innerHTML = prettyPrintAmount(asset.fee, asset['fee-symbol']);

  const SPAN_balance = document.getElementById('transactionFailed-balance');
  SPAN_balance.innerHTML = prettyPrintAmount(asset.balance, asset.symbol);

  const SPAN_source = document.getElementById('transactionFailed-source');
  SPAN_source.innerHTML = transaction.source || asset.address;

  const SPAN_target = document.getElementById('transactionFailed-target');
  SPAN_target.innerHTML = transaction.target;

  const SPAN_url = document.getElementById('transactionFailed-url');
  SPAN_url.innerHTML = window.location.href;

  const SPAN_walletVersion = document.getElementById('transactionFailed-walletVersion');
  SPAN_walletVersion.innerHTML = hybrix.version.wallet;

  const SPAN_hybrixdVersion = document.getElementById('transactionFailed-hybrixdVersion');
  SPAN_hybrixdVersion.innerHTML = hybrix.version.hybrixd;

  const SPAN_time = document.getElementById('transactionFailed-time');
  const now = new Date();
  const time = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate() + ' ' + now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();
  SPAN_time.innerHTML = time;

  const SPAN_userAgent = document.getElementById('transactionFailed-userAgent');
  SPAN_userAgent.innerHTML = navigator.userAgent;

  // TODO you'll be send back to the webshop in x seconds
  /* if (parameters.hasOwnProperty('referer')) {
    const redirect = () => {
      const href = decodeURIComponent(parameters.referer) + '?txid=null';// TODO decrypt & encrypt
      window.location.href = href;
    };
    setTimeout(redirect, 5000);

    BUTTON_close.onclick = redirect;
    BUTTON_back.style.display = 'none';
  } else {
  */
    BUTTON_back.style.display = 'inline-block';
    BUTTON_back.onclick = () => hybrix.view.pop();
    BUTTON_close.onclick = () => {
      hybrix.view.close('transactionReview');
      hybrix.view.close('transaction');
      hybrix.view.close('transactionFailed');
    };
  //}
};
