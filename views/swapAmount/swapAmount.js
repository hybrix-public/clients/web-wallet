const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;
const {rout} = require('../swapAsset/swapAsset');
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const Decimal = require('../../lib/decimal').Decimal;
const {addAsset} = require('../../lib/assets');
const feeWarningLevel = 0.1; // warn user when 10% or more of the swap is fees
const MAX_FACTOR = 8; // max amount of decimals to use in the input boxes
const lowerMaximum = 0.99; // lowers the max available amount to avoid price fluctuation issues
const minFeeMultiplication = 2.5 // minimum multiplication of fees to avoid dust swaps

exports.rout = rout;

const collectBalances = parameters => results => {
  const estimate = results.estimate;
  const fromBalance = results.balance;
  const asset = results.asset[parameters.fromSymbol];
  if (asset.symbol === asset['fee-symbol']) {
    render(parameters)(estimate, fromBalance, asset, 0);
  } else {
    hybrix.lib.getBalance({symbol: asset['fee-symbol']},
      fromBaseBalance => {
        render(parameters)(estimate, fromBalance, asset, fromBaseBalance);
      },
      failEstimate(parameters));
  }
};

const render = parameters => (estimate, fromBalance, asset, fromBaseBalance) => {
  const BUTTON_next = document.getElementById('swapAmount-next');
  const SPAN_action = document.getElementById('swapAmount-action');
  const DIV_message = document.getElementById('swapAmount-message');
  const DIV_conversion = document.getElementById('swapAmount-conversion');
  const DIV_networkFee = document.getElementById('swapAmount-networkFee');
  const DIV_allocatorFee = document.getElementById('swapAmount-allocatorFee');
  const DIV_allocatorFeePercentage = document.getElementById('swapAmount-allocatorFeePercentage');
  const DIV_spinner = document.getElementById('swapAmount-spinner');
  DIV_spinner.style.display = 'none';

  const SPAN_balance_value = document.getElementById('swapAmount-balance-value');
  const BUTTON_max = document.getElementById('swapAmount-max-button');
  const INPUT_from = document.getElementById('swapAmount-fromAmount');
  const INPUT_to = document.getElementById('swapAmount-toAmount');
  const INPUT_nrOfFuels = document.getElementById('swapAmount-nrOfFuels');

  const allocatorFeeRatio = new Decimal(estimate.fees.allocator).dividedBy(estimate.ask.amount);
  const allocatorFeePercentage = allocatorFeeRatio.times(100);
  fromBalance = isNaN(fromBalance) ? new Decimal(0) : new Decimal(fromBalance);
  fromBaseBalance = isNaN(fromBaseBalance) ? new Decimal(0) : new Decimal(fromBaseBalance);
  const factor = Math.min(asset.factor, MAX_FACTOR);
  const fromSymbol = parameters.fromSymbol;
  const networkFee = new Decimal(estimate.fees.network);
  const fromFeeSymbol = asset['fee-symbol'];
  const fromFee = new Decimal(fromFeeSymbol === fromSymbol ? asset.fee : 0);
  const fromBaseFee = new Decimal(fromFeeSymbol !== fromSymbol ? 0 : asset.fee);

  parameters.estimate = estimate;

  let translatedAction = parameters.hasOwnProperty('action') && parameters.action ? parameters.action : '{{SWAPAMOUNT:ACTION_SWAP}}';
  switch (translatedAction) {
    case 'buy': translatedAction='{{SWAPAMOUNT:ACTION_BUY}}'; break;
    case 'sell': translatedAction='{{SWAPAMOUNT:ACTION_SELL}}'; break;
    case 'move': translatedAction='{{SWAPAMOUNT:ACTION_MOVE}}'; break;
  }
  SPAN_action.innerText = translatedAction;

  DIV_message.innerHTML = '';
  INPUT_from.innerText = '';
  INPUT_to.innerText = '';

  const askMinusNetworkFees = new Decimal(estimate.ask.amount).minus(networkFee);
  const conversion = new Decimal(1).dividedBy(askMinusNetworkFees);

  DIV_conversion.innerHTML = prettyPrintAmount(conversion, null, false, true);

  DIV_networkFee.innerHTML = prettyPrintAmount(networkFee, fromSymbol, false, true);
  DIV_allocatorFeePercentage.innerHTML = prettyPrintAmount(allocatorFeePercentage, null, true, true);

  const allocatorFee = fromBalance.times(allocatorFeeRatio);
  const higherMaximum = 1+(1-lowerMaximum);
  const fees = (fromFee.plus(networkFee).plus(allocatorFee) ).times(higherMaximum);
  const maxBalanceAvailable = fromBalance.minus(fees);
  SPAN_balance_value.innerHTML = prettyPrintAmount(maxBalanceAvailable, null, false, true);

  BUTTON_max.removeAttribute('disabled');
  BUTTON_max.onclick = () => {
    const warnings = [];
    if (isNaN(conversion)) {
      warnings.push('No rate available!');
      DIV_message.innerHTML = '';
      INPUT_from.value = 0;
      INPUT_to.value = 0;
      INPUT_nrOfFuels.value = 0;
      DIV_allocatorFee.innerHTML = '...';
    } else {
      BUTTON_max.setAttribute('disabled',true);

      let maxValue = maxBalanceAvailable.greaterThan(0)
        ? new Decimal(maxBalanceAvailable.toFixed(factor))
        : new Decimal(0);

      // warn if not enough liquidity
      const maxAvailableLiquidity = new Decimal(estimate.ask.sufficiency)
        .times(lowerMaximum) // deduct 1.5% to prevent round of troubles
        .minus(networkFee)
        .minus(allocatorFee);

      if (maxValue.gt(maxAvailableLiquidity)) { // determine max based on sufficiency
        warnings.push(`{{SWAPAMOUNT:WARN_REMOTE_LIQUIDITY_A}}${prettyPrintAmount(estimate.bid.sufficiency, estimate.bid.symbol)}{{SWAPAMOUNT:WARN_REMOTE_LIQUIDITY_B}}`);
        maxValue = maxAvailableLiquidity;
      }

      INPUT_from.value = maxValue.toString();
      INPUT_from.oninput(); // trigger oninput for INPUT_to.value to get filled in

      if (maxValue.lt(0) || maxValue.lessThan(networkFee.times(minFeeMultiplication))) {
        BUTTON_next.setAttribute('disabled',true);
        warnings.push(`{{SWAPAMOUNT:WARN_BALANCE}} {{SWAPAMOUNT:WARN_AVAILABLE}} ${prettyPrintAmount(fromBalance, fromSymbol)},
        {{SWAPAMOUNT:WARN_REQUIRED}} ${prettyPrintAmount(fees.times(minFeeMultiplication), fromSymbol, false, true)}.`);
      } else validateInput(fromBalance, estimate, networkFee, fromSymbol, fromFee, fromBaseFee, fromFeeSymbol, fromBaseBalance);
    }
    DIV_message.innerHTML += ` ${warnings.join(' ')}`;
  };

  BUTTON_next.onclick = () => {
    const amount = INPUT_to.value;
    if (!isNaN(INPUT_from.value) && new Decimal(INPUT_from.value).times(feeWarningLevel).lt(networkFee)) {
      hybrix.view.open('swapFeeWarning', {...parameters, amount});
    } else {
      hybrix.view.open('swapReview', {...parameters, amount});
    }
  };

  INPUT_from.oninput = INPUT_from.onpaste = () => {
    INPUT_from.value = INPUT_from.value.replace(',','.');
    BUTTON_max.removeAttribute('disabled');
    if (isNaN(INPUT_from.value) || INPUT_from.value === '') {
      INPUT_to.value = 0;
      INPUT_nrOfFuels.value = 0;
      DIV_allocatorFee.innerHTML = '...';
      DIV_message.innerHTML = '{{SWAPAMOUNT:ERROR_EXPECTED_VALID}}';
    } else if (isNaN(conversion)) {
      INPUT_to.value = 0;
      INPUT_nrOfFuels.value = 0;
      DIV_allocatorFee.innerHTML = '...';
      DIV_message.innerHTML = '{{SWAPAMOUNT:ERROR_FAILED_RATE}}';
    } else {
      const allocatorFee = fromBalance.times(allocatorFeeRatio).times(conversion);
      const fromValue = new Decimal(INPUT_from.value)
      const inputValue = fromValue // TO = (FROM * (1-ALLOCATOR%) - NETWORK) * CONVERSION
        .times(new Decimal(1).minus(allocatorFeeRatio))
        .minus(networkFee)
        .times(conversion);
      const maxAvailableLiquidity = new Decimal(estimate.bid.sufficiency)
        .times(lowerMaximum) // deduct 1.5% to prevent round of troubles
        .minus(allocatorFee);
      if (inputValue.lessThanOrEqualTo(0) || fromValue.lessThan(networkFee.times(minFeeMultiplication))) {
        BUTTON_next.setAttribute('disabled',true);
        const allocatorFee = new Decimal(INPUT_from.value)
          .times(allocatorFeeRatio);
        const fees = allocatorFee
          .plus(networkFee);
        INPUT_to.value = 0;
        DIV_allocatorFee.innerHTML = '...';
        DIV_message.innerHTML = `{{SWAPAMOUNT:ERROR_MORE_NEEDED_A}}${fromSymbol.toUpperCase()}{{SWAPAMOUNT:ERROR_MORE_NEEDED_B}}
        {{SWAPAMOUNT:ERROR_REQUIRED_FEES}} ${prettyPrintAmount(fees.times(minFeeMultiplication), fromSymbol, false, true)}.`;
      } else if (inputValue.gt(maxAvailableLiquidity)) { // determine max based on sufficiency
        INPUT_to.value = inputValue.toFixed(MAX_FACTOR).toString();
        DIV_message.innerHTML = `{{SWAPAMOUNT:WARN_REMOTE_LIQUIDITY_A}}${prettyPrintAmount(estimate.bid.sufficiency, estimate.bid.symbol)}{{SWAPAMOUNT:WARN_REMOTE_LIQUIDITY_C}}`;
      } else {
        INPUT_to.value = inputValue.toFixed(MAX_FACTOR).toString();
        validateInput(fromBalance, estimate, networkFee, fromSymbol, fromFee, fromBaseFee, fromFeeSymbol, fromBaseBalance);
      }
    }
  };

  INPUT_to.oninput = INPUT_to.onpaste = () => {
    INPUT_to.value = INPUT_to.value.replace(',','.');
    BUTTON_max.removeAttribute('disabled');
    if (isNaN(INPUT_to.value) || INPUT_to.value === '') {
      INPUT_from.value = 0;
      DIV_allocatorFee.innerHTML = '...';
      DIV_message.innerText = '{{SWAPAMOUNT:ERROR_EXPECTED_VALID}}';
    } else if (isNaN(conversion)) {
      INPUT_from.value = 0;
      DIV_allocatorFee.innerHTML = '...';
      DIV_message.innerText = '{{SWAPAMOUNT:ERROR_FAILED_RATE}}';
    } else {
      const allocatorFee = fromBalance.times(allocatorFeeRatio).times(conversion);
      const toValue = new Decimal(INPUT_to.value).plus(networkFee.times(conversion));
      const inputValue = new Decimal(toValue) // FROM = ((TO / CONVERSION) + NETWORK) * (1-ALLOCATOR%)
        .dividedBy(conversion)
        .dividedBy(new Decimal(1).minus(allocatorFeeRatio));
      const maxAvailableLiquidity = new Decimal(estimate.bid.sufficiency)
        .times(0.985) // deduct 1.5% to prevent round of troubles
        .minus(allocatorFee);

      if (inputValue.lessThanOrEqualTo(0) || inputValue.lessThan(networkFee.times(minFeeMultiplication))) {
        BUTTON_next.setAttribute('disabled',true);
        const allocatorFee = new Decimal(toValue)
          .times(allocatorFeeRatio);
        const fees = allocatorFee
          .plus(networkFee);
        INPUT_from.value = 0;
        INPUT_nrOfFuels.value = 0;
        DIV_allocatorFee.innerHTML = '...';
        DIV_message.innerHTML = `{{SWAPAMOUNT:ERROR_MORE_NEEDED_A}}${parameters.toSymbol.toUpperCase()}{{SWAPAMOUNT:ERROR_MORE_NEEDED_B}}
        {{SWAPAMOUNT:ERROR_REQUIRED_FEES}} ${prettyPrintAmount(fees.times(minFeeMultiplication), fromSymbol, false, true)}.`;
      } else if (toValue.gt(maxAvailableLiquidity)) { // determine max based on sufficiency
        INPUT_from.value = inputValue.toFixed(factor).toString();
        DIV_message.innerHTML = `{{SWAPAMOUNT:WARN_REMOTE_LIQUIDITY_A}}${prettyPrintAmount(estimate.bid.sufficiency, estimate.bid.symbol)}{{SWAPAMOUNT:WARN_REMOTE_LIQUIDITY_C}}`;
      } else {
        INPUT_from.value = inputValue.toFixed(factor).toString();
        validateInput(fromBalance, estimate, networkFee, fromSymbol, fromFee, fromBaseFee, fromFeeSymbol, fromBaseBalance);
      }
    }
  };

  showFuelButtons(parameters);
};

const failEstimate = parameters => error => {
  console.error(error);
  const DIV_conversion = document.getElementById('swapAmount-conversion');
  DIV_conversion.innerText = '{{SWAPAMOUNT:N/A}}';
  DIV_conversion.innerText = '{{SWAPAMOUNT:N/A}}';
  const DIV_message = document.getElementById('swapAmount-message');
  DIV_message.innerText = `{{SWAPAMOUNT:ERROR_FAILED_GET_ESTIMATE}}${parameters.fromSymbol}:${parameters.toSymbol}!`;
};

const validateInput = (fromBalance, estimate, networkFee, fromSymbol, fromFee, fromBaseFee, fromFeeSymbol, fromBaseBalance) => {
  const INPUT_nrOfFuels = document.getElementById('swapAmount-nrOfFuels');
  const DIV_allocatorFee = document.getElementById('swapAmount-allocatorFee');
  const INPUT_from = document.getElementById('swapAmount-fromAmount');
  const INPUT_to = document.getElementById('swapAmount-toAmount');
  const BUTTON_next = document.getElementById('swapAmount-next');
  const DIV_message = document.getElementById('swapAmount-message');
  const TR_lastFixedFee = document.getElementById('swapAmount-lastFixedFeeRow');
  const TABLE_fees = TR_lastFixedFee.parentNode;

  let valid = true;
  const warnings = [];
  let allocatorFee;
  const inputAmount = INPUT_from.value;
  if (isNaN(inputAmount)) {
    valid = false;
    INPUT_from.value = 0;
    INPUT_to.value = 0;
    warnings.push('{{SWAPAMOUNT:WARN_EXPECTED_NUMBER}}');
  } else if (Number(inputAmount) === 0) {
    INPUT_from.value = 0;
    INPUT_to.value = 0;
    valid = false; // no warning, zero is clear enough
  } else {
    const amount = new Decimal(inputAmount);
    if (amount.gt(0)) {
      const allocatorFeeRatio = new Decimal(estimate.fees.allocator).dividedBy(estimate.ask.amount);
      allocatorFee = amount.times(allocatorFeeRatio);

      if (estimate) {
        // warn if balance too low
        const requiredBalance = amount
          .minus(fromFee)
          .minus(networkFee)
          .minus(allocatorFee);
        if (requiredBalance.gt(fromBalance)) {
          warnings.push(`{{SWAPAMOUNT:WARN_BALANCE_LESS_A}}${prettyPrintAmount(fromBalance, estimate.ask.symbol, false, true)}{{SWAPAMOUNT:WARN_BALANCE_LESS_B}}`);
          valid = false;
        }

        // warn if not enough liquidity
        const availableLiquidity = new Decimal(estimate.ask.sufficiency)
          .minus(amount)
          .minus(networkFee)
          .minus(allocatorFee);
        if (availableLiquidity.lt(0)) {
          warnings.push(`{{SWAPAMOUNT:WARN_REMOTE_LIQUIDITY_A}}${prettyPrintAmount(estimate.bid.sufficiency, estimate.bid.symbol)}{{SWAPAMOUNT:WARN_REMOTE_LIQUIDITY_D}}`);
          valid = false;
        }

        // warn for excessive fees
        const minimalAmount = amount.times(feeWarningLevel);
        if (minimalAmount.lt(allocatorFee)) {
          warnings.push(`{{SWAPAMOUNT:WARN_HIGH_FEE_ALLOCATOR}}${prettyPrintAmount(allocatorFee, estimate.fees.symbol)}{{SWAPAMOUNT:WARN_HIGH_FEE_COMPARED}}`);
        }
        if (minimalAmount.lt(networkFee)) {
          warnings.push(`{{SWAPAMOUNT:WARN_HIGH_FEE_NETWORK}}${prettyPrintAmount(networkFee, estimate.fees.symbol)}{{SWAPAMOUNT:WARN_HIGH_FEE_COMPARED}}`);
        }
        if (fromSymbol !== fromFeeSymbol) {
          if (fromBaseBalance.lt(fromBaseFee)) {
            warnings.push(`{{SWAPAMOUNT:WARN_NEED_BALANCE_A}}${fromFeeSymbol.toUppercase()}{{SWAPAMOUNT:WARN_NEED_BALANCE_B}}${fromSymbol.toUppercase()}{{SWAPAMOUNT:WARN_NEED_BALANCE_C}}${prettyPrintAmount(fromBaseBalance, fromFeeSymbol)}{{SWAPAMOUNT:WARN_NEED_BALANCE_D}} {{SWAPAMOUNT:WARN_NEED_BALANCE_E}}${prettyPrintAmount(fromBaseFee, fromFeeSymbol)}{{SWAPAMOUNT:WARN_NEED_BALANCE_F}}`);
            valid = false;
          } else {
            // TODO check value of native fromFee with respect to minimalAmount
          }
        } else if (minimalAmount.lt(fromFee)) {
          warnings.push(`{{SWAPAMOUNT:WARN_HIGH_FEE_TRANSACTION}}${prettyPrintAmount(fromFee, fromSymbol)}{{SWAPAMOUNT:WARN_HIGH_FEE_COMPARED}}`);
        }
      } else {
        warnings.push('{{SWAPAMOUNT:WARN_NO_ESTIMATE}}');
        valid = false;
      }
    } else {
      INPUT_from.value = 0;
      INPUT_to.value = 0;
      warnings.push('{{SWAPAMOUNT:WARN_AMOUNT_MORE_ZERO}}');
      valid = false;
    }
  }
  if (!isNaN(INPUT_to.value) && typeof INPUT_nrOfFuels.fee !== 'undefined') {
    INPUT_nrOfFuels.value = new Decimal(INPUT_to.value).dividedBy(INPUT_nrOfFuels.fee).toString();
  }

  cleanTransactionFees();
  if (valid) {
    BUTTON_next.removeAttribute('disabled');
    hybrix.lib.getFee({symbol: fromSymbol, amount: INPUT_from.value}, feeObject => {
      cleanTransactionFees();
      for (const feeSymbol in feeObject) {
        const fee = feeObject[feeSymbol];
        const TR_transactionFee = document.createElement('TR');
        TR_transactionFee.innerHTML = `<td>- ${prettyPrintAmount(fee, feeSymbol)}</td><td>{{SWAPAMOUNT:FEES_TRANSACTION}}${fromSymbol.toUpperCase()}</td>`;
        TABLE_fees.appendChild(TR_transactionFee);
      }
    },
    error => {
      BUTTON_next.setAttribute('disabled',true);
      cleanTransactionFees();
      const TR_transactionFee = document.createElement('TR');
      TR_transactionFee.innerHTML = `<td style="color:red;">- {{SWAPAMOUNT:N/A}}</td><td style="color:red;">{{SWAPAMOUNT:FEES_TRANSACTION}}${fromSymbol.toUpperCase()}</td>`;
      TABLE_fees.appendChild(TR_transactionFee);
      warnings.push('{{SWAPAMOUNT:WARN_SMALLER_AMOUNT}}');
    });

    DIV_allocatorFee.innerHTML = prettyPrintAmount(allocatorFee, fromSymbol, false, true);
  } else {
    BUTTON_next.setAttribute('disabled',true);
    DIV_allocatorFee.innerHTML = '...';
  }

  DIV_message.innerHTML = warnings.join('<br>');
};

const renderFuelButtons = parameters => asset => {
  const INPUT_to = document.getElementById('swapAmount-toAmount');
  const INPUT_nrOfFuels = document.getElementById('swapAmount-nrOfFuels');
  const BUTTON_min = document.getElementById('swapAmount-min-button');
  const BUTTON_plus = document.getElementById('swapAmount-plus-button');
  const SPAN_fuelForSymbol = document.getElementById('swapAmount-fuelForSymbol');
  const fee = new Decimal(asset.fee);
  INPUT_nrOfFuels.fee = fee;
  const updateToAmount = () => {
    INPUT_to.value = fee.times(INPUT_nrOfFuels.value).toString();
    INPUT_to.onpaste(); // refresh validations and update fromAmount accordingly etc.
  };
  SPAN_fuelForSymbol.innerText = parameters.fuelForSymbol.toUpperCase();
  INPUT_nrOfFuels.value = 1;
  INPUT_nrOfFuels.onchange = () => {
    if (INPUT_nrOfFuels.value <= 0) {
      BUTTON_min.disabled = true;
      BUTTON_min.setAttribute('disabled', true);
      INPUT_nrOfFuels.value = 0;
    } else {
      BUTTON_min.disabled = null;
      BUTTON_min.removeAttribute('disabled');
    }
    updateToAmount();
  };
  BUTTON_min.onclick = () => {
    INPUT_nrOfFuels.value = Number(INPUT_nrOfFuels.value) - 1;

    if (INPUT_nrOfFuels.value <= 0) {
      BUTTON_min.setAttribute('disabled', true);
      BUTTON_min.disabled = true;
      INPUT_nrOfFuels.value = 0;
    }
    updateToAmount();
  };
  BUTTON_plus.onclick = () => {
    INPUT_nrOfFuels.value = Number(INPUT_nrOfFuels.value) + 1;
    if (INPUT_nrOfFuels.value > 0) {
      BUTTON_min.disabled = null;
      BUTTON_min.removeAttribute('disabled');
    }
    updateToAmount();
  };
  updateToAmount(); // initialize with 1 x fuel
};

function showFuelButtons (parameters) {
  // getAssetData = (symbol, callback)
  const DIV_fuelContainer = document.getElementById('swapAmount-fuelContainer');
  if (!parameters.hasOwnProperty('fuelForSymbol') || typeof parameters.fuelForSymbol !== 'string') DIV_fuelContainer.style.display = 'none';
  else {
    DIV_fuelContainer.style.display = 'block';
    addAsset(parameters.fuelForSymbol, renderFuelButtons(parameters), console.error);
  }
}

function cleanTransactionFees () {
  const TR_lastFixedFee = document.getElementById('swapAmount-lastFixedFeeRow');
  while (TR_lastFixedFee.nextElementSibling) { // clean transaction fees
    TR_lastFixedFee.nextElementSibling.parentNode.removeChild(TR_lastFixedFee.nextElementSibling);
  }
}

exports.open = function (parameters) {
  const INPUT_to = document.getElementById('swapAmount-toAmount');
  const INPUT_from = document.getElementById('swapAmount-fromAmount');
  const BUTTON_cancel = document.getElementById('swapAsset-cancel');
  const BUTTON_back = document.getElementById('swapAmount-back');
  const BUTTON_next = document.getElementById('swapAmount-next');
  const BUTTON_max = document.getElementById('swapAmount-max-button');
  const DIV_from = document.getElementById('swapAmount-fromSymbol');
  const DIV_to = document.getElementById('swapAmount-toSymbol');
  const DIV_networkFee = document.getElementById('swapAmount-networkFee');
  const DIV_networkFeeSymbol = document.getElementById('swapAmount-networkFeeSymbol');
  const DIV_allocatorFee = document.getElementById('swapAmount-allocatorFee');
  const DIV_allocatorFeePercentage = document.getElementById('swapAmount-allocatorFeePercentage');
  const DIV_fuelContainer = document.getElementById('swapAmount-fuelContainer');
  const DIV_spinner = document.getElementById('swapAmount-spinner');
  DIV_spinner.style.display = 'block';
  const SPAN_balance_value = document.getElementById('swapAmount-balance-value');
  const SPAN_balance_symbol = document.getElementById('swapAmount-balance-symbol');
  SPAN_balance_value.innerText = '...';
  SPAN_balance_symbol.innerText = '';

  DIV_fuelContainer.style.display = 'none';
  // clean the numeric input fields
  INPUT_from.value = '';
  INPUT_to.value = '';

  BUTTON_cancel.onclick = () => hybrix.view.closeAll();
  BUTTON_back.onclick = () => hybrix.view.pop();
  BUTTON_max.setAttribute('disabled',true);
  BUTTON_next.setAttribute('disabled',true);

  DIV_from.innerText = SPAN_balance_symbol.innerText = parameters.fromSymbol.toUpperCase();
  DIV_to.innerText = parameters.toSymbol.toUpperCase();
  DIV_networkFee.innerHTML = '...';
  DIV_networkFeeSymbol.innerText = parameters.toSymbol.toUpperCase();
  DIV_allocatorFee.innerHTML = '...';
  DIV_allocatorFeePercentage.innerHTML = '...';
  cleanTransactionFees();

  const DIV_message = document.getElementById('swapAmount-message');

  DIV_message.innerHTML = '';

  const steps = {
    asset: {data: {symbol: parameters.fromSymbol}, step: 'addAsset'},
    balance: {data: {symbol: parameters.fromSymbol}, step: 'getBalance'},
    estimate: {data: {query: `/e/swap/deal/estimate/${parameters.fromSymbol}/${parameters.toSymbol}`, regular: false, cache: 120, channel: 'y'}, step: 'rout'}
  };
  hybrix.lib.parallel(steps, collectBalances(parameters), failEstimate(parameters));

  const DIV_progress = document.getElementById('swapAmount-progress');
  progress(DIV_progress, {
    '{{SWAPAMOUNT:PROGRESS_DETAILS}}': () => hybrix.view.pop(),
    '{{SWAPAMOUNT:PROGRESS_AMOUNT}}': null,
    '{{SWAPAMOUNT:PROGRESS_REVIEW}}': null,
    '{{SWAPAMOUNT:PROGRESS_SWAP}}': null
  }, 1);

  BUTTON_next.onclick = null;
};
