const hybrix = require('../../lib/hybrix').hybrix;

exports.rout = parameters => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('confirm', parameters);
};

function open (parameters) {
  const H2_title = document.getElementById('confirm-title');
  const P_question = document.getElementById('confirm-question');
  const P_question_more = document.getElementById('confirm-question-more');

  if (typeof parameters.title === 'string') H2_title.innerText =  parameters.title;
  if (typeof parameters.text === 'string') P_question.innerText =  parameters.text;
  if (typeof parameters.textMore === 'string') P_question_more.innerText =  parameters.textMore;

  const BUTTON_yes = document.getElementById('confirm-yes-button');
  const BUTTON_no = document.getElementById('confirm-no-button');
  BUTTON_yes.onclick = () => {
    if (typeof parameters.yesCallback === 'function') parameters.yesCallback(parameters);
    hybrix.view.pop();
  }
  BUTTON_no.onclick = () => {
    if (typeof parameters.noCallback === 'function') parameters.noCallback(parameters);
    hybrix.view.pop();
  }
};

exports.open = open;
