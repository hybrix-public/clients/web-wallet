const DEFAULT_CURRENCY = 'usd';

const hybrix = require('../../lib/hybrix').hybrix;
const {contacts} = require('../../lib/contacts');
const {version} = require('../../tmp/version');
const {getAssetPreferences} = require('../../lib/assets');

let selectedCurrency = DEFAULT_CURRENCY;

function downloadPreferences () {
  const A = document.createElement('A');
  const symbols = getAssetPreferences();
  const dataStr = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify({contacts, currency: selectedCurrency, symbols}));
  A.setAttribute('href', dataStr);
  A.setAttribute('download', 'hybrix-preferences.json');
  A.click();
}

function saveCurrencyPreference (currency) {
  hybrix.lib.save({key: hybrix.constants.storagePrefixes.currencyPreference, value: currency, sessionKey: true, channel: 'y'}, () => {}, () => {});
}

function addCurrencyOptions (valuations) {
  let html = '<option DISABLED HIDDEN>{{SETTINGS:SELECT_SYMBOL}}</option>';
  for (const currency of valuations) {
    html += `<option value="${currency}" ${selectedCurrency.toUpperCase() === currency.toUpperCase() ? 'SELECTED' : ''}>${currency.toUpperCase()}</option>`;
  }
  document.querySelector('.available-currencies').innerHTML = html;
}

function setCurrencyPreference (currency) {
  if (typeof currency !== 'string') currency = DEFAULT_CURRENCY;
  selectedCurrency = currency;
  document.querySelector('.available-currencies').value = selectedCurrency;
  hybrix.view.exec('listAssets', 'updateAssetValuations');
}

function fallbackOnCurrencyPreferenceFail (error) {
  console.log('Currency preference data blank; using ' + DEFAULT_CURRENCY.toUpperCase() + ' default.');
  selectedCurrency = DEFAULT_CURRENCY;
  document.querySelector('.available-currencies').value = selectedCurrency;
}

exports.loadCurrencyPreference = () => {
  hybrix.lib.load({key: hybrix.constants.storagePrefixes.currencyPreference, sessionKey: true, channel: 'y'}, setCurrencyPreference, fallbackOnCurrencyPreferenceFail);
};

function setReporting(callbackFollowup, previousEmail) {
  hybrix.view.open('input', {
    title:'{{SETTINGS:EMAIL_DIALOG_TITLE}}',
    text:'{{SETTINGS:EMAIL_DIALOG_TEXT_A}}',
    textMore:'{{SETTINGS:EMAIL_DIALOG_TEXT_B}}',
    textPending:'{{SETTINGS:EMAIL_DIALOG_PENDING}}',
    placeholder:'{{SETTINGS:EMAIL_DIALOG_PLACEHOLDER}}',
    pattern:'^[A-Za-z0-9+_.\\-]+@[A-Za-z0-9.\\-]+$',
    maxlength:254,
    fieldValue: previousEmail?previousEmail:'',
    callback: email => {
      hybrix.lib.save({key: hybrix.constants.storagePrefixes.email, value: email, sessionKey: true, channel: 'y'}, () => {}, console.error);
      hybrix.view.pop();
      callbackFollowup(email);
    }
  });
}

exports.once = () => {
  const SPAN_walletVersion2 = document.getElementById('settings-wallet-version');
  SPAN_walletVersion2.innerText = version;
  
  const BUTTON_importPreferences = document.getElementById('settings-importPreferences');
  BUTTON_importPreferences.onclick = () => hybrix.view.open('importPreferences');

  const BUTTON_exportPreferences = document.getElementById('settings-exportPreferences');
  BUTTON_exportPreferences.onclick = downloadPreferences;

  addCurrencyOptions([ 'HY', 'CNY', 'EUR', 'USD', 'GBP', 'JPY', 'AUD', 'CAD', 'BGN', 'BRL', 'CHF', 'CZK', 'DKK', 'HUF', 'HKD', 'HRK', 'IDR', 'ILS', 'INR', 'ISK', 'KRW', 'MXN', 'MYR', 'NOK', 'NZD', 'PHP', 'PLN', 'RON', 'RUB', 'SEK', 'SGD', 'THB', 'TRY', 'ZAR', 'BTC', 'ETH', 'XRP', 'BNB', 'LTC', 'DOGE' ]);
  // DISABLED FOR NOW TO AVOID TOO MANY OPTIONS: hybrix.lib.rout({query: '/e/valuations/list'}, addCurrencyOptions, console.error);
  const SELECT_currency = document.querySelector('.available-currencies');
  SELECT_currency.onchange = () => {
    const currency = SELECT_currency.value;
    setCurrencyPreference(currency);
    saveCurrencyPreference(currency);
  };

  const SPAN_reportingTarget = document.getElementById('settings-reporting-target');
  hybrix.lib.load({key: hybrix.constants.storagePrefixes.email, sessionKey: true, channel: 'y'}, email => {
    SPAN_reportingTarget.innerText = email;
  });

  const BUTTON_setReporting = document.getElementById('settings-reporting-edit');
  BUTTON_setReporting.onclick = () => {
    setReporting(email => {
      SPAN_reportingTarget.innerText = email;
    }, SPAN_reportingTarget.innerText);
  };
};

exports.rout = () => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('settings');
};

exports.setCurrencyPreference = currency => {
  if (currency === selectedCurrency) return;
  setCurrencyPreference(currency);
  saveCurrencyPreference(selectedCurrency);
};

exports.getCurrencyPreference = () => selectedCurrency;
exports.setReporting = setReporting;
