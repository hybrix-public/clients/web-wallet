const hybrix = require('../../lib/hybrix').hybrix;
const {saveToContacts} = require('../../lib/contacts');
const {importAssetPreferences, storeAssetPreferences} = require('../../lib/assets');

function importPreferences (preferences) {
  if (typeof preferences !== 'object' || preferences === null) return false;
  // "contacts":{"John":[{"DUMMY":{"test":"_dummyaddress_"}}],"Appeltaart":[{"SD":{"sd":""}}]},
  if (preferences.hasOwnProperty('contacts')) {
    if (preferences.contacts === null || typeof preferences.contacts !== 'object') return false;
    for (const contactId in preferences.contacts) {
      const entries = preferences.contacts[contactId];
      if (!(entries instanceof Array)) return false;
      for (const entry of entries) { // [{"DUMMY":{"test":"_dummyaddress_"}...]
        if (entry === null || typeof entry !== 'object') return false;
        for (const symbol in entry) { // {"test":"_dummyaddress_"}
          const addressesByLabel = entry[symbol];
          if (addressesByLabel === null || typeof addressesByLabel !== 'object') return false;
          for (const label in addressesByLabel) {
            const address = addressesByLabel[label];
            saveToContacts({symbol, address, contactId, label}, () => {}, console.error);
          }
        }
      }
    }
  }
  // "currency":"usd",
  if (preferences.hasOwnProperty('currency')) {
    if (typeof preferences.currency !== 'string') return false;
    hybrix.view.exec('settings', 'setCurrencyPreference', preferences.currency);
  }
  // "symbols":{"hy":0,"zec":0,"dummy":0,"tomo.hy":0,"mock.btc":0,"mock.eth":0,"eth.hy":0,"eth":0,"burst.ocean":1}
  if (preferences.hasOwnProperty('symbols')) {
    if (preferences.symbols === null || typeof preferences.symbols !== 'object') return false;
    importAssetPreferences(preferences.symbols);
    storeAssetPreferences();
  }
  return true;
}

exports.once = () => {
  const DIV_message = document.getElementById('importPreferences-message');
  const BUTTON_done = document.getElementById('importPreferences-done');
  BUTTON_done.onclick = () => hybrix.view.pop();
  const INPUT_upload = document.getElementById('importPreferences-upload');
  INPUT_upload.onchange = () => {
    DIV_message.innerHTML = '';
    const file = INPUT_upload.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsText(file, 'UTF-8');
      reader.onload = function (evt) {
        const preferencesString = evt.target.result;
        let preferences;
        try {
          preferences = JSON.parse(preferencesString);
        } catch (e) {
          DIV_message.innerHTML = 'Error parsing json file';
          return;
        }
        if (importPreferences(preferences)) DIV_message.innerHTML = 'Successfully loaded new preferences.';
        else DIV_message.innerHTML = 'Something went wrong. Could not load all new preferences.';
      };
      reader.onerror = function (evt) {
        DIV_message.innerHTML = 'Error reading file';
      };
    }
  };
};

exports.open = () => {
  const DIV_message = document.getElementById('importPreferences-message');
  DIV_message.innerHTML = '';
  const INPUT_upload = document.getElementById('importPreferences-upload');
  INPUT_upload.value = '';
};

exports.rout = () => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('importPreferences');
};
