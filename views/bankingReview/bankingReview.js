const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const {rout} = require('../banking/banking');
exports.rout = rout;

exports.open = parameters => {
  const DIV_progress = document.getElementById('bankingReview-progress');
  const DIV_spinner = document.getElementById('bankingReview-spinner');
  const BUTTON_confirm = document.getElementById('bankingReview-confirm');
  const BUTTON_cancel = document.getElementById('bankingReview-cancel');
  const DIV_action = document.getElementById('bankingReview-action');
  const DIV_amount = document.getElementById('bankingReview-amount');
  const DIV_source = document.getElementById('bankingReview-source');
  const DIV_target = document.getElementById('bankingReview-target');
  const DIV_feeAllocator = document.getElementById('bankingReview-feeAllocator');
  const DIV_feeService = document.getElementById('bankingReview-feeNetwork');
  const DIV_feeTransaction = document.getElementById('bankingReview-feeTransaction');
  const DIV_email = document.getElementById('bankingReview-email');
  const DIV_description = document.getElementById('bankingReview-description');

  progress(DIV_progress, {
    '{{BANKINGREVIEW:PROGRESS_DETAILS}}': null,
    '{{BANKINGREVIEW:PROGRESS_AMOUNT}}': () => hybrix.view.pop(),
    '{{BANKINGREVIEW:PROGRESS_REVIEW}}': null,
    '{{BANKINGREVIEW:PROGRESS_TRANSACT}}': null
  }, 2);

  const action = parameters.action;

  if (!isNaN(parameters.amount) && parameters.amount > 0 && !isNaN(parameters.transactionFee) && parameters.transactionFee >= 0) {
    DIV_amount.innerHTML = `${prettyPrintAmount((parameters.payAmount + parameters.transactionFee), parameters.bankingCurrency)}`;

    let translatedAction = parameters.hasOwnProperty('action') && parameters.action ? parameters.action : '{{BANKINGREVIEW:ACTION_TRANSFER}}';
    switch (translatedAction) {
      case 'deposit': translatedAction='{{BANKINGREVIEW:ACTION_DEPOSIT}}'; break;
      case 'withdraw': translatedAction='{{BANKINGREVIEW:ACTION_WITHDRAW}}'; break;
      case 'transfer': translatedAction='{{BANKINGREVIEW:ACTION_TRANSFER}}'; break;
    }
    DIV_action.innerText = translatedAction;

    if (action === 'deposit') {
      DIV_source.innerText = parameters.source;
      DIV_target.innerText = parameters.target;
    } else {
      DIV_source.innerText = parameters.source;
      DIV_target.innerText = parameters.target.join(', ');
    }
    DIV_description.innerText = parameters.description ? parameters.description : 'voucher {ID}';
    DIV_email.innerText = parameters.email;

    DIV_feeAllocator.innerHTML = `${prettyPrintAmount(parameters.bankingMethod.fees[action].percent * parameters.amount, parameters.bankingCurrency, false, true)}`;
    DIV_feeService.innerHTML = `${prettyPrintAmount(parameters.bankingMethod.fees[action].fixed, parameters.bankingCurrency, false, true)}`;
    DIV_feeTransaction.innerHTML = `${prettyPrintAmount(parameters.transactionFee, parameters.bankingCurrency, false, true)}`;;

    BUTTON_cancel.onclick = () => hybrix.view.pop();

    BUTTON_confirm.onclick = () => {
      hybrix.view.open('bankingComplete', parameters);
    };
  } else hybrix.view.pop();
};
