const hybrix = require('../../lib/hybrix').hybrix;
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const progress = require('../../lib/progress').progress;
const Decimal = require('../../lib/decimal').Decimal;
const {rout} = require('../swapAsset/swapAsset');
exports.rout = rout;

function render (parameters) {
  const DIV_spinner = document.getElementById('swapReview-spinner');

  const {fromBase, toSymbol, amount, estimate, target} = parameters;

  const TR_target = document.getElementById('swapReview-target-container');
  if (target) {
    TR_target.style.display = 'table-row';
    const TD_target = document.getElementById('swapReview-target');
    TD_target.innerText = target;
  } else TR_target.style.display = 'none';

  const SPAN_action = document.getElementById('swapReview-action');
  
  let translatedAction = parameters.hasOwnProperty('action') && parameters.action ? parameters.action : '{{SWAPREVIEW:ACTION_SWAP}}';
  switch (translatedAction) {
    case 'buy': translatedAction='{{SWAPREVIEW:ACTION_BUY}}'; break;
    case 'sell': translatedAction='{{SWAPREVIEW:ACTION_SELL}}'; break;
    case 'move': translatedAction='{{SWAPREVIEW:ACTION_MOVE}}'; break;
  }
  SPAN_action.innerText = translatedAction;  

  // TODO if there is no .ask then the rout has failed, there needs to be error handling for that!
  // TODO check from,to and amount
  // TODO check if sufficient balance
  const BUTTON_swap = document.getElementById('swapReview-swap');
  const TD_ask = document.getElementById('swapReview-ask');
  const TD_bid = document.getElementById('swapReview-bid');
  const TD_feeAllocator = document.getElementById('swapReview-feeAllocator');
  const TD_feeNetwork = document.getElementById('swapReview-feeNetwork');
  const TD_feeTransaction = document.getElementById('swapReview-feeTransaction');
  TD_ask.innerHTML = prettyPrintAmount(estimate.ask.amount, estimate.ask.symbol, false, true);
  TD_bid.innerHTML = prettyPrintAmount(estimate.bid.amount, estimate.bid.symbol, false, true);
  TD_feeAllocator.innerHTML = prettyPrintAmount(estimate.fees.allocator, estimate.fees.symbol, false, true);
  TD_feeNetwork.innerHTML = prettyPrintAmount(estimate.fees.network, estimate.fees.symbol, false, true);

  const requiredAmount = new Decimal(estimate.ask.amount).add(estimate.fees.network).add(estimate.fees.allocator);
  const askAmount = new Decimal(estimate.ask.amount);
  if (askAmount.gt(estimate.ask.sufficiency)) {
    const DIV_message = document.getElementById('swapReview-message');
    DIV_message.innerHTML = `{{SWAPREVIEW:WARN_NO_LIQUIDITY}} ${prettyPrintAmount(estimate.ask.sufficiency, estimate.ask.symbol)}. {{SWAPREVIEW:REQUIRED}} ${prettyPrintAmount(estimate.ask.amount, estimate.ask.symbol)}.`;
    DIV_spinner.style.display = 'none';
  } else if (requiredAmount.gt(parameters.balance)) {
    const DIV_message = document.getElementById('swapReview-message');
    DIV_message.innerHTML = `{{SWAPREVIEW:WARN_NO_BALANCE}} ${prettyPrintAmount(parameters.balance, estimate.fees.symbol)}. {{SWAPREVIEW:REQUIRED}} ${prettyPrintAmount(requiredAmount.toString(), estimate.fees.symbol)}.`;
    DIV_spinner.style.display = 'none';
  } else {
    BUTTON_swap.onclick = () => {
      if (!BUTTON_swap.classList.contains('disabled')) { // make sure user cannot mash button while rout call is ongoing
        hybrix.view.open('swapComplete', parameters);
      }
    };
    BUTTON_swap.classList.remove('disabled');
    DIV_spinner.style.display = 'none';
  }

  hybrix.lib.getFee({symbol: parameters.fromSymbol, amount: estimate.ask.amount}, feeObject => {
    let html = '';
    for (const feeSymbol in feeObject) {
      const fee = feeObject[feeSymbol];
      html += prettyPrintAmount(fee, feeSymbol, false, true) + '<br>';
    }
    TD_feeTransaction.innerHTML = html;
  },
  error => {
    TD_feeTransaction.innerHTML = '<span style="color:red;">{{SWAPREVIEW:ERROR_FAIL_GET_FEES}}</span>';
    console.error('Failed to retrieve transaction fees', error); // TODO
  });
}

exports.once = function () {
  const BUTTON_back = document.getElementById('swapReview-back');
  BUTTON_back.onclick = () => hybrix.view.pop();
};

const fail = parameters => error => {
  const DIV_message = document.getElementById('swapReview-message');
  DIV_message.innerHTML = `{{SWAPREVIEW:ERROR_NO_PAIR_A}}${parameters.fromSymbol}:${parameters.toSymbol}{{SWAPREVIEW:ERROR_NO_PAIR_B}}<br>` + error;
  const TD_ask = document.getElementById('swapReview-ask');
  TD_ask.innerText = '{{SWAPREVIEW:N/A}}';
  const TD_bid = document.getElementById('swapReview-bid');
  TD_bid.innerText = '{{SWAPREVIEW:N/A}}';
  const DIV_spinner = document.getElementById('swapReview-spinner');
  DIV_spinner.style.display = 'none';
};

exports.open = function (parameters, amount) {
  const TR_target = document.getElementById('swapReview-target-container');
  TR_target.style.display = 'none';

  const BUTTON_swap = document.getElementById('swapReview-swap');
  BUTTON_swap.classList.add('disabled');

  const DIV_progress = document.getElementById('swapReview-progress');

  const DIV_spinner = document.getElementById('swapReview-spinner');
  DIV_spinner.style.display = 'block';

  const TD_ask = document.getElementById('swapReview-ask');
  TD_ask.innerText = '...';
  const TD_bid = document.getElementById('swapReview-bid');
  TD_bid.innerText = '...';
  const TD_feeAllocator = document.getElementById('swapReview-feeAllocator');
  TD_feeAllocator.innerText = '...';
  const TD_feeNetwork = document.getElementById('swapReview-feeNetwork');
  TD_feeNetwork.innerText = '...';
  const TD_feeTransaction = document.getElementById('swapReview-feeTransaction');
  TD_feeTransaction.innerText = '...';

  const DIV_message = document.getElementById('swapReview-message');
  if (parameters.swap) {
    DIV_message.innerText = `{{SWAPREVIEW:WARN_TARGET_OTHER_LEDGER_A}}${parameters.toSymbol}{{SWAPREVIEW:WARN_TARGET_OTHER_LEDGER_B}}`;
    progress(DIV_progress, {
      '{{SWAPREVIEW:PROGRESS_PREPARE}}': () => hybrix.view.pop(),
      '{{SWAPREVIEW:PROGRESS_REVIEW}}': null,
      '{{SWAPREVIEW:PROGRESS_SWAP}}': null
    }, 1);
  } else {
    progress(DIV_progress, {
      '{{SWAPREVIEW:PROGRESS_DETAILS}}': null,
      '{{SWAPREVIEW:PROGRESS_AMOUNT}}': () => hybrix.view.pop(),
      '{{SWAPREVIEW:PROGRESS_REVIEW}}': null,
      '{{SWAPREVIEW:PROGRESS_SWAP}}': null
    }, 2);
    DIV_message.innerText = '';
  }
  hybrix.lib.getBalance({symbol: parameters.fromSymbol}, balance => {
    const amount = typeof parameters.amount !== 'undefined' ? parameters.amount : 0;
    hybrix.lib.rout({query: `/e/swap/deal/estimate/${parameters.fromSymbol}/${parameters.toSymbol}/${amount}`, regular: false},
      estimate => render({...parameters, balance, estimate}),
      fail(parameters)
    );
  }, console.error);
};
