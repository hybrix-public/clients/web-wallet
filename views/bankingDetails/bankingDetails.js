const hybrix = require('../../lib/hybrix').hybrix;
const progress = require('../../lib/progress').progress;
const {rout} = require('../banking/banking');
exports.rout = rout;

function errorCallback (error) {
  const DIV_message = document.getElementById('bankingDetails-message');
  DIV_message.innerHTML = error;
  console.error(error);
}

exports.open = function (parameters) {
  const DIV_fields = document.getElementById('bankingDetails-dynamic-fields');
  const DIV_message = document.getElementById('bankingDetails-message');
  const BUTTON_next = document.getElementById('bankingDetails-next');
  const BUTTON_back = document.getElementById('bankingDetails-back');
  const DIV_detailsInput = document.getElementById('bankingDetails-input-div');
  const DIV_spinner = document.getElementById('bankingDetails-spinner');
  const DIV_spinnerMessage = document.getElementById('bankingDetails-spinner-message');
  const SPAN_action = document.getElementById('bankingDetails-action');
  const SPAN_bankingOption = document.getElementById('bankingDetails-bankingOption');
  const SPAN_selectedCurrency = document.getElementById('bankingDetails-selectedCurrency');
  const DIV_descriptionField = document.getElementById('bankingDetails-description-container');

  const DIV_progress = document.getElementById('bankingDetails-progress');
  progress(DIV_progress, {
    '{{BANKINGDETAILS:PROGRESS_DETAILS}}': () => hybrix.view.pop(),
    '{{BANKINGDETAILS:PROGRESS_AMOUNT}}': null,
    '{{BANKINGDETAILS:PROGRESS_REVIEW}}': null,
    '{{BANKINGDETAILS:PROGRESS_TRANSACT}}': null
  }, 0);

  BUTTON_next.setAttribute('disabled',true);
  DIV_fields.style.display = 'none';
  DIV_spinnerMessage.innerText = '';
  DIV_spinner.style.display = 'block';

  const MAX_DETAIL_FIELDS = 8;
  const INPUT_details = [];
  for (let i = 0; i < MAX_DETAIL_FIELDS; i++) {
    INPUT_details[i] = document.getElementById(`bankingDetails-input-${i}`);
    INPUT_details[i].style.display = 'none';
    INPUT_details[i].value = '';
    INPUT_details[i].placeholder = '';
    INPUT_details[i].onchange = INPUT_details[i].oninput = INPUT_details[i].onpaste = null;
  }
  
  const [action,bankingMethod,bankingCurrency] = [parameters.action,parameters.bankingMethod,parameters.bankingCurrency];
  const gatewayType = bankingMethod.type.split(':')[1];

  let translatedAction = parameters.hasOwnProperty('action') && parameters.action ? parameters.action : '{{BANKINGDETAILS:ACTION_TRANSFER}}';
  switch (translatedAction) {
    case 'deposit': translatedAction='{{BANKINGDETAILS:ACTION_DEPOSIT}}'; break;
    case 'withdraw': translatedAction='{{BANKINGDETAILS:ACTION_WITHDRAW}}'; break;
    case 'transfer': translatedAction='{{BANKINGDETAILS:ACTION_TRANSFER}}'; break;
  }
  SPAN_action.innerText = translatedAction;
  SPAN_bankingOption.innerText = bankingMethod.hasOwnProperty('accountName') && bankingMethod.accountName ? bankingMethod.accountName : '{{BANKINGDETAILS:ACCOUNT}}';
  SPAN_selectedCurrency.innerText = bankingCurrency;

  const descriptionText = `${action.replace(/^\w/, (c) => c.toUpperCase())} {{BANKINGDETAILS:DESCRIPTION}}`;
  const INPUT_description = document.getElementById('bankingDetails-input-description');
  INPUT_description.readonly = true;
  INPUT_description.placeholder = descriptionText;
  INPUT_description.value = '{{BANKINGDETAILS:VOUCHER}} {ID}';

  const checkInput = () => {
    let fieldsFilled = 0;
    for (let j = 0; j < MAX_DETAIL_FIELDS; j++) {
      // TODO: also check the necessary regex?!?
      if (INPUT_details[j].value.trim() !== '') {
        fieldsFilled++;
      }
    }
    if (fieldsFilled >= fieldsActive && INPUT_description.value !== '') {
      BUTTON_next.removeAttribute('disabled');
    } else {
      BUTTON_next.setAttribute('disabled',true);
    }
  }

  let fieldsActive = 0;
  if (parameters.action !== 'deposit') {
    switch (gatewayType) {
      case 'iban':
        fieldsActive = 2;
        INPUT_details[0].placeholder = '{{BANKINGDETAILS:PH_ACCOUNT_HOLDER_NAME}}';
        INPUT_details[1].placeholder = '{{BANKINGDETAILS:PH_IBAN}}';
      break;
      default:
        fieldsActive = 5;
        INPUT_details[0].placeholder = '{{BANKINGDETAILS:PH_ACCOUNT_HOLDER_NAME}}';
        INPUT_details[1].placeholder = '{{BANKINGDETAILS:PH_ACCOUNT_HOLDER_NUMBER}}';
        INPUT_details[2].placeholder = '{{BANKINGDETAILS:PH_ACCOUNT_HOLDER_ADDRESS}}';
        INPUT_details[3].placeholder = '{{BANKINGDETAILS:PH_BANK_AND_BRANCH}}';
        INPUT_details[4].placeholder = '{{BANKINGDETAILS:PH_TARGET}}';
    }
  } else {
    checkInput();
  }
  
  if (fieldsActive) {
    if (parameters.action !== 'transfer') {
      hybrix.lib.load({key: hybrix.constants.storagePrefixes.bankingUserAccount, sessionKey: true, channel: 'y'}, targetData => {
        if (targetData instanceof Array) {
          for (let i = 0; i < fieldsActive; i++) {
            if (typeof targetData[i] !== 'undefined' && targetData[i]) {
              INPUT_details[i].value = targetData[i];
            }
            checkInput();
          }
        }
        DIV_spinner.style.display = 'none';
      }, () => {
        DIV_spinner.style.display = 'none';
      });
      DIV_descriptionField.style.display = 'none';
    } else {
      INPUT_description.readonly = false;
      DIV_descriptionField.style.display = 'block';
      DIV_spinner.style.display = 'none';
    }
    DIV_fields.style.display = 'block';
  } else {
    DIV_spinner.style.display = 'none';
  }
  
  for (let i = 0; i < MAX_DETAIL_FIELDS; i++) {
    if (INPUT_details[i].placeholder !== '') INPUT_details[i].style.display = 'block';
    INPUT_details[i].onchange = INPUT_details[i].oninput = INPUT_details[i].onpaste = () => {
      checkInput();
    }
  }
  INPUT_description.onchange = INPUT_description.oninput = INPUT_description.onpaste = () => {
    checkInput();
  }

  BUTTON_back.onclick = () => hybrix.view.pop();
  BUTTON_next.onclick = () => {
    let target = [];
    for (let i = 0; i < fieldsActive; i++) {
      target.push(INPUT_details[i].value.trim());
    }
    if (parameters.action !== 'transfer') hybrix.lib.save({key: hybrix.constants.storagePrefixes.bankingUserAccount, value: target, sessionKey: true, channel: 'y'}, () => {}, console.error);
    const description = INPUT_description.value; // TODO: sanitize stuff here?
    const source = parameters.source;
    hybrix.view.open('bankingAmount', {source, target, description, ...parameters});
  }
};
