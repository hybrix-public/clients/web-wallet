const hybrix = require('../../lib/hybrix').hybrix;
const {prettyPrintAmount, renderDate} = require('../../lib/prettyPrintAmount');

const hour = 60 * 60 * 1000;
const DISPUTE_TIME = 3 * hour;
const DISPUTE_LIMIT = 2 * 7 * 24 * hour;

const IMG_dispute = '<div id="swapDispute-image"><img alt="Dispute" src="./svg/dispute.svg" /></div>';

exports.rout = parameters => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('pending');
};

function errorCallback (error) {
  const DIV_message = document.getElementById('swapDispute-message');
  DIV_message.innerHTML = error;
  console.error(error);
}

exports.open = function (parameters) {
  const DIV_message = document.getElementById('swapDispute-message');
  const BUTTON_dispute = document.getElementById('swapDispute-dispute');
  const BUTTON_cancel = document.getElementById('swapDispute-cancel');
  const INPUT_dispute = document.getElementById('swapDispute-input');
  const DIV_disputeInput = document.getElementById('swapDispute-input-div');
  const DIV_spinner = document.getElementById('swapDispute-spinner');
  const DIV_spinnerMessage = document.getElementById('swapDispute-spinner-message');

  DIV_spinnerMessage.innerText = '';
  DIV_spinner.style.display = 'none';

  DIV_message.innerText = '...';
  BUTTON_dispute.classList.add('disabled');
  BUTTON_dispute.onclick = null;
  BUTTON_cancel.onclick = () => hybrix.view.pop();

  INPUT_dispute.value = '';
  DIV_disputeInput.style.display = 'none';

  const {proposal} = parameters;

  const epochNow = new Date().getTime() / 1000
  const disputeDelayHours = 3;
  const disputeDelayTime = epochNow - (3600 * disputeDelayHours); // three hours

/*
 *
 * {"transactionID":"vic.hy:0x9dcb728f22014ab129ec37edda940e756be6647dd94cfc689728f19713a40b48",
 * "id":"3bd857486d89fd99","ask":{"symbol":"hy","amount":"0.994999274","sufficiency":"1281.861988588","target":"hy:06K2G11Q7Znc2ykq6TQiu6kySmxFXTGqjn14CTvtkvvP26hNWQBVBqEagbT6fmQLv6Qo7XPU3zhFnaFBuNP9jTB4XWZhbkk6e",
 * "txid":"vic.hy:0x9dcb728f22014ab129ec37edda940e756be6647dd94cfc689728f19713a40b48"},"bid":{"symbol":"hy.eur","amount":"1.10","sufficiency":"1436.64","target":"hy.eur:02CnbfWxh1WNpoC9FmnkPtXUMtDXPa1uycXK5qHL33eevjWVFpi7Q6K9mpnGv493pgWQHyx528cD5u5HY2KBR9QkxMcCGrKX","txid":"vic.euro:0x6550a9c60d77e99720c0a92d684006ac3c51c9c529f8b1ad4dbf07137c90fbf4"},"fees":{"symbol":"hy","allocator":"0.004790059","network":"0.01"},"type":"autonomous","creation":"1641295303","deadline":"1641295903","pushtime":null}
 *
 */

  let html = '';

  // after three hours a proposal may be disputed...
  if (proposal.deadline<disputeDelayTime) {
    if (!proposal.ask.txid) { // proposal ask has no txid: needs proper claiming
      html += '{{SWAPDISPUTE:MESSAGE_NO_TXID}}';
      INPUT_dispute.placeholder = '{{SWAPDISPUTE:INPUT_TXID}}';
      DIV_disputeInput.style.display = 'block';

      hybrix.lib.getPending({remove: false}, (pending) => {
        // try to find the transaction that was not properly claimed...
        function sortPendingTransactions (pendingTransactions) {
          return Object.fromEntries(
            Object.entries(pendingTransactions).sort(([key1, tx1], [key2, tx2]) => tx2.timestamp - tx1.timestamp)
          );
        }
        for (const key in sortPendingTransactions(pending)) {
          const keySymbol = key.split(':')[0];
          if (keySymbol === proposal.ask.symbol) {
            // find the most recent transaction pertaining to this disputed one
            INPUT_dispute.value = key.split(':').splice(1).join(':');
            BUTTON_dispute.classList.remove('disabled');
            continue;
          }
        }
       }, () => {}
      );

      INPUT_dispute.oninput = INPUT_dispute.onpaste = () => {
        INPUT_dispute.value = INPUT_dispute.value.replace(/[^\w\-\.\:]/gi,''); // remove all non-word characters and check if input value is longer than 16 chars
        if(INPUT_dispute.value.length>16) {
          BUTTON_dispute.classList.remove('disabled');
        } else {
          BUTTON_dispute.classList.add('disabled');
        }
      }
      BUTTON_dispute.onclick = () => {
        if(!BUTTON_dispute.classList.contains('disabled')) {
          const txid = INPUT_dispute.value.replace(/[^\w\-\.\:]/gi,'').substr(0,128);
          BUTTON_dispute.classList.add('disabled');
          DIV_spinnerMessage.innerText = `{{SWAPDISPUTE:DISPUTING}} ${proposal.id}...`;
          DIV_spinner.style.display = 'block';
            hybrix.lib.rout({query: `/e/swap/deal/dispute/${proposal.id}/${txid}`},
            message => {
              DIV_spinnerMessage.innerText = `{{SWAPDISPUTE:CLAIMING}} ${txid}...`;
              hybrix.lib.rout({query: `/e/swap/deal/claim/${proposal.id}/${txid}`},
                message => {
                  DIV_message.innerHTML = IMG_dispute + message + '<br><br>{{SWAPDISPUTE:DISPUTE_SUCCESS}}';
                  DIV_spinner.style.display = 'none';
                  DIV_disputeInput.style.display = 'none';
                },
                error => {
                  DIV_message.innerHTML = IMG_dispute + '{{SWAPDISPUTE:ERROR_CLAIM}}' + error;
                  DIV_spinner.style.display = 'none';
                });
            },
            error => {
              DIV_message.innerHTML = IMG_dispute + '{{SWAPDISPUTE:ERROR_SUBMIT}}' + error;
              DIV_spinner.style.display = 'none';
              DIV_disputeInput.style.display = 'none';
            });
        }
      }
    } else if (!proposal.bid.txid) { // proposal has not been pushed txid: needs proper re-entry if not already disputed
      html += '{{SWAPDISPUTE:MESSAGE_NO_PUSH}}';
      BUTTON_dispute.classList.remove('disabled');
      BUTTON_dispute.onclick = () => {
        if(!BUTTON_dispute.classList.contains('disabled')) {
          BUTTON_dispute.classList.add('disabled');
          DIV_spinnerMessage.innerText = `{{SWAPDISPUTE:DISPUTING}} ${proposal.id}...`;
          DIV_spinner.style.display = 'block';
          hybrix.lib.rout({query: `/e/swap/deal/dispute/${proposal.id}`},
            message => {
              DIV_message.innerHTML = message + '<br><br>{{SWAPDISPUTE:DISPUTE_SUCCESS}}';
              DIV_spinner.style.display = 'none';
            },
            error => {
              DIV_message.innerHTML = IMG_dispute + '{{SWAPDISPUTE:ERROR_SUBMIT}}' + error;
              DIV_spinner.style.display = 'none';
            }
          );
        }
      }
    } else { // we cannot discover what is wrong, all seems fine: send notification message to node operator if not already disputed
      html += '{{SWAPDISPUTE:MESSAGE_ALL_OKAY}}';
      INPUT_dispute.placeholder = '{{SWAPDISPUTE:INPUT_DESCRIPTION}}';
      DIV_disputeInput.style.display = 'block';
      setTimeout(() => {
        BUTTON_dispute.classList.remove('disabled');
      }, 5000);
      BUTTON_dispute.onclick = () => {
        if(!BUTTON_dispute.classList.contains('disabled')) {
          DIV_spinnerMessage.innerText = `{{SWAPDISPUTE:DISPUTING}} ${proposal.id}...`;
          BUTTON_dispute.classList.add('disabled');
          DIV_spinner.style.display = 'block';
          const description = INPUT_dispute.value.replace(/[^\w\-.,]/gi,'').replace(' ','_').substr(0,255); // replace special chars by underscores
          hybrix.lib.rout({query: `/e/swap/deal/dispute/${proposal.id}/${proposal.ask.txid}/${description}`},
            message => {
              DIV_message.innerHTML = message + '<br><br>{{SWAPDISPUTE:DISPUTE_SUCCESS}}';
              DIV_spinner.style.display = 'none';
              DIV_disputeInput.style.display = 'none';
            },
            error => {
              DIV_message.innerHTML = IMG_dispute + '{{SWAPDISPUTE:ERROR_SUBMIT}}' + error;
              DIV_spinner.style.display = 'none';
              DIV_disputeInput.style.display = 'none';
            }
          );
        }
      }
    }
  } else {
    html += `{{SWAPDISPUTE:MESSAGE_PROCESSING_A}}${proposal.type}{{SWAPDISPUTE:MESSAGE_PROCESSING_B}}`;
  }

  html += IMG_dispute + '{{SWAPDISPUTE:MESSAGE_DISPUTING}}<br><br>';
  DIV_message.innerHTML = html;

};
