const {contacts, removeContact} = require('../../lib/contacts');
const {onclickCopyToClipboard} = require('../../lib/clipboard');
const {createElement} = require('../../lib/html');
const hybrix = require('../../lib/hybrix').hybrix;

exports.rout = () => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('contacts');
};

const onChange = (INPUT_search, DIV_contacts) => () => {
  const needle = INPUT_search.value.trim().toLowerCase();
  for (const DIV_contact of [...DIV_contacts.children]) {
    DIV_contact.style.display = DIV_contact.getAttribute('data').toLowerCase().includes(needle) ? 'block' : 'none';
  }
};

const removeAndRefreshContact = (parameters) => {
  removeContact(parameters.contactId);
  hybrix.view.exec('contacts', 'open'); // refresh contacts list
}

const render = () => {
  const DIV_contacts = document.getElementById('contacts-contacts');
  DIV_contacts.innerHTML = '';

  for (const contactId of Object.keys(contacts).sort()) {
    const contact = contacts[contactId];

    const DIV_contact = createElement({
      className: 'contacts-contact',
      innerHTML: `<div class="contactId">👤 ${contactId}</div>`,
      parentNode: DIV_contacts
    });

    const BUTTON_edit = createElement({
      className: 'contacts-edit',
      parentNode: DIV_contact,
      onclick: () => hybrix.view.open('contactsEdit', {contactId})
    });

    const BUTTON_delete = createElement({
      className: 'contacts-delete',
      parentNode: DIV_contact,
      onclick: () =>
        hybrix.view.open('confirm', {
          text:'{{CONTACTS:DELETE_SURE}}',
          textMore:'{{CONTACTS:NO_RETURN}}',
          yesCallback: removeAndRefreshContact,
          contactId
      })
    });

    const DIV_entries = createElement({className: 'contacts-entries', parentNode: DIV_contact});

    DIV_contact.onclick = () => {
      for (const DIV_otherContact of document.getElementsByClassName('contacts-contact')) {
        DIV_otherContact.classList.remove('contacts-expanded'); //  collapse all others
      }
      DIV_contact.classList.add('contacts-expanded');
    };
    let needle = contactId;

    for (const entry of contact) {
      for (const symbol in entry) {
        for (const label in entry[symbol]) {
          needle += label;
          const DIV_entry = createElement({
            className: 'contacts-entry',
            parentNode: DIV_entries
          });
          const address = entry[symbol][label];
          // if prefixed, then replace symbol by that! (for unified cases)
          const targetSymbol = address.includes(':') ? address.split(':')[0].toLowerCase() : symbol.toLowerCase();

          const BUTTON_copy = createElement({
            className: 'contacts-copy',
            parentNode: DIV_entry,
            innerHTML: `${address} ${label && label !== '-' ? '➤ ' + label : ''}`
          });

          onclickCopyToClipboard(BUTTON_copy, address, () => {
            //const DIV_copy = document.getElementById('receiveAsset-copySuccess');
            BUTTON_copy.classList.add('contacts-copy-clicked');
            const originalContent = BUTTON_copy.innerHTML;
            BUTTON_copy.innerHTML = '{{CONTACTS:COPIED}}';
            setTimeout( () => {
              BUTTON_copy.innerHTML = originalContent;
              BUTTON_copy.classList.remove('contacts-copy-clicked');
            }, 3000);
          }, console.error);

        }
      }
    }
    DIV_contact.setAttribute('data', needle);
  }

  const INPUT_search = document.getElementById('contacts-search');
  INPUT_search.value = '';
  INPUT_search.keyup = INPUT_search.onchange = INPUT_search.oninput = INPUT_search.onpaste = onChange(INPUT_search, DIV_contacts);

  const DIV_deselector = document.getElementById('contacts-deselector');
  DIV_deselector.onclick = INPUT_search.onclick = () => render(); // refresh contacts list
  const BUTTON_new = document.getElementById('contacts-new-button');
  BUTTON_new.onclick = () => hybrix.view.open('contactsEdit');
  const BUTTON_cancel = document.getElementById('contacts-cancel-button');
  BUTTON_cancel.onclick = () => hybrix.view.pop();
};

exports.open = render;
