const hybrix = require('../../lib/hybrix').hybrix;
const {progress, radial} = require('../../lib/progress');

const MAX_CLAIM_ATTEMPTS = 5;
const CLAIM_RETRY_DELAY_MS = 2000;

const remitDeal = (rawTx, proposal, updateRadial) => (retries = 0) => {
  const dealID = proposal.id;
  const txType = 'deal';
  hybrix.lib.addPending({transaction: {ref: `${txType}:${dealID}`, type: `${txType}`, meta: proposal}},()=>{},console.error); // always ensure the proposal is added to pending!
  hybrix.lib.rout({query: `/e/swap/deal/remit/${dealID}/_`, data: rawTx, regular: false},
    transactionID => {
      proposal.ask.txid = transactionID;
      hybrix.lib.addPending({transaction: {ref: `${txType}:${dealID}`, type: `${txType}`, meta: proposal}},()=>{},console.error);
      const BUTTON_done = document.getElementById('swapComplete-done');
      BUTTON_done.classList.remove('disabled');
      updateRadial(1, '{{SWAPCOMPLETE:RADIAL_DEAL_ACTIVE}}');
      const SWAP_message = document.getElementById('swapComplete-message');
      SWAP_message.innerHTML = `{{SWAPCOMPLETE:MESSAGE_DEAL_A}}<span id="swapComplete-pending">{{SWAPCOMPLETE:MESSAGE_DEAL_B}}</span>{{SWAPCOMPLETE:MESSAGE_DEAL_C}}<b>${dealID}</b>{{SWAPCOMPLETE:MESSAGE_DEAL_D}}`;
      const SPAN_pending = document.getElementById('swapComplete-pending');
      if (SPAN_pending) SPAN_pending.onclick = () => hybrix.view.open('pending');
      hybrix.view.exec('pending', 'clearPendingCache');
      hybrix.view.exec('viewAsset', 'refreshPending', proposal.ask.symbol);
    },
    error => {
      console.error('Failed to remit swap deal!', dealID, error);
      if (retries > MAX_CLAIM_ATTEMPTS) { // retry attempts exhausted
        const BUTTON_done = document.getElementById('swapComplete-done');
        BUTTON_done.classList.remove('disabled');
        failProposal(updateRadial, '{{SWAPCOMPLETE:RADIAL_FAILED_INIT}}', `{{SWAPCOMPLETE:ERROR_CLAIM_A}}${dealID}{{SWAPCOMPLETE:ERROR_CLAIM_B}}`)(error);
      } else { // another attempt
        setTimeout(() => {
          remitDeal(rawTx, proposal, updateRadial)(retries + 1);
        }, Math.pow(2, retries) * CLAIM_RETRY_DELAY_MS);
      }
    },
    progress => {
      if (retries > 0) updateRadial(0.85 + 0.2 * progress, `{{SWAPCOMPLETE:RADIAL_REMIT}}<br>({{SWAPCOMPLETE:ATTEMPT}} ${retries+1}/${MAX_CLAIM_ATTEMPTS})...`);
      else updateRadial(0.85 + 0.2 * progress, '{{SWAPCOMPLETE:RADIAL_DEAL_REMIT}}');
    }
  );
};

const handleProposal = (parameters, updateRadial) => proposal => {
  const {fromBase, toSymbol, amount, estimate, target} = parameters;

  // TODO check from,to and amount with proposal
  // TODO check if proposal matches estimate within parameters
  // TODO check if sufficient balance

  const transaction = {
    symbol: proposal.ask.symbol,
    amount: proposal.ask.amount,
    target: proposal.ask.target,
    addPending: false // will be added to pending as a swap.
  };

  hybrix.lib.rawTransaction(transaction, rawTx => {
    const dealID = proposal.id;
    remitDeal(rawTx, proposal, updateRadial)(1)   // first attempt
  },
  failProposal(updateRadial, '{{SWAPCOMPLETE:RADIAL_FAILED_TX}}', '{{SWAPCOMPLETE:ERROR_FAILED_TX}}'),
  progress => updateRadial(0.6 + 0.2 * progress, '{{SWAPCOMPLETE:RADIAL_DEAL_CREATE}}')
  );

};

const failProposal = (updateRadial, message, description) => error => {
  const DIV_progress = document.getElementById('swapComplete-progress');
  const BUTTON_done = document.getElementById('swapComplete-done');
  BUTTON_done.classList.remove('disabled');
  updateRadial(false, message);
  DIV_progress.children[5].classList.add('failed');
  DIV_progress.children[6].classList.add('failed');
  const SWAP_message = document.getElementById('swapComplete-message');
  SWAP_message.innerHTML = description + '<br><br>' + error;
};

const getProposal = (parameters, updateRadial) => target => {
  parameters.target = target;
  hybrix.lib.rout({query: `/e/swap/deal/proposal/${parameters.fromSymbol}/${parameters.toSymbol}/${parameters.amount}/${target}`, regular: false},
    handleProposal(parameters, updateRadial),
    failProposal(updateRadial, '{{SWAPCOMPLETE:RADIAL_FAILED_PROPOSAL}}', '{{SWAPCOMPLETE:ERROR_FAILED_PROPOSAL}}'),
    progress => updateRadial(0.2 + 0.2 * progress, '{{SWAPCOMPLETE:RADIAL_DEAL_PROPOSAL}}')
  );
};

exports.open = function (parameters) {
  const BUTTON_done = document.getElementById('swapComplete-done');
  BUTTON_done.classList.add('disabled');

  const SWAP_message = document.getElementById('swapComplete-message');
  SWAP_message.innerHTML = '{{SWAPCOMPLETE:MESSAGE_DEAL_START}}';

  const DIV_progress = document.getElementById('swapComplete-progress');
  progress(DIV_progress, {
    '{{SWAPCOMPLETE:PROGRESS_DETAILS}}': () => {
      if(!BUTTON_done.classList.contains('disabled')) {
        hybrix.view.pop();
      }
    },
    '{{SWAPCOMPLETE:PROGRESS_AMOUNT}}': null,
    '{{SWAPCOMPLETE:PROGRESS_REVIEW}}': null,
    '{{SWAPCOMPLETE:PROGRESS_SWAP}}': null  
  }, 3);
  const DIV_radial = document.getElementById('swapComplete-radial');

  const updateRadial = radial(DIV_radial, '{{SWAPCOMPLETE:RADIAL_DEAL_PROPOSAL}}', 0.2);

  if (parameters.hasOwnProperty('target')) {
    getProposal(parameters, updateRadial)(parameters.target);
  } else {
    hybrix.lib.getAddress({symbol: parameters.toSymbol}, getProposal(parameters, updateRadial), console.error); // TODO
  }

  BUTTON_done.onclick = () => {
    if(!BUTTON_done.classList.contains('disabled')) {
      hybrix.view.closeAll();
    }
  }
};
