const hybrix = require('../../lib/hybrix').hybrix;

exports.rout = parameters => {
  hybrix.view.swap('listAssets');
  hybrix.view.open('banking');
};

function open (parameters) {
  const DIV_message = document.getElementById('bankingPilot-message');
  const BUTTON_deposit = document.getElementById('bankingPilot-deposit');
  const BUTTON_withdraw = document.getElementById('bankingPilot-withdraw');
  const BUTTON_transfer = document.getElementById('bankingPilot-transfer');
  const DIV_progress = document.getElementById('bankingPilot-progress');
}

exports.open = open;
