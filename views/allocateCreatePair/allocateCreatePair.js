const spinner = require('../../lib/spinner');
const hybrix = require('../../lib/hybrix').hybrix;
const {setPair, SWAP_STATS_CACHE_TIME} = require('../allocateManagePair/allocateManagePair.js');
const progress = require('../../lib/progress').progress;
const {prettyPrintAmount} = require('../../lib/prettyPrintAmount');
const { DEFAULT_ALLOCATION_OFFSET, refreshPairs} = require('../allocate/allocate.js');
const { addEventListener, getBalance} = require('../../lib/assets');

exports.rout = parameters => {
  hybrix.view.swap('allocate');
  hybrix.view.open('allocateCreatePair', parameters);
};

exports.once = () => {
  const DIV_progress = document.getElementById('allocateCreatePair-progress');
  progress(DIV_progress, {
    Prepare: () => hybrix.view.pop(),
    Allocate: null,
    Create: null
  }, 1);
};

exports.open = parameters => {
  const {fromSymbol, toSymbol} = parameters;
  const pair = fromSymbol + ':' + toSymbol;
  const DIV_spinner = document.getElementById('allocateCreatePair-spinner');
  const DIV_mainBalance = document.getElementById('allocateCreatePair-mainBalance');
  const DIV_message = document.getElementById('allocateCreatePair-message');
  const DIV_allocationBalance = document.getElementById('allocateCreatePair-allocationBalance');
  const DIV_feeRange = document.getElementById('allocateCreatePair-feeRange');
  const INPUT_fee = document.getElementById('allocateCreatePair-fee');
  const BUTTON_cancel = document.getElementById('allocateCreatePair-cancel');
  const BUTTON_create = document.getElementById('allocateCreatePair-create');
  const BUTTON_allocate = document.getElementById('allocateCreatePair-allocate');

  BUTTON_create.setAttribute('disabled', true);
  BUTTON_cancel.removeAttribute('disabled');
  DIV_allocationBalance.innerHTML = '';
  DIV_mainBalance.innerHTML = '';
  DIV_message.innerHTML = '';
  DIV_feeRange.innerHTML = '';
  INPUT_fee.value = 0.35;

  let hasAllocatedBalance = false;

  spinner.apply(DIV_mainBalance);
  addEventListener('refreshAsset:' + toSymbol, () => {
    spinner.remove(DIV_mainBalance);
    const balance = getBalance(toSymbol);
    if (balance > 0) {
      DIV_mainBalance.innerHTML = prettyPrintAmount(balance, toSymbol);
      BUTTON_allocate.removeAttribute('disabled');
    } else {
      DIV_mainBalance.innerHTML = prettyPrintAmount(0, toSymbol, true, true);
      BUTTON_allocate.setAttribute('disabled', true);
    }
  }, 'allocateCreatePair');

  // we get the balance separately first, because waiting for the listener below takes too long
  spinner.apply(DIV_allocationBalance);
  addEventListener('refreshAsset:' + toSymbol + '#' + DEFAULT_ALLOCATION_OFFSET, () => {
    spinner.remove(DIV_allocationBalance);
    const balance = getBalance(toSymbol + '#' + DEFAULT_ALLOCATION_OFFSET);
    if (balance > 0) {
      hasAllocatedBalance = true;
      DIV_allocationBalance.innerHTML = prettyPrintAmount(balance, toSymbol);
      if (!isNaN(INPUT_fee.value) && INPUT_fee.value > 0 && INPUT_fee.value < 5) BUTTON_create.removeAttribute('disabled');
      DIV_message.innerHTML = '';
    } else {
      DIV_allocationBalance.innerHTML = prettyPrintAmount(0, toSymbol, true, true);
      DIV_message.innerHTML = 'Please transfer some funds to put on offer before the pair can be created!';
    }
  }, 'allocateCreatePair');

  /* DEPRECATED
  hybrix.lib.getBalance({symbol: toSymbol, offset: DEFAULT_ALLOCATION_OFFSET},
    balance => {
      spinner.remove(DIV_allocationBalance);
      allocationBalanceAction(balance);
    },
    error => {
      DIV_allocationBalance.innerHTML = 'N/A';
      DIV_message.innerHTML = 'Error: Cannot retrieve balance information!';
      console.error(error);
    }
  );
  */

  BUTTON_allocate.onclick = () => {
    hybrix.view.open('transaction', {
      text: 'This transaction will allocate funds from your main wallet to make them available for the new pair to be created.',
      fromOffset: 0,
      toOffset: DEFAULT_ALLOCATION_OFFSET,
      symbol: toSymbol
    });
  };

  INPUT_fee.oninput = INPUT_fee.onpaste = () => {
    if (INPUT_fee.value > 0 && INPUT_fee.value <= 5 && hasAllocatedBalance) BUTTON_create.removeAttribute('disabled');
    else BUTTON_create.setAttribute('disabled', true);
  };

  hybrix.lib.rout({query: '/e/swap/allocation/pair/stats', regular: false, cache: SWAP_STATS_CACHE_TIME },
    pairs => {
      if (pairs.hasOwnProperty(pair)) {
        DIV_feeRange.innerHTML = pairs[pair].fee;
      } else {
        DIV_feeRange.innerHTML = 'N/A';
      }
    },
    error => {
      DIV_feeRange.innerHTML = 'Error: Failed to retrieve fee range information!';
      console.error(error);
    }
  );

  BUTTON_cancel.onclick = () => hybrix.view.pop();

  BUTTON_create.onclick = () => {
    DIV_spinner.style.display = 'block';
    BUTTON_create.setAttribute('disabled', true);
    BUTTON_cancel.setAttribute('disabled', true);
    setTimeout( () => {  // give UI chance to update first
      const fee = INPUT_fee.value;
      const risk = 15;  // TODO customizable!
      const balanceWeight = 5;  // TODO customizable!
      // type is now auto-determined: const type = 'autonomous';
      setPair(fromSymbol, toSymbol, fee, risk, balanceWeight, () => {
        DIV_spinner.style.display = 'none';
        hybrix.view.closeAll();
        refreshPairs();
        //const pair = fromSymbol + ':' + toSymbol;
        //hybrix.view.open('allocateManagePair', {pair});
      }, error => {
        BUTTON_create.removeAttribute('disabled');
        BUTTON_cancel.removeAttribute('disabled');
        DIV_spinner.style.display = 'none';
        DIV_message.innerHTML = 'Error: ' + error;
        console.error(error);
      });
    }, 500);
  };
};
