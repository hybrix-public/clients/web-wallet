// (C) 2024 hybrix / Joachim de Koning
// QR login pass generator app for hybrix web-wallet

const urlbase = window.location.href.split('/app/')[0];
const arguments = getArguments (window.location.hash.substr(1));
window.location.hash = ''; // immediately remove arguments from visible URI
document.title = 'hybrix card pass';

function getArguments (hash) {
  return result = hash.split('&').reduce(function (res, item) {
    var parts = item.split('=');
    res[parts[0]] = parts[1];
    return res;
  }, {});
}

function appendCanvas () {
  const capture = document.querySelector("#capture");
  if (capture) {
    html2canvas(capture).then(canvas => {
      document.body.appendChild(canvas);
    });
  }
}

function mkUrl (credentials, url_, cb) {
  const userid = credentials['userid']
  const passwd = credentials['passwd']
  const name = credentials['name'] ? decodeURI(credentials['name']).substr(0,40) : '';
  const baseUrl = `${urlbase}/#`
  const credentialsStr = `login/id=${userid}${passwd}`
  //NOTE: compressed version becomes larger than original, so ignore
  //const compressedCredentials = LZString.compressToEncodedURIComponent(credentialsStr)
  //const url = `${baseUrl}/z/${compressedCredentials}`

  const url = `${baseUrl}/${credentialsStr}`
  mkNewQRCode(url, userid, passwd, name, cb)

  // append to the end of the body!
  appendCanvas();
}

function mkNewQRCode (url, userid, passwd, name, cb) {
  const qrCode = document.getElementById('qrCode')
  qrCode.innerHTML = '';
  if (userid !== '' && passwd !== '') {
    var code = new QRCode(qrCode, {
      text: url,
      width: 300,
      height: 300,
      colorDark: '#000000',
      colorLight: '#ffffff',
      correctLevel: QRCode.CorrectLevel.H
    })
    document.getElementById('userid').innerHTML = userid;
    document.getElementById('passwd').innerHTML = passwd;
    document.getElementById('nameTextA').innerHTML = name ? name : '';
    document.getElementById('nameTextB').innerHTML = name ? name : '';
    cb(userid, passwd)
  }
}

function main (data) {
  window.hybrix = new Hybrix.Interface({XMLHttpRequest: XMLHttpRequest})
  DIV_content = document.getElementById('content');
  if (typeof data.arguments.id === 'string' && typeof data.arguments.pass === 'string' && data.arguments.id && data.arguments.pass) {
    mkUrl({
      userid: data.arguments.id,
      passwd: data.arguments.pass,
      name: data.arguments.name
    }, '.url', () => {});
  } else {
    DIV_content.innerHTML = '<p class="error">MISSING DATA INPUT!<br><br>CLOSING TAB ...</p>';
    setTimeout(() => { window.close(); },3000);
  }
  
}

main({urlbase,arguments});
