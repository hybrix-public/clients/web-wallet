const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  entry: ['./tmp/index.js'], // DEPRECATED: , './tmp/styles.scss'],

  output: {
    path: path.resolve(__dirname, '.'),
    filename: './tmp/index.js'
  },
  resolve: {
    fallback: {
      fs: false,
      http: false,
      https: false,
      crypto: false,
      assert: require.resolve('assert'),
      util: require.resolve('util'),
      buffer: require.resolve('buffer/'),
      zlib: require.resolve('browserify-zlib'),
      stream: require.resolve('stream-browserify')
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/
      }
    ]
  },
  optimization: {
    minimizer: [new TerserPlugin()]
  },
  node: {
  //  fs: 'empty'
  },
  plugins: [new webpack.DefinePlugin({
    // 'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    'process.env.NODE_DEBUG': JSON.stringify(process.env.NODE_DEBUG),
    'process.type': JSON.stringify(process.type),
    'process.version': JSON.stringify(process.version)
  })]
};
