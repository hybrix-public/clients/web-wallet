/*
 * Translator for the web-wallet views
 * (C) 2023 Joachim de Koning
 */

const fs = require('fs');
const viewName = process.argv[2];
const language = process.argv[3]||'en';
const translationFile = __dirname + `/../../views/${viewName}/${viewName}-translation-${language}.json`;

let translation = {};
if (fs.existsSync(translationFile)) translation = require(translationFile);

let data = '';
process.stdin.resume();
process.stdin.setEncoding('utf8');
process.stdin.on('data', chunk => { data += chunk; });
process.stdin.on('end', function () {
  // replaceAll using split/join: "[boo].blah.[boo].blah".split("[boo]").join("(scare)")
  for (let key in translation) {
    data = data.split('{{'+viewName.toUpperCase()+':'+key+'}}').join(translation[key]);
  }
  console.log(data);
});
