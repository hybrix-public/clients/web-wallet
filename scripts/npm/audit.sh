#!/bin/sh
WHEREAMI="`pwd`";
OLDPATH="$PATH"

# $HYBRIXLIB/scripts/npm  => $HYBRIXLIB
SCRIPTDIR="`dirname \"$0\"`"
WEB_WALLET="`cd \"$SCRIPTDIR/../..\" && pwd`"

export PATH="$WEB_WALLET/node_binaries/bin:$PATH"

cd "$WEB_WALLET"
echo "[.] Auditing web-wallet..."
npm i
npm update
npm audit fix --force

export PATH="$OLDPATH"
cd "$WHEREAMI"
