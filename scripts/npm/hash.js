const djb2 = require('../../common/crypto/hashDJB2');
const fs = require('fs');

const fileName = process.argv[2];
const content = fs.readFileSync(fileName).toString();
const hash = djb2.hash(content);
process.stdout.write(hash);
