#!/bin/sh
WHEREAMI="`pwd`"

# $HYBRIXD/$NODE/scripts/npm  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"

NODEJS="$HYBRIXD/nodejs"
COMMON="$HYBRIXD/common"
HYBRIXLIB="$HYBRIXD/hybrix-lib"
NODE="$HYBRIXD/node"
WEB_WALLET="`cd \"$SCRIPTDIR/../..\" && pwd`"

if [ "`uname`" = "Darwin" ]; then
    SYSTEM="darwin-x64"
elif [ "`uname -m`" = "i386" ] || [ "`uname -m`" = "i686" ] || [ "`uname -m`" = "x86_64" ]; then
    SYSTEM="linux-x64"
elif [ "`uname -m`" = "arm64" ] || [ "`uname -m`" = "aarch64" ]; then
    SYSTEM="linux-arm64"
else
    echo "[!] Unknown architecture (or incomplete implementation)"
    export PATH="$OLDPATH"
    cd "$WHEREAMI"
    exit 1;
fi

if [ -e "$HYBRIXD/hybrixd" ] || [ "$1" = "public" ] ; then
    ENVIRONMENT="public"
    echo "[i] Environment is public"
elif [ -e "$HYBRIXD/node" ] || [ "$1" = "dev" ] ; then
    ENVIRONMENT="dev"
    echo "[i] Environment is development"
else
    echo "[!] Could not determine environment"
    read -p " [?] Please enter environment [dev/public] " ENVIRONMENT
fi

if [ "$ENVIRONMENT" = "public" ] || [ "$ENVIRONMENT" = "dev" ]; then
    URL_HYBRIXD="https://gitlab.com/hybrix-public/node.git"
    URL_HYBRIXLIB="https://gitlab.com/hybrix-public/hybrix-lib.git"
    URL_COMMON="https://gitlab.com/hybrix-public/common.git"
    URL_NODEJS="https://gitlab.com/hybrix-public/dependencies/nodejs.git"
else
    echo "[!] Unknown Environment (please use npm run setup[:dev])"
    export PATH="$OLDPATH"
    cd "$WHEREAMI"
    exit 1
fi

# HYBRIXD
if [ ! -e "$NODE" ];then

    echo " [!] $NODE not found"
    cd "$HYBRIXD"
    echo " [.] Clone hybrixd files"
    git clone "$URL_HYBRIXD"
fi

# NODE_BINARIES
if [ ! -e "$NODEJS" ];then
  echo " [!] $HYBRIXD/node_binaries not found"
  cd "$HYBRIXD"
  echo " [.] Clone nodejs runtime files"
  git clone "$URL_NODEJS"
fi
if [ ! -e "$WEB_WALLET/node_binaries" ];then
    echo " [!] $WEB_WALLET/node_binaries not found"
    echo " [.] Link nodejs for architecture $SYSTEM"
    ln -sf "$NODEJS/$SYSTEM" "$WEB_WALLET/node_binaries"
fi

# COMMON
if [ ! -e "$WEB_WALLET/common" ];then
    echo " [!] web_wallet/common not found"

    if [ ! -e "$COMMON" ];then
        cd "$HYBRIXD"
        echo " [.] Clone common files"
        git clone "$URL_COMMON"
    fi
    echo " [.] Link common files"
    ln -sf "$COMMON" "$WEB_WALLET/common"
fi

# HYBRIXLIB
if [ ! -e "$WEB_WALLET/hybrix-lib" ];then

    echo " [!] web-wallet/hybrix-lib not found"

    if [ ! -e "$HYBRIXLIB" ];then
        cd "$HYBRIXD"
        echo " [i] Clone hybrix-lib files"
        git clone "$URL_HYBRIXLIB"
    fi
    echo " [.] Link hybrix-lib files"
    ln -sf "$HYBRIXLIB/dist" "$WEB_WALLET/hybrix-lib"
fi

if [ ! -d "$WEB_WALLET/node_modules" ]; then
    echo " [.] Installing NPM packages"
    cd $WEB_WALLET
    npm i
fi

# GIT HOOKS
# DEPRECATED: sh "$COMMON/hooks/hooks.sh" "$WEB_WALLET"

cd "$WHEREAMI"
echo "[i] Setup process done"
