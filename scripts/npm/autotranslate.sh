#!/bin/sh
WHEREAMI="`pwd`"
OLDPATH="$PATH"

LANGUAGE="en"
TARGET="$1"

if [ -z "$1" ]; then
  echo '[!] Error: No input language specified! (Example: nl)'
  exit 1
fi

# $HYBRIXD/deterministic/scripts/npm  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"
WEB_WALLET="`cd \"$SCRIPTDIR/../..\" && pwd`"
export PATH=$HYBRIXD/node/node_binaries/bin:"$PATH"

VIEW_BASEPATH="$WEB_WALLET/views"
HTML_FILEPATH="$WEB_WALLET/tmp/index.html"
SCSS_FILEPATH="$WEB_WALLET/tmp/index.scss"
JS_FILEPATH="$WEB_WALLET/tmp/index.js"
VERSION_FILEPATH="$WEB_WALLET/tmp/version.js"

if [ -z "$2" ]; then

  echo "[i] Auto-translating object strings to language: $1"
  while read VIEW_ID ; do
    if [ -z "${VIEW_ID}" ]; then
      continue
    fi
    echo "[i] Starting translation of view: ${VIEW_ID}"
    node "$WEB_WALLET/scripts/npm/autotranslate.js" "$VIEW_ID" "$LANGUAGE" "$1"
  done < "$VIEW_BASEPATH/compileList"

  export PATH="$OLDPATH"
  cd "$WHEREAMI"
  echo "[i] Auto-translation process done"

else

  node "$WEB_WALLET/scripts/npm/autotranslate.js" "$2" "$LANGUAGE" "$1"

fi
