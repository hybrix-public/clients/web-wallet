#!/bin/sh

WHEREAMI=`pwd`

# $HYBRIXD/$NODE/scripts/npm  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
WEB_WALLET="`cd \"$SCRIPTDIR/../..\" && pwd`"

NODE="$WEB_WALLET/../node"

echo "[.] Copy web-wallet distributables to node."
rsync -aK "$WEB_WALLET/dist/" "$NODE/modules/web-wallet/" --delete

cd "$WHEREAMI"
echo "[i] Distribution process done"
