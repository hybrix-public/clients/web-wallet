/*
 * Auto-translator utilizing trans for the web-wallet views
 * (C) 2023 Joachim de Koning
 */

const fs = require('fs');
const viewName = process.argv[2];
const languageFrom = process.argv[3]||'en';
const languageTo = process.argv[4]||'test';
const translationFileFrom = __dirname + `/../../views/${viewName}/${viewName}-translation-${languageFrom}.json`;
const translationFileTo = __dirname + `/../../views/${viewName}/${viewName}-translation-${languageTo}.json`;
const execSync = require('child_process').execSync;

let translation = {};
let translated = '';
let FIRST = true;

if (fs.existsSync(translationFileFrom)) {
  translation = require(translationFileFrom);
  fs.writeFileSync(translationFileTo, '{'); // open object
  // translate object input file one at a time...
  for (let key in translation) {
    const languageString = translation[key]
    if (languageString !== languageExceptions(languageString)) translated = languageExceptions(languageString);
    else if (key !== 'LANGUAGE') translated = String(execSync(`trans -b :${languageTo} "${languageString}"`)).replace(/[\n\r]/g, '');
    else if (key !== 'HYBRIX') translated = languageTo;
    else translated = translation[key];
    fs.appendFileSync(translationFileTo, (FIRST?'':',') + "\n" + `  "${key}":"${translated}"`); // add to object
    FIRST = false;
    console.log(`[.] ${key}: '${stringTruncateFromCenter(languageString,25)}' -> '${stringTruncateFromCenter(translated,25)}'`);
  }
  fs.appendFileSync(translationFileTo, "\n" + '}'); // close object

} else console.log('[!] Skipping non-existing file: '+translationFileFrom);

function languageExceptions(i,b) {
  let o = i;
  if (languageTo === 'nl') {  
    switch (i.trim()) {
      case 'Back':
        o = 'Terug'; break;
      case 'Clear':
        o = 'Wissen'; break;
      case 'balance:':
        o = 'balans:'; break;
      case 'hybrixd:':
        o = 'hybrixd:'; break;
    }
  }
  return o;
}

function stringTruncateFromCenter(str, maxLength) {
    const midChar = "...";      // character to insert into the center of the result
    var left, right;

    if (str.length <= maxLength) return str;

    // length of beginning part      
    left = Math.ceil(maxLength / 2);

    // start index of ending part   
    right = str.length - Math.floor(maxLength / 2) + 1;   

    return str.substr(0, left) + midChar + str.substring(right);
}
